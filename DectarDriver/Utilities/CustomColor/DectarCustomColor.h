//
//  DectarCustomColor.h
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//#define SetBlueColor [UIColor colorWithRed:40/255.0f green:203/255.0f blue:249/255.0f alpha:1]

#define SetBlueColor [UIColor whiteColor]

#define SetRedcolor [UIColor redColor]

#define subBlueColor [UIColor colorWithRed:38/255.0f green:142/255.0f blue:255/255.0f alpha:1]


#define SetSubColor [UIColor colorWithRed:28/255.0f green:168/255.0f blue:195/255.0f alpha:1]

#define SetDarkBlueColor [UIColor colorWithRed:0/255.0f green:162/255.0f blue:208/255.0f alpha:1]
#define setGreenColor [UIColor colorWithRed:110/255.0f green:240/255.0f blue:130/255.0f alpha:1]
#define setRedColor [UIColor colorWithRed:255/255.0f green:51/255.0f blue:51/255.0f alpha:1]
#define setTransparentBlack [UIColor colorWithRed:1/255.0f green:1/255.0f blue:1/255.0f alpha:0.6]
#define setLightGray [UIColor colorWithRed:230/255.0f green:230/255.0f blue:230/255.0f alpha:1]



#define SetThemeColor [UIColor colorWithRed:4/255.0f green:68/255.0f blue:128/255.0f alpha:1.0]

#define SetMapPolyLineColor [UIColor colorWithRed:47/255.0f green:163/255.0f blue:225/255.0f alpha:1]

#define bluetheme [UIColor colorWithRed:2/255.0f green:98/255.0f blue:178/255.0f alpha:1]

#define skyBlue [UIColor colorWithRed:40/255.0f green:203/255.0f blue:249/255.0f alpha:1]



//newcolors


#define DarkskyBlue [UIColor colorWithRed:230/255.0f green:240/255.0f blue:245/255.0f alpha:1]
#define buttonskyBlue [UIColor colorWithRed:6/255.0f green:152/255.0f blue:212/255.0f alpha:1]


@interface DectarCustomColor : UIColor
@end
