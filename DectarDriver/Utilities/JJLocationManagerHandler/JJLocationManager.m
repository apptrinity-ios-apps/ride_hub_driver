//
//  JJLocationManager.h
//  DectarDriver
//
//  Created by Aravind Natarajan on 12/05/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

#import "JJLocationManager.h"

@interface JJLocationManager () <CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *currentLocation;
@property (nonatomic) CLLocation *previousLocation;
@property (nonatomic) CLLocation *previousLocationBefore100Metres;
@property (nonatomic) CLLocation *previousLocationBefore5etres;
@property (nonatomic) JJLocationManagerMonitorMode mode;

@end;
@implementation JJLocationManager
@synthesize isDriverLocationUpdatedInitially,previousLocationBefore100Metres,isDriverLocationUpdatedServerInitially,previousLocationBefore5etres;

#pragma mark - Static

static JJLocationManager *sharedManager;

#pragma mark - Initialization

- (id)init
{
    return [self initWithLocationManager:[[CLLocationManager alloc] init]];
}

- (id)initWithLocationManager:(CLLocationManager *)locationManager
{
    self = [super init];
    if (self) {
        NSParameterAssert(locationManager);
        _locationManager = locationManager;
        _locationManager.delegate = self;
        
    }
    return self;
}



#pragma mark - Public static methods

+ (JJLocationManager *)sharedManager {
    if (sharedManager == nil) {
        sharedManager = [[JJLocationManager alloc] init];
    };
    return sharedManager;
}

+ (float)kilometresBetweenPlace1:(CLLocation*)place1 andPlace2:(CLLocation*) place2
{
    CLLocationDistance dist = [place1 distanceFromLocation:place2]/1000;
    NSString *strDistance = [NSString stringWithFormat:@"%.2f", dist];
    return [strDistance floatValue];
}

+ (float)kilometresFromLocation:(CLLocation*)location
{
    return [self kilometresBetweenPlace1:location andPlace2:[self sharedManager].currentLocation];
}

#pragma mark - Public methods

- (void)startLocationUpdates
{
    if (self.mode == kJJLocationManagerModeStandard) {
        [self requestAlwaysAuthorization]; // for significant location changes this auth is required (>= ios8)
        [self.locationManager startUpdatingLocation];
    }
    else if (self.mode == kJJLocationManagerModeStandardWhenInUse) {
        [self requestWhenInUseAuthorization]; // for significant location changes this auth is required (>= ios8)
        [self.locationManager startUpdatingLocation];
    }
    else {
        [self requestAlwaysAuthorization]; // for significant location changes this auth is required (>= ios8)
        [self.locationManager startMonitoringSignificantLocationChanges];
    }
    
    // based on docs, locationmanager's location property is populated with latest
    // known location even before we started monitoring, so let's simulate a change
    if (self.locationManager.location) {
        [self locationManager:self.locationManager didUpdateLocations:@[self.locationManager.location]];
    }
}

- (void)startLocationUpdates:(JJLocationManagerMonitorMode)mode
              distanceFilter:(CLLocationDistance)filter
                    accuracy:(CLLocationAccuracy)accuracy
{
    self.mode = mode;
    self.locationManager.distanceFilter = filter;
    self.locationManager.desiredAccuracy = accuracy;
    
    [self startLocationUpdates];
}

- (void)stopLocationUpdates
{
    if (self.mode == kJJLocationManagerModeStandard) {
        [self.locationManager stopUpdatingLocation];
    }
    else {
        [self.locationManager stopMonitoringSignificantLocationChanges];
    }
    
}

#pragma mark - Private

/**
 iOS 8 requires you to request authorisation prior to starting location updates
 */
- (void)requestAlwaysAuthorization
{
    if (![self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        return;
    }
    
    [self.locationManager requestAlwaysAuthorization];
}

- (void)requestWhenInUseAuthorization
{
    if (![self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        return;
    }
    
    [self.locationManager requestWhenInUseAuthorization];
}

+ (void)setSharedManager:(JJLocationManager *)manager
{
    sharedManager = manager;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if (![locations count]) {
        return;
    }
    
    // location didn't change
    if (nil != self.currentLocation && [[locations lastObject] isEqual:self.currentLocation]) {
        return;
    }
    
    self.currentLocation = [locations lastObject];
    if(self.isLocationUpdated==NO||isDriverLocationUpdatedServerInitially==NO){
        if(self.currentLocation.coordinate.latitude!=0){
            if(self.isLocationUpdated==NO){
                self.isLocationUpdated=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:kJJLocationManagerNotificationLocationUpdatedInitially
                                                                    object:self];
            }
            
            if([Theme UserIsLogin]){
                isDriverLocationUpdatedServerInitially=YES;
                [self performSelector:@selector(updateLocationOnBackgroundThread) withObject:self afterDelay:2];
            }
            
            
        }
    }
    
    
    [self locationManagerUpdate:self.locationManager didUpdateToLocation:self.currentLocation fromLocation:self.previousLocation];
    
    // notify about the change
    [[NSNotificationCenter defaultCenter] postNotificationName:kJJLocationManagerNotificationLocationUpdatedName
                                                        object:self];
}

-(void)updateLocationOnBackgroundThread{
    @try {
        [NSThread detachNewThreadSelector:@selector(updateLoc) toTarget:self withObject:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception in Location Update thread..");
    }
}


- (void)locationManagerUpdate:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    if(oldLocation.coordinate.latitude!=0){
        _previousLocation=oldLocation;
    }else{
        _previousLocation=newLocation;
    }
    if(isDriverLocationUpdatedInitially==NO){
        isDriverLocationUpdatedInitially=YES;
        previousLocationBefore100Metres=newLocation;
        previousLocationBefore5etres=newLocation;
    }
    float totalDistTowardsDest= GMSGeometryDistance(previousLocationBefore100Metres.coordinate, newLocation.coordinate);
    float totalDistFromPrevious= GMSGeometryDistance(previousLocationBefore5etres.coordinate, newLocation.coordinate);
    
    if(totalDistFromPrevious>=5){
        previousLocationBefore5etres=newLocation;
    }
    if(totalDistTowardsDest>=100){
        previousLocationBefore100Metres=newLocation;
        if([Theme UserIsLogin]){
            [self updateLocationOnBackgroundThread];
        }
    }
}

-(void)refreshDriverCurrentLocation{
    [_locationManager startUpdatingLocation];
    
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kJJLocationManagerNotificationFailedName
                                                        object:self
                                                      userInfo:@{@"error": error}];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [[NSNotificationCenter defaultCenter] postNotificationName:kJJLocationManagerNotificationAuthorizationChangedName
                                                        object:self userInfo:@{@"status": @(status)}];
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            //   NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            // NSLog(@"User hates you");
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [self startLocationUpdates];; //Will update location immediately
        } break;
        default:
            break;
    }
}

-(void)updateLoc{
    
    
    
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web UpdateUserLocation:[self setParametersUpdateLocation:self.currentLocation]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             
             NSString * str=[Theme checkNullValue:responseDictionary[@"response"][@"availability"]];
             NSString * verifyStr=[Theme checkNullValue:responseDictionary[@"response"][@"verify_status"]];
             if(![verifyStr isEqualToString:@"Yes"]||[str isEqualToString:@"Unavailable"]){
                 [[NSNotificationCenter defaultCenter] postNotificationName:kDriverNotifForPendingAndVerifyStatus
                                                                     object:responseDictionary];
             }
         }
     }
                    failure:^(NSError *error)
     {
         
         // [self.view makeToast:kErrorMessage];
         
     }];
    
}



-(NSDictionary *)setParametersUpdateLocation:(CLLocation *)loc{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"latitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]],
                                  @"longitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]],
                                  };
    return dictForuser;
}
@end
