//
//  Theme.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "Theme.h"
#include <math.h>

@implementation Theme

#pragma mark Set Font Methods
#pragma --
//////////////////////////////////////////////////////////////////////////////////
+(UILabel *)setNormalFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return label;
}
+(UILabel *)setNormalSmallFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return label;
}
+(UILabel *)setBoldFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:20]];
    return label;
}
+(UILabel *)setSmallBoldFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return label;
}
+(UILabel *)setLargeBoldFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:28]];
    return label;
}
+(UILabel *)setMediumLargeBoldFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:20]];
    return label;
}
+(UITextField *)setLargeBoldFontForTextField:(UITextField *)textField{
    [textField setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:28]];
    return textField;
}
+(UIButton *)setNormalFontForButton:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return btn;
}
+(UIButton *)setLargeFontForButton:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:20]];
    return btn;
}
+(UIButton *)setBoldFontForButton:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return btn;
}
+(UIButton *)setBoldFontForButtonBold:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:16]];
    return btn;
}
+(UIButton *)setSmallBoldFontForButtonBold:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:13]];
    return btn;
}
+(UIButton *)setBoldSmallFontForButton:(UIButton *)btn{
    [btn.titleLabel setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:13]];
    return btn;
}
//+(UILabel *)setHeaderFontForLabel:(UILabel *)label{
//    [label setFont: [UIFont fontWithName:@"RobotoCondensed-Regular" size:18]];
//    return label;
//}
+(UITextField *)setNormalFontForTextfield:(UITextField *)textField{
    [textField setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return textField;
}

+(UITextView *)setNormalFontForTextView:(UITextView *)UITextView{
    [UITextView setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:14]];
    return UITextView;
}

+(UILabel *)setHeaderFontForLabel:(UILabel *)label{
    [label setFont: [UIFont fontWithName:@"Poppins-SemiBold" size:18]];
    return label;
}


//////////////////////////////////////////////////////////////////////////////////
+(NSString *)checkNullValue:(NSString *)str{
    if (str == nil || [str isKindOfClass:[NSNull class]]) {
        str=@"";
    }
    NSString * returnString=[NSString stringWithFormat:@"%@",str];
    return returnString;
}
+(void)savePushNotificationID:(NSString *)token{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kdeviceTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(void)saveTrafficEnabled:(NSString *)traffic{
    [[NSUserDefaults standardUserDefaults] setObject:traffic forKey:kTrafficKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)getTrafficStatus{
   
    NSString *savedValue =[[NSUserDefaults standardUserDefaults]stringForKey:kTrafficKey];
    BOOL status=NO;
     if([savedValue isEqualToString:@"YES"]){
         status =YES;
    }
    return status;
}

+(NSString *)getDeviceToken{
    //@"21a6d650d0063c66fca06e0dc5426d23a3a823be5ac8af13f004e9e415085a7f";
    NSString *savedValue =[[NSUserDefaults standardUserDefaults]stringForKey:kdeviceTokenKey];
    if(savedValue==nil){
        savedValue=@"";
    }
    return savedValue;
}
+(void)saveUserDetails:(DriverInfoRecords*)userInfoRec{
    NSData* data=[NSKeyedArchiver archivedDataWithRootObject:[self recordsToDict:userInfoRec]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kDriverInfo];
}



+(NSDictionary *)recordsToDict:(DriverInfoRecords *)userInfoRec{
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setObject:userInfoRec.driverId forKey:@"driver_id"];
    [dict setObject:userInfoRec.driverName forKey:@"driver_name"];
    [dict setObject:userInfoRec.driverImage forKey:@"driver_image"];
    [dict setObject:userInfoRec.driverEmail forKey:@"email"];
    [dict setObject:userInfoRec.driverVehicModel forKey:@"vehicle_model"];
    [dict setObject:userInfoRec.driverVehicNumber forKey:@"vehicle_number"];
    [dict setObject:userInfoRec.driverKey forKey:@"key"];
    return dict;
}
+(void)ClearUserDetails{
    NSData* data=[NSKeyedArchiver archivedDataWithRootObject:nil];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kDriverInfo];
}

+(NSDictionary *)retrieveUserData{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kDriverInfo];
    NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return myDictionary;
}
+(BOOL)UserIsLogin{
    if ([[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:kDriverInfo]){
        NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kDriverInfo];
        NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(myDictionary!=nil){
            return YES;
        }
    }
    return NO;
}
+(NSDictionary *)DriverAllInfoDatas{
    if([self UserIsLogin]){
        NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kDriverInfo];
        NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        return myDictionary;
    }
    return nil;
}
+(void)SaveUSerISOnline:(NSString*)driverStatus{
    [[NSUserDefaults standardUserDefaults] setObject:[Theme checkNullValue:driverStatus] forKey:kDriverGoOnline];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(BOOL )retrieveDriverOnline{
    BOOL IsOnline=NO;
    if([Theme UserIsLogin]){
      NSString *  savedValue = [[NSUserDefaults standardUserDefaults]
                      stringForKey:kDriverGoOnline];
        if([savedValue isEqualToString:@"1"]){
            IsOnline=YES;
        }else{
          IsOnline=YES;
        }
    }
    
    return IsOnline;
}
+ (NSString *)findCurrencySymbolByCode:(NSString *)_currencyCode
{
    NSNumberFormatter *fmtr = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [self findLocaleByCurrencyCode:_currencyCode];
    NSString *currencySymbol;
    if (locale)
        [fmtr setLocale:locale];
    [fmtr setNumberStyle:NSNumberFormatterCurrencyStyle];
    currencySymbol = [fmtr currencySymbol];
    
    if (currencySymbol.length > 1)
        currencySymbol = [currencySymbol substringToIndex:1];
    return currencySymbol;
}
+(NSLocale *) findLocaleByCurrencyCode:(NSString *)_currencyCode
{
    NSArray *locales = [NSLocale availableLocaleIdentifiers];
    NSLocale *locale = nil;
    
    for (NSString *localeId in locales) {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:localeId];
        NSString *code = [locale objectForKey:NSLocaleCurrencyCode];
        if ([code isEqualToString:_currencyCode])
            break;
        else
            locale = nil;
    }
    return locale;
}
+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
+(NSDate*)setDate:(NSString*)dateStr{
    if ([dateStr isKindOfClass:[NSDate class]]) {
        return (NSDate*)dateStr;
    }
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
}
+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
+ (BOOL)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return YES;
    else
        return NO;
}
+(void)SetLanguageToApp{
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"LanguageNameCabilyPartner"];
    if([savedValue isEqualToString:@"es"]){
        setApplicationLanguage(SpanishLanguageShortName);
    }else{
        setApplicationLanguage(EnglishUSLanguageShortName);
    }
}
+(void)saveLanguage:(NSString *)str{
    if([str isEqualToString:@"es"]){
        str=@"es";
    }else{
        str=@"en";
    }
    NSString *valueToSave = str;
    [[NSUserDefaults standardUserDefaults] setObject:valueToSave forKey:@"LanguageNameCabilyPartner"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString*)getCurrentLanguage
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"LanguageNameCabilyPartner"]);
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"LanguageNameCabilyPartner"];
}


/// xmpp
+(void)saveXmppUserCredentials:(NSString *)str{
   
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"xmppusercredentialsdectar"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
+(NSString*)getXmppUserCredentials
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"xmppusercredentialsdectar"];
}


+(NSString*)project_getAppName {
    return NSBundle.mainBundle.infoDictionary[@"CFBundleName"];
}
+(void)saveDistanceString:(NSString *)str{
    
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"DistanceStingCabilyPartner"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
+(NSString*)getCurrentDistanceString
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"DistanceStingCabilyPartner"];
}

+(void)saveAppDetails:(AppInfoRecords*)appInforec{
    NSData* data=[NSKeyedArchiver archivedDataWithRootObject:[self recordsToAppDict:appInforec]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kAppInfo];
}

+(NSDictionary *)recordsToAppDict:(AppInfoRecords *)appInforec{
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setObject:appInforec.serviceContactEmail forKey:@"site_contact_mail"];
    [dict setObject:appInforec.serviceNumber forKey:@"customer_service_number"];
    [dict setObject:appInforec.serviceSiteUrl forKey:@"site_url"];
    [dict setObject:appInforec.serviceXmppHost forKey:@"xmpp_host_url"];
    [dict setObject:appInforec.serviceXmppPort forKey:@"xmpp_host_name"];
    [dict setObject:appInforec.serviceFacebookAppId forKey:@"facebook_id"];
    [dict setObject:appInforec.serviceGooglePlusId forKey:@"google_plus_app_id"];
    [dict setObject:appInforec.servicePhoneMaskingStatus forKey:@"phone_masking_status"];
    [dict setObject:appInforec.hasPendingRide forKey:@"ongoing_ride"];
    [dict setObject:appInforec.pendingRideID forKey:@"ongoing_ride_id"];
    [dict setObject:appInforec.hasPendingRating forKey:@"rating_pending"];
    [dict setObject:appInforec.pendingRateId forKey:@"rating_pending_ride_id"];
    return dict;
    
    
}

+(NSDictionary *)retrieveAppData{
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kAppInfo];
    NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return myDictionary;
}
+(void)ClearAppDetails{
    NSData* data=[NSKeyedArchiver archivedDataWithRootObject:nil];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kAppInfo];
}
+(NSDictionary *)AppAllInfoDatas{
    if([self UserIsLogin]){
        NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kAppInfo];
        NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        return myDictionary;
    }
    return nil;
}
+(BOOL)hasAppDetails{
    if ([[[NSUserDefaults standardUserDefaults] dictionaryRepresentation].allKeys containsObject:kAppInfo]){
        NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:kAppInfo];
        NSDictionary* myDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(myDictionary!=nil){
            return YES;
        }
    }
    return NO;
}
+(double) DegreeBearing:(CLLocation*) A locationB: (CLLocation*)B{
    
    
    // OLD CODE
//    double lant1 = [self ToRad:A.coordinate.latitude];
//    double lont1 = [self ToRad:A.coordinate.longitude];
//
//    double lant2 = [self ToRad:B.coordinate.latitude];
//    double lont2 = [self ToRad:B.coordinate.longitude];
//
//    double dLon = lont2-lont1;
//
//    double y = sin(dLon)*cos(lant2);
//    double x = cos(lant1)*sin(lant2) - sin(lant1)*cos(lant2)*cos(dLon);
//    double radianBearing = [self ToDegrees:atan2(y, x)];
//
//
//    if (radianBearing<0.0){
//        radianBearing +=2*M_PI;
//    }
//
//
//
//    return [self ToDegrees:radianBearing];
//

//  NEW CODE
//
//    float lat1 = A.coordinate.latitude;
//    float lon1 = A.coordinate.longitude;
//    float lat2 = B.coordinate.latitude;
//    float lon2 = B.coordinate.longitude;
//    float dLon = lon1 - lon2;
//    float x = cos(dLon) * sin(lat2);
//    float y = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
//    float radiansBearing = atan2(x, y);
//    float rotation = radiansBearing + 360;
//    float rotationmarker = (rotation/360);
//
//
//
//    return rotationmarker+180 ;

    
//    float fromlat =  [self ToRad:A.coordinate.latitude];
//    float fromlong = [self ToRad:A.coordinate.longitude];
//    float tolat = [self ToRad:B.coordinate.latitude];
//    float toLong = [self ToRad:B.coordinate.longitude];
//
//    float degree = [self ToDegrees:(atan2(sin(toLong - fromlong) * cos(tolat), cos(fromlat) * sin(tolat) - sin(fromlat) * cos(tolat) * cos(toLong -fromlong)))];
//
//
//
//    if (degree >= 0) {
//
//        return degree - 180.0 ;
//    }else{
//        return (360.0+degree) -180.0 ;
//    }
//
//
    
    
    float deltaLongitude = B.coordinate.longitude - A.coordinate.longitude;
    float deltaLatitude = B.coordinate.latitude - A.coordinate.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    if (deltaLongitude > 0)
        return angle;
    else if (deltaLongitude < 0)
        return angle + M_PI;
    else if (deltaLatitude < 0)
        return M_PI;
    return 0.0f;
    
    
}



+(double) ToRad: (double)degrees{
    return ( degrees*M_PI / 180.0);
}

+(double) ToBearing:(double)radians{
    return [self ToDegrees:radians] + 360 % 360;
}

+(double) ToDegrees: (double)radians{
    return (radians * 180.0 / M_PI);
}
+(UIButton *)setDropShadow:(UIButton *) shadowView{
   // [shadowView.layer setCornerRadius:shadowView.frame.size.width/2];
    
    // border
    [shadowView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [shadowView.layer setBorderWidth:1.5f];
    
    // drop shadow
    [shadowView.layer setShadowColor:[UIColor blackColor].CGColor];
    [shadowView.layer setShadowOpacity:0.8];
    [shadowView.layer setShadowRadius:3.0];
    [shadowView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    return shadowView;
}
+(UIView *)setBackGroundImage:(UIView *)view{
    
    
    UIGraphicsBeginImageContext(view.frame.size);
    [[UIImage imageNamed:@"splashscreen2"] drawInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    return view;
    
    
}
//+(UIView *)sideSetBackGroundImage:(UIView *)view;
//{
//    UIGraphicsBeginImageContext(view.frame.size);
//    [[UIImage imageNamed:@"splashscreen"] drawInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    view.backgroundColor = [UIColor colorWithPatternImage:image];
//    return view;
//}















-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {corner = corner | UIRectCornerTopLeft;}
        if (tr) {corner = corner | UIRectCornerTopRight;}
        if (bl) {corner = corner | UIRectCornerBottomLeft;}
        if (br) {corner = corner | UIRectCornerBottomRight;}
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}


@end
