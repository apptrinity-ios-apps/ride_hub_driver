//
//  Constant.h
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Constant : NSObject


#define NoNetworkCon @"NoNetworkConnection"
#define kdeviceTokenKey @"deviceToken"
#define kTrafficKey @"TrafficKey"
#define kDriverInfo @"DriverInfokey"
#define kDriverGoOnline @"DriverIsOnline"
#define kDriverReceiveNotif @"DriverIsONReceiveNotif"
#define kDriverCashPaymentNotif @"DriverCashPaymentNotif"
#define kDriverCashPaymentNotifWhenQuit @"DriverCashPaymentNotifWhenQuited"
#define kDriverPaymentCompletedNotif @"DriverPaymentCompletedNotif"
#define kUserCancelledDrive @"kUserCancelledDriveNotif"
#define kDriverReturnNotif @"AcceptReturntoHomeNotif"
#define kDriverAdvtInfo @"kDriverAdvtInfoNotif"
#define kDriverSessionOut @"sessionOutNotif"
#define kDriverNotifForPendingAndVerifyStatus @"kDriverNotifForPendingAndVerifyStatusNotif"

#define kPushSaveKey @"PushSaveKey"
#define kAppInfo @"DriverAppInfokey"
#define kNewTripKey @"newTripNotif"
#define kOpenGoogleMapScheme @"DectarDriver://"
#define kErrorMessage @"Unable_to_connect_server"

#define kGoogleApiKey @"AIzaSyD0AZLQc2BBi8t6krc_2jkcF6lD5mPtdso"// change

//#define kGoogleApiKey @"AIzaSyBDqhyKVHIMwJtOXA8QwYI5pOtnENeEoXU"// change
#define kGoogleServerKey @"AIzaSyADNJeqee2XtzyRr0f8QIlYTiR-i2eQXY8"//115.118.31.166
#define kGoogleClientId @"739435501135-4j7ddsp9ukpfeuank895chdfms8a3o2q.apps.googleusercontent.com"

#define kAppSupportEmailId @"info@zoplay.com"

#define kHockeyAppIdentifier @"0fd9b50950214edf83db1a3f1ff3b2a5"

//ewheels  previous one
//#define baseUrl @"http://52.74.12.27/v4/"



// Testing
//#define baseUrl @"http://13.229.251.7/v4/"
//#define xmppHostName @"http://13.229.251.7"   ////192.168.1.150 //67.219.149.186




// Production
//#define baseUrl @"https://ridehub.co/v4/"
//Testing
#define baseUrl @"https://18.141.126.93/v4/"
//#define xmppHostName @"ridehub.co"
//production
#define xmppHostName @"54.169.136.144"





//#define baseUrl @"http://54.169.136.144/v4/"

//#define baseUrl @"https://ridehub.us/v4/"
#define BGCOLOR [UIColor colorWithRed:29.0/255.0 green:59.0/255.0 blue:111.0/255.0 alpha:1.0]
#define dotlineGreen [UIColor colorWithRed:81.0/255.0 green:190.0/255.0 blue:19.0/255.0 alpha:1.0]
#define dotlineBlue [UIColor colorWithRed:246.0/255.0 green:249.0/255.0 blue:255.0/255.0 alpha:1.0]
#define segmentTextColor [UIColor colorWithRed:34/255.0 green:43/255.0 blue:69/255.0 alpha:1.0]
#define segmentBlueColor [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.7]
#define segmentGrayColor [UIColor colorWithRed:159/255.0 green:185/255.0 blue:203/255.0 alpha:0.7]

#define headingTextColor [UIColor colorWithRed:22/255.0 green:48/255.0 blue:61/255.0 alpha:0.7]

#define SkyBlueColor [UIColor colorWithRed:6.0/255.0 green:152.0/255.0 blue:212.0/255.0 alpha:1.0]
#define GreenColor  [UIColor colorWithRed:81.0/255.0 green:190.0/255.0 blue:19.0/255.0 alpha:1.0]

#define segmentBlueColor [UIColor colorWithRed:68/255.0 green:174/255.0 blue:218/255.0 alpha:1.0]
#define segmnetGrayColor  [UIColor colorWithRed:159/255.0 green:185/255.0 blue:203/255.0 alpha:0.4]
#define segmentTextColor  [UIColor colorWithRed:34/255.0 green:43/255.0 blue:69/255.0 alpha:1.0]




//#define baseUrl @"http://project.dectar.com/fortaxi/v4/"
//#define baseUrl @"http://192.168.1.251:8081/product-working/dectar/cabily/v4/"


//#define xmppHostName @"67.219.149.186"

#define xMppJabberIdentity @"messaging.dectar.com" //messaging.dectar.com //casp83

#define RegUrl baseUrl@"app/driver/signup"
#define CompletionUrl baseUrl@"app/driver/signup/success"
#define LoginUrl baseUrl@"provider/login"
#define UpdateCurrentLocationUrl baseUrl@"provider/update-driver-geo"
#define GoOnlineUrl baseUrl@"provider/update-availability"
#define DriverRejectRequest baseUrl@"provider/cancellation-reason"
#define DriverAcceptRequest baseUrl@"provider/accept-ride"
#define DriverArrivedLocation baseUrl@"provider/arrived"
#define DriverStartedTrip baseUrl@"provider/begin-ride"
#define DriverEndTrip baseUrl@"provider/end-ride"
#define DriverCacelTripWithReason baseUrl@"provider/cancel-ride"
#define getUserInfo baseUrl@"provider/get-rider-info"
#define getDriverTripsList baseUrl@"provider/my-trips/list"
#define getDriverTripsDetail baseUrl@"provider/my-trips/view"
#define requestForCashOTP baseUrl@"provider/receive-payment"

#define cashReceivedUrl baseUrl@"provider/payment-received"
#define requestPayment baseUrl@"provider/request-payment"
#define rateTheUser baseUrl@"app/review/options-list"
#define ReferPointsURl baseUrl@"provider/get-points-list"
#define InviteAndEarnURL baseUrl@"provider/get-invites"

#define submitRate baseUrl@"app/review/submit"
#define LogoutUrl baseUrl@"provider/logout"
#define saveBankingUrl baseUrl@"provider/save-banking-info"
#define getBankingUrl baseUrl@"provider/get-banking-info"
#define paymentListUrl baseUrl@"provider/payment-list"
#define kPaymentDetailUrl baseUrl@"provider/payment-summary"
#define ContinueRideUrl baseUrl@"provider/continue-trip"
#define NoNeedPaymentUrl baseUrl@"provider/payment-completed"
#define GoogleLocation @"https://maps.googleapis.com/maps/api/place/autocomplete/json?&language=en"
#define ForgotPasswordUrl baseUrl@"provider/forgot-password"
#define ChangePasswordUrl baseUrl@"provider/change-password" 
#define StarterDataUrl baseUrl@"provider/dashboard?" //api/xmpp-status
#define XmppUpdateUrl baseUrl@"api/xmpp-status?"
#define getRouteFromGoogle @"https://maps.googleapis.com/maps/api/directions/json"
#define getLatLongToAddressGoogle @"https://maps.googleapis.com/maps/api/geocode/json?"
#define CheckPaymentStatusUrl baseUrl@"api/v3/check-trip-status"
#define getDriverLocationsUrl baseUrl@"v3/app/get-location-list"
#define getDriverCategoryUrl baseUrl@"v3/app/get-category-list"
#define getCountryListURL baseUrl@"v3/app/get-country-list"
#define getVehicleListURL baseUrl@"v3/app/get-vehicle-list"
#define getVehicleMakerListURL baseUrl@"v3/app/get-maker-list"
#define getVehicleModelListURL baseUrl@"v3/app/get-model-list"
#define getVehicleYearListURL baseUrl@"v3/app/get-year-list"
#define sendOTPRegURL baseUrl@"v3/app/send-otp-driver"
#define sendDriverImageURL baseUrl@"v3/app/save-image"
#define regDriverByApp baseUrl@"v3/app/register-driver"
#define GetAppInformation baseUrl@"api/v3/get-app-info"



@end
