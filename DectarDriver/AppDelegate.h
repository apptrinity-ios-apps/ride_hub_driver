//
//  AppDelegate.h
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Theme.h"
#import "StarterViewController.h"
#import "InitialViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ITRAirSideMenu.h"
#import "ITRLeftMenuController.h"
#import "UIView+Toast.h"
#import "XMPP.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMLReader.h"
#import <AVFoundation/AVFoundation.h>
#import "JJLocationManager.h"
#import "AppInfoRecords.h"
#import "OpinionzAlertView.h"
#import "TripViewController.h"
#import "PushKit/PushKit.h"

@import HockeySDK;


@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    BOOL isXMPPDisConnected;
    AVAudioPlayer * audioPlayer;
}
@property ITRAirSideMenu *itrAirSideMenu;
@property (strong, nonatomic) UIWindow *window;
@property (assign , nonatomic) BOOL isXmppActive;
@property (assign , nonatomic) BOOL isInternetAvailable;
-(BOOL) isInternetAvailableFor;
-(void)setInitialViewController;
+ (BOOL) checkLocationServicesTurnedOn;
+(BOOL) checkApplicationHasLocationServicesPermission;


///////////// xmpp
@property (nonatomic, readonly) XMPPStream *xmppStream;
@property (nonatomic, readonly) XMPPRoster *xmppRoster;
@property (nonatomic, readonly) XMPPRosterCoreDataStorage *xmppRosterStorage;
@property(nonatomic,strong)NSTimer *connectionTimer;
@property(assign,nonatomic)NSInteger appInfoIteration;
@property (nonatomic, assign) NSString * backgroundStatus;
@property NSString * appdelegateRideID;


@property NSString * drop_lat;
@property NSString * drop_long;



@property NSString * offline_Pickup_Address;
@property NSString * offline_drop_Address;

@property NSString * offline_drop_lattitude;
@property NSString * offline_drop_longtitude;

@property NSString * offline_pickup_lattitude;
@property NSString * offline_pickup_longtitude;


@property NSString * vehicleNumber;



@property NSString * user_id;
@property NSString * user_name;
@property NSString * user_email;
@property NSString * user_image;


@property NSString * SETTOTALDISTANCE;

-(void)connectToXmpp;
- (BOOL)connect;
- (void)disconnect;
-(void)logoutXmpp;
-(void)xmppState;
-(void)GetAppInfo;
-(void)xmppUpdateLoc:(CLLocationCoordinate2D)curLocation withReceiver:(NSString *)receiverId withRideId:(NSString *)rideId withBearing:(NSString *)bearingValue;

-(void)getInitialDatas;
@property (assign , nonatomic) BOOL appInfoIsLoading;
@property(strong,nonatomic)NSString * xmppJabberIdStr;


@end

