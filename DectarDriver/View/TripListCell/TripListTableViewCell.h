//
//  TripListTableViewCell.h
//  DectarDriver
//
//  Created by Casperon Technologies on 8/31/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TripRecords.h"
#import "Theme.h"
#import "DectarCustomColor.h"

@interface TripListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *rideStatusLbl;

@property (weak, nonatomic) IBOutlet UIView *cellView;

@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (strong, nonatomic) IBOutlet UIView *img_backgroundView;
@property (strong, nonatomic) IBOutlet UIView *greenDotLine;

@property (weak, nonatomic) IBOutlet UILabel *destinationLbl;


@property NSInteger segement;
-(void)setDatasToTripList:(TripRecords *)tripRecs;
@end
