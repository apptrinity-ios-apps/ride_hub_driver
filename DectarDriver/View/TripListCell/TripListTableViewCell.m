//
//  TripListTableViewCell.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/31/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "TripListTableViewCell.h"

@implementation TripListTableViewCell
@synthesize locationLbl,rideStatusLbl,
timeLbl,imgView,greenDotLine,cellView,destinationLbl;

- (void)awakeFromNib {
    
    self.cellView.layer.cornerRadius = 5;
    self.cellView.layer.masksToBounds = YES;
//    self.cellView.layer.borderColor =subBlueColor.CGColor;
    
    [self setShadow:cellView];

    
    CAShapeLayer *yourViewBorder1 = [CAShapeLayer layer];
    yourViewBorder1.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder1.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder1.lineDashPattern = @[@3,@3];
    yourViewBorder1.frame = self.greenDotLine.bounds;
    yourViewBorder1.path = [UIBezierPath bezierPathWithRect:self.greenDotLine.bounds].CGPath;
    [self.greenDotLine.layer addSublayer:yourViewBorder1];
    
    // Initialization code
    [super awakeFromNib];
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)setDatasToTripList:(TripRecords *)tripRecs{
    [self setFont];
    locationLbl.text=tripRecs.tripLocation;
    destinationLbl.text = tripRecs.droplocation;
    timeLbl.text=[NSString stringWithFormat:@"%@, %@",tripRecs.tripDate,tripRecs.tripTime];
    
  //  imgView.image=[UIImage imageNamed:@"car"];
   // NSLog(@"%@",tripRecs.tripStatus);
    
    if([tripRecs.tripStatus isEqualToString:@"cancelled"]){
        NSString * lableText =tripRecs.tripStatus;
        rideStatusLbl.text = [lableText capitalizedString];
        rideStatusLbl.backgroundColor= [UIColor redColor];
        // imgView.image=[UIImage imageNamed:@"car"];
    
    }else if ([tripRecs.tripStatus isEqualToString:@"completed"]){
        
        rideStatusLbl.backgroundColor= [UIColor colorWithRed:38/255.0f green:142/255.0f blue:255/255.0f alpha:1];

        NSString * lableText =tripRecs.tripStatus;
        rideStatusLbl.text = [lableText capitalizedString];
    }else
    {
        NSString * lableText =tripRecs.tripStatus;
        rideStatusLbl.text = [lableText capitalizedString];
        rideStatusLbl.backgroundColor= subBlueColor;
       
    }
    
}
-(void)setFont{
    
//    locationLbl=[Theme setNormalFontForLabel:locationLbl];
//    destinationLbl=[Theme setNormalFontForLabel:destinationLbl];
    timeLbl=[Theme setNormalSmallFontForLabel:timeLbl];
    rideStatusLbl.layer.cornerRadius = 8;
    rideStatusLbl.layer.masksToBounds = YES;
    self.cellView.layer.borderColor =subBlueColor.CGColor;
  
   
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

//    float shadowSize = 5.0f;
//    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(self.cellView.frame.origin.x - shadowSize-5,
//                                                                           self.cellView.frame.origin.y - shadowSize-7,
//                                                                           self.cellView.frame.size.width + shadowSize-2,
//                                                                           self.cellView.frame.size.height + shadowSize)];
//    self.cellView.layer.masksToBounds = NO;
//    self.cellView.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.cellView.layer.shadowOffset = CGSizeMake(0.1f, 0.1f);
//    self.cellView.layer.shadowOpacity = 0.2f;
//    self.cellView.layer.shadowRadius = 3;
//    self.cellView.layer.shadowPath = shadowPath.CGPath;
//    self.cellView.layer.borderColor =subBlueColor.CGColor;
//    [super setSelected:selected animated:animated];

}

@end
