//
//  PaymentListTableViewCell.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/14/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "PaymentListTableViewCell.h"

@implementation PaymentListTableViewCell
@synthesize paymentHeaderLbl,paymentDateLbl,amountHeaderLbl,amountLbl,RecDateHeaderLbl,receiverDateLbl,contentSubView,headView, cellbackView;

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    [self setShadow:contentSubView];
    contentSubView.layer.cornerRadius = 5;
//    [self roundCornersOnView:headView onTopLeft:true topRight:true bottomLeft:false bottomRight:false radius:5];
    
    
//    [paymentHeaderLbl setText:JJLocalizedString(@"Payment", nil)];
//    [amountHeaderLbl setText:JJLocalizedString(@"Amount", nil)];
//    [RecDateHeaderLbl setText:JJLocalizedString(@"Received_Date", nil)];
    //cellbackView
//    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii: (CGSize){20.0, 10.}].CGPath;
//    self.cellbackView.layer.mask = maskLayer;
//    self.contentSubView.layer.cornerRadius = 15;
//    self.contentSubView.layer.masksToBounds = true;
//    self.contentSubView.layer.borderColor = [UIColor grayColor].CGColor;
//    self.contentSubView.layer.borderWidth = 1;
//    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
//    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){20.0, 10.}].CGPath;
//    self.contentView.layer.mask = maskLayer;
    
    // Initialization code
    
}


-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br)
    {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        //  maskLayer.frame = roundedView.bounds;   /// p333
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}


-(void)setDatasToList:(PaymentRecords *)objRecs{
    [self setFont];
    paymentDateLbl.text=[NSString stringWithFormat:@"   %@ to %@",objRecs.fromDate,objRecs.toDate];
   // paymentDateLbl.textAlignment = NSTextAlignmentCenter;
    amountLbl.text=[NSString stringWithFormat:@"   %@ %@",objRecs.PriceSymbol,objRecs.Amount];
  //  amountLbl.textAlignment = NSTextAlignmentCenter;
    NSString*space = @"  ";
    receiverDateLbl.text = [space stringByAppendingString:objRecs.ReceivedDate];
    //receiverDateLbl.textAlignment = NSTextAlignmentCenter;
 
}
-(void)setFont{
//    paymentHeaderLbl=[Theme setNormalFontForLabel:paymentHeaderLbl];
//    paymentDateLbl=[Theme setSmallBoldFontForLabel:paymentDateLbl];
//    amountHeaderLbl=[Theme setNormalFontForLabel:amountHeaderLbl];
//    amountLbl=[Theme setSmallBoldFontForLabel:amountLbl];
//    RecDateHeaderLbl=[Theme setNormalFontForLabel:RecDateHeaderLbl];
//    receiverDateLbl=[Theme setNormalSmallFontForLabel:receiverDateLbl];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
