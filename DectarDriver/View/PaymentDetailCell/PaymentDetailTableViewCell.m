//
//  PaymentDetailTableViewCell.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/14/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "PaymentDetailTableViewCell.h"

@implementation PaymentDetailTableViewCell
@synthesize paymentHeaderLbl,paymentDateLbl,amountHeaderLbl,amountLbl,RecDateLbl,FieldbackView,dotLine,dateHeaderLbl;

- (void)awakeFromNib {
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    [self setShadow:FieldbackView];
    
    FieldbackView.layer.cornerRadius = 5;
    
    paymentHeaderLbl.layer.cornerRadius = 5;
    amountHeaderLbl.layer.cornerRadius = 5;
    dateHeaderLbl.layer.cornerRadius = 5;
    
    paymentDateLbl.layer.cornerRadius = 5;
    amountLbl.layer.cornerRadius = 5;
    RecDateLbl.layer.cornerRadius = 5;
    
    paymentHeaderLbl.layer.masksToBounds = YES;
    amountHeaderLbl.layer.masksToBounds = YES;
    dateHeaderLbl.layer.masksToBounds = YES;
    
    paymentDateLbl.layer.masksToBounds = YES;
    amountLbl.layer.masksToBounds = YES;
    RecDateLbl.layer.masksToBounds = YES;
    
    paymentDateLbl.layer.borderWidth = 1;
    amountLbl.layer.borderWidth = 1;
    RecDateLbl.layer.borderWidth = 1;
    
    paymentDateLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    amountLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    RecDateLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    
    
//    [paymentHeaderLbl setText:JJLocalizedString(@"Ride_Id",nil)];
//    [amountHeaderLbl  setText:JJLocalizedString(@"Amount", nil)];
//    [_dateHeaderLbl   setText:JJLocalizedString(@"Received_Date", nil)];
//    _dateHeaderLbl.textAlignment = NSTextAlignmentCenter;
//    amountHeaderLbl.textAlignment = NSTextAlignmentCenter;
//    paymentHeaderLbl.textAlignment = NSTextAlignmentCenter; // 10
//
//    FieldbackView.layer.cornerRadius = 10;
//    FieldbackView.layer.borderWidth = 1;
//    FieldbackView.layer.borderColor = [UIColor grayColor].CGColor;
//    FieldbackView.layer.masksToBounds = YES;/// 123
//
    // Initialization code
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)setDatasToDetailList:(PaymentDetailRecords *)objRecs{
    [self setFont];
    NSString *space = @"     ";
    paymentDateLbl.text=[space stringByAppendingString:objRecs.rideId];
    amountLbl.text=[NSString stringWithFormat:@"     %@ %@",objRecs.currencySymbol,objRecs.amount];
    RecDateLbl.text=[space stringByAppendingString:objRecs.date];
    
}
-(void)setFont{
//    paymentHeaderLbl=[Theme setNormalFontForLabel:paymentHeaderLbl];
//    paymentDateLbl=[Theme setSmallBoldFontForLabel:paymentDateLbl];
//    amountHeaderLbl=[Theme setNormalFontForLabel:amountHeaderLbl];
//    amountLbl=[Theme setSmallBoldFontForLabel:amountLbl];
//    RecDateLbl=[Theme setNormalSmallFontForLabel:RecDateLbl];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
