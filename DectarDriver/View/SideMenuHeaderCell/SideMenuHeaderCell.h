//
//  SideMenuHeaderCell.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 14/11/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SideMenuHeaderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *backView;

@property (strong, nonatomic) IBOutlet UIImageView *profile_imageView;
@property (strong, nonatomic) IBOutlet UILabel *name_LBL;


@property (strong, nonatomic) IBOutlet UIButton *viewProfile_Btn;

@property (strong, nonatomic) IBOutlet UIImageView *edit_ImageView;

@property (strong, nonatomic) IBOutlet UIView *dottedLineView;



@end

NS_ASSUME_NONNULL_END
