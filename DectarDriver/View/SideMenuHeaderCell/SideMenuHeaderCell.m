//
//  SideMenuHeaderCell.m
//  RideHubDriver
//
//  Created by nagaraj  kumar on 14/11/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import "SideMenuHeaderCell.h"

@implementation SideMenuHeaderCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.profile_imageView.layer.cornerRadius = self.profile_imageView.frame.size.height / 2;
    self.profile_imageView.clipsToBounds = YES;
    
//    self.profile_imageView.layer.borderWidth = 3;
//    self.profile_imageView.layer.borderColor = [UIColor whiteColor].CGColor;
//
    self.edit_ImageView.layer.cornerRadius = self.edit_ImageView.frame.size.height / 2;
    self.edit_ImageView.clipsToBounds = YES;
    
//    self.edit_ImageView.layer.borderWidth = 3;
//    self.edit_ImageView.layer.borderColor = [UIColor whiteColor].CGColor;

    self.dottedLineView.layer.masksToBounds = YES;
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.lineDashPattern = @[@3,@5];
    yourViewBorder.frame = self.dottedLineView.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.dottedLineView.bounds].CGPath;
    [self.dottedLineView.layer addSublayer:yourViewBorder];
    
    
  
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
