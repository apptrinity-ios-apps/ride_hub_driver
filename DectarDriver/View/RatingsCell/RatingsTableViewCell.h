//
//  RatingsTableViewCell.h
//  DectarDriver
//
//  Created by Casperon Technologies on 9/3/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theme.h"
#import "Constant.h"
#import "TPFloatRatingView.h"
#import "RatingsRecords.h"

@protocol RatingDelegate<NSObject>
-(void)ratingSelected:(NSIndexPath *)index withValue:(float )rateValue;
@end

@interface RatingsTableViewCell : UITableViewCell<TPFloatRatingViewDelegate>
@property(strong,nonatomic)id<RatingDelegate> delegate;
@property(strong,nonatomic)NSIndexPath * indexpath;
@property (weak, nonatomic) IBOutlet UILabel *reasonLbl;
@property (weak, nonatomic) IBOutlet TPFloatRatingView *ratingView;

@property (weak, nonatomic) IBOutlet UIButton *star1;
@property (weak, nonatomic) IBOutlet UIButton *star2;
@property (weak, nonatomic) IBOutlet UIButton *star3;
@property (weak, nonatomic) IBOutlet UIButton *star4;
@property (weak, nonatomic) IBOutlet UIButton *star5;

@property (weak, nonatomic) IBOutlet UIButton *approchStar1;
@property (weak, nonatomic) IBOutlet UIButton *approchStar2;
@property (weak, nonatomic) IBOutlet UIButton *approchStar3;
@property (weak, nonatomic) IBOutlet UIButton *approchStar4;
@property (weak, nonatomic) IBOutlet UIButton *approchStar5;




-(void)setRaingRecs:(RatingsRecords *)recs;
@end
