//
//  InviteFriendsViewController.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+REFrostedViewController.h"
#import "Theme.h"
#import "RootBaseViewController.h"
#import "ReferPoints_Cell.h"
#import "UrlHandler.h"

@interface InviteFriendsViewController : RootBaseViewController<UIActionSheetDelegate>
+ (instancetype) controller;

@property (weak, nonatomic) IBOutlet UILabel *message_Lbl;

@property (weak, nonatomic) IBOutlet UIView *shareView;

@property (weak, nonatomic) IBOutlet UILabel *shareHeadingLBL;
@property (weak, nonatomic) IBOutlet UILabel *inviteCodeLBL;
- (IBAction)inviteAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *inviteBtn;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *dotLine;






@end
