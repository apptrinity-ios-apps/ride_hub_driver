//
//  InviteFriendsViewController.m
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "Theme.h"
#import "RootBaseViewController.h"
#import "ReferPoints_Cell.h"
#import "UrlHandler.h"

@interface InviteFriendsViewController ()


{
    NSString * Earn_Amount;
}
@end

@implementation InviteFriendsViewController

@synthesize dotLine,inviteBtn,inviteCodeLBL;

+ (instancetype) controller{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"ReferPointsViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
   
   // [self roundCornersOnView:_shareView onTopLeft:YES topRight:YES bottomLeft:YES bottomRight:YES radius:10.0f];
  //  [self roundCornersOnView:_shareHeadingLBL onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10.0f];

    
    self.inviteCodeLBL.clipsToBounds = YES;
    self.inviteCodeLBL.layer.cornerRadius = self.inviteCodeLBL.frame.size.height/2;
    
    self.inviteBtn.clipsToBounds = YES;
      self.inviteBtn.layer.cornerRadius = self.inviteBtn.frame.size.height/2;
    
    
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    inviteCodeLBL.layer.cornerRadius = inviteCodeLBL.frame.size.height/2;
    inviteCodeLBL.layer.borderWidth = 1;
    inviteCodeLBL.layer.borderColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    
    [self setShadow:inviteBtn];
    
  //  _shareView.backgroundColor = [UIColor whiteColor];
    [self InviteAndEarnApi];
    
    
   // NSString * str = @"SEND YOUR FRIENDS FREE RIDES Share the Ride Hub love and give friends free rides to try Ride. Worth up $";
   
    
    [self AttributedTextInUILabelWithGreenText:@"100" withYellowText:@"" withBlueBoldText:@""];
    
    
}
-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)AttributedTextInUILabelWithGreenText:(NSString *)greenText withYellowText:(NSString *) yellowText withBlueBoldText:(NSString *)blueBoldText
{
    NSString *text = [NSString stringWithFormat:@"Share the Ride Hub love and give friends free rides to try Ride. Worth up $100 each !",greenText,blueBoldText];
    
    //Check If attributed text is unsupported (below iOS6+)
    if (![self.message_Lbl respondsToSelector:@selector(setAttributedText:)]) {
        self.message_Lbl.text = text;
    }
    // If attributed text is available
    else {
        // Define general attributes like color and fonts for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: self.message_Lbl.textColor,
                                  NSFontAttributeName: self.message_Lbl.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // green text attributes
        UIColor *greenColor = [UIColor colorWithRed:255/255.0f green:159/255.0f blue:27/255.0f alpha:1.0f];
        NSRange greenTextRange = [text rangeOfString:greenText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:greenColor}
                                range:greenTextRange];
        
        // yellow text attributes
        UIColor *yellowColor = [UIColor colorWithRed:38/255.0f green:180/255.0f blue:229/255.0f alpha:1.0f];
        NSRange yellowTextRange = [text rangeOfString:greenText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:yellowColor}
                                range:yellowTextRange];
        
        // blue and bold text attributes
        UIColor *blueColor = [UIColor colorWithRed:255/255.0f green:159/255.0f blue:27/255.0f alpha:1.0f];
        UIFont *boldFont = [UIFont boldSystemFontOfSize:self.message_Lbl.font.pointSize];
        NSRange blueBoldTextRange = [text rangeOfString:blueBoldText];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:blueColor,
                                        NSFontAttributeName:boldFont}
                                range:blueBoldTextRange];
        self.message_Lbl.attributedText = attributedText;
        
    }


}


-(NSDictionary *)setParametersReferPoints{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId
                                  };
    return dictForuser;
}

-(void)InviteAndEarnApi{
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web InviteAndEarn:[self setParametersReferPoints]
             success:^(NSMutableDictionary *responseDictionary)
     {
         
         NSLog(@"%@",responseDictionary);
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             NSString * statusStr =[Theme checkNullValue:[responseDictionary objectForKey:@"status"]];
             if ([statusStr isEqualToString:@"1"]) {
                 
                 self.inviteCodeLBL.text = [[[responseDictionary objectForKey:@"response"] objectForKey:@"details"] objectForKey:@"referral_code"];
                  Earn_Amount = [[[responseDictionary objectForKey:@"response"] objectForKey:@"details"] objectForKey:@"your_earn_amount"];
                  [self AttributedTextInUILabelWithGreenText:[NSString stringWithFormat:@"$%@",Earn_Amount] withYellowText:@"" withBlueBoldText:@"FREE RIDES"];
                 
             }else{
                 
             }
             
         }else{
             
             // [self.view makeToast:@"Some error occured"];
         }
     }
             failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:@"No internet Connection"];
         
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br)
    {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        //  maskLayer.frame = roundedView.bounds;   /// p333
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}
- (IBAction)inviteAction:(id)sender {
    
    
    NSString * message = [NSString stringWithFormat:@"I have an RideHub Driver coupon worth $%@ for you. Sign up with mu code %@  to ride free. Enjoy! https://itunes.apple.com/us/app/ridehubdriver/id1361567934",Earn_Amount,inviteCodeLBL.text];
    NSArray * shareItems = @[message];
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
    
    
    
    
}
- (IBAction)backAction:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
    
}
@end
