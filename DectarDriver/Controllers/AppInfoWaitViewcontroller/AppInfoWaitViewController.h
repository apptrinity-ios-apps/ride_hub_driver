//
//  AppInfoWaitViewController.h
//  DectarDriver
//
//  Created by Aravind Natarajan on 27/04/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Theme.h"
#import "JJLocationManager.h"



@interface AppInfoWaitViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UILabel *headerLbl;
@property (strong, nonatomic) IBOutlet UILabel *messageLbl;

@end
