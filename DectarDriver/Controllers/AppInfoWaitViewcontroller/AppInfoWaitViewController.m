//
//  AppInfoWaitViewController.m
//  DectarDriver
//
//  Created by Aravind Natarajan on 27/04/16.
//  Copyright © 2016 Casperon Technologies. All rights reserved.
//
#import "AppInfoWaitViewController.h"
#import "LanguageHandler.h"
@interface AppInfoWaitViewController ()
@end
@implementation AppInfoWaitViewController
@synthesize headerLbl,messageLbl;
@synthesize containerView;
- (void)viewDidLoad {
    [super viewDidLoad];
    containerView.layer.cornerRadius=5;
    containerView.layer.masksToBounds=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveAppInfoNotification:)
                                                 name:@"NotifForAppInfo"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getLocationInitially:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedInitially
                                               object:nil];
    
    headerLbl.text=JJLocalizedString(@"SORRY_FOR_THE_DELAY", nil);
    messageLbl.text=JJLocalizedString(@"WE_HAD_SOME_PROBLEM", nil);
    // Do any additional setup after loading the view.
}
- (void)receiveAppInfoNotification:(NSNotification *) notification
{
    
    CLLocation *location = [[JJLocationManager sharedManager] currentLocation];
    if(location==nil||location.coordinate.latitude==0){
        
    }else{
        if(self.view.window){
             [self dismissViewControllerAnimated:NO completion:nil];
        }
       
    }
    
    
}
-(void)getLocationInitially:(NSNotification*)notification
{
    CLLocation *location = [[JJLocationManager sharedManager] currentLocation];
    if(location!=nil||location.coordinate.latitude!=0){
        if([Theme hasAppDetails]){
            if(self.view.window){
                [self dismissViewControllerAnimated:NO completion:nil];
            }
        }
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
