//
//  RideUserViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/25/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "RideUserViewController.h"

@interface RideUserViewController ()
//Open Maps
@property(nonatomic, strong) MapRequestModel *model;
@property(nonatomic, assign) LocationGroup pendingLocationGroup;
@end

@implementation RideUserViewController{
    FBShimmeringView *_shimmeringView;
    GMSMarker *  marker;
    GMSMarker *  marker2;
    GMSPath *pathDrawn;
    GMSPolyline *singleLine;
    int zoompoint;
    int wpoint;
     JJLocationManager * jjLocManager;
}
@synthesize mapView,buttonContentView,arrivedBtn,headerLbl,optionBtn,addressView,riderNameLbl,addressLbl,Camera,GoogleMap,timeLbl,objRiderRecords,userImageView,riderLattitude,riderLongitude,custIndicatorView,isFirstUpdateLoc,marker3,wayArray,isDriverDeviatesStatusNum,isMapZoomed;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(jjLocManager==nil){
        jjLocManager=[JJLocationManager sharedManager];
    }
    
    
    CAShapeLayer *yourViewBorder4 = [CAShapeLayer layer];
    yourViewBorder4.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder4.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder4.lineDashPattern = @[@6,@3];
    yourViewBorder4.frame = self.dottedView.bounds;
    yourViewBorder4.path = [UIBezierPath bezierPathWithRect:self.dottedView.bounds].CGPath;
    [self.dottedView.layer addSublayer:yourViewBorder4];
    
    
    
    self.cancelrideBtn.layer.cornerRadius = self.cancelrideBtn.frame.size.height/2 ;
    self.cancelrideBtn.clipsToBounds = YES;
    
    self.popupOkBtn.layer.cornerRadius = self.popupOkBtn.frame.size.height/2 ;
    self.popupOkBtn.clipsToBounds = YES;
    
    self.noteForRiderPopUpview.layer.cornerRadius = 10;
    self.noteForRiderPopUpview.clipsToBounds = YES;
    
    self.popUpBackBtn.hidden = YES;
    self.noteForRiderPopUpview.hidden = YES;
    
    
    
    
    
    
    [self setShadow:arrivedBtn];
    
    isDriverDeviatesStatusNum=0;
    zoompoint=15;
    wayArray=[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kUserCancelledDrive
                           	                    object:nil];
    [self setFontAndColor];
    [self setUserDatas];
   // [self setGlowBtn];
    marker3=[[GMSMarker alloc]init];
    self.points=[[NSMutableArray alloc]init];
   
   // [self showActivityIndicator:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLocation:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedName
                                               object:nil];
  
    // Do any additional setup after loading the view.
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
   zoompoint=position.zoom;
    // handle you zoom related logic
}
-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    if(gesture==YES){
        isMapZoomed=YES;
    }else{
        isMapZoomed=NO;
    }
}

-(void)setGlowBtn{
    
    _shimmeringView = [[FBShimmeringView alloc] init];
    _shimmeringView.shimmering = NO;
    _shimmeringView.shimmeringBeginFadeDuration = 0.2;
     _shimmeringView.shimmeringOpacity = 1;
       [self.view addSubview:_shimmeringView];
    
    arrivedBtn.delegate=self;
    [arrivedBtn setTitle:JJLocalizedString(@"SLIDE_TO_ARRIVED", nil) forState:UIControlStateNormal];
    _shimmeringView.contentView = arrivedBtn;
    CGRect shimmeringFrame = arrivedBtn.frame;
    shimmeringFrame.origin.y= self.view.frame.size.height-70;
    shimmeringFrame.origin.x= self.view.center.x -100 ;
    shimmeringFrame.size.height= 50;
     shimmeringFrame.size.width=self.view.frame.size.width-44;
    _shimmeringView.frame = shimmeringFrame;
    
    
}

- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"ride_cancelled"]){
            NSString * rideCancelled=JJLocalizedString(@"Rider_Cancelled_the_ride", nil);
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Sorry !!" message:rideCancelled delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=100;
            [alert show];
        }
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag==100){
        for (UIViewController *controller1 in self.navigationController.viewControllers) {
            
            if ([controller1 isKindOfClass:[HomeViewController class]]) {
                
                [self.navigationController popToViewController:controller1
                                                      animated:YES];
                break;
            }
        }
    }
  else  if(alertView.tag==101){
      if (buttonIndex==[alertView cancelButtonIndex]) {
            
        }else {
            [self drawRoadRouteBetweenTwoPoints:jjLocManager.currentLocation];
        }
    }
   
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)setUserDatas{
    headerLbl.text=objRiderRecords.userName;
    
    addressLbl.text=objRiderRecords.userLocation;
    timeLbl.text=objRiderRecords.userTime;
    
   
    riderLattitude=[objRiderRecords.userLattitude floatValue];
    riderLongitude=[objRiderRecords.userLongitude floatValue];
    
}
-(void)drawRoadRouteBetweenTwoPoints:(CLLocation *)currentLocation{
    
    self.openInMapsBtn.hidden=YES;
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getGoogleRoute:[self setParametersDrawLocation:currentLocation]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         @try {
             NSArray * arr=[responseDictionary objectForKey:@"routes"];
             if([arr count]>0){
                 [self stopActivityIndicator];
                 
                 pathDrawn =[GMSPath pathFromEncodedPath:responseDictionary[@"routes"][0][@"overview_polyline"][@"points"]];
                 singleLine = [GMSPolyline polylineWithPath:pathDrawn];
                 singleLine.strokeWidth = 7;
               //  singleLine.strokeColor = SetMapPolyLineColor;
                 
                 GMSStrokeStyle *blueToGreen = [GMSStrokeStyle gradientFromColor:SkyBlueColor toColor:GreenColor];
                 singleLine.spans = @[[GMSStyleSpan spanWithStyle:blueToGreen]];
                 
                 singleLine.map = self.GoogleMap;
                 GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathDrawn];
                 GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:50.0f];
                 //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
                 [GoogleMap animateWithCameraUpdate:update];
             }else{
                 [self stopActivityIndicator];
                 // [self drawRoadRouteBetweenTwoPoints:location];
                 [self.view makeToast:@"can't_find_route"];
             }
         }
         @catch (NSException *exception) {
             
         }
        
        
 
         
     }
                    failure:^(NSError *error)
     {
          [self stopActivityIndicator];
          if(self.view.window){
              NSString *noroute=JJLocalizedString(@"can't_find_route", nil);
         UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[Theme project_getAppName] message:noroute delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
         alert.tag=101;
         [alert show];
          }
          [self.view makeToast:@"can't_find_route"];
         
     }];
}

-(NSDictionary *)setParametersDrawLocation:(CLLocation *)loc{

    NSDictionary *dictForuser = @{
                                  @"origin":[NSString stringWithFormat:@"%f,%f",loc.coordinate.latitude,loc.coordinate.longitude],
                                  @"destination":[NSString stringWithFormat:@"%f,%f",riderLattitude,riderLongitude],
                                  @"sensor":@"true",
                                   @"key":kGoogleServerKey
                                  };
    return dictForuser;
}




-(void)viewWillAppear:(BOOL)animated{
   
    
    
    if ([objRiderRecords.userNote  isEqual: @""] || [objRiderRecords.userNote  isEqual: @"0"]){
        
        self.popupOkBtn.hidden = YES;
        self.noteForRiderPopUpview.hidden = YES;
        self.popUpBackBtn.hidden = YES;
        
        
    }else{
        
        self.popupOkBtn.hidden = NO;
        self.noteForRiderPopUpview.hidden = NO;
        self.popUpBackBtn.hidden = NO;
        self.noteForRider_Lbl.text = objRiderRecords.userNote;
        
        
    }
   
    
    if(isFirstUpdateLoc==NO){
        [self loadGoogleMap:jjLocManager.currentLocation];
        [self drawRoadRouteBetweenTwoPoints:jjLocManager.currentLocation];
        [self loadMovingMarker];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
   
}
-(void)changeLocation:(NSNotification*)notification
{
    if(self.view.window){
        @try {
            [NSThread detachNewThreadSelector:@selector(loadMovingMarker) toTarget:self withObject:nil];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception in LiveTracking..");
        }
        
    }
}
-(void)loadMovingMarker{
    if(self.view.window){
        if(isMapZoomed==NO){
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:jjLocManager.currentLocation.coordinate.latitude
                                                                    longitude:jjLocManager.currentLocation.coordinate.longitude
                                                                         zoom:zoompoint];
//                                                                      bearing:90
//                                                                 viewingAngle:0];
            
            //New Changes by Naveen bearing and viewangle
            
            
            [GoogleMap animateToCameraPosition:camera];
        }
        
        
            [CATransaction begin];
            [CATransaction setAnimationDuration:2.0];
          //  marker3.rotation = [Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocationBefore5etres];
           
        
        marker3.groundAnchor = CGPointMake(0.5, 0.5);
        marker3.position = CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude,jjLocManager.currentLocation.coordinate.longitude);
        
        
        float angel = [Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocation];
        
        
         marker3.rotation = angel;
         marker3.groundAnchor = CGPointMake(0.5, 0.5);
        [CATransaction commit];
        
        marker3.icon = [UIImage imageNamed:@"cartopview"];
        NSString * youhere=JJLocalizedString(@"You_are_Here", nil);
        marker3.snippet = [NSString stringWithFormat:@"%@",youhere];
        marker3.appearAnimation = kGMSMarkerAnimationPop;
        marker3.map = GoogleMap;
        if(self.view.window){
            AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            NSString * str=[Theme checkNullValue:[NSString stringWithFormat:@"%f",[Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocation]]];
             [appdelegate xmppUpdateLoc:jjLocManager.currentLocation.coordinate withReceiver:objRiderRecords.UserId withRideId:objRiderRecords.RideId withBearing:str];
          
        }
    }
}




-(void)updateLoc:(CLLocation *)loc{
    
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web UpdateUserLocation:[self setParametersUpdateLocation:loc]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             
             
         }else{
             
           //  [self.view makeToast:kErrorMessage];
         }
     }
                    failure:^(NSError *error)
     {
       //  [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersUpdateLocation:(CLLocation *)loc{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"latitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]],
                                  @"longitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]],
                                  @"ride_id":objRiderRecords.RideId
                                  };
    return dictForuser;
}

-(void)viewWillDisappear:(BOOL)animated{
    //[self.locationManager stopUpdatingLocation];
}
-(void)loadGoogleMap:(CLLocation *)loc{
    isFirstUpdateLoc=YES;
    if(TARGET_IPHONE_SIMULATOR)
    {
        Camera = [GMSCameraPosition cameraWithLatitude:13.0827
                                             longitude:80.2707
                                                  zoom:15];
    }else{
        Camera = [GMSCameraPosition cameraWithLatitude:loc.coordinate.latitude
                                             longitude:loc.coordinate.longitude
                                                  zoom:zoompoint];
//                                               bearing:90
//                                          viewingAngle:0];
        
        // New Change bearing and viewangle
        
    }
    
    GoogleMap = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, mapView.frame.size.height+200) camera:Camera];
   // GoogleMap.myLocationEnabled = YES;
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle-silver" withExtension:@"json"];
    NSError *error;
    
    //     Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    GoogleMap.mapStyle = style;
    
    marker=[[GMSMarker alloc]init];
    marker.position=Camera.target;
    NSString * youhere=JJLocalizedString(@"Starting Place", nil);
    marker.snippet =youhere;
    marker.icon = [UIImage imageNamed:@"StartingPin"];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = GoogleMap;
    
    
    marker2=[[GMSMarker alloc]init];
    marker2.position=CLLocationCoordinate2DMake(riderLattitude,riderLongitude);;
    marker2.snippet =headerLbl.text;
    marker2.icon = [UIImage imageNamed:@"UserLocationImg"];
    marker2.appearAnimation = kGMSMarkerAnimationPop;
    marker2.map = GoogleMap;
    GoogleMap.userInteractionEnabled=YES;
    GoogleMap.delegate = self;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    
    marker3.position = CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude,jjLocManager.currentLocation.coordinate.longitude);
    
    float angel = [Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocation];
    
   marker3.rotation = angel;
    
    marker3.icon = [UIImage imageNamed:@"cartopview"];
    NSString * youhere1=JJLocalizedString(@"You_are_Here", nil);
    marker3.snippet = [NSString stringWithFormat:@"%@",youhere1];
    marker3.appearAnimation = kGMSMarkerAnimationPop;
    marker3.map = GoogleMap;
     marker3.groundAnchor = CGPointMake(0.5, 0.5);

    [CATransaction commit];
    [mapView addSubview:GoogleMap];
    
}

-(void)setFontAndColor{
   
  //  [arrivedBtn setBackgroundColor:setRedColor];
    arrivedBtn.layer.cornerRadius=arrivedBtn.frame.size.height/2;
    arrivedBtn.layer.masksToBounds=YES;
    userImageView.layer.cornerRadius=userImageView.frame.size.width/2;
    userImageView.layer.masksToBounds=YES;
//    headerLbl=[Theme setHeaderFontForLabel:headerLbl];
    addressLbl=[Theme setNormalSmallFontForLabel:addressLbl];
    timeLbl=[Theme setNormalSmallFontForLabel:timeLbl];
  //  riderNameLbl=[Theme setBoldFontForLabel:riderNameLbl];
}
-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
     [custIndicatorView stopAnimating];
     custIndicatorView=nil;
     [self.container removeFromSuperview];
}

- (IBAction)chating_btnAction:(id)sender {
    
    
    
    chatviewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"chatviewController"];
    
    AppDelegate* delegate = [UIApplication sharedApplication].delegate;
    delegate.user_id = objRiderRecords.UserId;
    delegate.user_name = objRiderRecords.userName;
    delegate.user_email = objRiderRecords.userEmail;
    delegate.user_image = objRiderRecords.userImage;
    
    
    
    [self.navigationController pushViewController:objRatingsVc animated:YES];
    
    
    
}

- (IBAction)popUpBackBtnAction:(id)sender {
    
    self.popupOkBtn.hidden = YES;
    self.noteForRiderPopUpview.hidden = YES;
    self.popUpBackBtn.hidden = YES;
    
    
}

- (IBAction)didClickRefreshBtn:(id)sender {
    isMapZoomed=NO;
    [jjLocManager refreshDriverCurrentLocation];
    if(pathDrawn==nil){
        [self drawRoadRouteBetweenTwoPoints:jjLocManager.currentLocation];
    }
}

- (IBAction)didClickCallBtn:(id)sender {
    NSString * phoneNum=[NSString stringWithFormat:@"tel:%@",objRiderRecords.userPhoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];
}

- (IBAction)didClickArrivedBtn:(id)sender {
    
    UIButton * btn=sender;
    btn.userInteractionEnabled=NO;
     [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web DriverArrLocation:[self setParametersDriverArrived]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             TripViewController * objTripVc=[self.storyboard instantiateViewControllerWithIdentifier:@"TripVCSID"];
             [objTripVc setObjRiderRecs:objRiderRecords];
             [self.navigationController pushViewController:objTripVc animated:YES];
         }else{
              btn.userInteractionEnabled=YES;
             [self.view makeToast:kErrorMessage];
         }
     }
                    failure:^(NSError *error)
     {
         [self stopActivityIndicator];
          btn.userInteractionEnabled=YES;
         [self.view makeToast:kErrorMessage];
         
     }];
    
    
    
}
- (void) slideActive{
    @try {
        arrivedBtn.userInteractionEnabled=NO;
        [self showActivityIndicator:YES];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web DriverArrLocation:[self setParametersDriverArrived]
                       success:^(NSMutableDictionary *responseDictionary)
         {
             [self stopActivityIndicator];
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 TripViewController * objTripVc=[self.storyboard instantiateViewControllerWithIdentifier:@"TripVCSID"];
                 [objTripVc setObjRiderRecs:objRiderRecords];
                 [self.navigationController pushViewController:objTripVc animated:YES];
             }else{
                 arrivedBtn.userInteractionEnabled=YES;
                 [self.view makeToast:kErrorMessage];
             }
         }
                       failure:^(NSError *error)
         {
             [self stopActivityIndicator];
             arrivedBtn.userInteractionEnabled=YES;
             [self.view makeToast:kErrorMessage];
             
         }];
    }
    @catch (NSException *exception) {
        
    }
}

-(NSDictionary *)setParametersDriverArrived{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":objRiderRecords.RideId
                                  };
    return dictForuser;
}

- (IBAction)didClickOptionBtn:(id)sender {
    
    InfoViewController * objInfoVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InfoVCSID"];
    [objInfoVc setRideId:objRiderRecords.RideId];
    [self.navigationController pushViewController:objInfoVc animated:YES];
    
}

- (IBAction)cancelRideAction:(id)sender {
    
    
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    CancelRideViewController * objCancelVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CancelVCSID"];
    [objCancelVC setRideId:objRiderRecords.RideId];
    [self.navigationController pushViewController:objCancelVC animated:YES];
    
}

- (IBAction)didClickMoveToGoogleMapsBtn:(id)sender {
    //Open maps
    if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
        
    }
    [self openDirectionsInGoogleMaps];
}
//Open Maps
- (void)openDirectionsInGoogleMaps {
    GoogleDirectionsDefinition *directionsDefinition = [[GoogleDirectionsDefinition alloc] init];
    if (self.model.startCurrentLocation) {
        directionsDefinition.startingPoint = nil;
    } else {
        GoogleDirectionsWaypoint *startingPoint = [[GoogleDirectionsWaypoint alloc] init];
        startingPoint.queryString = self.model.startQueryString;
        startingPoint.location = self.model.startLocation;
        directionsDefinition.startingPoint = startingPoint;
    }
    if (self.model.destinationCurrentLocation) {
        directionsDefinition.destinationPoint = nil;
    } else {
        GoogleDirectionsWaypoint *destination = [[GoogleDirectionsWaypoint alloc] init];
        destination.queryString = self.model.destinationQueryString;
        destination.location = self.model.desstinationLocation;
        directionsDefinition.destinationPoint = destination;
    }
    
    directionsDefinition.travelMode = [self travelModeAsGoogleMapsEnum:self.model.travelMode];
    [[OpenInGoogleMapsController sharedInstance] openDirections:directionsDefinition];
}
// Convert our app's "travel mode" to the official Google Enum
- (GoogleMapsTravelMode)travelModeAsGoogleMapsEnum:(TravelMode)appTravelMode {
    switch (appTravelMode) {
        case kTravelModeBicycling:
            return kGoogleMapsTravelModeBiking;
        case kTravelModeDriving:
            return kGoogleMapsTravelModeDriving;
        case kTravelModePublicTransit:
            return kGoogleMapsTravelModeTransit;
        case kTravelModeWalking:
            return kGoogleMapsTravelModeWalking;
        case kTravelModeNotSpecified:
            return 0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


////////////////////////////////////////////////////////////////////////////////////////////

/*-(void)UpdateAlternativeroutBetweenTwoPoints{
    NSString *joinedString=@"";
    if(wayArray.count>0){
        joinedString = [wayArray componentsJoinedByString:@"|"];
    }
    wpoint++;
    NSString * wayscount=JJLocalizedString(@"ways_count", nil);
    
    [self.view makeToast:[NSString stringWithFormat:@"%d %@",wpoint,wayscount ]];
    
    NSString *urlString = [NSString stringWithFormat:
                           @"%@?origin=%f,%f&destination=%f,%f&waypoints=%@&sensor=true",
                           @"https://maps.googleapis.com/maps/api/directions/json",
                           startLocationDriver.coordinate.latitude,
                           startLocationDriver.coordinate.longitude,
                           riderLattitude,
                           riderLongitude,joinedString];
    NSURL *directionsURL = [NSURL URLWithString:urlString];
    
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:directionsURL];
    [request startSynchronous];
    NSError *error = [request error];
    [self stopActivityIndicator];
    if (!error) {
        NSString *response = [request responseString];
        NSLog(@" %@",response);
        NSDictionary *json =[NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:&error];
        NSLog(@"%@",json);
        NSArray * arr=[json objectForKey:@"routes"];
        if([arr count]>0){
            if(singleLine!=nil){
                [singleLine setMap:nil];
            }
            pathDrawn =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
            singleLine = [GMSPolyline polylineWithPath:pathDrawn];
            singleLine.strokeWidth = 10;
            singleLine.strokeColor = SetThemeColor;
            singleLine.map = self.GoogleMap;
            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathDrawn];
            GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:50.0f];
            //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
            [GoogleMap animateWithCameraUpdate:update];
            [_locationManager stopUpdatingLocation];
        }else{
          
        }
        
        // }
        
    }
}*/







@end
