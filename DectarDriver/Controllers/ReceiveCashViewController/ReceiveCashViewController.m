//
//  ReceiveCashViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/2/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "ReceiveCashViewController.h"

@interface ReceiveCashViewController ()

@end

@implementation ReceiveCashViewController
@synthesize fareAmt,receiveCashHeaderLbl,fareLbl,receiveBtn,custIndicatorView,RideId,backBtn,textLbl,dotLine1,dotLine2,dotLine3,hideView,popUpSuccesView,successMesgLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFont];
    fareLbl.text=fareAmt;
    
    hideView.hidden = YES;
    popUpSuccesView.hidden = YES;
    popUpSuccesView.layer.cornerRadius = 5;
    
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine1.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine1.bounds].CGPath;
    [self.dotLine1.layer addSublayer:yourViewBorder2];
    
    CAShapeLayer *yourViewBorder3 = [CAShapeLayer layer];
    yourViewBorder3.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder3.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder3.lineDashPattern = @[@3,@3];
    yourViewBorder3.frame = self.dotLine2.bounds;
    yourViewBorder3.path = [UIBezierPath bezierPathWithRect:self.dotLine2.bounds].CGPath;
    [self.dotLine2.layer addSublayer:yourViewBorder3];
    
    CAShapeLayer *yourViewBorder4 = [CAShapeLayer layer];
    yourViewBorder4.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder4.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder4.lineDashPattern = @[@3,@3];
    yourViewBorder4.frame = self.dotLine3.bounds;
    yourViewBorder4.path = [UIBezierPath bezierPathWithRect:self.dotLine3.bounds].CGPath;
    [self.dotLine3.layer addSublayer:yourViewBorder4];
    
    [self setShadow:receiveBtn];
    // Do any additional setup after loading the view.
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

-(void)setFont{
//    receiveCashHeaderLbl=[Theme setHeaderFontForLabel:receiveCashHeaderLbl];
    fareLbl=[Theme setLargeBoldFontForLabel:fareLbl];
    textLbl=[Theme setHeaderFontForLabel:textLbl];
    receiveBtn=[Theme setBoldFontForButton:receiveBtn];
    
    [receiveCashHeaderLbl setText:JJLocalizedString(@"Receive_Cash", nil)];
    [receiveBtn setTitle:JJLocalizedString(@"Received", nil) forState:UIControlStateNormal];
    [textLbl setText:JJLocalizedString(@"Please_collect_the_below", nil)];
    
    
    receiveBtn.layer.cornerRadius=receiveBtn.frame.size.height/2;
    receiveBtn.layer.masksToBounds=YES;
}
-(void)viewDidAppear:(BOOL)animated{
    if([[self backViewController]isKindOfClass:[PaymentWaitingViewController class]]){
        backBtn.hidden=YES;
    }else{
         backBtn.hidden=NO;
    }
    
}
- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (IBAction)didClickReceiveBtn:(id)sender {
    
    
    backBtn.userInteractionEnabled=NO;
    receiveBtn.userInteractionEnabled=NO;
    [self.view makeToast:@"Please_wait_until_transaction"];
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web receiveCash:[self setParametersForOtp]
        success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             [self performSelector:@selector(moveToHomeVc) withObject:self afterDelay:1];
             
             self.hideView.hidden = NO;
             self.popUpSuccesView.hidden = NO;
             self.successMesgLbl.text = @"Payment Received";
             
          //   [self.view makeToast:@"Payment_Received"];
         }else{
             backBtn.userInteractionEnabled=YES;
             receiveBtn.userInteractionEnabled=YES;

             [self.view makeToast:kErrorMessage];
         }
     }
        failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         backBtn.userInteractionEnabled=YES;
         receiveBtn.userInteractionEnabled=YES;
         [self.view makeToast:kErrorMessage];
         
     }];
    
    //[self performSelector:@selector(moveToHomeVc) withObject:self afterDelay:1];
   // [self.view makeToast:@"Payment_Received"];
    
}
-(void)moveToHomeVc{
    RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
    [objRatingsVc setRideid:RideId];
    [self.navigationController pushViewController:objRatingsVc animated:YES];
    
//    for (UIViewController *controller in self.navigationController.viewControllers) {
//        
//        if ([controller isKindOfClass:[HomeViewController class]]) {
//            
//            [self.navigationController popToViewController:controller
//                                                  animated:YES];
//            break;
//        }
//    }
}
-(NSDictionary *)setParametersForOtp{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    AppDelegate * appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSLog(@"dictForuser is %@",appdelegate.appdelegateRideID);
    
   
    NSDictionary *dictForuser = @{
                                  @"driver_id":[Theme checkNullValue:driverId],
                                  @"ride_id": [Theme checkNullValue:appdelegate.appdelegateRideID],
                                  @"amount":[Theme checkNullValue:fareAmt]
                                  };
   
    return dictForuser;
}
-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];





    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}

- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
