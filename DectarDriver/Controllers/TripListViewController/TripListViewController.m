//
//  TripListViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/28/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "TripListViewController.h"

@interface TripListViewController ()

{
    CALayer *bottomBorder;
    
    UIView * container;
}

@end

@implementation TripListViewController
@synthesize custIndicatorView,tripArray,tripListTableView,onRideArray,headerLbl,cancelledArray,dotLine,allView,allLbl,onrideLbl,onrideView,completeLbl,completedView,cancellLbl,cancelledView,segmentBavckView,
CompletedArray,filterSegmentControl,containerView,sortArray,isSort;
+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"TripListVCSID"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    bottomBorder = [CALayer layer];
    segmentBavckView.layer.cornerRadius = segmentBavckView.frame.size.height/2;
    segmentBavckView.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotifWhenQuit
                                               object:nil];
//    [self setFont];
//    tripListTableView.layer.cornerRadius = 10;
//    tripListTableView.layer.masksToBounds = true;
    tripListTableView.backgroundColor = [UIColor whiteColor];
    tripListTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    CGRect frame= filterSegmentControl.frame;
    filterSegmentControl.layer.cornerRadius = filterSegmentControl.frame.size.height/2;
    [filterSegmentControl setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 45)];
    filterSegmentControl.tintColor = [UIColor clearColor];
    
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"Poppins-SemiBold" size:14], UITextAttributeFont,
                                segmentTextColor, UITextAttributeTextColor, nil];
    [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    NSDictionary *highlightedAttributes = [NSDictionary
                                           dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];
    [filterSegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
    [filterSegmentControl setSelectedSegmentIndex:0];
    
    
    [self setShadow:segmentBavckView];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    

//    CALayer *bottomBorder = [CALayer layer];
//    bottomBorder.borderColor = DarkskyBlue.CGColor;
//    bottomBorder.borderWidth = 3;
//    bottomBorder.frame = CGRectMake(0, 30 , self.filterSegmentControl.frame.size.width/4, 1);
//    [self.filterSegmentControl.layer addSublayer:bottomBorder];

    
//
//    UIFont *font = [UIFont fontWithName:@"Poppins-SemiBold" size:17.0f];
//    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font
//                                                           forKey:NSFontAttributeName];
//    [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//
   // [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:DarkskyBlue} forState:UIControlStateNormal];
    
    
//   [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateNormal];
//   [filterSegmentControl setSelectedSegmentIndex:1];
//
//
//   [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateNormal];
//   [filterSegmentControl setSelectedSegmentIndex:2];
//
//
//   [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateNormal];
//   [filterSegmentControl setSelectedSegmentIndex:3];
//
//
//    [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]} forState:UIControlStateNormal];
//
    
    [self setMaskTo:allView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
    [self setMaskTo:onrideView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
    [self setMaskTo:completedView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
    [self setMaskTo:cancelledView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
    
    
    allView.layer.backgroundColor = segmentBlueColor.CGColor;
    allLbl.textColor = [UIColor whiteColor];
    onrideView.layer.backgroundColor = [UIColor clearColor].CGColor;
    onrideLbl.textColor = segmentTextColor;
    completedView.layer.backgroundColor = [UIColor clearColor].CGColor;
    completeLbl.textColor = segmentTextColor;
    cancelledView.layer.backgroundColor = [UIColor clearColor].CGColor;
    cancellLbl.textColor = segmentTextColor;

    [self getTripList];
    // Do any additional setup after loading the view.
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(5, 5);
    view.layer.shadowRadius = 5;
    view.layer.shadowOpacity = 0.3;
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(17.0, 17.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    shape.frame = view.bounds;
    view.layer.mask = shape;
    view.layer.masksToBounds=YES;
}



- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
            NSString * rideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
            NSString * CurrId=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict objectForKey:@"key4"]]];
            NSString * fareStr=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            NSString * fareAmt=[NSString stringWithFormat:@"%@ %@",CurrId,fareStr];
            
            [self stopActivityIndicator];
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:rideId];
            [objReceiveCashVC setFareAmt:fareAmt];
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


//-(void)setFont{
//   // headerLbl=[Theme setHeaderFontForLabel:headerLbl];
//   // [headerLbl setText:JJLocalizedString(@"Trip Summary", nil)];
//   // [headerLbl setFont:[UIFont systemFontOfSize:25]];
//    [filterSegmentControl setTitle:JJLocalizedString(@"All", nil) forSegmentAtIndex:0];
//    [filterSegmentControl setTitle:JJLocalizedString(@"Onride", nil) forSegmentAtIndex:1];
//    [filterSegmentControl setTitle:JJLocalizedString(@"Completed", nil) forSegmentAtIndex:2];
//
//}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];   //it hides
}

-(void)getTripList{
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getDriverTripList:[self setParametersDriverTripList]
                   success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             tripArray=[[NSMutableArray alloc]init];
             CompletedArray=[[NSMutableArray alloc]init];
             onRideArray=[[NSMutableArray alloc]init];
             cancelledArray=[[NSMutableArray alloc]init];
             for (NSDictionary * reasonDict in responseDictionary[@"response"][@"rides"]) {
                 TripRecords * objTripRecs=[[TripRecords alloc]init];
                 objTripRecs.tripId=[Theme checkNullValue:reasonDict[@"ride_id"]];
                 objTripRecs.tripTime=[Theme checkNullValue:reasonDict[@"ride_time"]];
                 objTripRecs.tripDate=[Theme checkNullValue:reasonDict[@"ride_date"]];
                 objTripRecs.tripLocation=[Theme checkNullValue:reasonDict[@"pickup"]];
                 objTripRecs.tripStatus=[Theme checkNullValue:reasonDict[@"group"]];
                 objTripRecs.tripeOrigDate=[Theme checkNullValue:reasonDict[@"datetime"]];
                  objTripRecs.droplocation=[Theme checkNullValue:reasonDict[@"drop"]];

                 if([objTripRecs.tripStatus isEqualToString:@"onride"]||[objTripRecs.tripStatus isEqualToString:@"Pickuped"]){
                     [onRideArray addObject:objTripRecs];
                 }else if ([objTripRecs.tripStatus isEqualToString:@"completed"]){
                     [CompletedArray addObject:objTripRecs];
                     NSLog(@"%@",objTripRecs.tripStatus);
                     // [[JJLocationManager sharedManager] stopLocationUpdates];
                 }
                 else if ([objTripRecs.tripStatus isEqualToString:@"cancelled"]) {

                      [cancelledArray addObject:objTripRecs];
                    // NSLog(@"%@",objTripRecs.tripStatus);

                 }
                 [tripArray addObject:objTripRecs];
             }
             if([tripArray count]==0){
                 [self.view makeToast:@"No_Records_Found"];
             }
             [tripListTableView reloadData];
         }else{

             [self.view makeToast:kErrorMessage];
         }
     }
                   failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];

     }];

    
}



-(NSDictionary *)setParametersDriverTripList{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"trip_type":@"all",
                                  };
    return dictForuser;
}



-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
        
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];
//
//
        
        
        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
       
         webView.userInteractionEnabled = NO;
         webView.layer.cornerRadius = webView.frame.size.height/2;
         webView.layer.masksToBounds = YES;
         webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];
        

        
        
        
        
    }
}
-(void)stopActivityIndicator{
    
    
  //  [custIndicatorView stopAnimating];
    custIndicatorView=nil;
    
    [self.container removeFromSuperview];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rowCount=0;
    if(filterSegmentControl.selectedSegmentIndex==0){
        
        if(isSort==YES){
            rowCount=[sortArray count];
            

            
        }else{
            rowCount=[tripArray count];
        }
        
    }else if (filterSegmentControl.selectedSegmentIndex==1){
        if(isSort==YES){
            rowCount=[sortArray count];
        }else{
            rowCount=[onRideArray count];
        }
        
    }else if (filterSegmentControl.selectedSegmentIndex==2){
        if(isSort==YES){
            rowCount=[sortArray count];
        }else{
            rowCount=[CompletedArray count];
        }
    }else if (filterSegmentControl.selectedSegmentIndex==3){
        if(isSort==YES){
            rowCount=[sortArray count];
        }else{
            rowCount=[cancelledArray count];
        }
    }
    
    
    if(rowCount==0){
        _noRecsImg.hidden=NO;
    }else{
        _noRecsImg.hidden=YES;
    }
    return rowCount;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TripListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripListIdentifier"];
    if (cell == nil) {
        cell = [[TripListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:@"TripListIdentifier"];
    }
    
    
   
  //  cell.locationLbl.textColor = SetThemeColor;
   // cell.timeLbl.textColor = SetThemeColor;
   
    TripRecords * objTripRecs=[[TripRecords alloc]init];
    if(filterSegmentControl.selectedSegmentIndex==0){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
            NSLog(@"%@",sortArray);
            cell.rideStatusLbl.hidden = NO;
            
        }else{
            objTripRecs=[tripArray objectAtIndex:indexPath.row];
            NSLog(@"%@",tripArray);
 cell.rideStatusLbl.hidden = NO;
        }
    }else if (filterSegmentControl.selectedSegmentIndex==1){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
            
            cell.rideStatusLbl.hidden = YES;
            
        }else{
            objTripRecs=[onRideArray objectAtIndex:indexPath.row];
             cell.rideStatusLbl.hidden = YES;
        }
        
    }else if (filterSegmentControl.selectedSegmentIndex==2){
        if(isSort==YES){
             cell.rideStatusLbl.hidden = YES;
            
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[CompletedArray objectAtIndex:indexPath.row];
             cell.rideStatusLbl.hidden = YES;
        }
    }else if (filterSegmentControl.selectedSegmentIndex==3){
        if(isSort==YES){
             cell.rideStatusLbl.hidden = YES;
            
           
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[cancelledArray objectAtIndex:indexPath.row];
             cell.rideStatusLbl.hidden = YES;
        }
    }
    
    [cell setDatasToTripList:objTripRecs];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.height/2;
    cell.imgView.layer.masksToBounds = YES;
   // cell.imgView.layer.borderColor = DarkskyBlue.CGColor;
  //  cell.imgView.layer.borderWidth = 1;
    
    
    
    cell.img_backgroundView.layer.cornerRadius = cell.img_backgroundView.frame.size.height/2;
    cell.img_backgroundView.layer.masksToBounds = YES;
    cell.img_backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.img_backgroundView.layer.borderWidth = 1;
    
    
    return cell;
 
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TripRecords * objTripRecs=[[TripRecords alloc]init];
    if(filterSegmentControl.selectedSegmentIndex==0){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[tripArray objectAtIndex:indexPath.row];
        }
        
    }else if (filterSegmentControl.selectedSegmentIndex==1){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[onRideArray objectAtIndex:indexPath.row];
        }
        
    }else if (filterSegmentControl.selectedSegmentIndex==2){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[CompletedArray objectAtIndex:indexPath.row];
        }
    }else if (filterSegmentControl.selectedSegmentIndex==3){
        if(isSort==YES){
            objTripRecs=[sortArray objectAtIndex:indexPath.row];
        }else{
            objTripRecs=[cancelledArray objectAtIndex:indexPath.row];
        }
    }
    TripDetailViewController * objTripDetailVc=[self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailVCSID"];
    [objTripDetailVc setTripId:objTripRecs.tripId];
    [self.navigationController pushViewController:objTripDetailVc animated:YES];
}

- (IBAction)didClickMenuBtn:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

- (IBAction)didselectSegmentFilter:(id)sender {
    isSort=NO;
    
    
    UISegmentedControl * segControl=sender;
//[filterSegmentControl.layer removeFromSuperlayer];
    
    bottomBorder.borderWidth = 3;
    
//    for(int i = 0 ; i<4;i++){
//
//        if (segControl.selectedSegmentIndex == i){
//            bottomBorder.borderColor = DarkskyBlue.CGColor;
//
//        }else{
//            bottomBorder.borderColor = [UIColor whiteColor].CGColor;
//        }
//    }
    
    if(segControl.selectedSegmentIndex==0){
        
        
//        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    [UIFont fontWithName:@"Poppins-SemiBold" size:14], UITextAttributeFont,
//                                    [UIColor darkGrayColor], UITextAttributeTextColor, nil];
//        [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
        
//        NSDictionary *highlightedAttributes = [NSDictionary
//                                               dictionaryWithObject:buttonskyBlue forKey:UITextAttributeTextColor];
//        [filterSegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
        
        [self setMaskTo:allView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
        allView.layer.backgroundColor = segmentBlueColor.CGColor;
        allLbl.textColor = [UIColor whiteColor];

        
        onrideView.layer.backgroundColor = [UIColor clearColor].CGColor;
        onrideLbl.textColor = segmentTextColor;
        completedView.layer.backgroundColor = [UIColor clearColor].CGColor;
        completeLbl.textColor = segmentTextColor;
        cancelledView.layer.backgroundColor = [UIColor clearColor].CGColor;
        cancellLbl.textColor = segmentTextColor;
        
        [filterSegmentControl setSelectedSegmentIndex:0];
        
        if([tripArray count]==0){
            [self.view makeToast:@"No_Records_Found"];
        }
    }else if (segControl.selectedSegmentIndex==1){
        
//        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    [UIFont fontWithName:@"Poppins-SemiBold" size:14], UITextAttributeFont,
//                                    [UIColor darkGrayColor], UITextAttributeTextColor, nil];
//        [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//
//        NSDictionary *highlightedAttributes = [NSDictionary
//                                               dictionaryWithObject:buttonskyBlue forKey:UITextAttributeTextColor];
//        [filterSegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
        [self setMaskTo:onrideView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
        onrideView.layer.backgroundColor = segmentBlueColor.CGColor;
        onrideLbl.textColor = [UIColor whiteColor];

        
        allView.layer.backgroundColor = [UIColor clearColor].CGColor;
        allLbl.textColor = segmentTextColor;
        cancelledView.layer.backgroundColor = [UIColor clearColor].CGColor;
        cancellLbl.textColor = segmentTextColor;
        completedView.layer.backgroundColor = [UIColor clearColor].CGColor;
        completeLbl.textColor = segmentTextColor;
        
        [filterSegmentControl setSelectedSegmentIndex:1];
        
        
       
//        [bottomBorder removeFromSuperlayer];
//
//        // Creating new layer for selection
//        bottomBorder  = [CALayer layer];
//        bottomBorder.borderColor = [UIColor redColor].CGColor;
//        bottomBorder.borderWidth = 3;
//
//        // Calculating frame
//        CGFloat width = self.filterSegmentControl.frame.size.width/3;
//        CGFloat x = self.view.bounds.size.width - self.filterSegmentControl.frame.size.width/3;
//        CGFloat y = self.filterSegmentControl.frame.size.height - bottomBorder.borderWidth;
//        bottomBorder.frame = CGRectMake(x, y,width, bottomBorder.borderWidth);
//        // Adding selection to segment
//        [self.filterSegmentControl.layer addSublayer:bottomBorder];
//
        
        if([onRideArray count]==0){
            [self.view makeToast:@"No_Records_Found"];
        }
    }else if (segControl.selectedSegmentIndex==2){
        
//        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    [UIFont fontWithName:@"Poppins-SemiBold" size:14], UITextAttributeFont,
//                                    [UIColor darkGrayColor], UITextAttributeTextColor, nil];
//        [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//
//        NSDictionary *highlightedAttributes = [NSDictionary
//                                               dictionaryWithObject:buttonskyBlue forKey:UITextAttributeTextColor];
//        [filterSegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
        
        [self setMaskTo:completedView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
        completedView.layer.backgroundColor = segmentBlueColor.CGColor;
        completeLbl.textColor = [UIColor whiteColor];
        
        allView.layer.backgroundColor = [UIColor clearColor].CGColor;
        allLbl.textColor = segmentTextColor;
        onrideView.layer.backgroundColor = [UIColor clearColor].CGColor;
        onrideLbl.textColor = segmentTextColor;
        cancelledView.layer.backgroundColor = [UIColor clearColor].CGColor;
        cancellLbl.textColor = segmentTextColor;
        
        [filterSegmentControl setSelectedSegmentIndex:2];
        
//
//        [bottomBorder removeFromSuperlayer];
//
//        // Creating new layer for selection
//        bottomBorder  = [CALayer layer];
//        bottomBorder.borderColor = [UIColor redColor].CGColor;
//        bottomBorder.borderWidth = 3;
//
//        // Calculating frame
//        CGFloat width = self.filterSegmentControl.frame.size.width/3;
//        CGFloat x = self.view.bounds.size.width - self.filterSegmentControl.frame.size.width/3;
//        CGFloat y = self.filterSegmentControl.frame.size.height - bottomBorder.borderWidth;
//        bottomBorder.frame = CGRectMake(x, y,width, bottomBorder.borderWidth);
//        // Adding selection to segment
//        [self.filterSegmentControl.layer addSublayer:bottomBorder];
//
//
        
        if([CompletedArray count]==0){
            [self.view makeToast:@"No_Records_Found"];
        }
    }else if (segControl.selectedSegmentIndex==3){
        
//        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                    [UIFont fontWithName:@"Poppins-SemiBold" size:14], UITextAttributeFont,
//                                    [UIColor darkGrayColor], UITextAttributeTextColor, nil];
//        [filterSegmentControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//
//        NSDictionary *highlightedAttributes = [NSDictionary
//                                               dictionaryWithObject:buttonskyBlue forKey:UITextAttributeTextColor];
//        [filterSegmentControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateSelected];
        
//        [self setMaskTo:cancelledView byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerTopLeft | UIRectCornerTopRight)];
        [self setMaskTo:cancelledView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft | UIRectCornerBottomLeft)];
        cancelledView.layer.backgroundColor = segmentBlueColor.CGColor;
        cancellLbl.textColor = [UIColor whiteColor];
        

        
        allView.layer.backgroundColor = [UIColor clearColor].CGColor;
        allLbl.textColor = segmentTextColor;
        onrideView.layer.backgroundColor = [UIColor clearColor].CGColor;
        onrideLbl.textColor = segmentTextColor;
        completedView.layer.backgroundColor = [UIColor clearColor].CGColor;
        completeLbl.textColor = segmentTextColor;
        
        
        
        [filterSegmentControl setSelectedSegmentIndex:3];
        
//
//        [bottomBorder removeFromSuperlayer];
//
//        // Creating new layer for selection
//        bottomBorder  = [CALayer layer];
//        bottomBorder.borderColor = [UIColor redColor].CGColor;
//        bottomBorder.borderWidth = 3;
//
//        // Calculating frame
//        CGFloat width = self.filterSegmentControl.frame.size.width/3;
//        CGFloat x = self.view.bounds.size.width - self.filterSegmentControl.frame.size.width/3;
//        CGFloat y = self.filterSegmentControl.frame.size.height - bottomBorder.borderWidth;
//        bottomBorder.frame = CGRectMake(x, y,width, bottomBorder.borderWidth);
//        // Adding selection to segment
//        [self.filterSegmentControl.layer addSublayer:bottomBorder];
//
        
        if([cancelledArray count]==0){
            [self.view makeToast:@"No_Records_Found"];
        }
    }
    
     // [filterSegmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor] } forState:UIControlStateNormal];
    
    
    [tripListTableView reloadData];
    
}
- (IBAction)menuAction:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
    
    
    
}

- (IBAction)didClickFilterBtn:(id)sender {
    containerView.userInteractionEnabled=NO;
    NSArray * arryList=[[NSArray alloc]init];  arryList=@[JJLocalizedString(@"All", nil) ,JJLocalizedString(@"Last_one_week", nil),JJLocalizedString(@"Last_two_weeks", nil),JJLocalizedString(@"Last_one_month", nil),JJLocalizedString(@"Last_one_year", nil)];
    [Dropobj fadeOut];
    [self showPopUpWithTitle:JJLocalizedString(@"Sort", nil) withOption:arryList xy:CGPointMake(self.view.frame.size.width-205, 57) size:CGSizeMake(195, 310) isMultiple:NO];
}

///////FilterView
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDown_R:255.0 G:255.0 B:255.0 alpha:0.90];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    containerView.userInteractionEnabled=YES;
    isSort=YES;
    [self filterTripAccordingToDate:anIndex];
}
-(void)filterTripAccordingToDate:(NSInteger)index{
    switch (index) {
        case 0:
            isSort=NO;
            break;
        case 1:
            [self filterTableViewWithIndex:1];
            break;
        case 2:
            [self filterTableViewWithIndex:2];
            break;
        case 3:
            [self filterTableViewWithIndex:3];
            break;
        case 4:
            [self filterTableViewWithIndex:4];
            break;
        default:
            break;
    }
    [tripListTableView reloadData];
}
-(void)filterTableViewWithIndex:(NSInteger)index{
    sortArray=[[NSMutableArray alloc]init];
    NSMutableArray * dummayArray=[[NSMutableArray alloc]init];
    if(filterSegmentControl.selectedSegmentIndex==0){
        dummayArray=tripArray;
    }else if (filterSegmentControl.selectedSegmentIndex==1){
        dummayArray=onRideArray;
    }else if (filterSegmentControl.selectedSegmentIndex==2){
        dummayArray=CompletedArray ;
    }else if (filterSegmentControl.selectedSegmentIndex==3){
        dummayArray=cancelledArray ;
    }
    for (int i=0; i<[dummayArray count]; i++) {
        TripRecords * objTripRecs=[dummayArray objectAtIndex:i];
        NSTimeInterval distanceBetweenDates = [[self getDateWithIndex:index] timeIntervalSinceDate:[Theme setDate:objTripRecs.tripeOrigDate]];
        double secondsInMinute = 60;
        NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
        
        if (secondsBetweenDates <= 0){
            [sortArray addObject:objTripRecs];
        }
    }
}
-(NSDate *)getDateWithIndex:(NSInteger)index{
    NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo;
    if(index==1){
        sevenDaysAgo = [now dateByAddingTimeInterval:-7*24*60*60];
    }
    else if (index==2){
        sevenDaysAgo = [now dateByAddingTimeInterval:-14*24*60*60];
    }else if (index==3){
        sevenDaysAgo = [now dateByAddingTimeInterval:-31*24*60*60];
    }else if (index==3){
        sevenDaysAgo = [now dateByAddingTimeInterval:-365*24*60*60];
    }
    return sevenDaysAgo;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        containerView.userInteractionEnabled=YES;
        [Dropobj fadeOut];
    }
    [tripListTableView reloadData];
}
@end
