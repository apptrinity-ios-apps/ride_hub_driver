//
//  TripViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/26/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "TripViewController.h"
#import "LocationShareModel.h"
#import "AddressBook/AddressBook.h"
#import "Contacts/Contacts.h"
#import "CustomMarkerView.h"

@interface TripViewController ()<moveToNectVCFromFareSummary,DropLocDelegate,GMSMapViewDelegate>{
     JJLocationManager * jjLocManager;
     FareSummaryViewController *controller;
     NSTimer *mydistanceTimer;
    UIApplication *app;
     NSTimer *backgroundTimer;
     long second;
     RCounter *counter;
    int zoompoint;
    int forLocation;
    GMSPath *pathDrawn;
     GMSMutablePath *path;
    GMSPolyline *singleLine;
    CLLocationCoordinate2D OldLocationNext;
    CLLocationCoordinate2D oldLocation;
    NSDictionary * googleApiParamDict;
    NSString* wayPointLatLon;
    CustomMarkerView *markerView;
    
    
    GMSPolyline * driverline;
    GMSPolyline * defaultline;
    
    NSArray<CLLocation *> *stepsCoords ;
    NSInteger iPosition ;
    
     CLLocationCoordinate2D sampNewLoc_lat;
     CLLocationCoordinate2D sampNewLoc_long;
    
    
    
}
@property (strong,nonatomic) LocationShareModel * shareModel;
@end

@implementation TripViewController{
    FBShimmeringView *_shimmeringView;
}
@synthesize headerLbl,cancelTripBtn,mapView,bottomView,rideBtn,objRiderRecs,Camera,
GoogleMap,custIndicatorView,isMapLoaded,totalDistance,currencyCodeAndAmount,
isForFareController,marker,rideIdLbl,riderNameLbl,phoneBtn,riderImageView,waitBtn,
waitTextLbl,meterView,selectDropView,destMarker,startMarker,refreshBtn,isMapZoomed,dropLocLbl,dotLine,carNameLbl,carModelLbl,carNumberLbl;


- (void)viewDidLoad {
    forLocation = 0;
    
    
    path = [GMSMutablePath path];
    
    carNameLbl.layer.cornerRadius = carNumberLbl.frame.size.height/2;
    carNameLbl.layer.masksToBounds = YES;
    carNameLbl.clipsToBounds = YES;
    
 
      _startNavBtn.hidden = YES;
    self.locationView.hidden = NO;
    self.startNavigationLBL.hidden = YES;
    rideBtn.userInteractionEnabled = YES;
    
    
    self.locationView.layer.cornerRadius = 5;
    self.locationView.layer.masksToBounds = YES;
    
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appenterBackGround:) name:@"appEnterBackgroud_State" object:nil];
    app = [UIApplication sharedApplication];

    _startNavBtn.lineBreakMode = NSLineBreakByWordWrapping;
    _startNavBtn.titleLabel.numberOfLines = 2;
    _startNavBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    _startNavBtn.hidden = TRUE;
    
    if(jjLocManager==nil){
        jjLocManager=[JJLocationManager sharedManager];
    }
    isForFareController=NO;
    zoompoint=15;
    marker=[[GMSMarker alloc]init];
    destMarker=[[GMSMarker alloc]init];
     startMarker=[[GMSMarker alloc]init];
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotif
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverPaymentCompletedNotif
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kUserCancelledDrive
                                               object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];

    totalDistance=0;
    self.points=[[NSMutableArray alloc]init];
    [self setFontAndColor];
    [self setGlowBtn];
    [self setDatas];
    
    isMapLoaded=NO;
   
    [self showActivityIndicator:YES];
   
    
    waitTextLbl.hidden=YES;
   
    
    if([objRiderRecs.hasDropLocation isEqualToString:@"1"]){
         [self showAddressOrEnterAddress:YES];
       // float lat1 = [objRiderRecs.dropLat floatValue];
      //  float lon1 = [objRiderRecs.dropLong floatValue];
        
         dropSelLocation =  [[CLLocation alloc] initWithLatitude:[objRiderRecs.dropLat doubleValue] longitude:[objRiderRecs.dropLong doubleValue]];
    }else{
         [self showAddressOrEnterAddress:NO];
    }
   
    
    selectDropView.layer.masksToBounds = NO;
    selectDropView.layer.shadowOffset = CGSizeMake(-15, 20);
    selectDropView.layer.shadowRadius = 5;
    selectDropView.layer.shadowOpacity = 0.5;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLocation:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedName
                                               object:nil];
    

    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    
   
    
}
//- (void)onAppDidEnterBackground:(NSNotification *)notification {
//    NSTimer *alarmm = [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector:@selector(updateLoc:) userInfo:nil repeats:YES];
//}

-(void)appenterBackGround:(NSNotification*)notification
{
//    [[JJLocationManager sharedManager] startLocationUpdates];
//    [mydistanceTimer invalidate];
//    NSLog(@".........applicationDidEnterBackground..........");
//    //backgroundTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(updateLoc:) userInfo:nil repeats:YES];
//
//dispatch_async(dispatch_get_main_queue(), ^{
//        // Do the work associated with the task, preferably in chunks.
//      backgroundTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLoc) userInfo:nil repeats:true]; [[NSRunLoop mainRunLoop] addTimer: backgroundTimer forMode:NSRunLoopCommonModes];
//    });
    
//    UIApplication *app = [UIApplication sharedApplication];
//
//    //create new uiBackgroundTask
//    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
//        [app endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//    }];
//
//    //and create new timer with async call:
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        //run function methodRunAfterBackground
//        NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(updateLoc) userInfo:nil repeats:NO];
//        [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
//        [[NSRunLoop currentRunLoop] run];
//    });
  
    
    
    CLLocationManager *locationManager = [TripViewController sharedLocationManager];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
    //Use the BackgroundTaskManager to manage all the background Task
    self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.shareModel.bgTask beginNewBackgroundTask];
    
    
    
        //self.shareModel.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
//                                                               selector:@selector(updateLoc)
//                                                               userInfo:nil
//                                                                repeats:NO];

}
+ (CLLocationManager *)sharedLocationManager
{
    static CLLocationManager *_locationManager;
    @synchronized(self) {
        if (_locationManager == nil) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]) {
                _locationManager.allowsBackgroundLocationUpdates = YES;
            }
        }
    }
    return _locationManager;
}
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    
//    
//    forLocation = forLocation+1;
//    
//    if(forLocation == 5){
//        
//        forLocation = 0;
 //       [self updateLoc];
//        
//    }
//    
//    
//    if(self.view.window){
//        AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//        NSString * str=[Theme checkNullValue:[NSString stringWithFormat:@"%f",[Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocationBefore5etres]]];
//        [appdelegate xmppUpdateLoc:jjLocManager.currentLocation.coordinate withReceiver:objRiderRecs.UserId withRideId:objRiderRecs.RideId withBearing:str];
//        
//        
//        
//
//    }
//    
//    
//
//  
//
//}

-(void)stopLocationDelayBy10Seconds{
    CLLocationManager *locationManager = [TripViewController sharedLocationManager];
    [locationManager stopUpdatingLocation];
    
    
    //NSLog(@"locationManager stop Updating after 10 seconds");
}

-(void)changeLocation:(NSNotification*)notification
{
    if(self.view.window){
        @try {
            [NSThread detachNewThreadSelector:@selector(loadMovingMarker) toTarget:self withObject:nil];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception in LiveTracking..");
        }
      
    }
}

-(void)showAddressOrEnterAddress:(BOOL)isAddress{
    
    if(isAddress==YES){
        self.phoneBtn.hidden=NO;
        self.addressView.hidden=NO;
        self.selectDropView.hidden=YES;
        rideBtn.hidden=NO;
        refreshBtn.hidden=NO;
        GoogleMap.frame=CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height+200);
//        mapView.frame=CGRectMake(mapView.frame.origin.x, _addressView.frame.origin.y+_addressView.frame.size.height, mapView.frame.size.width, self.view.frame.size.height-(_addressView.frame.size.height+70));
        
    }else{
        self.phoneBtn.hidden=YES;
        self.addressView.hidden=YES;
        self.selectDropView.hidden=NO;
        rideBtn.hidden=YES;
        refreshBtn.hidden=YES;
   //    GoogleMap.frame=CGRectMake(mapView.frame.origin.x, _addressView.frame.origin.y, mapView.frame.size.width, self.view.frame.size.height-70);
//        mapView.frame=CGRectMake(mapView.frame.origin.x, _addressView.frame.origin.y, mapView.frame.size.width, self.view.frame.size.height-70);
        
    }
    dispatch_async (dispatch_get_main_queue(), ^
                    {
    
    [mapView addSubview:GoogleMap];
                        
                    });
    
    
}

-(void)setGlowBtn
{
    _shimmeringView = [[FBShimmeringView alloc] init];
    _shimmeringView.shimmering = NO;
    _shimmeringView.shimmeringBeginFadeDuration = 0.2;
    _shimmeringView.shimmeringOpacity = 1;
    [self.view addSubview:_shimmeringView];

    rideBtn.delegate=self;
    
    _shimmeringView.contentView = rideBtn;
    CGRect shimmeringFrame = rideBtn.frame;
    shimmeringFrame.origin.y= self.view.frame.size.height-70;
    shimmeringFrame.origin.x= self.view.center.x - 100;
    shimmeringFrame.size.height=50;
   
    shimmeringFrame.size.width=self.rideBtn.frame.size.width;
    _shimmeringView.frame = shimmeringFrame;
    _shimmeringView.layer.cornerRadius = rideBtn.frame.size.height/2;
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [self stopActivityIndicator];
}
-(void)setFontAndColor{
   // rideBtn=[Theme setBoldFontForButton:rideBtn];
   // [rideBtn setBackgroundColor:SetBlueColor];
    
    rideBtn.layer.cornerRadius=rideBtn.frame.size.height/2;
    rideBtn.layer.masksToBounds=YES;
    
    
//    headerLbl=[Theme setHeaderFontForLabel:headerLbl];
    [dropLocLbl setText:JJLocalizedString(@"please_enter_drop_location_to_start_the_trip", nil)];
    riderImageView.layer.cornerRadius=riderImageView.frame.size.width/2;
    riderImageView.layer.masksToBounds=YES;
    phoneBtn.layer.cornerRadius=phoneBtn.frame.size.width/2;
   // phoneBtn.layer.borderWidth=.5;
   // phoneBtn.layer.borderColor=setLightGray.CGColor;
   // phoneBtn.layer.backgroundColor=SetThemeColor.CGColor;
    phoneBtn.layer.masksToBounds=YES;
    rideIdLbl=[Theme setNormalFontForLabel:rideIdLbl];
    riderNameLbl=[Theme setSmallBoldFontForLabel:riderNameLbl];
    
    [waitBtn setTitle:JJLocalizedString(@"Start_Wait", nil) forState:UIControlStateNormal];
    waitBtn=[Theme setNormalFontForButton:waitBtn];
     waitTextLbl=[Theme setLargeBoldFontForLabel:waitTextLbl];
    
//    _addressView.layer.masksToBounds = NO;
//    _addressView.layer.shadowOffset = CGSizeMake(-5, 5);
//    _addressView.layer.shadowRadius = 5;
//    _addressView.layer.shadowOpacity = 0.5;
    
//    _addressInsideView.layer.cornerRadius=5;
//     _addressInsideView.layer.masksToBounds = YES;
    _dropLocBtn.layer.cornerRadius=5;
    _dropLocBtn.layer.masksToBounds = YES;
    
}
- (void)receiveNotification:(NSNotification *) notification
{
    NSDictionary * dict=notification.userInfo;
    if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
        [self stopActivityIndicator];
        if(self.view.window){
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:objRiderRecs.RideId];
            
            NSString * str=[NSString stringWithFormat:@"%@ %@",[Theme checkNullValue:[Theme findCurrencySymbolByCode:[dict objectForKey:@"key4"]]],[Theme checkNullValue:[dict objectForKey:@"key3"]]];
            [objReceiveCashVC setFareAmt:str];
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
    if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"payment_paid"]){
        [self stopActivityIndicator];
        if(self.view.window){
            [self.view makeToast:@"Payment_Received"];
            [self performSelector:@selector(moveToHome) withObject:self afterDelay:0];
        }
    }
    if(self.view.window){
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"ride_cancelled"]){
            NSString *ridercancrelled=JJLocalizedString(@"Rider_Cancelled_the_ride", nil);
            
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Sorry !!" message:ridercancrelled delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=103;
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag==103)
    {
        
        BOOL isHome=NO;
        BOOL isRootSkip=NO;
        for (UIViewController *controller1 in self.navigationController.viewControllers) {
            
            if ([controller1 isKindOfClass:[HomeViewController class]]) {
                
                [self.navigationController popToViewController:controller1
                                                      animated:YES];
                isHome=YES;
                isRootSkip=YES;
                
                break;
            }
        }
        if(isHome==NO){
            for (UIViewController *controller1 in self.navigationController.viewControllers) {
                
                if ([controller1 isKindOfClass:[StarterViewController class]]) {
                    
                    [self.navigationController popToViewController:controller1
                                                          animated:YES];
                    isRootSkip=YES;
                    break;
                }
            }
        }
        if(isRootSkip==NO){
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
    }
    else if (alertView.tag==102){
        
        if (buttonIndex==[alertView cancelButtonIndex]) {
            
        }else {
            [self drawRoadRouteBetweenTwoPoints:jjLocManager.currentLocation];
        }
        
    }
}

-(void)moveToHome{
    RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
    [objRatingsVc setRideid:objRiderRecs.RideId];
    [self.navigationController pushViewController:objRatingsVc animated:YES];
//    for (UIViewController *controller1 in self.navigationController.viewControllers) {
//        
//        if ([controller1 isKindOfClass:[HomeViewController class]]) {
//            
//            [self.navigationController popToViewController:controller1
//                                                  animated:YES];
//            break;
//        }
//    }
}
-(void)dealloc{
   [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setDatas{
    NSString * rideid=JJLocalizedString(@"RideId", nil);
    riderNameLbl.text=objRiderRecs.userName;
    headerLbl.text = objRiderRecs.userName;
    _phoneNumberLBL.text = objRiderRecs.userPhoneNumber;
    _headerLocationLBL .text = objRiderRecs.dropAddress;
    
   // [headerLbl setText:[NSString stringWithFormat:@"%@\n%@ : %@",JJLocalizedString(@"Current_Ride", nil),rideid,objRiderRecs.RideId]];
    
    
    

    [riderImageView sd_setImageWithURL:[NSURL URLWithString:objRiderRecs.userImage] placeholderImage:[UIImage imageNamed:@"userDefault"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [rideBtn setTitle:JJLocalizedString(@"CLICK_TO_BEGIN_TRIP", nil) forState:UIControlStateNormal];
    _startNavBtn.hidden = YES;
    
}
- (IBAction)didClickRideOptionBtn:(id)sender{
  //  UIButton * btn=sender;
    
    
}
- (void) slideActive{
    
    
    if([rideBtn.currentTitle isEqualToString:JJLocalizedString(@"CLICK_TO_BEGIN_TRIP", nil)]){
        [self beginTrip];
        
        
        NSLog(@"%@",[self setParametersDriverBeginRide]);
        
        
        mydistanceTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                           target: self
                                                            selector: @selector(updateLoc)
                                                         userInfo: nil
                                                          repeats: YES];
        
        
        
        // [self.view makeToast:@"started distance"];
        [[PSLocationManager sharedLocationManager] prepLocationUpdates];
        [[PSLocationManager sharedLocationManager] startLocationUpdates];
    }else if ([rideBtn.currentTitle isEqualToString:JJLocalizedString(@"SLIDE_TO_END_TRIP", nil)]){
        CLLocationManager *locationManager = [TripViewController sharedLocationManager];
        [locationManager stopUpdatingLocation];
        [[JJLocationManager sharedManager] stopLocationUpdates];
        [timerWaitLabel pause];
        [mydistanceTimer invalidate];
        [waitBtn setTitle:JJLocalizedString(@"Start_Wait", nil) forState:UIControlStateNormal];
        [[PSLocationManager sharedLocationManager] stopLocationUpdates];
        [self EndTrip];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [self performSelector:@selector(enableRideBtn) withObject:nil afterDelay:30];
    }
}

-(void)appWillResignActive:(NSNotification*)note
{
    //stop using location here. I'm not familar with GMSMapView, but assuming you have a reference to the map view, the documentation suggests this:
   // self.mapView.myLocationEnabled = NO;
    self.GoogleMap.myLocationEnabled = NO;
}
-(void)beginTrip{
    
    
     if(jjLocManager.currentLocation.coordinate.latitude!=0){
         isForFareController=NO;
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web DriverStartedRide:[self setParametersDriverBeginRide]
                   success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
//             counter = [[RCounter alloc] initWithFrame:CGRectMake(0, 0, 160, 70) andNumberOfDigits:3];
//             [meterView addSubview:counter];
//             meterView.hidden=NO;
              _startNavBtn.hidden = NO;
             
             AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
             appdelegate.SETTOTALDISTANCE = @"0";
             
             [rideBtn setTitle:JJLocalizedString(@"SLIDE_TO_END_TRIP", nil) forState:UIControlStateNormal];
             [rideBtn setBackgroundColor:setRedColor];
             waitBtn.hidden=NO;
             cancelTripBtn.hidden=YES;
             self.locationView.hidden = YES;
             phoneBtn.hidden = YES;
             headerLbl.text = @"Current Ride";
              self.startNavigationLBL.hidden = NO;
         }else{
             
             [self.view makeToast:kErrorMessage];
         }
     }
                   failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
     }
    
    
}
-(NSDictionary *)setParametersDriverBeginRide{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSString * latitude=@"";
    NSString * longitude=@"";
    NSString * droplatitude=@"";
    NSString * droplongitude=@"";
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        latitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.latitude]];
        longitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.longitude]];
         droplatitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",dropSelLocation.coordinate.latitude]];
         droplongitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",dropSelLocation.coordinate.longitude]];
        
    }
  
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":objRiderRecs.RideId,
                                  @"pickup_lat":latitude,
                                  @"pickup_lon":longitude,
                                  @"drop_lat":droplatitude,
                                  @"drop_lon":droplongitude
                                  };
    googleApiParamDict = dictForuser;
    
    
    NSLog(@"%@",dictForuser);
    
    
    return dictForuser;
}

-(void)enableRideBtn{
    
    rideBtn.userInteractionEnabled=YES;
}
//- (void)applicationDidEnterBackground:(UIApplication *)application {
//    [[JJLocationManager sharedManager] startLocationUpdates];
//    [mydistanceTimer invalidate];
//    NSLog(@".........applicationDidEnterBackground..........");
//  backgroundTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(updateLoc:) userInfo:nil repeats:YES];
//
//}
//
//
//- (void)applicationWillEnterForeground:(UIApplication *)application {
//
//    [[JJLocationManager sharedManager] stopLocationUpdates];
//    [backgroundTimer invalidate];
//}

-(NSDictionary *)updateLocationOnRide{
    
    NSString * driverId=@"";
    NSString * totalDistanceStr=[Theme checkNullValue:[self stringWithDouble:totalDistance/1000]];
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSString * latitude=@"";
    NSString * longitude=@"";
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        latitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.latitude]];
        longitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.longitude]];
    }
    AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":objRiderRecs.RideId,
                                  @"latitude":latitude,
                                  @"longitude":longitude,
                                  @"distance":totalDistanceStr,
                                  @"device_type":@"ios",
                                  @"background":appdelegate.backgroundStatus,
                                  @"wait_time":[Theme checkNullValue:waitTextLbl.text]
                                  };
    
    
    return dictForuser;
    
    
}
-(void)EndTrip{
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        rideBtn.userInteractionEnabled=NO;
        isForFareController=NO;
        [self showActivityIndicator:YES];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web DriverEndRide:[self setParametersDriverEndRide]
                       success:^(NSMutableDictionary *responseDictionary)
         {
             
             [self stopActivityIndicator];
             
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 totalDistance=0;
                 waitBtn.hidden=YES;
                 waitTextLbl.hidden=YES;
            AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                 appdelegate.SETTOTALDISTANCE = @"0";
                
                 [mydistanceTimer invalidate];
                 [self.view makeToast:@"Ride_Finished"];
                 NSDictionary * summaryDict=responseDictionary[@"response"][@"fare_details"];
                 FareRecords * objFareRecrds=[[FareRecords alloc]init];
                 objFareRecrds.currency=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[summaryDict objectForKey:@"currency"]]];
                 
                 objFareRecrds.rideDistance=[Theme checkNullValue:[summaryDict objectForKey:@"ride_distance"]];
                 objFareRecrds.rideDuration=[Theme checkNullValue:[summaryDict objectForKey:@"ride_duration"]];
                 objFareRecrds.rideFare=[Theme checkNullValue:[NSString stringWithFormat:@"%.02f",[[summaryDict objectForKey:@"ride_fare"] floatValue]]];
                  objFareRecrds.waitingDuration=[Theme checkNullValue:[summaryDict objectForKey:@"waiting_duration"]];
                 objFareRecrds.needPayment=[Theme checkNullValue:[summaryDict objectForKey:@"need_payment"]];
                 
                 objFareRecrds.needCash = [Theme checkNullValue:[responseDictionary[@"response"] objectForKey:@"receive_cash"]];
                 
                 currencyCodeAndAmount=[NSString stringWithFormat:@"%@ %@",objFareRecrds.currency,objFareRecrds.rideFare];
                 _startNavBtn.hidden = YES;
                controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FareSummaryVCSID"];
                 controller.objFareRecs=[[FareRecords alloc]init];
                 [controller setObjFareRecs:objFareRecrds];
                 controller.rideID= objRiderRecs.RideId;

                 controller.delegate=self;
                 [controller presentInParentViewController:self];
             }else{
                 rideBtn.userInteractionEnabled=YES;
                 [self.view makeToast:kErrorMessage];
             }
         }
                       failure:^(NSError *error)
         {
             rideBtn.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             [self.view makeToast:kErrorMessage];
         }];
    }
}
-(NSDictionary *)setParametersDriverEndRide{
    NSString * driverId=@"";
    NSString * totalDistanceStr=[Theme checkNullValue:[self stringWithDouble:totalDistance/1000]];
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSString * latitude=@"";
    NSString * longitude=@"";
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        latitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.latitude]];
        longitude=[Theme checkNullValue:[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.longitude]];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":objRiderRecs.RideId,
                                  @"drop_lat":latitude,
                                  @"drop_lon":longitude,
                                  @"distance":totalDistanceStr,
                                  @"wait_time":[Theme checkNullValue:waitTextLbl.text]
                                  };
    
    return dictForuser;
}
- (IBAction)didClickCancelTripBtn:(id)sender{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    CancelRideViewController * objCancelVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CancelVCSID"];
    [objCancelVC setRideId:objRiderRecs.RideId];
    [self.navigationController pushViewController:objCancelVC animated:YES];
}


- (float)angleFromCoordinate:(CLLocationCoordinate2D)first toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    if (deltaLongitude > 0)
        return angle;
    else if (deltaLongitude < 0)
        return angle + M_PI;
    else if (deltaLatitude < 0)
        return M_PI;
    return 0.0f;
    
    
}


-(void)loadMovingMarker{
    
    CLLocationCoordinate2D newLocation;
    if (isMapZoomed==NO) {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:jjLocManager.currentLocation.coordinate.latitude
                                                                longitude:jjLocManager.currentLocation.coordinate.longitude
                                                                     zoom:zoompoint
                                                                  bearing:0
                                                             viewingAngle:0];
        
        // new change  bearing and view angle
    }
    
   [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    UIImage *markerIcon = [UIImage imageNamed:@"cartopview"];
    markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
    self.marker.icon = markerIcon;
    marker.position = CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude, jjLocManager.currentLocation.coordinate.longitude);
   
    float angel = [self angleFromCoordinate:(jjLocManager.previousLocation.coordinate) toCoordinate:jjLocManager.currentLocation.coordinate];
    marker.rotation = angel ;

    self.marker.icon = markerIcon;
    marker.groundAnchor = CGPointMake(0, 0);
    [CATransaction commit];
    
    
    if (marker == nil) {
        marker = [GMSMarker markerWithPosition:(jjLocManager.currentLocation.coordinate)];
        UIImage *markerIcon = [UIImage imageNamed:@"cartopview"];
        markerIcon = [markerIcon imageWithAlignmentRectInsets:UIEdgeInsetsMake(0, 0, (markerIcon.size.height/2), 0)];
        self.marker.icon = markerIcon;
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.groundAnchor = CGPointMake(0, 0);
        marker.map = GoogleMap;
    } else
    {
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        float angel = [self angleFromCoordinate:(jjLocManager.previousLocation.coordinate) toCoordinate:jjLocManager.currentLocation.coordinate];
        marker.rotation = angel;
         marker.groundAnchor = CGPointMake(0, 0);
        [CATransaction commit];
        
    }
    self.marker.icon = markerIcon;
    NSString *yourhere=JJLocalizedString(@"You_are_Here", nil);
    marker.snippet = [NSString stringWithFormat:@"%@",yourhere];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.groundAnchor = CGPointMake(1, 1);
    marker.map = GoogleMap;
  //  GoogleMap.camera = [GMSCameraPosition   cameraWithTarget:marker.position zoom:16 bearing:0 viewingAngle:0];
    if(self.view.window){
        AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        NSString * str=[Theme checkNullValue:[NSString stringWithFormat:@"%f",[Theme DegreeBearing:jjLocManager.previousLocation locationB:jjLocManager.currentLocation]]];
        [appdelegate xmppUpdateLoc:jjLocManager.currentLocation.coordinate withReceiver:objRiderRecs.UserId withRideId:objRiderRecs.RideId withBearing:str];
    }
    if (OldLocationNext.latitude == 0 ||  OldLocationNext.longitude == 0) {
        
        OldLocationNext.latitude = jjLocManager.currentLocation.coordinate.latitude;
        OldLocationNext.longitude = jjLocManager.currentLocation.coordinate.longitude;
    }
    else{
        OldLocationNext.latitude = oldLocation.latitude;
        
        OldLocationNext.longitude = oldLocation.longitude;
    }
    newLocation.latitude = jjLocManager.currentLocation.coordinate.latitude;
    
    newLocation.longitude = jjLocManager.currentLocation.coordinate.longitude;

     marker.groundAnchor = CGPointMake(0, 0);
    [CATransaction begin];
    [CATransaction setAnimationDuration:2.0];
    float angel1 = [self angleFromCoordinate:(jjLocManager.previousLocation.coordinate) toCoordinate:jjLocManager.currentLocation.coordinate];
    marker.rotation = angel1;
    oldLocation.latitude =  newLocation.latitude;
    oldLocation.longitude = newLocation.longitude;
    
    [CATransaction commit];
    
    ////// addedr
    if(pathDrawn == nil){
        
    }else{
        if (GMSGeometryContainsLocation(jjLocManager.currentLocation.coordinate, pathDrawn, YES)) {
            NSLog(@"locationPoint is in polygonPath.");
            
        } else {
            driverline.map = nil;
            
            NSLog(@"locationPoint is NOT in polygonPath");
        
           // GMSMutablePath *path = [GMSMutablePath path];

//            [path addCoordinate:CLLocationCoordinate2DMake(jjLocManager.previousLocation.coordinate.latitude,
//                                                           jjLocManager.previousLocation.coordinate.longitude)];
//
//            [path addCoordinate:CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude,
//                                                           jjLocManager.currentLocation.coordinate.longitude)];

            
//            [path addLatitude:jjLocManager.currentLocation.coordinate.latitude longitude:jjLocManager.currentLocation.coordinate.longitude];
//            driverline = [GMSPolyline polylineWithPath:path];
//            driverline.strokeWidth = 3;
//            driverline.strokeColor = SetMapPolyLineColor;
//            driverline.map = self.GoogleMap;


//            NSDictionary *dictForuser = @{
//                                          @"origin":[NSString stringWithFormat:@"%f,%f", jjLocManager.previousLocation.coordinate.latitude,jjLocManager.previousLocation.coordinate.longitude],
//                                          @"destination":[NSString stringWithFormat:@"%f,%f",jjLocManager.currentLocation.coordinate.latitude,jjLocManager.currentLocation.coordinate.longitude],
//                                          @"sensor":@"true",
//                                          @"key":kGoogleServerKey
//                                          };
//
//
//
//           // [self showActivityIndicator:YES];
//            UrlHandler *web = [UrlHandler UrlsharedHandler];
//            [web getGoogleRoute:dictForuser
//                        success:^(NSMutableDictionary *responseDictionary)
//             {
//                 @try {
//                     NSArray * arr=[responseDictionary objectForKey:@"routes"];
//                     if([arr count]>0){
//                         [self stopActivityIndicator];
//
//                         pathDrawn =[GMSPath pathFromEncodedPath:responseDictionary[@"routes"][0][@"overview_polyline"][@"points"]];
//                         driverline = [GMSPolyline polylineWithPath:pathDrawn];
//
//                         driverline.strokeWidth = 3;
//                         driverline.strokeColor = SetMapPolyLineColor;
//
//                         driverline.map = self.GoogleMap;
//
//                         GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathDrawn];
//                         GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:60.0f];
//                         //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
//                         [GoogleMap animateWithCameraUpdate:update];
//                         //  [_locationManager stopUpdatingLocation];
//                     }else{
//                         [self stopActivityIndicator];
//                         [self.view makeToast:@"can't_find_route"];
//                     }
//                 }
//                 @catch (NSException *exception) {
//
//                 }
//
//             }
//                        failure:^(NSError *error)
//             {
//                 [self stopActivityIndicator];
//                 if(self.view.window){
//                     UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[Theme project_getAppName] message:JJLocalizedString(@"Cant_able_to_get_location", nil) delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
//                     alert.tag=102;
//                     [alert show];
//                 }
//                 [self.view makeToast:@"can't_find_route"];
//
//             }];

            wayPointLatLon = [NSString stringWithFormat:@"%f,%f",newLocation.latitude,newLocation.longitude];
            
            [self DrawDirectionPathWithWaypoint:initialLocation.coordinate.latitude userlng:initialLocation.coordinate.longitude drop:[objRiderRecs.dropLat doubleValue] droplng:[objRiderRecs.dropLong doubleValue] wayPointLatLon:wayPointLatLon];
            
           // [self DrawDirectionPathWithWaypoint:initialLocation.coordinate.latitude userlng:initialLocation.coordinate.longitude drop:[objRiderRecs.dropLat doubleValue] droplng:[objRiderRecs.dropLong doubleValue] wayPointLatLon:wayPointLatLon];
            
        }
    }
   
}

- (float)getHeadingForDirectionFromCoordinate:(CLLocation*)fromLoc toCoordinate:(CLLocation*)toLoc
{
    
    float fromlat =  [self ToRad:fromLoc.coordinate.latitude];
    float fromlong = [self ToRad:fromLoc.coordinate.longitude];
    float tolat = [self ToRad:toLoc.coordinate.latitude];
    float toLong = [self ToRad:toLoc.coordinate.longitude];

float degree = [self ToDegrees:(atan2(sin(toLong - fromlong) * cos(tolat), cos(fromlat) * sin(tolat) - sin(fromlat) * cos(tolat) * cos(toLong -fromlong)))];



    if (degree >= 0) {
        return degree - 180.0 ;
    }else{
        return (360.0+degree) -180.0 ;
    }
    

}


-(double) ToRad: (double)degrees{
    
    return ( degrees * M_PI / 180.0);
}

-(double) ToBearing:(double)radians{
    return [self ToDegrees:radians] + 360 % 360;
}

-(double) ToDegrees: (double)radians{
    return (radians * 180.0 / M_PI);
}


-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    zoompoint=position.zoom;
    // handle you zoom related logic
}
-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    if(gesture==YES){
        isMapZoomed=YES;
    }else{
        isMapZoomed=NO;
    }
}


-(void)updateLoc{
    //setParametersDriverEndRide
   
    UrlHandler *web = [UrlHandler UrlsharedHandler];
   // [web UpdateUserLocation:[self setParametersUpdateLocation:loc]
    [web UpdateUserLocation:[self updateLocationOnRide]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             
             
         }else{
             
             //  [self.view makeToast:kErrorMessage];
         }
     }
                    failure:^(NSError *error)
     {
         //  [self.view makeToast:kErrorMessage];
         
     }];
}


-(NSDictionary *)setParametersUpdateLocation:(CLLocation *)loc{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"latitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]],
                                  @"longitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]],
                                  @"ride_id":objRiderRecs.RideId
                                  };
    
    return dictForuser;
}

-(void)viewWillDisappear:(BOOL)animated{
    
   // [self.locationManager stopUpdatingLocation];
    
}

-(void)viewWillAppear:(BOOL)animated{
    if(isMapLoaded==NO){
        [PSLocationManager sharedLocationManager].delegate = self;
        [self loadGoogleMap:jjLocManager.currentLocation];
    }
    [self loadMovingMarker];
    
    totalDistance = 0;
}



-(void)loadGoogleMap:(CLLocation *)loc{
    [self stopActivityIndicator];
    isMapLoaded=YES;
    if(TARGET_IPHONE_SIMULATOR)
    {
        Camera = [GMSCameraPosition cameraWithLatitude:13.0827
                                             longitude:80.2707
                                                  zoom:15];
    }else{
        Camera = [GMSCameraPosition cameraWithLatitude:loc.coordinate.latitude
                                             longitude:loc.coordinate.longitude
                                                  zoom:zoompoint ];
//                                               bearing:90
//                                          viewingAngle:0];
        
        // new change  bearing and view angle
        
    }
    
    initialLocation=loc;
    GoogleMap = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, mapView.frame.size.height+100) camera:Camera];
    startMarker.position = Camera.target;
    
    startMarker.icon = [UIImage imageNamed:@"StartingPin"];
    NSString * Starting_locetion=JJLocalizedString(@"Starting_Location", nil);
    startMarker.snippet = [NSString stringWithFormat:@"%@",Starting_locetion];
    startMarker.appearAnimation = kGMSMarkerAnimationPop;
    startMarker.map = GoogleMap;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle-silver" withExtension:@"json"];
    NSError *error;
    
    //     Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    GoogleMap.mapStyle = style;
   
    
    GoogleMap.userInteractionEnabled=YES;
    GoogleMap.delegate = self;
    [mapView addSubview:GoogleMap];
    
    if([objRiderRecs.hasDropLocation isEqualToString:@"1"]){
        //        float lat1 = [objRiderRecs.dropLat floatValue];
        //        float lon1 = [objRiderRecs.dropLong floatValue];
        destMarker.position=CLLocationCoordinate2DMake( [objRiderRecs.dropLat doubleValue],[objRiderRecs.dropLong doubleValue]);
        destMarker.snippet = [NSString stringWithFormat:@"Destination Location"];
        destMarker.icon = [UIImage imageNamed:@"DestinationPin"];
        destMarker.appearAnimation = kGMSMarkerAnimationPop;
        destMarker.map = GoogleMap;
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:[objRiderRecs.dropLat doubleValue] longitude:[objRiderRecs.dropLong doubleValue]];
        [self drawRoadRouteBetweenTwoPoints:LocationAtual];
    }
    
    [self loadMovingMarker];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        if(isForFareController==YES){
//             custIndicatorView.center =controller.view.center;
//        }else{
//             custIndicatorView.center =self.view.center;
//        }
//
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}
#pragma mark PSLocationManagerDelegate

- (void)locationManager:(PSLocationManager *)locationManager signalStrengthChanged:(PSLocationManagerGPSSignalStrength)signalStrength {
    
    if (signalStrength == PSLocationManagerGPSSignalStrengthWeak) {
       // [self.view makeToast:@"Signal_is_too_weak"];
    } else if (signalStrength == PSLocationManagerGPSSignalStrengthStrong) {
      //  [self.view makeToast:@"Strong"];
    } else {
       //  [self.view makeToast:@"....."];
    }
    
  // [self.view makeToast:strengthText];
}

- (void)locationManagerSignalConsistentlyWeak:(PSLocationManager *)locationManager {
   //  [self.view makeToast:@"Signal_is_Consistently_Weak"];
}

- (void)locationManager:(PSLocationManager *)locationManager distanceUpdated:(CLLocationDistance)distance {// distancenow
    
    totalDistance=distance;
    
    forLocation = forLocation+1;
    
    if(forLocation == 5){
        
        forLocation = 0;
        [self updateLoc];
        
    }
    
    
    if(self.view.window){
        AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        NSString * str=[Theme checkNullValue:[NSString stringWithFormat:@"%f",[Theme DegreeBearing:jjLocManager.previousLocation locationB:jjLocManager.currentLocation]]];
        [appdelegate xmppUpdateLoc:jjLocManager.currentLocation.coordinate withReceiver:objRiderRecs.UserId withRideId:objRiderRecs.RideId withBearing:str];
        
        
    }
    
    //[counter updateCounter:totalDistance animate:YES];
   //  [self.view makeToast:[NSString stringWithFormat:@"%.2f", distance]];
}
- (void)locationManager:(PSLocationManager *)locationManager waypoint:(CLLocation *)waypoint calculatedSpeed:(double)calculatedSpeed{
    // [self.view makeToast:[NSString stringWithFormat:@"%.2f", calculatedSpeed]];
}
- (void)locationManager:(PSLocationManager *)locationManager error:(NSError *)error {
    // location services is probably not enabled for the app
     [self.view makeToast:@"Unable_to_determine_location"];
}

- (NSString *)stringWithDouble:(double)value {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[NSLocale currentLocale]];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumFractionDigits:2];
    return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:value]];
}
-(void)moveToNextVc:(NSInteger )index{
    if(index==1){
        CashOTPViewController * objCashOtpController=[self.storyboard instantiateViewControllerWithIdentifier:@"CashOTPVCSID"];
        [objCashOtpController setRideId:objRiderRecs.RideId];
        [self.navigationController pushViewController:objCashOtpController animated:YES];
    }else if (index==0){
        [controller.view makeToast:@"Please_wait_until_transaction"];
        [self sendrequestForPayment];
    }else if (index==2){
        [self sendrequestForNoPayment];
    }
}

-(void)sendrequestForPayment{
    isForFareController=YES;
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web requestForPayment:[self setParametersForPaymentRequest]
        success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
           //  [controller.view makeToast:@"Please wait until transaction is complete. Don't minimize or go back"];
             PaymentWaitingViewController * objPayWait=[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWaitingVCSID"];
             [objPayWait setRideIdPay:objRiderRecs.RideId];
             [self.navigationController pushViewController:objPayWait animated:YES];
             
         }else{
             [self stopActivityIndicator];
             [controller.view makeToast:kErrorMessage];
         }
     }
        failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [controller.view makeToast:kErrorMessage];
         
     }];
}

-(void)sendrequestForNoPayment{
    
    isForFareController=YES;
    RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
    [objRatingsVc setRideid:objRiderRecs.RideId];
    [self.navigationController pushViewController:objRatingsVc animated:YES];
//    [self showActivityIndicator:YES];
//    UrlHandler *web = [UrlHandler UrlsharedHandler];
//    [web NoNeedPayment:[self setParametersForPaymentRequest]
//                   success:^(NSMutableDictionary *responseDictionary)
//     {
//         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
//             RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
//             [objRatingsVc setRideid:objRiderRecs.RideId];
//             [self.navigationController pushViewController:objRatingsVc animated:YES];
//         }else{
//             [self stopActivityIndicator];
//             [controller.view makeToast:kErrorMessage];
//         }
//     }
//                   failure:^(NSError *error)
//     {
//         [self stopActivityIndicator];
//         [controller.view makeToast:kErrorMessage];
//         
//     }];
}

-(NSDictionary *)setParametersForPaymentRequest{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":objRiderRecs.RideId
                                  };
    return dictForuser;
}
- (IBAction)didClickRefreshBtn:(id)sender {
    isMapZoomed=NO;
  //  if(![rideBtn.currentTitle isEqualToString:@"SLIDE TO BEGIN TRIP"]){
        [jjLocManager refreshDriverCurrentLocation];
   // }
}
- (IBAction)startNavigationBtn:(id)sender {
    

    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {


        NSString * sourcestrLatti = [NSString stringWithFormat:@"%@",[googleApiParamDict valueForKey:@"pickup_lat"] ];
        NSString * sourcestrLong = [NSString stringWithFormat:@"%@",[googleApiParamDict valueForKey:@"pickup_lon"] ];
        NSString * deststrLatti = [NSString stringWithFormat:@"%@",[googleApiParamDict valueForKey:@"drop_lat"] ];
        NSString * deststrLong = [NSString stringWithFormat:@"%@",[googleApiParamDict valueForKey:@"drop_lon"] ];

        NSString * directionsRequest = [NSString stringWithFormat:@"comgooglemaps://?saddr=\(%@),\(%@)&daddr=\(%@),\(%@)&directionsmode=driving&zoom=14&views=traffic"@"&x-success=sourceapp://?resume=true&x-source=AirApp",sourcestrLatti,sourcestrLong,deststrLatti,deststrLong];

        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [[UIApplication sharedApplication] openURL:directionsURL];




    } else {
        NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"GoogleMaps is not Installed on Your Device "
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];

        
       // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
 
}


- (IBAction)didClickPhone:(id)sender {
    NSString * phoneNum=[NSString stringWithFormat:@"tel:%@",objRiderRecs.userPhoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];
}
- (IBAction)didClickWaitBtn:(id)sender {
    UIButton * btn=(UIButton *)sender;
     waitTextLbl.hidden=NO;

    if(timerWaitLabel==nil){
        timerWaitLabel = [[MZTimerLabel alloc] initWithLabel:waitTextLbl andTimerType:MZTimerLabelTypeStopWatch];
        timerWaitLabel.timeFormat = @"HH:mm:ss";
        
    }
    if([timerWaitLabel counting]){
        [timerWaitLabel pause];
        [btn setTitle:JJLocalizedString(@"Start_Wait", nil) forState:UIControlStateNormal];
    }else{
        [timerWaitLabel start];
        [btn setTitle:JJLocalizedString(@"End_Wait", nil) forState:UIControlStateNormal];
    }
    
}

- (IBAction)didClickDropLocationBtn:(id)sender {
    
    
    DropLocationViewController * objDropLocVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DropLocationVCSID"];
    [objDropLocVC setLattitude:jjLocManager.currentLocation.coordinate.latitude];
    [objDropLocVC setLongitude:jjLocManager.currentLocation.coordinate.longitude];
    objDropLocVC.delegate=self;
    [self.navigationController pushViewController:objDropLocVC animated:YES];
  
}


-(void)sendDropLocation:(CLLocationCoordinate2D )locSelected{
    if(locSelected.latitude!=0){
       dropSelLocation = [[CLLocation alloc] initWithLatitude:locSelected.latitude longitude:locSelected.longitude];
        [self showAddressOrEnterAddress:YES];
        destMarker.position=CLLocationCoordinate2DMake(locSelected.latitude,locSelected.longitude);
        NSString * destination=JJLocalizedString(@"Destination_Location", nil);
         destMarker.snippet = [NSString stringWithFormat:@"%@",destination];
        destMarker.icon = [UIImage imageNamed:@"DestinationPin"];
        destMarker.appearAnimation = kGMSMarkerAnimationPop;
        destMarker.map = GoogleMap;
        CLLocation *loca = [[CLLocation alloc] initWithLatitude:locSelected.latitude longitude:locSelected.longitude];
        [self drawRoadRouteBetweenTwoPoints:loca];

    }else{
        [self.view makeToast:@"Cant_able_to_get_location"];
    }
    
}


-(void)drawRoadRouteBetweenTwoPoints:(CLLocation *)currentLocation{

    singleLine.map = nil;

    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getGoogleRoute:[self setParametersDrawLocation:currentLocation]
                success:^(NSMutableDictionary *responseDictionary)
     {
         @try {
             NSArray * arr=[responseDictionary objectForKey:@"routes"];
             if([arr count]>0){
                 [self stopActivityIndicator];
                 
                 pathDrawn =[GMSPath pathFromEncodedPath:responseDictionary[@"routes"][0][@"overview_polyline"][@"points"]];
                 singleLine = [GMSPolyline polylineWithPath:pathDrawn];
                 
                 singleLine.strokeWidth = 7;
                // singleLine.strokeColor = SetMapPolyLineColor;
            
                 GMSStrokeStyle *blueToGreen = [GMSStrokeStyle gradientFromColor:SkyBlueColor toColor:GreenColor];
                 singleLine.spans = @[[GMSStyleSpan spanWithStyle:blueToGreen]];
                 
                 singleLine.map = self.GoogleMap;
                 
                 GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathDrawn];
                 GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:60.0f];
                 //bounds = [bounds includingCoordinate:PickUpmarker.position   coordinate:Dropmarker.position];
                 [GoogleMap animateWithCameraUpdate:update];
                 //  [_locationManager stopUpdatingLocation];
             }else{
                 [self stopActivityIndicator];
                 [self.view makeToast:@"can't_find_route"];
             }
         }
         @catch (NSException *exception) {
             
         }
         
     }
                failure:^(NSError *error)
     {
         [self stopActivityIndicator];
          if(self.view.window){
         UIAlertView * alert=[[UIAlertView alloc]initWithTitle:[Theme project_getAppName] message:JJLocalizedString(@"Cant_able_to_get_location", nil) delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
         alert.tag=102;
         [alert show];
          }
         [self.view makeToast:@"can't_find_route"];
         
     }];
}

-(NSDictionary *)setParametersDrawLocation:(CLLocation *)loc{
    
    NSDictionary *dictForuser = @{
                                  @"origin":[NSString stringWithFormat:@"%f,%f", initialLocation.coordinate.latitude,initialLocation.coordinate.longitude],
                                  @"destination":[NSString stringWithFormat:@"%f,%f",loc.coordinate.latitude,loc.coordinate.longitude],
                                  @"sensor":@"true",
                                   @"key":kGoogleServerKey
                                  };
    return dictForuser;
}


-(void)DrawDirectionPathWithWaypoint: (CGFloat )userlat userlng:(CGFloat )userlng drop:(CGFloat )droplat droplng:(CGFloat )droplng wayPointLatLon:(NSString*)wayPointLatLon
{
    //Testing

    //defaultSourceLocation.latitude = userlat;
   // defaultSourceLocation.longitude = userlng;
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getGoogleRoute:[self setParametersDrawLocationWithWaypoint:userlat withLocLong:userlng withDestLoc:droplat withDestLonf:droplng wayPointLatLon:wayPointLatLon]
                success:^(NSMutableDictionary *responseDictionary)
     {
         @try {
             NSArray * arr=[responseDictionary objectForKey:@"routes"];

             if([arr count]>0){
                 pathDrawn =[GMSPath pathFromEncodedPath:responseDictionary[@"routes"][0][@"overview_polyline"][@"points"]];

                 // [singleLine setMap:nil];
                // [GoogleMap clear];
//

                 singleLine = [GMSPolyline polylineWithPath:pathDrawn];
                 singleLine.map = nil;
                 singleLine.strokeWidth = 6;
                // singleLine.strokeColor = BGCOLOR;
                 GMSStrokeStyle *blueToGreen = [GMSStrokeStyle gradientFromColor:SkyBlueColor toColor:GreenColor];
                 singleLine.spans = @[[GMSStyleSpan spanWithStyle:blueToGreen]];
                 singleLine.map = self.GoogleMap;
                 //marker for source
                 Camera = [GMSCameraPosition cameraWithLatitude:userlat
                                                      longitude:userlng
                                                           zoom:zoompoint];
//                                                        bearing:30
//                                                   viewingAngle:45];
                 
                 // new change bearing and viewangle
                 
                 if (self.markerhintvalue == YES ){
                  
                     
                     
                 }else{
                        //   markerView =  [[[NSBundle mainBundle] loadNibNamed:@"CustomMarkerView" owner:self options:nil] objectAtIndex:0];
                      //    markerView.locationLBL.text = @"Madhapur";
                     //  startMarker.iconView = markerView;
                    //  startMarker.position = Camera.target;
                   //    startMarker.icon = [UIImage imageNamed:@"StartingPin"];
                     NSString * Starting_locetion=JJLocalizedString(@"Starting_Location", nil);
                     
                 //    startMarker.snippet = [NSString stringWithFormat:@"%@",Starting_locetion];
                //   startMarker.appearAnimation = kGMSMarkerAnimationPop;
               //   startMarker.map = GoogleMap;
                     
                     self.markerhintvalue = YES;
                     markerView =  [[[NSBundle mainBundle] loadNibNamed:@"CustomMarkerView" owner:self options:nil] objectAtIndex:0];
                     markerView.locationLBL.text = objRiderRecs.dropAddress;
                     destMarker.iconView = markerView;
                   //destMarker.position = Camera.target;
                     destMarker.position=CLLocationCoordinate2DMake( [objRiderRecs.dropLat doubleValue],[objRiderRecs.dropLong doubleValue]);
                     destMarker.snippet = [NSString stringWithFormat:@"%@", objRiderRecs.dropAddress];
                   //destMarker.icon = [UIImage imageNamed:@"DestinationPin"];
                     destMarker.appearAnimation = kGMSMarkerAnimationPop;
                     destMarker.map = GoogleMap;
                     
                 }
                 
                 // marker for destination
//                 dropSelLocation = [[CLLocation alloc] initWithLatitude:droplat longitude:droplng];
//                 [self showAddressOrEnterAddress:YES];
//                 destMarker.position=CLLocationCoordinate2DMake(droplat,droplng);
//                 NSString * destination=JJLocalizedString(@"Destination_Location", nil);
//                 destMarker.snippet = [NSString stringWithFormat:@"%@",destination];
//                 destMarker.icon = [UIImage imageNamed:@"DestinationPin"];
//                 destMarker.appearAnimation = kGMSMarkerAnimationPop;
//                 destMarker.map = GoogleMap;
                 // marker for car
                 
                 [CATransaction begin];
                 [CATransaction setAnimationDuration:2.0];
                 
                 marker.position = CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude, jjLocManager.currentLocation.coordinate.longitude);
                 
                 
           // float angel = [Theme DegreeBearing:jjLocManager.previousLocation locationB:jjLocManager.currentLocation];
               //  float angel = [self angleFromCoordinate:(jjLocManager.previousLocation.coordinate) toCoordinate:jjLocManager.currentLocation.coordinate];
                 
             //  marker.rotation = angel;
              [CATransaction commit];
              
                 
                 if (marker == nil) {
                     marker = [GMSMarker markerWithPosition:(jjLocManager.currentLocation.coordinate)];
                     marker.icon = [UIImage imageNamed:@"cartopview"];
                     marker.appearAnimation = kGMSMarkerAnimationPop;
                     marker.map = GoogleMap;
                     
                 } else
                 {
                     [CATransaction begin];
                     [CATransaction setAnimationDuration:2.0];
                     marker.position = (jjLocManager.currentLocation.coordinate);
                     [CATransaction commit];

                 }

                 marker.icon = [UIImage imageNamed:@"cartopview"];
                 NSString *yourhere=JJLocalizedString(@"You_are_Here", nil);
                 marker.snippet = [NSString stringWithFormat:@"%@",yourhere];
                 marker.appearAnimation = kGMSMarkerAnimationPop;
                 marker.map = GoogleMap;
                 
                 
                 GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathDrawn];
                 GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:100.0f];
                 // bounds = [bounds initWithCoordinate:userMarker.position   coordinate:Drivermarker.position];
                 // [GoogleMap animateWithCameraUpdate:update];

             }else{

                // [self Toast:@"can't find route"];
             }



         }
         @catch (NSException *exception) {

         }


     }
                failure:^(NSError *error)
     {
        // [self Toast:@"can't find route"];
     }];
}
-(NSDictionary *)setParametersDrawLocationWithWaypoint:(CGFloat )loclat withLocLong:(CGFloat)locLong withDestLoc:(CGFloat)destLoc withDestLonf:(CGFloat)destLong wayPointLatLon:(NSString *)wayPointLatLon{
    //    @"waypoints":@"17.5168,78.4610|17.4399,78.4983|17.3858, 78.4795",
    //    @"optimizeWaypoints": @"true",

    NSString *lastChar = [wayPointLatLon substringFromIndex: [wayPointLatLon length] - 1];
    if([lastChar  isEqual: @"|"]){
        wayPointLatLon = [wayPointLatLon substringToIndex:[wayPointLatLon length] - 1];
    }
    // [NSString stringWithFormat:@"%@",wayPointLatLon]
    NSDictionary *dictForuser = @{
                                  @"origin":[NSString stringWithFormat:@"%f,%f",loclat,locLong],
                                  @"destination":[NSString stringWithFormat:@"%f,%f",destLoc,destLong],
                                  @"sensor":@"true",
                                  @"waypoints":wayPointLatLon,
                                  @"optimizeWaypoints": @"true",
                                  @"key":kGoogleServerKey
                                  };
    return dictForuser;
}



@end
