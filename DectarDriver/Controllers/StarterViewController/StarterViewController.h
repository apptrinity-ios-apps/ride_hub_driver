#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Theme.h"
#import "DriverInfoRecords.h"
#import <GoogleMaps/GoogleMaps.h>
#import "DectarCustomColor.h"
#import "RootBaseViewController.h"
#import "UrlHandler.h"
#import "UIView+Toast.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import <MapKit/MapKit.h>
#import "TPFloatRatingView.h"
#import "OpenInMapsViewController.h"
@class RootBaseViewController;
@interface StarterViewController : RootBaseViewController<CLLocationManagerDelegate,MKMapViewDelegate,GMSMapViewDelegate>{
   
}

+ (instancetype) controller;
@property(strong,nonatomic)RTSpinKitView * custIndicatorView;

@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIButton *yesBtn;
@property (strong, nonatomic) IBOutlet UIButton *noBtn;
@property(strong,nonatomic)GMSMapView * GoogleMap;
@property(strong,nonatomic)GMSMarker *marker;
@property(strong,nonatomic)GMSCameraPosition * Camera;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userVehModelLbl;
@property (weak, nonatomic) IBOutlet UIButton *goOnlineBtn;
@property (weak, nonatomic) IBOutlet MKMapView *currentMapView;
@property (weak, nonatomic) IBOutlet UILabel *rateLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *starterScrollView;
@property (weak, nonatomic) IBOutlet UIView *firstView;
@property (weak, nonatomic) IBOutlet UIView *tipsView;
@property (weak, nonatomic) IBOutlet UIView *lastTripView;
@property (weak, nonatomic) IBOutlet UIView *TodaysTotalView;
@property (weak, nonatomic) IBOutlet UILabel *tipsTripsLbl;
@property (weak, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *imgCircleLbl;
@property (strong, nonatomic) IBOutlet UILabel *numberPlateLbl;
@property (strong, nonatomic) IBOutlet UILabel *ratingLbl;
@property (strong, nonatomic) IBOutlet UIView *driverimageBackView;
@property (strong, nonatomic) IBOutlet UIView *driverImageview;


@property (assign, nonatomic) BOOL hasPushNotification;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *catgLbl;

- (IBAction)didClickGoOnlineBtn:(id)sender;
- (IBAction)didClickMenuBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lastTripTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *lastTripVehModelLbl;
@property (weak, nonatomic) IBOutlet UILabel *lastTripNerEarningsLbl;

@property (weak, nonatomic) IBOutlet UILabel *todaysOnlineLbl;
@property (weak, nonatomic) IBOutlet UILabel *todaysTripLbl;
@property (weak, nonatomic) IBOutlet UILabel *todaysEstimateLbl;
@property (weak, nonatomic) IBOutlet UILabel *tipsLbl;
@property(strong,nonatomic)NSString * currencyStr;

@property (weak, nonatomic) IBOutlet UIButton *starBtn1;
@property (weak, nonatomic) IBOutlet UIButton *starBtn2;
@property (weak, nonatomic) IBOutlet UIButton *starBtn3;
@property (weak, nonatomic) IBOutlet UIButton *starBtn4;
@property (weak, nonatomic) IBOutlet UIButton *starBtn5;


@property (weak, nonatomic) IBOutlet LabelThemeColor *lTripLbl;
@property (weak, nonatomic) IBOutlet UILabel *nEarningLbl;
@property (weak, nonatomic) IBOutlet UILabel *tEarningsLbl;


@property (weak, nonatomic) IBOutlet UILabel *eNetLbl;


@property (weak, nonatomic) IBOutlet LabelThemeColor *tTipsLbl;
@property (weak, nonatomic) IBOutlet UILabel *tLbl;
@property (strong, nonatomic) IBOutlet UIView *logoutView;
@property (weak, nonatomic) IBOutlet UIView *dotLIne1;
@property (weak, nonatomic) IBOutlet UIView *dotLIne2;
@property (weak, nonatomic) IBOutlet UIView *dotLine3;
@property (weak, nonatomic) IBOutlet UIView *dotLine4;
@property (weak, nonatomic) IBOutlet UIView *logOutBackView;
@property (weak, nonatomic) IBOutlet UIView *yesView;
@property (weak, nonatomic) IBOutlet UIView *noView;

@property (weak, nonatomic) IBOutlet UIView *goOnlineView;

//@property UIView * container;

@end
