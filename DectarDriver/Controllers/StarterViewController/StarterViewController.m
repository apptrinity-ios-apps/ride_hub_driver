//
//  StarterViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "StarterViewController.h"
#import "ITRAirSideMenu.h"
#import "AppDelegate.h"

#import <QuartzCore/QuartzCore.h>
#import "ATAppUpdater.h"
#import "Constant.h"


@interface StarterViewController (){
    NSString * vehNumber;
     JJLocationManager * jjLocManager;
    IBOutlet UIView *tTipsBackView;
    UIView *container;
}


@end
@class RootBaseViewController;
@implementation StarterViewController
@synthesize userImageView,userNameLbl,userVehModelLbl,goOnlineBtn,custIndicatorView,bottomView,currentMapView,starterScrollView,tipsView,lastTripTimeLbl,firstView,
lastTripVehModelLbl,
lastTripNerEarningsLbl,
todaysOnlineLbl,
todaysTripLbl,
todaysEstimateLbl,Camera,GoogleMap,marker,dotLIne1,dotLIne2,dotLine3,dotLine4,
tipsLbl,catgLbl,lastTripView,TodaysTotalView,tipsTripsLbl,ratingView,currencyStr,yesBtn,noBtn,logOutBackView,yesView,noView,goOnlineView,starBtn1,starBtn2,starBtn3,starBtn4,starBtn5;
+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"StarterVCSID"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.popUpView.hidden = YES;
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString *logoutCheck = [defaults valueForKey:@"logoutCheck"];
    firstView.layer.cornerRadius = 15;

    _topView.layer.cornerRadius = 10;
    _topView.layer.masksToBounds = YES;
    _topView.layer.borderColor = DarkskyBlue.CGColor;
    _topView.layer.borderWidth = 1;
    
    [self setShadow:firstView];
    [self setShadow:goOnlineView];
    [self setShadow:yesView];
    [self setShadow:noView];
    [self setShadow:logOutBackView];
    
    
//    custIndicatorView = [[RTSpinKitView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 80.0f)];
    logOutBackView.layer.cornerRadius = 5;
    goOnlineView.layer.cornerRadius = goOnlineView.frame.size.height/2;
    
    yesView.layer.cornerRadius = yesView.frame.size.height/2;
    noView.layer.cornerRadius = noView.frame.size.height/2;
    
    yesBtn.layer.cornerRadius = yesBtn.frame.size.height/2;
    yesBtn.layer.masksToBounds = YES;
    noBtn.layer.cornerRadius = noBtn.frame.size.height/2;
    noBtn.layer.masksToBounds = YES;
    
//    UIBezierPath *forYesButton = [UIBezierPath bezierPathWithRoundedRect:yesBtn.bounds
//                                                  byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft
//                                                        cornerRadii:CGSizeMake(15.0, 15.0)];
//    CAShapeLayer *shape1 = [[CAShapeLayer alloc] init];
//    [shape1 setPath:forYesButton.CGPath];
//    yesBtn.layer.mask = shape1;
   ///123
    
    
//    UIBezierPath *fornoBtn = [UIBezierPath bezierPathWithRoundedRect:noBtn.bounds
//                                                       byRoundingCorners:UIRectCornerBottomRight|UIRectCornerTopRight
//                                                             cornerRadii:CGSizeMake(15.0, 15.0)];
//    CAShapeLayer *shape2 = [[CAShapeLayer alloc] init];
//    [shape2 setPath:fornoBtn.CGPath];
//    noBtn.frame = noBtn.bounds;
//    noBtn.layer.mask = shape2;
    
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLIne1.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLIne1.bounds].CGPath;
    [self.dotLIne1.layer addSublayer:yourViewBorder2];
    
    CAShapeLayer *yourViewBorder3 = [CAShapeLayer layer];
    yourViewBorder3.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder3.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder3.lineDashPattern = @[@3,@3];
    yourViewBorder3.frame = self.dotLIne2.bounds;
    yourViewBorder3.path = [UIBezierPath bezierPathWithRect:self.dotLIne2.bounds].CGPath;
    [self.dotLIne2.layer addSublayer:yourViewBorder3];
    
    CAShapeLayer *yourViewBorder4 = [CAShapeLayer layer];
    yourViewBorder4.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder4.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder4.lineDashPattern = @[@3,@3];
    yourViewBorder4.frame = self.dotLine3.bounds;
    yourViewBorder4.path = [UIBezierPath bezierPathWithRect:self.dotLine3.bounds].CGPath;
    [self.dotLine3.layer addSublayer:yourViewBorder4];
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.lineDashPattern = @[@3,@3];
    yourViewBorder.frame = self.dotLine4.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.dotLine4.bounds].CGPath;
    [self.dotLine4.layer addSublayer:yourViewBorder];
    
//        [self setMaskTo:_lTripLbl byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)];
//        [self setMaskTo:_tEarningsLbl byRoundingCorners:(UIRectCornerTopRight| UIRectCornerTopLeft )];
//        [self roundCornersOnView:tTipsBackView onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:8.0f];
//        [self roundCornersOnView:_tTipsLbl onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:8.0f];

    if ([logoutCheck isEqualToString:@"YES"]) {
        self.logoutView.hidden = false;
        [self.view bringSubviewToFront:self.logoutView];
        //[self.view addSubview:self.logoutView]; //123
        [defaults setObject:@"NO" forKey:@"logoutCheck"];
    }
    else{
        
        self.logoutView.hidden = true;
     
    }
    if(jjLocManager==nil){
        jjLocManager=[JJLocationManager sharedManager];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotifWhenQuit
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLocation:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedName
                                               object:nil];

    
    [self setFont];
    [self setDatasOfUser];
    [self setShadow:tipsView];
//    [self setShadow:TodaysTotalView];
//    [self setShadow:lastTripView];
    
}



- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br)
    {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        //  maskLayer.frame = roundedView.bounds;   /// p333
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}



// for round rect corner any sides
- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    shape.frame = view.bounds;
    view.layer.mask = shape;
    view.layer.masksToBounds=YES;
}


-(void)enteredForeground{
    if(self.view.window){
       [[ATAppUpdater sharedUpdater] showUpdateWithForce];
    }
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 5;
    view.layer.shadowOpacity = 0.3;
}


-(void)loadData{
    
    [self.view endEditing:YES];
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web StarterData:[self setParametersForStarter]
             success:^(NSMutableDictionary *responseDictionary)
     {
         self.view.userInteractionEnabled=YES;
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             NSDictionary * dict=[responseDictionary objectForKey:@"response"];
             catgLbl.text=[Theme checkNullValue:[dict objectForKey:@"driver_category"]];
             NSString * strRate=[Theme checkNullValue:[dict objectForKey:@"driver_review"]];
             vehNumber=[Theme checkNullValue:[dict objectForKey:@"vehicle_number"]];
             if(strRate.length>0){
                 _rateLbl.text=[NSString stringWithFormat:@"( %@ / 5 )",strRate];
             }
             NSString * imgstr=[dict objectForKey:@"driver_image"];
             
             [userImageView sd_setImageWithURL:[NSURL URLWithString:imgstr] placeholderImage:[UIImage imageNamed:@"PlaceHolderImg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                 
             }];

             [self starRating:strRate];
             
            
             [self loadRating:[Theme checkNullValue:[dict objectForKey:@"driver_review"]]];
             if([[dict objectForKey:@"last_trip"] count]>0){
                 NSDictionary * dict1=[dict objectForKey:@"last_trip"];
                 NSString * str1=[Theme checkNullValue:[dict1 objectForKey:@"ride_time"]];
                 NSString * str2=[Theme checkNullValue:[dict1 objectForKey:@"ride_date"]];
                 NSString * str3=[Theme checkNullValue:[dict1 objectForKey:@"earnings"]];
                 NSString * str4=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict1 objectForKey:@"currency"]]];
                 currencyStr=str4;
                 if(str1.length>0){
                     lastTripTimeLbl.text=[NSString stringWithFormat:@"    %@",str1];
                 }else{
                     lastTripTimeLbl.text= @"    No recent trips"; // JJLocalizedString(@"No_recent_trips", nil);
                 }
                 if(str2.length>0){
                     lastTripVehModelLbl.text=[NSString stringWithFormat:@"    %@",str2];
                 }
                 else{
                     lastTripVehModelLbl.text=@"";
                 }
                 if(str3.length>0){
                     lastTripNerEarningsLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ %@",str4,str3] withSubStr:str4];
                     
                 }
                 else{
                     lastTripNerEarningsLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ 0.00",str4] withSubStr:str4];
                     
                 }
             }
             if([[dict objectForKey:@"today_earnings"] count]>0){
                 
                 NSDictionary * dict2=[dict objectForKey:@"today_earnings"];
                 NSString * str1=[Theme checkNullValue:[dict2 objectForKey:@"online_hours"]];
                 NSString * str2=[Theme checkNullValue:[dict2 objectForKey:@"trips"]];
                 NSString * str3=[Theme checkNullValue:[dict2 objectForKey:@"earnings"]];
                 NSString * str4=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict2 objectForKey:@"currency"]]];
                 currencyStr=str4;
                 if(str1.length>0){
                     todaysOnlineLbl.text=[NSString stringWithFormat:@"    %@",str1];
                 }else{
                     todaysOnlineLbl.text=@"";
                 }
                 if(str2.length>0){
                     todaysTripLbl.text=[NSString stringWithFormat:@"    %@ %@",str2,JJLocalizedString(@"Trips", nil)];
                 }
                 else{
                     todaysTripLbl.text=[NSString stringWithFormat:@"    0 %@",JJLocalizedString(@"Trips", nil)];
                 }
                 if(str3.length>0){
                     todaysEstimateLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ %@",str4,str3] withSubStr:str4];
                     
                 }
                 else{
                     todaysEstimateLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ 0.00",str4] withSubStr:str4];
                     
                 }
                 
             }else{
                 todaysTripLbl.text=[NSString stringWithFormat:@"    0 %@",JJLocalizedString(@"Trips", nil)];
             }
             if([[dict objectForKey:@"today_tips"] count]>0){
                 NSDictionary * dict3=[dict objectForKey:@"today_tips"];
                 NSString * str2=[Theme checkNullValue:[dict3 objectForKey:@"trips"]];
                 NSString * str3=[Theme checkNullValue:[dict3 objectForKey:@"tips"]];
                 NSString * str4=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict3 objectForKey:@"currency"]]];
                 currencyStr=str4;
                 if(str2.length>0){
                     tipsTripsLbl.text=[NSString stringWithFormat:@"    %@ %@",str2,JJLocalizedString(@"Trips", nil)];
                 }
                 else{
                     tipsTripsLbl.text=[NSString stringWithFormat:@"    0 %@",JJLocalizedString(@"Trips", nil)];
                 }
                 if(str3.length>0){
                     tipsLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ %@",str4,str3] withSubStr:str4];
                     
                 }
                 else{
                     tipsLbl.attributedText=[self mainStr:[NSString stringWithFormat:@"    %@ 0.00",str4] withSubStr:str4];
                     
                 }
             }else{
                 tipsTripsLbl.text=[NSString stringWithFormat:@"    0 %@",JJLocalizedString(@"Tips", nil)];
             }
             
             
             
             
             
         }else{
             self.view.userInteractionEnabled=YES;
             [self.view makeToast:kErrorMessage];
         }
     }
             failure:^(NSError *error)
     {
         self.view.userInteractionEnabled=YES;
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
}




-(void)starRating:(NSString*)rating {
    
    
    NSInteger rate = [rating integerValue];
    
    //starEmptySmall
    
    if (rate == 1){
        
         [self.starBtn1 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
         [self.starBtn2 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
         [self.starBtn3 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
         [self.starBtn4 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
         [self.starBtn5 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
        
    }else if(rate == 2){
        [self.starBtn1 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn2 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn3 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
        [self.starBtn4 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
        [self.starBtn5 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
    }else if(rate == 3){
        [self.starBtn1 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn2 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn3 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn4 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
        [self.starBtn5 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
    }else if(rate == 4){
        [self.starBtn1 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn2 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn3 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn4 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn5 setImage:[UIImage imageNamed:@"starEmptySmall"] forState:UIControlStateNormal];
    }else if(rate == 5){
        [self.starBtn1 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn2 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn3 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn4 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
        [self.starBtn5 setImage:[UIImage imageNamed:@"starSmall"] forState:UIControlStateNormal];
    }
    
    
    
    
    
    
    
    
    
    
    
}









-(NSMutableAttributedString *)mainStr:(NSString *)mainStr withSubStr:(NSString *)subStr{
    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:mainStr];
    NSRange rangeOfSubstring = [mainStr rangeOfString:subStr];
    [hogan addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]
                  range:rangeOfSubstring];
    return hogan;
}



-(NSDictionary *)setParametersForStarter{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  };
    return dictForuser;
}




-(void)Logout{
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web LogoutDriver:[self setParametersForLogout]
              success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         //if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [Theme ClearUserDetails];
         [testAppDelegate logoutXmpp];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         self.view.userInteractionEnabled=YES;
         //         }else{
         //
         //             [self.view makeToast:kErrorMessage];
         //         }
     }
              failure:^(NSError *error)
     {
         
         [self stopActivityIndicator];
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [testAppDelegate logoutXmpp];
         [Theme ClearUserDetails];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         self.view.userInteractionEnabled=YES;
         [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersForLogout{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"device":@"IOS"
                                  };
    return dictForuser;
}
- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
            NSString * rideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
            NSString * CurrId=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict objectForKey:@"key4"]]];
            NSString * fareStr=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            NSString * fareAmt=[NSString stringWithFormat:@"%@ %@",CurrId,fareStr];
            
            [self stopActivityIndicator];
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:rideId];
            [objReceiveCashVC setFareAmt:fareAmt];
            
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
    [testAppDelegate getInitialDatas];
    [self settext];
    
    [[ATAppUpdater sharedUpdater] showUpdateWithForce];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
   
    

    currentMapView.delegate = self;
    self.currentMapView.showsUserLocation = YES;
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = jjLocManager.currentLocation.coordinate.latitude;
    region.center.longitude = jjLocManager.currentLocation.coordinate.longitude;
    region.span.latitudeDelta = 0.02f;
    region.span.longitudeDelta = 0.02f;

    [currentMapView setRegion:region animated:YES];
    CLLocationCoordinate2D newCenter = jjLocManager.currentLocation.coordinate;
    newCenter.latitude -= currentMapView.region.span.latitudeDelta * 0.35;
    [self.currentMapView setCenterCoordinate:newCenter animated:YES];
    
    
    [self loadData];
    [self loadGoogleMap];
    
    //starterScrollView.contentSize=CGSizeMake(starterScrollView.frame.size.width, tipsView.frame.origin.y+tipsView.frame.size.height+36);
    
    starterScrollView.contentSize=CGSizeMake(starterScrollView.frame.size.width, tipsView.frame.origin.y+tipsView.frame.size.height+200);
}


-(void)loadGoogleMap{
    if(TARGET_IPHONE_SIMULATOR)
    {
        Camera = [GMSCameraPosition cameraWithLatitude:13.0827
                                             longitude:80.2707
                                                  zoom:18];
    }else{
        Camera = [GMSCameraPosition cameraWithLatitude:jjLocManager.currentLocation.coordinate.latitude
                                             longitude:jjLocManager.currentLocation.coordinate.longitude
                                                  zoom:17 ];
//                                               bearing:30
//                                          viewingAngle:0];
        
        // new change  bearing and view angle
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
    GoogleMap = [GMSMapView mapWithFrame:CGRectMake(currentMapView.frame.origin.x, currentMapView.frame.origin.y-100, currentMapView.frame.size.width+50, currentMapView.frame.size.height+50) camera:Camera];
    
    NSString * youhere=JJLocalizedString(@"You_are_Here", nil);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    GoogleMap.userInteractionEnabled=YES;
    GoogleMap.delegate = self;
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle-silver" withExtension:@"json"];
    NSError *error;
    
    //     Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    GoogleMap.mapStyle = style;
    
    [currentMapView addSubview:GoogleMap];
    });
    
}
    
-(void)applicationLanguageChangeNotification:(NSNotification*)notification{
    [self settext];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    static NSString * const identifier = @"MyCustomAnnotation";
    
    MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (annotationView)
    {
        annotationView.annotation = annotation;
    }
    else
    {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:identifier];
    }
    
    annotationView.canShowCallout = NO;
    annotationView.image = [UIImage imageNamed:JJLocalizedString(@"currentLoc_Img", nil)];
    
    return annotationView;
}

-(void)viewDidAppear:(BOOL)animated{
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
}
-(void)changeLocation:(NSNotification*)notification
{
    if(self.view.window){
        CLLocationCoordinate2D newCenter = jjLocManager.currentLocation.coordinate;
        newCenter.latitude -= currentMapView.region.span.latitudeDelta * 0.35;
        [self.currentMapView setCenterCoordinate:newCenter animated:YES];
    }
}




-(void)setDatasOfUser{
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        NSString * imgstr=[myDictionary objectForKey:@"driver_image"];
        [userImageView sd_setImageWithURL:[NSURL URLWithString:imgstr] placeholderImage:[UIImage imageNamed:@"PlaceHolderImg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];

        
        userNameLbl.text=[myDictionary objectForKey:@"driver_name"];
        userVehModelLbl.text=[myDictionary objectForKey:@"vehicle_model"];
       _numberPlateLbl.text =  [myDictionary objectForKey:@"vehicle_number"];
        
        AppDelegate* delegate = [UIApplication sharedApplication].delegate;
        delegate.vehicleNumber = [myDictionary objectForKey:@"vehicle_number"];
        
        _ratingLbl.text = @"4.9" ;
        catgLbl.text=@"";
        
       
        _driverImageview.layer.cornerRadius = _driverImageview.frame.size.height/2;
        _driverImageview.layer.masksToBounds = YES;
        _driverImageview.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _driverImageview.layer.borderWidth = 1;
        
        _ratingLbl.layer.cornerRadius = _ratingLbl.frame.size.height/2;
        _ratingLbl.layer.masksToBounds = YES;
        _ratingLbl.layer.borderColor = [UIColor whiteColor].CGColor;
        _ratingLbl.layer.borderWidth = 2;
        
    }
}
-(void)setFont{

    _headerLbl=[Theme setHeaderFontForLabel:_headerLbl];
    _imgCircleLbl.layer.cornerRadius=_imgCircleLbl.frame.size.width/2;
    _imgCircleLbl.layer.borderColor=SetSubColor.CGColor;
    _imgCircleLbl.layer.borderWidth=1;
    _imgCircleLbl.layer.masksToBounds=YES;
    userImageView.layer.cornerRadius=userImageView.frame.size.width/2;
    userImageView.layer.masksToBounds=YES;
    //    basicInfoView.frame=CGRectMake(basicInfoView.frame.origin.x, userImageView.frame.origin.y+userImageView.frame.size.height+30, basicInfoView.frame.size.width, basicInfoView.frame.size.height);
    //userNameLbl=[Theme setBoldFontForLabel:userNameLbl];
     userVehModelLbl=[Theme setNormalFontForLabel:userVehModelLbl];
//    goOnlineBtn=[Theme setBoldFontForButton:goOnlineBtn];
    // [goOnlineBtn setBackgroundColor:SetBlueColor];
    goOnlineBtn.layer.cornerRadius=18;
    goOnlineBtn.layer.masksToBounds=YES;
    goOnlineBtn.titleLabel.font = [UIFont systemFontOfSize:23];
    
//    tipsView.layer.cornerRadius=5;
//    tipsView.layer.masksToBounds=YES;
//    tipsView.layer.borderColor = DarkskyBlue.CGColor;
//    tipsView.layer.borderWidth = 1;
//
//    lastTripView.layer.cornerRadius=5;
//    lastTripView.layer.masksToBounds=YES;
//    lastTripView.layer.borderColor = DarkskyBlue.CGColor;
//    lastTripView.layer.borderWidth = 1;
//
//
//    TodaysTotalView.layer.cornerRadius=5;
//    TodaysTotalView.layer.masksToBounds=YES;
//    TodaysTotalView.layer.borderColor = DarkskyBlue.CGColor;
//    TodaysTotalView.layer.borderWidth = 1;
    
    userNameLbl.layer.cornerRadius = userNameLbl.frame.size.height/2;
    userNameLbl.layer.masksToBounds = true;
    
//    firstView.layer.cornerRadius = 15;
    
//    CAShapeLayer * maskLayer1 = [CAShapeLayer layer];
//    maskLayer1.path = [UIBezierPath bezierPathWithRoundedRect: firstView.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){15.0, 15.}].CGPath;
//
//    firstView.layer.mask = maskLayer1;
//
//     CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: tipsView.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){15.0, 15.}].CGPath;
//
//    tipsView.layer.mask = maskLayer;
    
    
    
    
   /* if(IS_IPHONE_6P){
        lastTripView.frame=CGRectMake(lastTripView.frame.origin.x, 240, lastTripView.frame.size.width, lastTripView.frame.size.height);
        TodaysTotalView.frame=CGRectMake(TodaysTotalView.frame.origin.x, lastTripView.frame.origin.y+lastTripView.frame.size.height+10, TodaysTotalView.frame.size.width, TodaysTotalView.frame.size.height);
        tipsView.frame=CGRectMake(tipsView.frame.origin.x, TodaysTotalView.frame.origin.y+TodaysTotalView.frame.size.height+10, tipsView.frame.size.width, tipsView.frame.size.height);
       
        _lTripLbl.frame = CGRectMake(lastTripView.frame.origin.x, lastTripView.frame.origin.y, lastTripView.frame.size.width, 29);
        _tEarningsLbl.frame = CGRectMake(TodaysTotalView.frame.origin.x, TodaysTotalView.frame.origin.y, TodaysTotalView.frame.size.width, 29);
        _tTipsLbl.frame = CGRectMake(tipsView.frame.origin.x, tipsView.frame.origin.y, tipsView.frame.size.width, 29);
        
        _lTripLbl.clipsToBounds = NO;
        _tEarningsLbl.clipsToBounds = NO;
        _tTipsLbl.clipsToBounds = NO;
        
    }*/
    //else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
        [self setMaskTo:_lTripLbl byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)];
        [self setMaskTo:_tEarningsLbl byRoundingCorners:(UIRectCornerTopRight| UIRectCornerTopLeft )];
        [self roundCornersOnView:tTipsBackView onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:8.0f];
        [self roundCornersOnView:_tTipsLbl onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:8.0f];
         });
    //}
}



-(void)settext{
    
    [goOnlineBtn setTitle:JJLocalizedString(@"Go_Online", nil) forState:UIControlStateNormal];
    _lTripLbl.text=JJLocalizedString(@"     LAST TRIP", nil);
    _nEarningLbl.text=JJLocalizedString(@"NET Earnings", nil);
//    _tEarningsLbl.text=JJLocalizedString(@"     TODAY'S EARNINGS", nil);
    _eNetLbl.text=JJLocalizedString(@"Estimated Net", nil);
    _tTipsLbl.text=JJLocalizedString(@"     TODAY'S TIPS", nil);
    _tLbl.text=JJLocalizedString(@"Tips", nil);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)didClickGoOnlineBtn:(id)sender {
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        [self GoOnline];
    }else{
        OpinionzAlertView *alert = [[OpinionzAlertView alloc] initWithTitle:JJLocalizedString(@"Oops", nil)
                                                                    message:JJLocalizedString(@"Sorry_cant_able", nil)
                                                          cancelButtonTitle:JJLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
        alert.iconType = OpinionzAlertIconWarning;
        [alert show];
    }
    
    //    OpenInMapsViewController * objOpenInMaps=[self.storyboard instantiateViewControllerWithIdentifier:@"OpenInMapsVCSID"];
    //   [self.navigationController pushViewController:objOpenInMaps animated:true];
}
-(void)enableLoginBtn{
    goOnlineBtn.userInteractionEnabled=YES;
}
-(void)GoOnline{
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    goOnlineBtn.userInteractionEnabled=NO;
    [self goOnlineActivityIndicator:YES];
    [web UserGoOfflineOnLineUrl:[self setParametersUpdateUserOnOff:@"Yes"]
                        success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             [Theme SaveUSerISOnline:@"1"];
             ///// 1234changed
             NSString *login_id = [responseDictionary valueForKey:@"login_id"];
              NSString *login_time = [responseDictionary valueForKey:@"login_time"];
             
             [[NSUserDefaults standardUserDefaults] setObject:login_id forKey:@"login_id"];
            [[NSUserDefaults standardUserDefaults] setObject:login_time forKey:@"login_time"];
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             NSString *login_id1 = [[NSUserDefaults standardUserDefaults]
                                     stringForKey:@"login_id"];
            
             /////
             HomeViewController * objHomeVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVCSID"];
             [objHomeVC setVehicleNumber:vehNumber];
             
             
             
             
             
             [self.navigationController pushViewController:objHomeVC animated:YES];
             goOnlineBtn.userInteractionEnabled=YES;
         }else{
             [self stopActivityIndicator];
             [self.view makeToast:kErrorMessage];
             goOnlineBtn.userInteractionEnabled=YES;
         }
         
         
     }
                        failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         goOnlineBtn.userInteractionEnabled=YES;
         
     }];
}

-(void)goOnlineActivityIndicator:(BOOL)isShow {
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//
//
//        custIndicatorView.center =self.view.center;
////
////        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading_location.gif"]];
////        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
////        [custIndicatorView addSubview:imageView];
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];
        
        
        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];
        
    }
}

-(void)showActivityIndicator:(BOOL)isShow {
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
     custIndicatorView=nil;
    
    [self.container removeFromSuperview];

}
-(NSDictionary *)setParametersUpdateUserOnOff:(NSString *)OnOffStr{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"availability":OnOffStr
                                  };
    return dictForuser;
}



- (IBAction)didClickMenuBtn:(id)sender {
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}
#pragma mark -
#pragma mark ITRAirSideMenu Delegate

- (void)sideMenu:(ITRAirSideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(ITRAirSideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(ITRAirSideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(ITRAirSideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView==starterScrollView){
        CGFloat offset=starterScrollView.contentOffset.y;
        CGFloat percentage=offset/184;
        CGFloat value=184*percentage; // negative when scrolling up more than the top
        // driven animation
        currentMapView.frame=CGRectMake(0, value, currentMapView.bounds.size.width, 184-value);
    }
}
-(void)loadRating:(NSString *)ratingCount{
    float rateCount=[ratingCount floatValue];
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"StarEmpty"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"StarFill"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 1;
    self.ratingView.rating = rateCount;
    self.ratingView.editable = NO;
    self.ratingView.halfRatings = YES;
    self.ratingView.floatRatings = YES;
}
- (IBAction)logoutYesAction:(id)sender {
    [self Logout];
}


- (IBAction)logoutNoAction:(id)sender {
    self.logoutView.hidden = true;
    //[self.logoutView removeFromSuperview];
   
}

@end
    
    
