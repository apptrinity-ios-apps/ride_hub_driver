//
//  HomeViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/24/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()<RideAcceptDelegate,NewJobsDelegate>{
    int ZoomPoint;
  
}

@end

@implementation HomeViewController{
    JJLocationManager * jjLocManager;
}



@synthesize mapView,Camera,GoogleMap,DriverNameHeaderLbl,
goOffLineBtn,marker,custIndicatorView,pendingLbl,tripId,VehicleNumber,custIndicatorViewUrl,isMapZoomed,ReservedJobId,isnotVerified,dotLine;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(jjLocManager==nil){
        jjLocManager=[JJLocationManager sharedManager];
    }
    ZoomPoint=17;
   
    [self setFont];
   
    marker=[[GMSMarker alloc]init];
    [self loadGoogleMap];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotifWhenQuit
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTableRefreshNotification:)
                                                 name:kDriverReceiveNotif
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNewJobNotif:)
                                                 name:kNewTripKey
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeLocation:)
                                                 name:kJJLocationManagerNotificationLocationUpdatedName
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivePendingJobNotif:)
                                                 name:kDriverNotifForPendingAndVerifyStatus
                                               object:nil];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveToPendingTransaction)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [pendingLbl addGestureRecognizer:tapGestureRecognizer];
    pendingLbl.userInteractionEnabled = YES;
    [goOffLineBtn setTitle:JJLocalizedString(@"Go_Offline", nil) forState:UIControlStateNormal];
   // goOffLineBtn=[Theme setBoldFontForButton:goOffLineBtn];
   
    goOffLineBtn.layer.cornerRadius = goOffLineBtn.frame.size.height/2;
    goOffLineBtn.layer.masksToBounds = YES;
    
    [self setShadow:goOffLineBtn];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)changeLocation:(NSNotification*)notification
{
    if(self.view.window){
        [self stopActivityIndicator];
        if(isMapZoomed==NO){
            Camera = [GMSCameraPosition cameraWithLatitude:jjLocManager.currentLocation.coordinate.latitude
                                                 longitude:jjLocManager.currentLocation.coordinate.longitude
                                                      zoom:ZoomPoint ];
//                                                   bearing:30
//                                              viewingAngle:45];
            
            // new change  bearing and view angle
            
            
            [GoogleMap animateToCameraPosition:Camera];
        }
        float totalDistTowardsDest= GMSGeometryDistance(jjLocManager.currentLocation.coordinate, jjLocManager.previousLocation.coordinate);
        if(totalDistTowardsDest>=5){
            [CATransaction begin];
            [CATransaction setAnimationDuration:2.0];
            
            marker.position = CLLocationCoordinate2DMake(jjLocManager.currentLocation.coordinate.latitude,jjLocManager.currentLocation.coordinate.longitude);
            marker.icon = [UIImage imageNamed:@"cartopview"];
            float angel = [Theme DegreeBearing:jjLocManager.currentLocation locationB:jjLocManager.previousLocation];
            
             marker.groundAnchor = CGPointMake(0.5, 0.5);
             marker.rotation = angel ;
            
            [CATransaction commit];
        }
       
        NSString * youhere=JJLocalizedString(@"You_are_Here", nil);
        
        marker.snippet = [NSString stringWithFormat:@"%@",youhere];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = GoogleMap;
    }
}


- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
            NSString * rideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
            NSString * CurrId=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict objectForKey:@"key4"]]];
            NSString * fareStr=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            NSString * fareAmt=[NSString stringWithFormat:@"%@ %@",CurrId,fareStr];
            
            [self stopActivityIndicator];
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:rideId];
            [objReceiveCashVC setFareAmt:fareAmt];
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)moveToPendingTransaction{
    if(isnotVerified==NO){
    TripDetailViewController * objTripDetailVc=[self.storyboard instantiateViewControllerWithIdentifier:@"TripDetailVCSID"];
    [objTripDetailVc setTripId:tripId];
    [self.navigationController pushViewController:objTripDetailVc animated:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    
    pendingLbl.hidden=YES;
    [self updateLoc:jjLocManager.currentLocation];
    
    [self loadGoogleMap];
    
    
}


-(void)viewDidAppear:(BOOL)animated{
 
    [self loadGoogleMap];
    
    
}

- (void)receivePendingJobNotif:(NSNotification *) notification
{
    NSLog(@"Received APNS with userInfo %@", notification.userInfo);
    NSDictionary * responseDictionary=notification.userInfo;
    if(self.view.window){
        if(responseDictionary.count>0){
            if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                NSString * str=[Theme checkNullValue:responseDictionary[@"response"][@"availability"]];
                NSString * verifyStr=[Theme checkNullValue:responseDictionary[@"response"][@"verify_status"]];
                NSString * showMsgStr=[Theme checkNullValue:responseDictionary[@"response"][@"errorMsg"]];
                if(![verifyStr isEqualToString:@"Yes"]){
                    isnotVerified=YES;
                    if(showMsgStr.length>0){
                        pendingLbl.text=showMsgStr;
                    }else{
                        pendingLbl.text=JJLocalizedString(@"You_are_not_yet_verified", nil);
                    }
                    
                    pendingLbl.hidden=NO;
                    
                    [pendingLbl setBackgroundColor:setRedColor];
                    pendingLbl.alpha=0.7;
                    
                }else if ([str isEqualToString:@"Unavailable"]){
                    isnotVerified=NO;
                    if(showMsgStr.length>0){
                        pendingLbl.text=showMsgStr;
                    }
                    tripId=[Theme checkNullValue:responseDictionary[@"response"][@"ride_id"]];
                    pendingLbl.hidden=NO;
                    [pendingLbl setBackgroundColor:[UIColor blackColor]];
                    pendingLbl.alpha=0.6;
                }else{
                    pendingLbl.text=@"";
                    isnotVerified=NO;
                    pendingLbl.hidden=YES;
                    [self stopActivityIndicator];
                }
                
            }
        }
    }
}
- (void)receiveNewJobNotif:(NSNotification *) notification
{
    NSLog(@"Received APNS with userInfo %@", notification.userInfo);
    NSDictionary * dict=notification.userInfo;
    if(self.view.window){
        RiderRecords * objRideAcceptRec=[[RiderRecords alloc]init];
        objRideAcceptRec.RideId=[Theme checkNullValue:[dict objectForKey:@"key6"]];
        ReservedJobId=objRideAcceptRec.RideId;
        objRideAcceptRec.userImage=[Theme checkNullValue:[dict objectForKey:@"key4"]];
        objRideAcceptRec.userName=[Theme checkNullValue:[dict objectForKey:@"key1"]];
        
        objRideAcceptRec.userTime=[Theme checkNullValue:[dict objectForKey:@"key10"]];
        objRideAcceptRec.userPhoneNumber=[Theme checkNullValue:[dict objectForKey:@"key3"]];
        objRideAcceptRec.userReview=[Theme checkNullValue:[dict objectForKey:@"key5"]];
        
        objRideAcceptRec.userLocation=[Theme checkNullValue:[dict objectForKey:@"key7"]];
        objRideAcceptRec.userLattitude=[Theme checkNullValue:[dict objectForKey:@"key8"]];
        objRideAcceptRec.userLongitude=[Theme checkNullValue:[dict objectForKey:@"key9"]];
        
        if(objRideAcceptRec.RideId.length>0){
            NewJobViewController * objJobPopup=[self.storyboard instantiateViewControllerWithIdentifier:@"NewJobVCSID"];
            objJobPopup.delegate=self;
            
            [objJobPopup setObjRiderRecs:objRideAcceptRec];
            if ([[UIDevice currentDevice].systemVersion integerValue] >= 8)
            {
                //For iOS 8
                objJobPopup.providesPresentationContextTransitionStyle = YES;
                objJobPopup.definesPresentationContext = YES;
                objJobPopup.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            }
            else
            {
                //For iOS 7
                objJobPopup.modalPresentationStyle = UIModalPresentationCurrentContext;
            }
            [self presentViewController:objJobPopup animated:NO completion:nil];
        }
    }
}


-(void)setFont{
    NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
    DriverNameHeaderLbl.text=[Theme checkNullValue:[NSString stringWithFormat:@"%@\n%@",[myDictionary objectForKey:@"driver_name"],[Theme checkNullValue:VehicleNumber]]];
    
    
    
//    DriverNameHeaderLbl=[Theme setHeaderFontForLabel:DriverNameHeaderLbl];
   // goOffLineBtn=[Theme setBoldSmallFontForButton:goOffLineBtn];
    pendingLbl=[Theme setHeaderFontForLabel:pendingLbl];
    
    
//
     _trafficBtn.layer.cornerRadius=4;
    _trafficBtn.layer.borderWidth=1;
    _trafficBtn.layer.borderColor=[UIColor darkGrayColor].CGColor;
     _trafficBtn.layer.masksToBounds = YES;
    
    
    
    
}


- (void)receiveTableRefreshNotification:(NSNotification *) notification
{
     NSLog(@"Received APNS with userInfo %@", notification.userInfo);
    NSDictionary * dict=notification.userInfo;
    if(self.view.window){
        RideAcceptViewController *  objRideAcceptVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RideAcceptVCSID"];
        
        RideAcceptRecord * objRideAcceptRec=[[RideAcceptRecord alloc]init];
        objRideAcceptRec.LocationName=[Theme checkNullValue:[dict objectForKey:@"key3"]];
        objRideAcceptRec.RideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
        objRideAcceptRec.expiryTime=[Theme checkNullValue:[dict objectForKey:@"key2"]];
        objRideAcceptRec.DropLocation = [Theme checkNullValue:[dict objectForKey:@"key4"]];
        objRideAcceptRec.rideTag=1;
        NSString * pickuprequest=JJLocalizedString(@"PickUp_Request", nil);
        
        objRideAcceptRec.headerTxt=[Theme checkNullValue:[NSString stringWithFormat:@"%@ 1",pickuprequest]];
        
        NSInteger expValue=[objRideAcceptRec.expiryTime integerValue];
        if(expValue<=5){
            objRideAcceptRec.expiryTime=@"15";
        }
        objRideAcceptRec.currentTimer=0;
        objRideAcceptVc.requestArray=[[NSMutableArray alloc]init];
        [objRideAcceptVc.requestArray addObject:objRideAcceptRec];
       
        objRideAcceptVc.delegate=self;
        if(objRideAcceptVc!=nil){
            [self.navigationController pushViewController:objRideAcceptVc animated:NO];
        }
    }
}

-(NSDictionary *)setParametersUpdateUserOnOff:(NSString *)OnOffStr{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
        
    }
    
    NSString *login_id = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"login_id"];
    NSString *login_time = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"login_time"];
    
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"availability":OnOffStr,
                                  @"login_id": login_id,
                                  @"login_time": login_time
                                  };
    return dictForuser;
}

-(void)loadGoogleMap{
    
    dispatch_async (dispatch_get_main_queue(), ^
    {
    
    
    if(TARGET_IPHONE_SIMULATOR)
    {
        Camera = [GMSCameraPosition cameraWithLatitude:13.0827
                                             longitude:80.2707
                                                  zoom:17];
    }else{
        Camera = [GMSCameraPosition cameraWithLatitude:jjLocManager.currentLocation.coordinate.latitude
                                             longitude:jjLocManager.currentLocation.coordinate.longitude
                                                  zoom:17 ];
//                                               bearing:30
//                                        viewingAngle:0];
        
        // new change  bearing and view angle
    }
    
    GoogleMap = [GMSMapView mapWithFrame:CGRectMake(mapView.frame.origin.x, mapView.frame.origin.y, mapView.frame.size.width+100, mapView.frame.size.height+100) camera:Camera];
    marker.position = Camera.target;
   
    marker.icon = [UIImage imageNamed:@"cartopview"];
    NSString * youhere=JJLocalizedString(@"You_are_Here", nil);
    marker.snippet = [NSString stringWithFormat:@"%@\n%@",DriverNameHeaderLbl.text,youhere];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = GoogleMap;
   
    GoogleMap.userInteractionEnabled=YES;
    GoogleMap.delegate = self;
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle-silver" withExtension:@"json"];
    NSError *error;
    
    //     Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    //GoogleMap.mapType = kGMSTypeTerrain;
    GoogleMap.mapStyle = style;

    [mapView addSubview:GoogleMap];
    if([Theme getTrafficStatus]){
        GoogleMap.trafficEnabled=YES;
        [_trafficBtn setImage:[UIImage imageNamed:@"TrafficOn"] forState:UIControlStateNormal];
        [_trafficBtn setBackgroundColor:SetThemeColor];
    }else{
        GoogleMap.trafficEnabled=NO;
        [_trafficBtn setImage:[UIImage imageNamed:@"TrafficOff"] forState:UIControlStateNormal];
        [_trafficBtn setBackgroundColor:setLightGray];
    }
    
    
    });
    
//    
//    GMSMutablePath *poly = [GMSMutablePath path];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.054894, 80.237541)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.055145, 80.243034)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.052365, 80.245319)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.050776, 80.244439)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.050745, 80.242755)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.051978, 80.241339)];
//    [poly addCoordinate:CLLocationCoordinate2DMake(13.054288, 80.241543)];
//    
//    GMSPolygon *polygon = [GMSPolygon polygonWithPath:poly];
//    polygon.fillColor = [UIColor colorWithRed:0 green:0.25 blue:0 alpha:0.3];
//    polygon.strokeColor = [UIColor greenColor];
//    polygon.strokeWidth = 5;
//    polygon.map = self.GoogleMap;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didClickGoOfflineBtn:(id)sender {
    goOffLineBtn.userInteractionEnabled=NO;
    [self showActivityIndicatorForURl:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web UserGoOfflineOnLineUrl:[self setParametersUpdateUserOnOff:@"No"]
                        success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicatorForUrl];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
            //[self.view makeToast:@"you are offline"];
             [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login_id"];
              [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login_time"];
             [[NSUserDefaults standardUserDefaults] synchronize];

             
         }else{
            // [self.view makeToast:@"Some error occured"];
         }
         [Theme SaveUSerISOnline:@"0"];
          [self.navigationController popViewControllerAnimated:YES];
          goOffLineBtn.userInteractionEnabled=YES;
     }
                        failure:^(NSError *error)
     {
         [self stopActivityIndicatorForUrl];
         [Theme SaveUSerISOnline:@"0"];
         [self.navigationController popViewControllerAnimated:YES];
         [self.view makeToast:kErrorMessage];
           goOffLineBtn.userInteractionEnabled=YES;
         
     }];
  //  [self navigateToLatitude:12.9856 longitude:80.2614 ];
}
//- (void) navigateToLatitude:(double)latitude
//                  longitude:(double)longitude
//{
//    if ([[UIApplication sharedApplication]
//         canOpenURL:[NSURL URLWithString:@"waze://"]]) {
//        
//        // Waze is installed. Launch Waze and start navigation
//        NSString *urlStr =
//        [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes",
//         latitude, longitude];
//        
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
//        
//    } else {
//        
//        // Waze is not installed. Launch AppStore to install Waze app
//        [[UIApplication sharedApplication] openURL:[NSURL
//                                                    URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
//    }
//}



-(void)viewWillDisappear:(BOOL)animated{
   
}

- (IBAction)didClickUpdateLocation:(id)sender {
    
    //GoogleMap.myLocationEnabled = YES;
    [self showActivityIndicator:YES];
    isMapZoomed=NO;
    [jjLocManager refreshDriverCurrentLocation];
    [self loadGoogleMap];
    
   
    
    
}


-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    ZoomPoint=position.zoom;
    
}
-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    
    if(gesture==YES){
        isMapZoomed=YES;
    }else{
         isMapZoomed=NO;
    }
}

-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];
        
        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];
        
    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}



-(void)showActivityIndicatorForURl:(BOOL)isShow{
    if(isShow==YES){
        if(custIndicatorViewUrl==nil){
            custIndicatorViewUrl = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
            
        }
        custIndicatorViewUrl.center =self.view.center;
        [custIndicatorViewUrl startAnimating];
        [self.view addSubview:custIndicatorViewUrl];
        [self.view bringSubviewToFront:custIndicatorViewUrl];
    }
}
-(void)stopActivityIndicatorForUrl{
    [custIndicatorViewUrl stopAnimating];
    custIndicatorViewUrl=nil;
}


-(void)updateLoc:(CLLocation *)loc{
    
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web UpdateUserLocation:[self setParametersUpdateLocation:loc]
                    success:^(NSMutableDictionary *responseDictionary)
     {
         
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             NSString * str=[Theme checkNullValue:responseDictionary[@"response"][@"availability"]];
             NSString * verifyStr=[Theme checkNullValue:responseDictionary[@"response"][@"verify_status"]];
              NSString * showMsgStr=[Theme checkNullValue:responseDictionary[@"response"][@"errorMsg"]];
             if(![verifyStr isEqualToString:@"Yes"]){
                 isnotVerified=YES;
                 if(showMsgStr.length>0){
                      pendingLbl.text=showMsgStr;
                 }else{
                     pendingLbl.text=JJLocalizedString(@"You_are_not_yet_verified", nil);
                 }
                 
                 pendingLbl.hidden=NO;
                
                 [pendingLbl setBackgroundColor:setRedColor];
                 pendingLbl.alpha=0.7;
                 
             }else if ([str isEqualToString:@"Unavailable"]){
                 isnotVerified=NO;
                 if(showMsgStr.length>0){
                     pendingLbl.text=showMsgStr;
                 }
                 tripId=[Theme checkNullValue:responseDictionary[@"response"][@"ride_id"]];
                 pendingLbl.hidden=NO;
                 [pendingLbl setBackgroundColor:[UIColor blackColor]];
                 pendingLbl.alpha=0.6;
             }else{
                 pendingLbl.text=@"";
                 isnotVerified=NO;
                 pendingLbl.hidden=YES;
                 [self stopActivityIndicator];
             }
             
         }
     }
                    failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         
         //      [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersUpdateLocation:(CLLocation *)loc{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"latitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.latitude]],
                                  @"longitude":[Theme checkNullValue:[NSString stringWithFormat:@"%f",loc.coordinate.longitude]],
                                  @"ride_id":@""
                                  };
    return dictForuser;
}

-(void)ridecancelled{
    [self.view makeToast:@"You_are_too_late"];
}
- (void) didClickNewJobsOk{
    if(ReservedJobId.length>0){
        tripId=ReservedJobId;
        [ self moveToPendingTransaction];
    }
}
-(double) DegreeBearing:(CLLocation*) A locationB: (CLLocation*)B{
    
  
//    double lant1 = [self ToRad:A.coordinate.latitude];
//    double lont1 = [self ToRad:A.coordinate.longitude];
//
//    double lant2 = [self ToRad:B.coordinate.latitude];
//    double lont2 = [self ToRad:B.coordinate.longitude];
//
//    double dLon = lont2-lont1;
//
//    double y = sin(dLon)*cos(lant2);
//    double x = cos(lant1)*sin(lant2) - sin(lant1)*cos(lant2)*cos(dLon);
//    double radianBearing = atan2(y, x);
//    if (radianBearing<0.0){
//        radianBearing +=2*M_PI;
//    }
//
//    return [self ToDegrees:radianBearing];
//
    
//    float lat1 = A.coordinate.latitude;
//    float lon1 = A.coordinate.longitude;
//    float lat2 = B.coordinate.latitude;
//    float lon2 = B.coordinate.longitude;
//    float dLon = lon1 - lon2;
//    float x = cos(dLon) * sin(lat2);
//    float y = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
//    float radiansBearing = atan2(x, y);
//    return radiansBearing;
//
    float deltaLongitude = B.coordinate.longitude - A.coordinate.longitude;
    float deltaLatitude = B.coordinate.latitude - A.coordinate.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)
        return angle;
    else if (deltaLongitude < 0)
        return angle + M_PI;
    else if (deltaLatitude < 0)
        return M_PI;
    
    return 0.0f;
    
    
//    double dlon = [self ToRad:(B.coordinate.longitude - A.coordinate.longitude)];
//    double dPhi = log(tan([self ToRad:B.coordinate.latitude ] / 2 + M_PI / 4) / tan([self ToRad:A.coordinate.latitude] / 2 + M_PI / 4));
//    if  (fabs(dlon) > M_PI){
//        dlon = (dlon > 0) ? (dlon - 2*M_PI) : (2*M_PI + dlon);
//    }
//
//    return [self ToBearing:atan2(dlon, dPhi)];
}

-(double) ToRad: (double)degrees{
    return degrees*(M_PI/180);
}

-(double) ToBearing:(double)radians{
    return [self ToDegrees:radians] + 360 % 360;
}

-(double) ToDegrees: (double)radians{
    return radians * 180 / M_PI;
}
- (IBAction)didClickTrafficBtn:(id)sender {
    
    UIButton * btn=(UIButton *)sender;
    if(GoogleMap.trafficEnabled==YES){
        GoogleMap.trafficEnabled=NO;
        [Theme saveTrafficEnabled:@"NO"];
        [btn setImage:[UIImage imageNamed:@"TrafficOff"] forState:UIControlStateNormal];
        [btn setBackgroundColor:setLightGray];
    }else{
        GoogleMap.trafficEnabled=YES;
         [Theme saveTrafficEnabled:@"YES"];
        [btn setImage:[UIImage imageNamed:@"TrafficOn"] forState:UIControlStateNormal];
        [btn setBackgroundColor:SetThemeColor];
    }
}
@end
