//
//  FareSummaryViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/1/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "FareSummaryViewController.h"
#import "AJRBackgroundDimmer.h"
#import "DectarCustomColor.h"
#import "Theme.h"
@interface FareSummaryViewController (){
    AJRBackgroundDimmer *backgroundGradientView;
}

@end

@implementation FareSummaryViewController
@synthesize summaryView,headerLbl,objFareRecs,fareLbl,timeTakenHeaderLbl,waitTimeHeaderLbl,distanceLbl,timeLbl,waitLbl,paymentBtn,requestCashBtn,okBtn,cancelBtn,isCloseEnabled,subview1,subview2,subview3,paymentView,requestCashView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self selfSetFont];
    [self setDatasToSummaryView];
    [self setShadow:subview1];
    [self setShadow:subview2];
    [self setShadow:subview3];
    [self setShadow:summaryView];
    [self setShadow:paymentView];
    [self setShadow:requestCashView];
    
    summaryView.layer.cornerRadius = 10;
    headerLbl.layer.cornerRadius = 10;
    headerLbl.layer.masksToBounds = YES;
    
    
    if (self.allowSwipeToDismiss) {
        //Add a swipe gesture recognizer to dismiss the view
        UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(close:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
        [self.view addGestureRecognizer:recognizer];
    }
    if (isCloseEnabled==YES) {
        cancelBtn.hidden=YES;
    }else{
        cancelBtn.hidden=YES;
    }
    [self performSelector:@selector(enableRideBtn) withObject:nil afterDelay:30];
}
-(void)enableRideBtn{
    requestCashBtn.userInteractionEnabled=YES;
   // paymentBtn.userInteractionEnabled=YES;
}
-(void)viewWillAppear:(BOOL)animated{
    requestCashBtn.userInteractionEnabled=YES;
    paymentBtn.userInteractionEnabled=YES;
    [self.view bringSubviewToFront:cancelBtn];
    if(isCloseEnabled==YES){
        cancelBtn.hidden=NO;
    }
}
-(void)viewWillLayoutSubviews{

    
    
    // self.subView.layer.masksToBounds = NO;
    
    // self.subview1.layer.masksToBounds = YES;
//    self.subview1 = (UIView *)[self roundCornersOnView:self.subview1 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:20.0];
//
//    self.distanceLbl = (UILabel *)[self roundCornersOnView:self.distanceLbl onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:NO radius:20.0];
//
//    //self.subview3.layer.masksToBounds = YES;
//    self.subview3 = (UIView *)[self roundCornersOnView:self.subview3 onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:20.0];
//
//    self.waitLbl = (UILabel *)[self roundCornersOnView:self.waitLbl onTopLeft:NO topRight:NO bottomLeft:NO bottomRight:YES radius:20.0];
//
//    self.subView11 = (UIView *)[self roundCornersOnView:self.subview3 onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:YES radius:10.0];
    
    
   // paymentBtn=[Theme setBoldFontForButton:paymentBtn];
    okBtn=[Theme setBoldFontForButton:okBtn];
   // [paymentBtn setTitle:JJLocalizedString(@"Request_Payment", nil) forState:UIControlStateNormal];
   // [paymentBtn setTitleColor:SetThemeColor forState:UIControlStateNormal];
   // paymentBtn.layer.cornerRadius = 5;
    
   // self.paymentView.layer.masksToBounds=YES;
  //  self.paymentView = (UIButton *)[self roundCornersOnView:self.paymentView onTopLeft:YES  topRight:NO bottomLeft:YES bottomRight:NO radius:15.0];
    
    
   // [requestCashBtn setTitle:JJLocalizedString(@"Receive_Cash", nil) forState:UIControlStateNormal];
   // requestCashBtn=[Theme setBoldFontForButton:requestCashBtn];
  //  [requestCashBtn setTitleColor:SetBlueColor forState:UIControlStateNormal];
    
    
   // self.requestCashView.layer.masksToBounds=YES;
   // self.requestCashView = (UIButton *)[self roundCornersOnView:self.requestCashView onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:YES radius:10.0];
    
    
//    self.buttonsView.layer.masksToBounds=YES;
//    self.buttonsView.layer.cornerRadius = 17;
//    self.buttonsView.layer.borderColor = skyBlue.CGColor;
//    self.buttonsView.layer.borderWidth = 1;
//
    
//    [okBtn setTitle:JJLocalizedString(@"OK", nil) forState:UIControlStateNormal];
//    [okBtn setTitleColor:SetThemeColor forState:UIControlStateNormal];
//    okBtn.layer.cornerRadius=2;
//    okBtn.layer.borderWidth=2;
//    okBtn.layer.borderColor=SetThemeColor.CGColor;
//    okBtn.layer.masksToBounds=YES;
    
//    [headerLbl setText:JJLocalizedString(@"Fare_Summary", nil)];
//    [_ridedistanceHeaderLbl setText:JJLocalizedString(@"Ride_Distance", nil)];
//    [waitTimeHeaderLbl setText:JJLocalizedString(@"Wait_Time:", nil)];
//    [timeTakenHeaderLbl setText:JJLocalizedString(@"Time_Taken", nil)];
//
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
   // self.subview1.layer.masksToBounds = YES;
    self.subview1.layer.cornerRadius = self.subview1.frame.size.height/2 ;
    
//    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
//    yourViewBorder.strokeColor = [UIColor blackColor].CGColor;
//    yourViewBorder.fillColor = [UIColor clearColor].CGColor;
//    yourViewBorder.lineDashPattern = @[@4];
//    yourViewBorder.frame = self.subview1.bounds;
//    yourViewBorder.path = [UIBezierPath  bezierPathWithRoundedRect:self.subview1.bounds cornerRadius:self.subview1.frame.size.height/2].CGPath;
//    [self.subview1.layer addSublayer:yourViewBorder];



  // self.subview2.clipsToBounds = YES;
    self.subview2.layer.cornerRadius = self.subview2.frame.size.height/2 ;
//
//    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
//    yourViewBorder2.strokeColor = [UIColor blackColor].CGColor;
//    yourViewBorder2.fillColor = [UIColor clearColor].CGColor;
//    yourViewBorder2.lineDashPattern = @[@4];
//    yourViewBorder2.frame = self.subview2.bounds;
//    yourViewBorder2.path = [UIBezierPath  bezierPathWithRoundedRect:self.subview2.bounds cornerRadius:self.subview2.frame.size.height/2].CGPath;
//    [self.subview2.layer addSublayer:yourViewBorder2];


//    self.subview3.layer.masksToBounds = YES;
    self.subview3.layer.cornerRadius = self.subview3.frame.size.height/2 ;

//    CAShapeLayer *yourViewBorder3 = [CAShapeLayer layer];
//    yourViewBorder3.strokeColor = [UIColor blackColor].CGColor;
//    yourViewBorder3.fillColor = [UIColor clearColor].CGColor;
//    yourViewBorder3.lineDashPattern = @[@4];
//    yourViewBorder3.frame = self.subview3.bounds;
//    yourViewBorder3.path = [UIBezierPath  bezierPathWithRoundedRect:self.subview3.bounds cornerRadius:self.subview3.frame.size.height/2].CGPath;
//    [self.subview3.layer addSublayer:yourViewBorder3];


    
    self.requestCashView.layer.masksToBounds = YES;
    self.requestCashView.layer.cornerRadius = requestCashView.frame.size.height/2;
        
    self.paymentView.layer.masksToBounds = YES;
    self.paymentView.layer.cornerRadius = paymentView.frame.size.height/2;
        
        

    self.subView.layer.masksToBounds = NO;
    
        CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
        yourViewBorder.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
        yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
        yourViewBorder.lineDashPattern = @[@3,@3];
        yourViewBorder.frame = self.dottedView.bounds;
        yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.dottedView.bounds].CGPath;
        [self.dottedView.layer addSublayer:yourViewBorder];
        
        
    });
    
    
    
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}



-(void)selfSetFont{
    
    
   // [headerLbl setText:JJLocalizedString(@"Fare_Summary", nil)];
//    headerLbl=[Theme setHeaderFontForLabel:headerLbl];
//    summaryView.layer.cornerRadius=20;
//    summaryView.layer.masksToBounds=YES;
//    fareLbl=[Theme setLargeBoldFontForLabel:fareLbl];
//    timeTakenHeaderLbl=[Theme setNormalFontForLabel:timeTakenHeaderLbl];
//    waitTimeHeaderLbl=[Theme setNormalFontForLabel:waitTimeHeaderLbl];
//    distanceLbl=[Theme setNormalFontForLabel:distanceLbl];
//    timeLbl=[Theme setNormalFontForLabel:timeLbl];
//    waitLbl=[Theme setNormalFontForLabel:waitLbl];
//    
    
    
dispatch_async(dispatch_get_main_queue(), ^{
    
//    self.subView.layer.masksToBounds = YES;
//    self.subView.layer.cornerRadius = 20;
//    self.subView .layer.borderWidth=1;
//    self.subView .layer.borderColor=skyBlue.CGColor;
//
//    self.subView.layer.shadowRadius  = 1.5f;
//    self.subView.layer.shadowColor   = [UIColor blackColor].CGColor;
//    self.subView.layer.shadowOffset  = CGSizeMake(5.0f, 5.0f);
//    self.subView.layer.shadowOpacity = 0.5f;
//   // self.subView.layer.masksToBounds = NO;
//
//   // self.subview1.layer.masksToBounds = YES;
//   self.subview1 = (UIView *)[self roundCornersOnView:self.subview1 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:20.0];
//
//    self.distanceLbl = (UILabel *)[self roundCornersOnView:self.distanceLbl onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:NO radius:20.0];
//
//    //self.subview3.layer.masksToBounds = YES;
//    self.subview3 = (UIView *)[self roundCornersOnView:self.subview3 onTopLeft:NO topRight:NO bottomLeft:YES bottomRight:YES radius:20.0];
//
//    self.waitLbl = (UILabel *)[self roundCornersOnView:self.waitLbl onTopLeft:NO topRight:NO bottomLeft:NO bottomRight:YES radius:20.0];
//
//    self.subView11 = (UIView *)[self roundCornersOnView:self.subview3 onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:YES radius:10.0];
//
//
//    paymentBtn=[Theme setBoldFontForButton:paymentBtn];
//    okBtn=[Theme setBoldFontForButton:okBtn];
//      [paymentBtn setTitle:JJLocalizedString(@"Request_Payment", nil) forState:UIControlStateNormal];
//   [paymentBtn setTitleColor:SetThemeColor forState:UIControlStateNormal];
//    paymentBtn.layer.cornerRadius = 5;
//
//    self.paymentView.layer.masksToBounds=YES;
//    self.paymentView = (UIButton *)[self roundCornersOnView:self.paymentView onTopLeft:YES  topRight:NO bottomLeft:YES bottomRight:NO radius:15.0];
//
//
//    [requestCashBtn setTitle:JJLocalizedString(@"Receive_Cash", nil) forState:UIControlStateNormal];
//    requestCashBtn=[Theme setBoldFontForButton:requestCashBtn];
//    [requestCashBtn setTitleColor:SetBlueColor forState:UIControlStateNormal];
//
//
//    self.requestCashView.layer.masksToBounds=YES;
//    self.requestCashView = (UIButton *)[self roundCornersOnView:self.requestCashView onTopLeft:NO topRight:YES bottomLeft:NO bottomRight:YES radius:10.0];
//
//
//    self.buttonsView.layer.masksToBounds=YES;
//    self.buttonsView.layer.cornerRadius = 17;
//    self.buttonsView.layer.borderColor = skyBlue.CGColor;
//    self.buttonsView.layer.borderWidth = 1;
//
//
//    [okBtn setTitle:JJLocalizedString(@"OK", nil) forState:UIControlStateNormal];
//    [okBtn setTitleColor:SetThemeColor forState:UIControlStateNormal];
//    okBtn.layer.cornerRadius=2;
//    okBtn.layer.borderWidth=2;
//    okBtn.layer.borderColor=SetThemeColor.CGColor;
//    okBtn.layer.masksToBounds=YES;
//
//    [headerLbl setText:JJLocalizedString(@"Fare_Summary", nil)];
//    [_ridedistanceHeaderLbl setText:JJLocalizedString(@"Ride_Distance", nil)];
//    [waitTimeHeaderLbl setText:JJLocalizedString(@"Wait_Time:", nil)];
//    [timeTakenHeaderLbl setText:JJLocalizedString(@"Time_Taken", nil)];
    
});
    
    
    
}

-(void)setDatasToSummaryView{
    
    NSString * curStr=objFareRecs.currency;
    fareLbl.text=[NSString stringWithFormat:@"%@ %@",curStr,objFareRecs.rideFare];
    _rideAmount = fareLbl.text;
    distanceLbl.text=objFareRecs.rideDistance;
    timeLbl.text=objFareRecs.rideDuration;
    waitLbl.text=objFareRecs.waitingDuration;
    if([objFareRecs.needPayment isEqualToString:@"YES"]){
        if([objFareRecs.needCash isEqualToString:@"Enable"]){
            okBtn.hidden=YES;
            paymentBtn.hidden=NO;
            requestCashBtn.hidden=NO;
        }else{
            okBtn.hidden=YES;
            paymentBtn.hidden=NO;
            requestCashBtn.hidden=YES;
            paymentBtn.frame=CGRectMake(paymentBtn.frame.origin.x, paymentBtn.frame.origin.y, requestCashBtn.frame.origin.x+requestCashBtn.frame.size.width, paymentBtn.frame.size.height);
        }
        
    }else{
        okBtn.hidden=NO;
        paymentBtn.hidden=YES;
        requestCashBtn.hidden=YES;
    }
    
}

- (void)presentInParentViewController:(UIViewController *)parentViewController {
    //Presents the view in the parent view controller
    
    if (self.shouldDimBackground == YES) {
        //Dims the background, unless overridden
        backgroundGradientView = [[AJRBackgroundDimmer alloc] initWithFrame:parentViewController.view.bounds];
        [parentViewController.view addSubview:backgroundGradientView];
    }
    
    //Adds the nutrition view to the parent view, as a child
    [parentViewController.view addSubview:self.view];
    [parentViewController addChildViewController:self];
    
    
    //Adds the bounce animation on appear unless overridden
    if (!self.shouldAnimateOnAppear)
        return;
    
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.duration = 0.4;
    bounceAnimation.delegate = self;
    
    bounceAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:0.7f],
                              [NSNumber numberWithFloat:1.2f],
                              [NSNumber numberWithFloat:0.9f],
                              [NSNumber numberWithFloat:1.0f],
                              nil];
    
    bounceAnimation.keyTimes = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:0.334f],
                                [NSNumber numberWithFloat:0.666f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
    
    bounceAnimation.timingFunctions = [NSArray arrayWithObjects:
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                       nil];
    
    [self.view.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
    
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    fadeAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    fadeAnimation.duration = 0.1;
    [backgroundGradientView.layer addAnimation:fadeAnimation forKey:@"fadeAnimation"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [self didMoveToParentViewController:self.parentViewController];
}

- (IBAction)close:(id)sender {
    //The close button
    [self dismissFromParentViewController];
}

- (void)dismissFromParentViewController {
    //Removes the nutrition view from the superview
    
    [self willMoveToParentViewController:nil];
    
    //Removes the view with or without animation
    if (!self.shouldAnimateOnDisappear) {
        [self.view removeFromSuperview];
        [backgroundGradientView removeFromSuperview];
        [self removeFromParentViewController];
        return;
    }
    else{
        [UIView animateWithDuration:0.4 animations:^ {
            CGRect rect = self.view.bounds;
            rect.origin.y += rect.size.height;
            self.view.frame = rect;
            backgroundGradientView.alpha = 1.0f;
        }
                         completion:^(BOOL finished) {
                             [self.view removeFromSuperview];
                             [backgroundGradientView removeFromSuperview];
                             [self removeFromParentViewController];
                         }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didClickPaymentBtn:(id)sender {
   // [self close:self];
    UIButton * btn=sender;
    if([[btn titleColorForState:UIControlStateNormal] isEqual:[UIColor lightGrayColor]]){
        [self.view makeToast:@"Request_for_payment_is"];
    }else{
        cancelBtn.hidden=YES;
       // requestCashBtn.userInteractionEnabled=NO;
        [paymentBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
       // paymentBtn.userInteractionEnabled=NO;
        [self.delegate moveToNextVc:0];
    }
    
}
- (IBAction)didClickRequestCashBtn:(id)sender {
    //[self close:self];
    cancelBtn.hidden=YES;
    
    //[self.delegate moveToNextVc:1];
    ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
    //[objReceiveCashVC setRideId:_rideID];
   
    [objReceiveCashVC setFareAmt:_rideAmount];
    
//    objReceiveCashVC.RideId =_rideID;
   AppDelegate * appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
   appdelegate.appdelegateRideID = _rideID;
    [self.navigationController pushViewController:objReceiveCashVC animated:YES];
}

- (IBAction)didClickOkBtn:(id)sender {
    okBtn.userInteractionEnabled=NO;
    [self performSelector:@selector(enableBtn) withObject:nil afterDelay:10];
     [self.delegate moveToNextVc:2];
}

- (IBAction)didClickCloseBtn:(id)sender {
    [self stopActivityIndicator];
    [self.delegate moveToNextVc:3];
    [self dismissFromParentViewController];
}
-(void)enableBtn{
    okBtn.userInteractionEnabled=YES;
}

-(UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) {corner = corner | UIRectCornerTopLeft;}
        if (tr) {corner = corner | UIRectCornerTopRight;}
        if (bl) {corner = corner | UIRectCornerBottomLeft;}
        if (br) {corner = corner | UIRectCornerBottomRight;}
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}






@end
