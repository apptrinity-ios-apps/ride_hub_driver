//
//  LoginViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "LoginViewController.h"
#import "Theme.h"
#import "SignUpWebViewController.h"



@interface LoginViewController ()<ITRAirSideMenuDelegate,CLLocationManagerDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *loginViewHightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *eyeBtn;

@property (weak, nonatomic) IBOutlet UIImageView *eyeImage;


@end
@class RootBaseViewController;
@implementation LoginViewController
@synthesize loginScrollView,userNameTxtField,passwordTextfield,signInBtn,custIndicatorView,forgotPasswordbtn,headerView;

- (IBAction)eyeBtnAction:(UIButton *)sender {
    
    
    if (_eyeBtn.selected)
    {
        
        _eyeBtn.selected = NO;
        
        passwordTextfield.secureTextEntry = YES;
        _eyeImage.image = [UIImage imageNamed:@"eyeDefault"];

        if (passwordTextfield.isFirstResponder) {
            [passwordTextfield resignFirstResponder];
            [passwordTextfield becomeFirstResponder];
        }
    }
    else
    {
        
        _eyeBtn.selected = YES;
        
        passwordTextfield.secureTextEntry = NO;
        _eyeImage.image = [UIImage imageNamed:@"eyeActive"];
        
        if (passwordTextfield.isFirstResponder) {
            [passwordTextfield resignFirstResponder];
            [passwordTextfield becomeFirstResponder];
        }
        
    }
   
}


- (void)viewDidLoad {
    
//    userNameTxtField.layer.cornerRadius=5.0;
//    userNameTxtField.layer.borderWidth=0.8;
//    userNameTxtField.layer.borderColor=[UIColor blackColor].CGColor;
//
//    passwordTextfield.layer.cornerRadius=5.0;
//    passwordTextfield.layer.borderWidth=0.8;
//    passwordTextfield.layer.borderColor=[UIColor blackColor].CGColor;


    [super viewDidLoad];
    
//    [_eyeBtn setImage:[UIImage imageNamed:@"eyeDefault"] forState:UIControlStateNormal];
//
//    [_eyeBtn setImage:[UIImage imageNamed:@"eyeActive"] forState:UIControlStateSelected];
    
    _eyeImage.image = [UIImage imageNamed:@"eyeDefault"];
    
    self.loginViewHightConstraint.constant = self.view.frame.size.height-30;

   // [self.view bringSubviewToFront:headerView];
    self.userNameTxtField.layer.cornerRadius = self.userNameTxtField.frame.size.height/2;
//    self.userNameTxtField.layer.masksToBounds = true;
    self.userNameTxtField.layer.borderWidth = 1;
    self.userNameTxtField.layer.borderColor = UIColor.lightGrayColor.CGColor;
    
    self.passwordTextfield.layer.cornerRadius = self.passwordTextfield.frame.size.height/2;
//    self.passwordTextfield.layer.masksToBounds = true;
    self.passwordTextfield.layer.borderWidth = 1;
    self.passwordTextfield.layer.borderColor = UIColor.lightGrayColor.CGColor;        

    self.signInBtn.layer.cornerRadius =  self.signInBtn.frame.size.height/2;
    self.signInBtn.layer.masksToBounds = true;
    [self setShadow:signInBtn];
    [self setShadow:userNameTxtField];
    [self setShadow:passwordTextfield];
   // self.signInBtn.layer.borderWidth = 1;
    //self.signInBtn.layer.borderColor = [UIColor whiteColor].CGColor;

    
    self.view= [Theme setBackGroundImage:self.view];
    [self setFontAndColor];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveReturnNotification:)
                                                 name:kDriverReturnNotif
                                               object:nil];
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

-(void)dismissKeyboard
{
    [userNameTxtField resignFirstResponder];//Assuming that UITextField Object is textField
    [loginScrollView setContentOffset:CGPointMake(0.0, 0) animated:YES];
    [passwordTextfield resignFirstResponder];
}

- (void)receiveReturnNotification:(NSNotification *) notification
{
    
    if(self.view.window){
        [self stopActivityIndicator];
        signInBtn.userInteractionEnabled=YES;
        NSLog(@"%@\n%@",JJLocalizedString(@"To_re-enable_please_go", nil),JJLocalizedString(@"To_re-enable_please_go", nil));
        [self.view makeToast:[NSString stringWithFormat:@"%@\n%@",JJLocalizedString(@"App_Permission_Denied", nil),
                              JJLocalizedString(@"To_re-enable_please_go", nil)]];
    }
}
-(void)setFontAndColor{
    @try {
        _headerLbl.text=JJLocalizedString(@"Sign_In", nil);
        
        userNameTxtField.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
        passwordTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0);
        
        [userNameTxtField setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        [passwordTextfield setValue:[UIColor darkGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
        
        userNameTxtField.placeholder=JJLocalizedString(@"Enter Mobile Number", nil);
        passwordTextfield.placeholder=JJLocalizedString(@"Password", nil);
        [signInBtn setTitle:JJLocalizedString(@"Login", nil) forState:UIControlStateNormal];
        
        
        NSMutableAttributedString* string = [[NSMutableAttributedString alloc]initWithString:JJLocalizedString(@"Forgot_password?", nil)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, string.length)];//TextColor
        [string addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:NSMakeRange(0, string.length)];
        [string addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, string.length)];//TextColor
        //[forgotPasswordbtn setAttributedTitle:string forState:UIControlStateNormal];
        
        
        _headerLbl=[Theme setHeaderFontForLabel:_headerLbl];
        signInBtn=[Theme setBoldFontForButton:signInBtn];
        
     
        //    signInBtn.layer.cornerRadius=2;
        //    signInBtn.layer.masksToBounds=YES;
        userNameTxtField=[Theme setNormalFontForTextfield:userNameTxtField];
        passwordTextfield=[Theme setNormalFontForTextfield:passwordTextfield];
//        userNameTxtField=[self leftPadding:userNameTxtField withImageName:@"EmailMessage"];
//        passwordTextfield=[self leftPadding:passwordTextfield withImageName:@"PasswordLock"];
    }
    @catch (NSException *exception) {
        
    }
   
    
}
-(UITextField *)leftPadding:(UITextField *)txtField withImageName:(NSString *)imgName{
    
    UIView *   arrow = [[UILabel alloc] init];
    arrow.frame = CGRectMake(0, 0,50, 50);
    UIImageView *   Img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    Img.frame = CGRectMake(10, 10,30, 30);
    [arrow addSubview:Img];
    arrow.contentMode = UIViewContentModeScaleToFill;
    txtField.leftView = arrow;
    txtField.leftViewMode = UITextFieldViewModeAlways;
    return txtField;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:userNameTxtField]){
        userNameTxtField.keyboardType = UIKeyboardTypeNumberPad;
        [loginScrollView setContentOffset:CGPointMake(0.0, textField.frame.origin.y-250) animated:YES];
    }else{
        [loginScrollView setContentOffset:CGPointMake(0.0,textField.frame.origin.y-300) animated:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [loginScrollView setContentOffset:CGPointMake(0.0, 0) animated:YES];
    return YES;
}
- (IBAction)didClickForgotPasswordBtn:(id)sender {
    ForgotPasswordCabViewController * objForgotVc=[self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordVCSID"];
   
    [self.navigationController pushViewController:objForgotVc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)didClickUserCredentialBtns:(id)sender {
}
- (IBAction)didClickBackBtn:(id)sender {
   [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)didClickSigninBtn:(id)sender {
    if([self validateTextField]){
        
        signInBtn.userInteractionEnabled=NO;
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [self showActivityIndicator:YES];
        [web PostLoginUrl:[self setParametersForLogin]
                  success:^(NSMutableDictionary *responseDictionary)
         {
             [self stopActivityIndicator];
             if([[Theme checkNullValue:[responseDictionary objectForKey:@"status"]] isEqualToString:@"1"]){
                 DriverInfoRecords * objDriverInfoRecs=[[DriverInfoRecords alloc]init];
                 objDriverInfoRecs.driverId=[Theme checkNullValue:responseDictionary[@"driver_id"]];
                 objDriverInfoRecs.driverName=[Theme checkNullValue:responseDictionary[@"driver_name"]];
                 objDriverInfoRecs.driverImage=[Theme checkNullValue:responseDictionary[@"driver_image"]];
                 objDriverInfoRecs.driverEmail=[Theme checkNullValue:responseDictionary[@"phone"]];
                 objDriverInfoRecs.driverVehicModel=[Theme checkNullValue:responseDictionary[@"vehicle_model"]];
                 objDriverInfoRecs.driverVehicNumber=[Theme checkNullValue:responseDictionary[@"vehicle_number"]];
                 objDriverInfoRecs.driverKey=[Theme checkNullValue:responseDictionary[@"key"]];
                 [Theme saveUserDetails:objDriverInfoRecs];
                 [Theme saveXmppUserCredentials:[Theme checkNullValue:responseDictionary[@"sec_key"]]];
                 if([Theme UserIsLogin]){
                     [self moveToStarterVC];
                      signInBtn.userInteractionEnabled=YES;
                 }
             }else{
                 
                 [self showAlertForAppInfo:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
                  signInBtn.userInteractionEnabled=YES;
             }
         }
                  failure:^(NSError *error)
         {
             
             [self stopActivityIndicator];
              signInBtn.userInteractionEnabled=YES;
         }];
    }
}

- (IBAction)signUpPage_Action:(id)sender {
    
    
    SignUpWebViewController * objSignUpVc=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpWebVSSID"];
    [self.navigationController pushViewController:objSignUpVc animated:YES];
    
    
}
-(void)showAlertForAppInfo:(NSString *)msg{
    OpinionzAlertView *alert = [[OpinionzAlertView alloc] initWithTitle:@"OOPS !!!"
                                                                message:msg
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
    alert.iconType = OpinionzAlertIconWarning;
    [alert show];
}

-(BOOL)validateTextField{
    BOOL validate=YES;
    if(userNameTxtField.text.length==0){
        [self showErrorMessage:JJLocalizedString(@"MobileNumber_cannot_be_empty", nil)];
        validate=NO;
        return validate;
    }else if (passwordTextfield.text.length==0){
          [self showErrorMessage:JJLocalizedString(@"Password_cannot_be_empty", nil)];
        validate=NO;
        return validate;
    }
    return validate;
}

-(void)showErrorMessage:(NSString *)str{
    WBErrorNoticeView *notice = [WBErrorNoticeView errorNoticeInView:self.view title:@"Oops !!!" message:JJLocalizedString(str, nil)];
    [notice show];
}

-(NSDictionary *)setParametersForLogin{

    NSDictionary *dictForuser = @{
                                  @"phone":userNameTxtField.text,
                                  @"password":passwordTextfield.text,
                                  @"deviceToken":[Theme getDeviceToken],
                                  @"gcm_id":@""
                                  };
    return dictForuser;
}
-(void)moveToStarterVC{
    AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
    [testAppDelegate setInitialViewController];
    [testAppDelegate connectToXmpp];
    
}
-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];


    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
    [self.container removeFromSuperview];

}
@end
