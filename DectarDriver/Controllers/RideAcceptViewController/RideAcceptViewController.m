//
//  RideAcceptViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/24/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "RideAcceptViewController.h"

@interface RideAcceptViewController (){
   
    JJLocationManager * jjLocManager;
    NSInteger pushTag;
}

@end

@implementation RideAcceptViewController
@synthesize countValue,acceptHeaderLbl,rideAcceptScrollView,setMaximumValue,custIndicatorView,objRideAccRec,requestArray,requestTableView,badgeBtn,isBooked;

- (void)viewDidLoad {
    if(jjLocManager==nil){
        jjLocManager=[JJLocationManager sharedManager];
    }
    countValue=0;
    [super viewDidLoad];
    pushTag=1;
    [requestTableView registerNib:[UINib nibWithNibName:@"RequestAcceptTableViewCell" bundle:nil] forCellReuseIdentifier:@"RequestAcceptCellIdentifier"];
    requestTableView.estimatedRowHeight=168;
    requestTableView.rowHeight=UITableViewAutomaticDimension;
    [self setFontAndColor];
     [self playAudio];
    
   // [requestTableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTableRefreshNotification:)
                                                 name:kDriverReceiveNotif
                                               object:nil];
    // [requestTableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveReturnNotification:)
                                                 name:kDriverReturnNotif
                                               object:nil];
    requestTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view.
}
- (void)receiveReturnNotification:(NSNotification *) notification
{
    
    if(self.view.window){
        isBooked=NO;
        [self stopAudioAndMoveToHome];
        NSLog(@"%@\n%@",JJLocalizedString(@"To_re-enable_please_go", nil),JJLocalizedString(@"To_re-enable_please_go", nil));
        [self.view makeToast:[NSString stringWithFormat:@"%@\n%@",JJLocalizedString(@"App_Permission_Denied", nil),
                              JJLocalizedString(@"To_re-enable_please_go", nil)]];
    }
}
- (void)receiveTableRefreshNotification:(NSNotification *) notification
{
    NSLog(@"Received APNS with userInfo %@", notification.userInfo);
    NSDictionary * dict=notification.userInfo;
    RideAcceptRecord * objRideAcceptRec=[[RideAcceptRecord alloc]init];

    if(self.view.window){
//
        if(/*[requestArray count]>0 &&*/ [requestArray count]<5){
            objRideAcceptRec.LocationName=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            objRideAcceptRec.RideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
            objRideAcceptRec.expiryTime=[Theme checkNullValue:[dict objectForKey:@"key2"]];
            objRideAcceptRec.DropLocation =[Theme checkNullValue:[dict objectForKey:@"key4"]];
            pushTag++;
            NSString * pickuprequest=@"Pickup Request";
            objRideAcceptRec.headerTxt=[Theme checkNullValue:[NSString stringWithFormat:@"%@ %ld",pickuprequest,(long)pushTag]];
            objRideAcceptRec.rideTag=pushTag;
            NSInteger expValue=[objRideAcceptRec.expiryTime integerValue];
            if(expValue<=5){
                objRideAcceptRec.expiryTime=@"15";
            }
            objRideAcceptRec.currentTimer=0;
            [requestArray addObject:objRideAcceptRec];
            [self playAudioNotif];
            badgeBtn=[self setbadgeToButton:badgeBtn withBadgeCount:[requestArray count]];
         [requestTableView reloadData];
        }
        // [requestTableView reloadData];
       
        
    }
}
-(void)playAudio{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/audio1.wav", [[NSBundle mainBundle] resourcePath]]];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = -1;
    [audioPlayer play];
}
- (void) stopAudio
{
    [audioPlayer stop];
    [audioPlayer setCurrentTime:0];
}
-(void)playAudioNotif{
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Dingdong.wav", [[NSBundle mainBundle] resourcePath]]];
    
    NSError *error;
    audioPlayer1 = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [audioPlayer1 play];
}


-(void)setDatasToView{

    setMaximumValue=[objRideAccRec.expiryTime floatValue];
    
}
-(void)setFontAndColor{
    acceptHeaderLbl.text=JJLocalizedString(@"Tap_to_accept_the_ride", nil);
    acceptHeaderLbl=[Theme setHeaderFontForLabel:acceptHeaderLbl];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(NSDictionary *)setParametersForAcceptRide:(NSString *)acceptRideId{
    NSString * latitude=@"";
    NSString * longitude=@"";
    if(jjLocManager.currentLocation.coordinate.latitude!=0){
        latitude=[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.latitude];
        longitude=[NSString stringWithFormat:@"%f",jjLocManager.currentLocation.coordinate.longitude];
    }
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":acceptRideId,
                                  @"driver_lat":latitude,
                                  @"driver_lon":longitude
                                  };
    return dictForuser;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated{
    [self stopAudio];
    for (UIView *view in requestTableView.subviews) {
        for (RequestAcceptTableViewCell *cell in view.subviews) {
            if([cell isKindOfClass:[RequestAcceptTableViewCell class]]){
                [cell.spinTimer stop];
                [cell.countDownTimerLbl pause];
            }
        }
    }
}


-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];
        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
    [self.container removeFromSuperview];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [requestArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RequestAcceptTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RequestAcceptCellIdentifier"];
    if (cell == nil) {
        cell = [[RequestAcceptTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                 reuseIdentifier:@"RequestAcceptCellIdentifier"];
    }
   
    cell.delegate=self;
    [cell setIndexpath:indexPath];
    RideAcceptRecord * objAcceptRecs=[[RideAcceptRecord alloc]init];
    objAcceptRecs =[requestArray objectAtIndex:indexPath.row];
    
    [cell setDatasToAcceptCell:objAcceptRecs];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
     [cell layoutIfNeeded];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 351;
}

-(void)updateRecordCellWhenSpinnerStarts:(NSInteger)riderTag withIndex:(NSIndexPath *)index{
    for (int i=0; i<[requestArray count]; i++) {
        RideAcceptRecord * objAcceptRecs=[[RideAcceptRecord alloc]init];
        objAcceptRecs =[requestArray objectAtIndex:i];
        if(objAcceptRecs.rideTag==riderTag){
            objAcceptRecs.rideCountStart=YES;
        }
        [requestArray setObject:objAcceptRecs atIndexedSubscript:i];
    }
    badgeBtn=[self setbadgeToButton:badgeBtn withBadgeCount:[requestArray count]];
    if([requestArray count]==0){
        [self stopAudioAndMoveToHome];
    }
    
}

-(void)RemoveCellWhenRequestExpired:(NSInteger)riderTag withIndex:(NSIndexPath *)index{
    if([requestArray count]>0){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [requestArray removeObjectAtIndex:indexPath.row];
        
        
        [requestTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]  withRowAnimation:UITableViewRowAnimationFade];
    }
    [requestTableView reloadData];
    badgeBtn=[self setbadgeToButton:badgeBtn withBadgeCount:[requestArray count]];
    if([requestArray count]==0){
        [self stopAudioAndMoveToHome];
    }
}

-(void)rejectParticularRide:(NSInteger)riderTag withIndex:(NSIndexPath *)index{
    
    for (int i=0; i<[requestArray count]; i++) {
        RideAcceptRecord * objAcceptRecs=[[RideAcceptRecord alloc]init];
        objAcceptRecs =[requestArray objectAtIndex:i];
        if(objAcceptRecs.rideTag==riderTag){
            [requestArray removeObject:objAcceptRecs];
            [requestTableView reloadData];
        }
    }
    badgeBtn=[self setbadgeToButton:badgeBtn withBadgeCount:[requestArray count]];
    if([requestArray count]==0){
        [self stopAudioAndMoveToHome];
    }
}
-(void)AcceptRide:(NSInteger)riderTag withIndex:(NSIndexPath *)index{
    requestTableView.userInteractionEnabled=NO;
    [self showActivityIndicator:YES];
    if([requestArray count]>0){
        
        RequestAcceptTableViewCell *cell = [requestTableView cellForRowAtIndexPath:index];
        [cell.spinTimer stop];
        [cell.countDownTimerLbl pause];
        
        RideAcceptRecord * objAcceptRecs=[[RideAcceptRecord alloc]init];
        objAcceptRecs =[requestArray objectAtIndex:index.row];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web DriverAccept:[self setParametersForAcceptRide:objAcceptRecs.RideId]
                  success:^(NSMutableDictionary *responseDictionary)
         {
             requestTableView.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 isBooked=YES;
                 [self stopAudio];
                 NSDictionary * dict=responseDictionary[@"response"][@"user_profile"];
                 RiderRecords * objRiderRecords=[[RiderRecords alloc]init];
                 objRiderRecords.RideId=[Theme checkNullValue:[dict objectForKey:@"ride_id"]];
                  objRiderRecords.UserId=[Theme checkNullValue:[dict objectForKey:@"user_id"]];
                 objRiderRecords.userEmail=[Theme checkNullValue:[dict objectForKey:@"user_email"]];
                 objRiderRecords.userImage=[Theme checkNullValue:[dict objectForKey:@"user_image"]];
                 objRiderRecords.userName=[Theme checkNullValue:[dict objectForKey:@"user_name"]];
                 objRiderRecords.userLocation=[Theme checkNullValue:[dict objectForKey:@"pickup_location"]];
                 objRiderRecords.userLattitude=[Theme checkNullValue:[dict objectForKey:@"pickup_lat"]];
                 objRiderRecords.userLongitude=[Theme checkNullValue:[dict objectForKey:@"pickup_lon"]];
                 objRiderRecords.userReview=[Theme checkNullValue:[dict objectForKey:@"user_review"]];
                 objRiderRecords.userTime=[Theme checkNullValue:[dict objectForKey:@"pickup_time"]];
                 objRiderRecords.userPhoneNumber=[Theme checkNullValue:[dict objectForKey:@"phone_number"]];
                 objRiderRecords.hasDropLocation=[Theme checkNullValue:[dict objectForKey:@"drop_location"]];
                 objRiderRecords.dropAddress=[Theme checkNullValue:[dict objectForKey:@"drop_loc"]];
                 objRiderRecords.dropLat=[Theme checkNullValue:[dict objectForKey:@"drop_lat"]];
                 objRiderRecords.dropLong=[Theme checkNullValue:[dict objectForKey:@"drop_lon"]];
                  objRiderRecords.userNote=[Theme checkNullValue:[dict objectForKey:@"note"]];
                 
                 AppDelegate *delegate = [UIApplication sharedApplication].delegate;
                 delegate.user_id = [Theme checkNullValue:[dict objectForKey:@"user_id"]];
                  delegate.user_email=[Theme checkNullValue:[dict objectForKey:@"user_email"]];
                 delegate.user_image=[Theme checkNullValue:[dict objectForKey:@"user_image"]];
                  delegate.user_name=[Theme checkNullValue:[dict objectForKey:@"user_name"]];
                 
                 
                 RideUserViewController * objRideUserVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RideUserVCSID"];
                 [objRideUserVc setObjRiderRecords:objRiderRecords];
                 [self.navigationController pushViewController:objRideUserVc animated:YES];
             }else{
                 isBooked=NO;
                 requestTableView.userInteractionEnabled=YES;
                 [self stopActivityIndicator];
                 [self.view makeToast:@"Ride_not_accepted"];
                 [self.delegate ridecancelled];
                 [self rejectParticularRide:objAcceptRecs.rideTag withIndex:index];
                 
                 
             }
         }
                  failure:^(NSError *error)
         {
              isBooked=NO;
             requestTableView.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             [self rejectParticularRide:objAcceptRecs.rideTag withIndex:index];
         }];
    }
    
    
   
    
    
    
    if([requestArray count]==0){
        [self stopAudioAndMoveToHome];
    }
    badgeBtn=[self setbadgeToButton:badgeBtn withBadgeCount:[requestArray count]];
}

-(void)stopAudioAndMoveToHome{
    [self stopAudio];
    if(isBooked==NO){
        [self.navigationController popViewControllerAnimated:NO];
    }
}

-(UIButton *)setbadgeToButton:(UIButton *)btnName withBadgeCount:(NSInteger)badgeCount{
    [btnName.badgeView setBadgeValue:badgeCount];
    [btnName.badgeView setIsTop:NO];
    [btnName.badgeView setPosition:MGBadgePositionTopRight];
    [btnName.badgeView setBadgeColor:[UIColor redColor]];
    return btnName;
}

@end
