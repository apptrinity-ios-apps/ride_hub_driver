//
//  chatviewController.m
//  RideHubDriver
//
//  Created by nagaraj  kumar on 24/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import "chatviewController.h"
#import "AppDelegate.h"

@interface chatviewController ()

@end

@implementation chatviewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    NSString * str = [[NSString alloc]init];
    
    
    str = [str stringByAppendingString:@"<script>\n"];
    str = [str stringByAppendingString:@"(function(t,a,l,k,j,s){\n"];
    str = [str stringByAppendingString:@"s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n"];
    str = [str stringByAppendingString:@";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n"];
    str = [str stringByAppendingString:@".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n"];
    str = [str stringByAppendingString:@"</script>\n"];
    
    str = [str stringByAppendingString:@"\n"];
    str = [str stringByAppendingString:@"<!-- container element in which TalkJS will display a chat UI -->\n"];
    str = [str stringByAppendingString:@"<div id=\"talkjs-container\" style=\"width:100%; margin: auto; height: 100%\"><i>Naveen chat...</i></div>\n"];
    str = [str stringByAppendingString:@"\n"];
    str = [str stringByAppendingString:@"<!-- TalkJS initialization code, which we'll customize in the next steps -->\n"];
    str = [str stringByAppendingString:@"<script>\n"];
    
    
    
    NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
    NSString* driverID =[myDictionary objectForKey:@"driver_id"];
    NSString * driverName = [myDictionary objectForKey:@"driver_name"];
    NSString *driverImage = [myDictionary objectForKey:@"driver_image"];
    
    
    str = [str stringByAppendingString:@"Talk.ready.then(function() {\n"];
    
   
    
    str = [str stringByAppendingString:@"var me = new Talk.User({\n"];
    NSString * _id = [NSString stringWithFormat:@"id: \"%@\",\n",driverID];
    NSString * Name = [NSString stringWithFormat:@"name: \"%@\",\n",driverName];
    str = [str stringByAppendingString:_id];
    str = [str stringByAppendingString:Name];
    str = [str stringByAppendingString:@"  role: \"user\",\n"];
    str = [str stringByAppendingString:@" email: \"Sebastian@example.com\",\n"];
    
    
   NSString * drivImage = [NSString stringWithFormat:@"photoUrl: \"https://%@\",\n",driverImage];
    str = [str stringByAppendingString:drivImage];
    str = [str stringByAppendingString:@"welcomeMessage: \"Hey, how can I help?\"\n"];
    str = [str stringByAppendingString:@"});\n"];
    str = [str stringByAppendingString:@"window.talkSession = new Talk.Session({\n"];
    str = [str stringByAppendingString:@"appId: \"M5iybBK9\",\n"];
    str = [str stringByAppendingString:@"me: me\n"];
    str = [str stringByAppendingString:@"});\n"];
    
    
    AppDelegate* delagate = [UIApplication sharedApplication].delegate;
    
    
    self.headerLbl.text = delagate.user_name;
    
    NSString * userID = [NSString stringWithFormat:@"id: \"%@\",\n",delagate.user_id];
     NSString * username = [NSString stringWithFormat:@"name: \"%@\",\n",delagate.user_name];
     NSString * userPIC = [NSString stringWithFormat:@"photoUrl: \"https://%@\",\n",delagate.user_image];
     NSString * userEmail = [NSString stringWithFormat:@"email: \"%@\",\n",delagate.user_email];
    
    str = [str stringByAppendingString:@"var other = new Talk.User({\n"];
    str = [str stringByAppendingString:userID];
    str = [str stringByAppendingString:username];
    str = [str stringByAppendingString:@"role: \"user\",\n"];
    str = [str stringByAppendingString:userEmail];
    str = [str stringByAppendingString:userPIC];
    
    
    
    
    str = [str stringByAppendingString:@" welcomeMessage: \"Hey there! How are you? :-)\"\n"];
    str = [str stringByAppendingString:@" });\n"];
    str = [str stringByAppendingString:@"\n"];
    str = [str stringByAppendingString:@"var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n"];
    str = [str stringByAppendingString:@"conversation.setParticipant(me);\n"];
    str = [str stringByAppendingString:@" conversation.setParticipant(other);\n"];
    str = [str stringByAppendingString:@"var inbox = talkSession.createInbox({selected: conversation});\n"];
    str = [str stringByAppendingString:@"inbox.mount(document.getElementById(\"talkjs-container\"));\n"];
    str = [str stringByAppendingString:@"});\n"];
    str = [str stringByAppendingString:@"</script>\n"];
    
    
    
    [self loadHtmlcontent:str];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
}



-(void)loadHtmlcontent:(NSString*)htmlcontent {
    
    
    NSString * htmlStr = [NSString stringWithFormat:@"<HTML><HEAD><meta name=\"viewport\" content=\"width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no\"></HEAD><BODY>%@</BODY></HTML>",htmlcontent];
    NSURL * url = [NSURL URLWithString:@"https://talkjs.com/dashboard/login"];
    [_wk_webView loadHTMLString:htmlStr baseURL: url];
    
    [_wk_webView sizeToFit];
    
    
    
}




- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
