//
//  chatviewController.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 24/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface chatviewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet WKWebView *wk_webView;

- (IBAction)backAction:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *headerLbl;

@property (weak, nonatomic) IBOutlet UIView *dotLine;



@end
