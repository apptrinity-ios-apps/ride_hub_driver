//
//  RatingsViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/3/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "RatingsViewController.h"

@interface RatingsViewController ()<RatingDelegate>

@end

@implementation RatingsViewController
@synthesize ratingHeaderLbl,txtLbl,ratingsTblView,rateRiderBtn,ratingsArray,custIndicatorView,rideid,commentTxtView,rateSuccessView,
rateOptionView,okBtn,dotLine,greenDotLine1,greenDotLine2;
AppDelegate * delegate;


NSString *behaviorButton;
NSString *wayOfButton;
NSString *approchButton;
NSInteger buttonIndex;
NSString *behaviourRate;
NSString *wayOfRate;

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    ratingsTblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.successBGVIEW.hidden = YES;
    
    wayOfRate = @"";
    behaviourRate = @"";
    behaviorButton = @"button0";
    wayOfButton = @"button0";
    
    self.rateSuccessView.layer.cornerRadius = 5;
    self.rateSuccessView.layer.masksToBounds = YES;
    
    self.okBtn.layer.cornerRadius = self.okBtn.frame.size.height/2;
    self.okBtn.layer.masksToBounds = YES;

    [self setFont];
    [self loadTxtView];
    [self getDatasForRating];
    
    [self setShadow:rateSuccessView];
    [self setShadow:okBtn];
    [self setShadow:rateRiderBtn];
    
    
    CAShapeLayer *yourViewBorder1 = [CAShapeLayer layer];
    yourViewBorder1.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder1.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder1.lineDashPattern = @[@3,@3];
    yourViewBorder1.frame = self.greenDotLine1.bounds;
    yourViewBorder1.path = [UIBezierPath bezierPathWithRect:self.greenDotLine1.bounds].CGPath;
    [self.greenDotLine1.layer addSublayer:yourViewBorder1];
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder1.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.lineDashPattern = @[@3,@3];
    yourViewBorder.frame = self.greenDotLine2.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.greenDotLine2.bounds].CGPath;
    [self.greenDotLine2.layer addSublayer:yourViewBorder];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)loadTxtView{
    commentTxtView.hidden=YES;
    commentTxtView.textColor=[UIColor darkGrayColor];
    commentTxtView.text=JJLocalizedString(@"Comment_Optional", nil);
    commentTxtView.textAlignment = NSTextAlignmentLeft;
    [commentTxtView setDelegate:self];
    commentTxtView.layer.borderWidth=1;
    commentTxtView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    commentTxtView.layer.cornerRadius=5;
}

-(void)setFont{
    [self.skipBtn setTitle:JJLocalizedString(@"Skip", nil) forState:UIControlStateNormal];
    [self.rateRiderBtn setTitle:JJLocalizedString(@"Rate_Rider", nil) forState:UIControlStateNormal];
    ratingHeaderLbl.text=JJLocalizedString(@"Please_Rate", nil);
    rateRiderBtn.hidden=YES;
//    ratingHeaderLbl=[Theme setHeaderFontForLabel:ratingHeaderLbl];
    txtLbl=[Theme setHeaderFontForLabel:txtLbl];
    commentTxtView=[Theme setNormalFontForTextView:commentTxtView];
    rateRiderBtn=[Theme setBoldFontForButton:rateRiderBtn];
    rateRiderBtn.layer.cornerRadius = rateRiderBtn.frame.size.height/2;
    rateRiderBtn.layer.masksToBounds=YES;
}

-(void)viewWillAppear:(BOOL)animated{
  [self.ratingsTblView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger rowCount=0;
    if([ratingsArray count]>0){
        rowCount=[ratingsArray count];
    }
    return rowCount;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RatingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ratingIdentifier"];
    if (cell == nil) {
        cell = [[RatingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:@"ratingIdentifier"];
        
    }
    
    cell.delegate=self;
    
    RatingsRecords * objRatingRec=[ratingsArray objectAtIndex:indexPath.row];
    [cell setRaingRecs:objRatingRec];
    [cell setIndexpath:indexPath];
    
    if (indexPath.row == 0) {
        
        [cell.star1 addTarget:self action:@selector(star1Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star2 addTarget:self action:@selector(star1Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star3 addTarget:self action:@selector(star1Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star4 addTarget:self action:@selector(star1Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star5 addTarget:self action:@selector(star1Tapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (behaviorButton == @"button1") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (behaviorButton == @"button2") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (behaviorButton == @"button3") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (behaviorButton == @"button4") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (behaviorButton == @"button5") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
        } else if (behaviorButton == @"button0"){
            [cell.star1 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        }
        
    } else {
        
        [cell.star1 addTarget:self action:@selector(star2Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star2 addTarget:self action:@selector(star2Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star3 addTarget:self action:@selector(star2Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star4 addTarget:self action:@selector(star2Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.star5 addTarget:self action:@selector(star2Tapped:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (wayOfButton == @"button1") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (wayOfButton == @"button2") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (wayOfButton == @"button3") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (wayOfButton == @"button4") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        } else if (wayOfButton == @"button5") {
            [cell.star1 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarFill"] forState:UIControlStateNormal];
        } else if (wayOfButton == @"button0"){
            [cell.star1 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star2 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star3 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star4 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
            [cell.star5 setImage:[UIImage imageNamed:@"StarEmpty"] forState:UIControlStateNormal];
        }
        
    }
    
       cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(IBAction)star1Tapped:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
     if (btn.tag == 0) {
         behaviorButton = @"button1";
         behaviourRate = @"1";
     } else if (btn.tag == 1) {
         behaviorButton = @"button2";
         behaviourRate = @"2";
     } else if (btn.tag == 2) {
         behaviorButton = @"button3";
         behaviourRate = @"3";
     } else if (btn.tag == 3) {
         behaviorButton = @"button4";
         behaviourRate = @"4";
     } else if (btn.tag == 4) {
         behaviorButton = @"button5";
         behaviourRate = @"5";
     }
    
    NSString * Value=[NSString stringWithFormat:@"%@",behaviourRate];
    
    RatingsRecords * objRatingsRecs=[ratingsArray objectAtIndex:0];
    objRatingsRecs.rateValue=Value;

    [ratingsArray setObject:objRatingsRecs atIndexedSubscript:0];

    [self.ratingsTblView reloadData];
}

-(IBAction)star2Tapped:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    if (btn.tag == 0) {
        wayOfButton = @"button1";
        wayOfRate = @"1";
    } else if (btn.tag == 1) {
        wayOfButton = @"button2";
        wayOfRate = @"2";
    } else if (btn.tag == 2) {
        wayOfButton = @"button3";
        wayOfRate = @"3";
    } else if (btn.tag == 3) {
        wayOfButton = @"button4";
        wayOfRate = @"4";
    } else if (btn.tag == 4) {
        wayOfButton = @"button5";
        wayOfRate = @"5";
    }
    
    NSString * Value=[NSString stringWithFormat:@"%@",wayOfRate];
    
    RatingsRecords * objRatingsRecs=[ratingsArray objectAtIndex:1];
    objRatingsRecs.rateValue=Value;
  
    [ratingsArray setObject:objRatingsRecs atIndexedSubscript:1];
    [self.ratingsTblView reloadData];
}

-(void)getDatasForRating {
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web RateUser:[self setParametersGettingRateData]
              success:^(NSMutableDictionary *responseDictionary)
     {
         ratingsArray=[[NSMutableArray alloc]init];
         
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             NSString * statusStr =[Theme checkNullValue:[responseDictionary objectForKey:@"ride_ratting_status"]];
             if ([statusStr isEqualToString:@"1"]) {
                 
                 rateOptionView.hidden=YES;
                 rateSuccessView.hidden=NO;
                 ratingHeaderLbl.text=JJLocalizedString(@"Thanks",nil);
                 
             }else{
                 for (NSDictionary * reasonDict in responseDictionary[@"review_options"]){
                     RatingsRecords * objRatingRecs=[[RatingsRecords alloc]init];
                     objRatingRecs.rateId=[Theme checkNullValue:reasonDict[@"option_id"]];
                     objRatingRecs.rateReason=[Theme checkNullValue:reasonDict[@"option_title"]];
                     objRatingRecs.rateValue=@"";
                     [ratingsArray addObject:objRatingRecs];
                 }
                 commentTxtView.hidden=NO;
                 [rateRiderBtn setHidden:NO];
                 [ratingsTblView reloadData];
             }
             
         }else{
             // [self.view makeToast:@"Some error occured"];
         }
     }
              failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];

}

-(NSDictionary *)setParametersGettingRateData{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
   
    NSDictionary *dictForuser = @{
                                  @"optionsFor":@"rider",
                                  @"ride_id" : delegate.appdelegateRideID,
                                  };

    return dictForuser;
}

-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
    
}
-(void)ratingSelected:(NSIndexPath *)index withValue:(float )rateValue {
    NSString * Value=[NSString stringWithFormat:@"%.2f",rateValue];
    
    RatingsRecords * objRatingsRecs=[ratingsArray objectAtIndex:index.row];
    objRatingsRecs.rateValue=Value;

    [ratingsArray setObject:objRatingsRecs atIndexedSubscript:index.row];
    [self.ratingsTblView beginUpdates];
    [self.ratingsTblView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [self.ratingsTblView endUpdates];
}


- (IBAction)didClickRateRideBtn:(id)sender {
    if([self setValidationForRating]){
        [self showActivityIndicator:YES];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web SubmitRateUser:[self setParametersForSubmitRating]
              success:^(NSMutableDictionary *responseDictionary)
         {
             [self stopActivityIndicator];
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 
                 self.successBGVIEW.hidden = NO;
                 self.rateSuccessView.hidden = NO;
                 
               //  [self.view makeToast:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
                // [self moveToHome];
                 
             }else{
                  [self.view makeToast:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
             }
             
         }
              failure:^(NSError *error)
         {
             [self stopActivityIndicator];
             // [self moveToHome];
             [self.view makeToast:kErrorMessage];
             
         }];
    }
}

-(void)moveToHome{
    BOOL isHome=NO;
    BOOL isRootSkip=NO;
    for (UIViewController *controller1 in self.navigationController.viewControllers) {
        
        if ([controller1 isKindOfClass:[HomeViewController class]]) {
            
            [self.navigationController popToViewController:controller1
                                                  animated:YES];
            isHome=YES;
            isRootSkip=YES;
            
            break;
        }
    }
    if(isHome==NO){
        for (UIViewController *controller1 in self.navigationController.viewControllers) {
            
            if ([controller1 isKindOfClass:[StarterViewController class]]) {
                
                [self.navigationController popToViewController:controller1
                                                      animated:YES];
                isRootSkip=YES;
                break;
            }
        }
    }
    if(isRootSkip==NO){
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}
- (IBAction)didClickSkipBtn:(id)sender {
    BOOL isHome=NO;
     BOOL isRootSkip=NO;
   
        for (UIViewController *controller1 in self.navigationController.viewControllers) {
            
            if ([controller1 isKindOfClass:[HomeViewController class]]) {
                
                [self.navigationController popToViewController:controller1
                                                      animated:YES];
                isHome=YES;
                  isRootSkip=YES;
                break;
            }
        }
        if(isHome==NO){
            for (UIViewController *controller1 in self.navigationController.viewControllers) {
                
                if ([controller1 isKindOfClass:[StarterViewController class]]) {
                    
                    [self.navigationController popToViewController:controller1
                                                          animated:YES];
                    isRootSkip=YES;
                    break;
                }
            }
        }
    
    if(isRootSkip==NO){
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}
-(BOOL)setValidationForRating{
    BOOL isValid=YES;
    for (int i=0; i<[ratingsArray count]; i++) {
        RatingsRecords * objRatingsRecs=[ratingsArray objectAtIndex:i];
        float rateVal=[objRatingsRecs.rateValue floatValue];
        if(rateVal==0){
            NSString * is_mandatory=JJLocalizedString(@"is_mandatory", nil);
            UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Oops !!" message:[NSString stringWithFormat:@"%@ %@",objRatingsRecs.rateReason,is_mandatory] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            isValid=NO;
            break;
        }
    }
    return isValid;
}

-(NSDictionary *)setParametersForSubmitRating {
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSString * cmtStr=[Theme checkNullValue:commentTxtView.text];
    if([cmtStr isEqualToString:JJLocalizedString(@"Comment_Optional", nil)]){
        cmtStr=@"";
    }
    NSMutableDictionary *dictForuser=[[NSMutableDictionary alloc] init];
    for (int i=0; i<[ratingsArray count]; i++) {
        NSString * str1=[NSString stringWithFormat:@"ratings[%d][option_title]",i];
        NSString * str2=[NSString stringWithFormat:@"ratings[%d][option_id]",i];
        NSString * str3=[NSString stringWithFormat:@"ratings[%d][rating]",i];
        RatingsRecords * objRatingsRecs=[ratingsArray objectAtIndex:i];
        [dictForuser setObject:objRatingsRecs.rateReason forKey:str1];
        [dictForuser setObject:objRatingsRecs.rateId forKey:str2];
        [dictForuser setObject:objRatingsRecs.rateValue forKey:str3];
    
//        NSString *str4 = behaviourRate;
//        [dictForuser setObject:str4 forKey:str3];
        
        }
    [dictForuser setObject:@"rider" forKey:@"ratingsFor"];
     [dictForuser setObject:delegate.appdelegateRideID forKey:@"ride_id"];
    
    [dictForuser setObject:[Theme checkNullValue:cmtStr] forKey:@"comments"];
    return dictForuser;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
   
    
   
    if ([textView.text isEqualToString:JJLocalizedString(@"Comment_Optional", nil)]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    
    if ([textView.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        
    }
    if ([textView.text isEqualToString:@""]) {
        textView.text = JJLocalizedString(@"Comment_Optional", nil);
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
     
        if ([textView.text isEqualToString:@""]) {
            textView.text = JJLocalizedString(@"Comment_Optional", nil);
            textView.textColor = [UIColor lightGrayColor]; //optional
        }

        return NO;
    }
    
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)okBtnAction:(id)sender {
    
    BOOL isHome=NO;
    BOOL isRootSkip=NO;

    for (UIViewController *controller1 in self.navigationController.viewControllers) {

        if ([controller1 isKindOfClass:[HomeViewController class]]) {

            [self.navigationController popToViewController:controller1
                                                  animated:YES];
            isHome=YES;
            isRootSkip=YES;
            break;
        }
    }
    
    if(isHome==NO){
        for (UIViewController *controller1 in self.navigationController.viewControllers) {
            
            if ([controller1 isKindOfClass:[StarterViewController class]]) {
                
                [self.navigationController popToViewController:controller1
                                                      animated:YES];
               isRootSkip=YES;
                break;
            }
        }
  }
    
    
  if(isRootSkip==NO){
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
    
    
   // [self moveToHome];
    
}
@end
