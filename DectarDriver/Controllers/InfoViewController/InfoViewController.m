//
//  InfoViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/26/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController
@synthesize infoScrollView,userImageView,userNameLbl,phoneNoBtn,ratingsView,cancelTripBtn,headerLbl,rideId,custIndicatorView,dotLIne;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFont];
    infoScrollView.hidden=YES;
    cancelTripBtn.userInteractionEnabled=NO;
    [self loadIUserData];
    
    
    self.cancelTripBtn.layer.cornerRadius = self.cancelTripBtn.frame.size.height/2;
    self.cancelTripBtn.layer.masksToBounds = YES;
    // Do any additional setup after loading the view.
}
-(void)setFont{
    userImageView.layer.cornerRadius=userImageView.frame.size.width/2;
    userImageView.layer.masksToBounds=YES;
//    headerLbl=[Theme setHeaderFontForLabel:headerLbl];
    userNameLbl=[Theme setHeaderFontForLabel:userNameLbl];
   // phoneNoBtn=[Theme setLargeFontForButton:phoneNoBtn];
    cancelTripBtn=[Theme setBoldFontForButton:cancelTripBtn];
    [cancelTripBtn setBackgroundColor:buttonskyBlue];
    [cancelTripBtn setTitle:JJLocalizedString(@"Cancel_Trip", nil) forState:UIControlStateNormal];
    [headerLbl setText:JJLocalizedString(@"Info", nil)];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLIne.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLIne.bounds].CGPath;
    [self.dotLIne.layer addSublayer:yourViewBorder2];
    
    [self setShadow:cancelTripBtn];
//    cancelTripBtn.layer.cornerRadius=2;
//    cancelTripBtn.layer.masksToBounds=YES;
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}


-(void)loadIUserData{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getUserInformation:[self setParametersForUserInformation]
                   success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             infoScrollView.hidden=NO;
             cancelTripBtn.userInteractionEnabled=YES;
             NSDictionary * UserDict=responseDictionary[@"response"][@"information"];
            
                 NSString * imgstr=[UserDict objectForKey:@"user_image"];
             
             [userImageView sd_setImageWithURL:[NSURL URLWithString:imgstr] placeholderImage:[UIImage imageNamed:@"PlaceHolderImg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                 
             }];
             
                 headerLbl.text=[Theme checkNullValue:[UserDict objectForKey:@"user_name"]];
                 userNameLbl.text=[Theme checkNullValue:[UserDict objectForKey:@"user_name"]];
             self.phoneStr=[Theme checkNullValue:[UserDict objectForKey:@"user_phone"]];
             
                 NSString * userReview=[Theme checkNullValue:[UserDict objectForKey:@"user_review"]];
             [self loadRating:userReview];
             
         }else{
             
             [self.view makeToast:kErrorMessage];
         }
     }
                   failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersForUserInformation{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":rideId
                                  };
    return dictForuser;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}

- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickPhoneBtn:(id)sender {
    NSString * phoneNum=[NSString stringWithFormat:@"tel:%@",self.phoneStr];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];
}

- (IBAction)didClickCancelTripBtn:(id)sender {
    
    
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    CancelRideViewController * objCancelVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CancelVCSID"];
    [objCancelVC setRideId:rideId];
    [self.navigationController pushViewController:objCancelVC animated:YES];
    
    
}
-(void)loadRating:(NSString *)ratingCount{
    float rateCount=[ratingCount floatValue];
    self.ratingsView.delegate = self;
    self.ratingsView.emptySelectedImage = [UIImage imageNamed:@"StarEmpty"];
    self.ratingsView.fullSelectedImage = [UIImage imageNamed:@"StarFill"];
    self.ratingsView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingsView.maxRating = 5;
    self.ratingsView.minRating = 1;
    self.ratingsView.rating = rateCount;
    self.ratingsView.editable = NO;
    self.ratingsView.halfRatings = YES;
    self.ratingsView.floatRatings = YES;
}
@end
