//
//  ChangePasswordCabViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 12/21/15.
//  Copyright © 2015 Casperon Technologies. All rights reserved.
//

#import "ChangePasswordCabViewController.h"

@interface ChangePasswordCabViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passTxt;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *changepasswordviewHghtconstraint;

@end

@implementation ChangePasswordCabViewController
@synthesize oldPassTxtField,retypePassTxtField,changePasswordScrollView,passTxt,headerView,dotLine,changebtn,oldPassView,passwordView,reTypePassView;
+ (instancetype) controller{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVCSID"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.oldPassTxtField.layer.borderWidth = 1;
    self.oldPassTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.passTxt.layer.borderWidth = 1;
    self.passTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.retypePassTxtField.layer.borderWidth = 1;
    self.retypePassTxtField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    self.changebtn.layer.cornerRadius = self.changebtn.frame.size.height/2;
    self.changebtn.layer.masksToBounds = YES;
    [self setShadow2:changebtn];
    [self setShadow2:oldPassView];
    [self setShadow2:passwordView];
    [self setShadow2:reTypePassView];

    oldPassView.layer.cornerRadius = oldPassView.frame.size.height/2;
    passwordView.layer.cornerRadius = passwordView.frame.size.height/2;
    reTypePassView.layer.cornerRadius = reTypePassView.frame.size.height/2;
    
    
    self.changepasswordviewHghtconstraint.constant = self.view.frame.size.height;
      [self.view bringSubviewToFront:headerView];
    self.oldPassTxtField.layer.cornerRadius = 25;
    self.oldPassTxtField.layer.masksToBounds = YES;
    
    self.retypePassTxtField.layer.cornerRadius = 25;
    self.retypePassTxtField.layer.masksToBounds = YES;
    
    self.passTxt.layer.cornerRadius = 25;
    self.passTxt.layer.masksToBounds = YES; // 11
    
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    
//    [self setMaskTo:oldPassTxtField byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight| UIRectCornerBottomLeft | UIRectCornerBottomRight)];
//    [self setMaskTo:retypePassTxtField byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight| UIRectCornerBottomLeft | UIRectCornerBottomRight)];
//    [self setMaskTo:passTxt byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight| UIRectCornerBottomLeft | UIRectCornerBottomRight)];
    
    [_headerlbl setText:JJLocalizedString(@"Change_Password", nil)];
    [changebtn setTitle:JJLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
//    [oldPassTxtField setPlaceholder:JJLocalizedString(@"Enter Old Password", nil)];
//    [retypePassTxtField setPlaceholder:JJLocalizedString(@"Retype New Password", nil)];
//    [passTxt setPlaceholder:JJLocalizedString(@"Enter New Password", nil)];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];   //it hides
}
- (IBAction)didClickMenuBtn:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
  
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

-(void)
setShadow2:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 2);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==oldPassTxtField){
        [changePasswordScrollView setContentOffset:CGPointMake(0.0, 50) animated:YES];
        
    }else{
        [changePasswordScrollView setContentOffset:CGPointMake(0.0,textField.frame.origin.y-170) animated:YES];
        
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [changePasswordScrollView setContentOffset:CGPointMake(0.0, 0) animated:YES];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    return YES;
}

- (IBAction)didClickChangePasswordBtn:(id)sender {
    if([self validateTxtFields]){
        [self.view endEditing:YES];
        
        [self showActivityIndicator:YES];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web ChangePassword:[self setParametersForForgotPassword]
                    success:^(NSMutableDictionary *responseDictionary)
         {
             self.view.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 
                 [self.view makeToast:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
             }else{
                 self.view.userInteractionEnabled=YES;
                [self.view makeToast:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
             }
         }
                    failure:^(NSError *error)
         {
             self.view.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             [self.view makeToast:kErrorMessage];
             
         }];
    }
}

-(NSDictionary *)setParametersForForgotPassword{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"password":oldPassTxtField.text,
                                  @"new_password":passTxt.text,
                                  };
    return dictForuser;
}
-(BOOL)validateTxtFields{
    if(oldPassTxtField.text.length==0){
        [self showErrorMessage:@"Old_Password_cannot_be_empty"];
        return NO;
    }
    if(passTxt.text.length==0){
        [self showErrorMessage:@"New_Password_cannot_be_empty"];
        return NO;
    }
    if(![passTxt.text isEqualToString:retypePassTxtField.text]){
        [self showErrorMessage:@"ReType_password_mismatches"];
        return NO;
    }
    
    return YES;
}
-(void)showErrorMessage:(NSString *)str{
    WBErrorNoticeView *notice = [WBErrorNoticeView errorNoticeInView:self.view title:@"Oops !!!" message:JJLocalizedString(str, nil)];
    [notice show];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
