//
//  OfflineViewController.m
//  RideHubDriver
//
//  Created by INDOBYTES on 23/03/20.
//  Copyright © 2020 Casperon Technologies. All rights reserved.
//

#import "OfflineViewController.h"
#import "DropLocationViewController.h"
#import "PickUpLocationViewController.h"
#import "SearchLocationViewController.h"


@interface OfflineViewController ()<offLinesearchLocation>

@end

@implementation OfflineViewController
+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"OfflineViewController"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view.
    self.pickUpView.layer.borderColor = [UIColor colorWithRed:6/255 green:152/255 blue:212/255 alpha:1.0].CGColor;
      self.pickUpView.layer.borderWidth = 1;
      self.pickUpView.layer.cornerRadius = self.pickUpView.frame.size.height / 2 ;
      self.pickUpView.clipsToBounds = YES;
    
    self.dropView.layer.borderColor = [UIColor colorWithRed:6/255 green:152/255 blue:212/255 alpha:1.0].CGColor;
      self.dropView.layer.borderWidth = 1;
      self.dropView.layer.cornerRadius = self.dropView.frame.size.height / 2 ;
      self.dropView.clipsToBounds = YES;
    
    self.userNameField.layer.borderColor = [UIColor colorWithRed:6/255 green:152/255 blue:212/255 alpha:1.0].CGColor;
        self.userNameField.layer.borderWidth = 1;
        self.userNameField.layer.cornerRadius = self.userNameField.frame.size.height / 2 ;
        self.userNameField.clipsToBounds = YES;
    
    self.userNumberField.layer.borderColor = [UIColor colorWithRed:6/255 green:152/255 blue:212/255 alpha:1.0].CGColor;
           self.userNumberField.layer.borderWidth = 1;
           self.userNumberField.layer.cornerRadius = self.userNumberField.frame.size.height / 2 ;
           self.userNumberField.clipsToBounds = YES;
    
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
       yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
       yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
       yourViewBorder2.lineDashPattern = @[@3,@3];
       yourViewBorder2.frame = self.dotView.bounds;
       yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotView.bounds].CGPath;
       [self.dotView.layer addSublayer:yourViewBorder2];
    
    
    
    
    
    
    
//    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
//    _userNumberField.leftView = paddingView;
//    _userNumberField.leftViewMode = UITextFieldViewModeAlways;
//
//    _userNameField.leftView = paddingView;
//    _userNameField.leftViewMode = UITextFieldViewModeAlways;
    
    
}
- (IBAction)pickUpAction:(id)sender {
//    SearchLocationViewController *OAVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
//       //  OAVC.delegate = self;
//       // [self.navigationController pushViewController:OAVC animated:NO];
//    [self presentViewController:OAVC animated:YES completion:nil];

 SearchLocationViewController *add = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
    [self presentViewController:add animated:YES completion:nil];

    
}
- (IBAction)dropAction:(id)sender {

//    SearchLocationViewController *OAVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
//         OAVC.delegate = self;
    SearchLocationViewController *add = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
    [self presentViewController:add animated:YES completion:nil];
}
- (IBAction)backAction:(id)sender {
    
   [self.view endEditing:YES];
       [self.frostedViewController.view endEditing:YES];
       [self.frostedViewController presentMenuViewController];
    
}
- (IBAction)submitAction:(id)sender {
    
    
    
    
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)offLinesearchLocation:(NSDictionary *)dictionary {
    
    NSDictionary *locationDict = [dictionary valueForKey:@"geometry"][@"location"];
                            NSString *addLatitude = locationDict[@"lat"];
                            NSString *addLongitude = locationDict[@"lng"];
                            
//                            NSDictionary *dictionary1 = @{@"latitude" : flStrForObj(addLatitude),
//                                                         @"longitude": flStrForObj(addLongitude),
//                                                         @"location" : flStrForObj(addressText)
//                                                         };
    
    NSLog(@"Typed %@",dictionary);
}

- (void)didCancelSearchLocation {
    
}


@end
