//
//  PickUpLocationViewController.m
//  Ezship
//
//  Created by surendra dathu on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//
#import "PickUpLocationViewController.h"
#import "AddressListViewController.h"
#import "SearchAddressTableViewCell.h"
#import "SaveAddressViewController.h"
#import "SearchLocationViewController.h"
#import "OrderAnythingViewController.h"
#import "MainViewController.h"
#import "SavePickUpLocationViewController.h"
#import "SearchLocationTableViewCell.h"
#import "GetCurrentLocation.h"
#import "CurrentLanguage.h"
@import GoogleMaps;
#define labelUnselected UIColorFromRGB(0x888888)
#define MAP_ZOOM_LEVEL 16
#define HeaderHeight 25
@interface PickUpLocationViewController ()<SavedAddressDelegate,SearchLocationDelegate,AddressListViewDelegate,SelectAddressViewControllerDelegate,SelectAddressViewControllerDelegate1,SelectedAddressViewControllerDelegate,UISearchControllerDelegate,UISearchBarDelegate,GMSAutocompleteViewControllerDelegate>
{
    GMSGeocoder *geocoder_;
    GMSMarker *locationMarker;
    BOOL isSearchBarBeganEditing;
    NSMutableDictionary *dictOfformatedAddress;
    NSString *customerID;
    BOOL iscomingFromSearchLocation;
    int addressType;
    int addressSearch;
    NSArray *arrayOfDBAddress;
    NSDictionary *address1;
    NSDictionary *address2;
    NSDictionary *arrayOfDBAddress1;
    Float32 homeLatitude;
    Float32 homeLongitude;
    Float32 officeLatitude;
    Float32 officeLongitude;
    NSMutableDictionary *dict;
    NSArray *arrayOfLocation;
    NSArray *arrayOfRecentSearch;
    BOOL isEstablistFound;
    UISearchController *searchController;
}
@property (strong, nonatomic) IBOutlet UIButton *setPickUpLocationButton;
@property (nonatomic, assign) float currentLatitude;
@property (nonatomic, assign) float currentLongitude;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end
@implementation PickUpLocationViewController
{
    NSArray *ApiMessage;
}

@synthesize mapView;
@synthesize currentLatitude;
@synthesize currentLongitude;
- (void)viewDidLoad {
    [super viewDidLoad];
    //[self initializeSearchController];
    
    ApiMessage = [NSArray arrayWithObjects: @"",@"DATABASE ERROR",@"ENTER ALL TAGS",@"DAILY QUOTA FOR SIGNUP REACHED IN DEVICE",@"OTP REQUEST EXCEEDED FOR DAY",@"PHONE NUMBER ALREADY REGISTERED",@"INVALID OTP",@"EMAIL ALREADY REGISTERED",@"TERM AND CONDITIONS NOT ACCEPTED",@"PASSWORD TRIES LIMIT EXCEDDED",@"PHONE NUMBER NOT EXIST",@"INVALID PASSWORD",@"PICKUP ZONE NOT IN RANGE",@"DROP ZONE NOT IN RANGE",@"PICKUP ZONE AND DROP ZONE MUST BE IN SAME CITY",@"FORGOT PASSWORD TRIES LIMIT EXCEDDED",@"CUSTOMER NOT FOUND",@"ORDER NOT FOUND",@"BIKER NOT FOUND",@"RECURSIVE ORDER NOT FOUND",@"ENTER CORRECT PHONE FORMAT",@"SESSION EXPIRED",@"VENDOR ALREADY REGISTERED",@"VENDOR NOT FOUND",@"VENDOR BULK ORDER NOT FOUND",@"CANNOT CANCEL THIS ORDER",@"OFFERCODEALREDYUSED",@"INVALIDDISCOINT",@"OFFER ID NOT FOUND",@"INVALID FROM TO DATES",@"OFFER CODE ALREADY USED",@"FIRST TIME CODE ALREADY USED",@"GROUP NOT FOUND",@"OFFER GROUP TYPE CODE ALREADY USED",@"OFFER LOCKED TRY AGAIN AFTER 30 MINUTES",@"INVALID REFERRAL CODE",@"SEASONAL OFFER EXPIRED",@"REFERRAL OFFER CODE ALREADY USED",nil];
    
    
    UIImage *image = [UIImage imageNamed:@"search.png"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _searchImageView.tintColor = UIColorFromRGB(0xEE9B0A);
    _searchImageView.image = image;

    
    addressSearch = 0;
    NSString *addressList = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"AddressList"];
    if([addressList  isEqual: @"1"]){
     [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"Check1"];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"Check"];
        _otherButton.hidden = YES;
        _otherLAbel.hidden = YES;
        _homeLabel.hidden = YES;
        _homeButton.hidden = YES;
        _officeButton.hidden = YES;
        _officeLabel.hidden = YES;
    }
    _otherButton.hidden = YES;
    _otherLAbel.hidden = YES;
    dict = [NSMutableDictionary dictionary];
    arrayOfLocation = [[NSArray alloc] init];
    arrayOfRecentSearch = [[NSArray alloc] init];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"searchButtonValue"];
    if([savedValue  isEqual: @"1"]){
        if([addressList  isEqual: @"1"]){
           self.navigationItem.title = @"Set Location";
             [_setPickUpLocationButton setTitle:@"SET LOCATION" forState:UIControlStateNormal];
        }else{
         self.navigationItem.title = @"Pickup Location";
            [_setPickUpLocationButton setTitle:@"SET PICKUP LOCATION" forState:UIControlStateNormal];
        }
        NSString *check = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"Check1"];
        NSLog(@"the check value is %@",check);
        if([check  isEqual: @"1"]){
            currentLatitude  = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLat];
            currentLongitude = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLong];
        }else{
            currentLatitude  = [[NSUserDefaults standardUserDefaults] floatForKey:@"latt"];
            currentLongitude = [[NSUserDefaults standardUserDefaults] floatForKey:@"lonn"];
        }
                    }else{
                        NSString *check = [[NSUserDefaults standardUserDefaults]
                                           stringForKey:@"Check2"];
                        NSLog(@"the check value is %@",check);
                        if([check  isEqual: @"1"]){
                            currentLatitude  = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLat];
                            currentLongitude = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLong];
                        }else{
                            currentLatitude  = [[NSUserDefaults standardUserDefaults] floatForKey:@"latt1"];
                            currentLongitude = [[NSUserDefaults standardUserDefaults] floatForKey:@"lonn1"];
                        }

                        if([addressList  isEqual: @"1"]){
                            self.navigationItem.title = @"Set Location";
                            [_setPickUpLocationButton setTitle:@"SET LOCATION" forState:UIControlStateNormal];
                        }else{
                            self.navigationItem.title = @"Drop Location";
                            [_setPickUpLocationButton setTitle:@"SET DROP LOCATION" forState:UIControlStateNormal];
                        }
           }
    [self.navigationController showNavigation];
    //[self backButtonAppearance];
    [self.navigationItem addLeftSpace];
    [self.navigationItem addRightSpace];
         geocoder_ = [[GMSGeocoder alloc] init];
    addressType = 3;
    _otherButton.selected = YES;
    self.pickUpAddressLabel.text = NSLocalizedString(@"Searching...", @"Searching...");
    customerID = [[NSUserDefaults standardUserDefaults]objectForKey:CustomerId];
    if(!customerID) {
        _otherButton.hidden = YES;
        _otherLAbel.hidden = YES;
        _homeLabel.hidden = YES;
        _homeButton.hidden = YES;
        _officeButton.hidden = YES;
        _officeLabel.hidden = YES;
    }
    else {
        if([addressList  isEqual: @"1"]){
        }else{
            [self getAddressFromDatabase];
        }
    }
    NSString *lat1 = [[NSNumber numberWithFloat:currentLatitude] stringValue];
    NSString *lon1 = [[NSNumber numberWithFloat:currentLongitude] stringValue];
    [self getAddressFromServer:lat1 longitude:lon1];
    [self loadRecentHistory];
    [self configMapView];
    
}
- (void)loadRecentHistory {
    arrayOfRecentSearch = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"recent_addresses"] mutableCopy];
    [self reloadTableWithAnimation];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *formattedString = [searchBar.text stringByReplacingOccurrencesOfString:@" "
                                                                          withString:@""];
    if (formattedString.length != 0) {
        [self searchAutocompleteLocationsWithSubstring:formattedString];
    }
    else {
        arrayOfLocation = nil;
        [self reloadTableWithAnimation];
    }
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self addDoneButtonToKeyboard:searchBar];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

#pragma mark - (Autocomplete SearchBar methods)

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)searchText {
    
    if (![[iDeliverReachabilityWrapper sharedInstance] isNetworkAvailable]) {
        [[ProgressIndicator sharedInstance]showMessage:NSLocalizedString(@"No Internet Connection",@"No Internet Connection")
                                                    On:self.view];
        return;
    }
    
    NSLog(@"Search String : %@",searchText);
    [self getGooglePlaceInformation:searchText
                       onCompletion:^(NSArray * results) {
                           
                           NSLog(@"Search result : %@",results);
                           if (results) {
                               arrayOfLocation = [results mutableCopy];
                               [self reloadTableWithAnimation];
                           }
                           else {
                               NSLog(@"Search result not found");
                           }
                       }];
}


#pragma mark - Google API Requests
- (void)getGooglePlaceInformation:(NSString *)searchWord
                     onCompletion:(void (^)(NSArray *))complete {
    
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&region=IN&components=country:IN&types=geocode&location=%f,%f&radius=500po&language=en&key=%@",searchWord,currentLatitude,currentLongitude,kGMServerGoogleMapsAPIKey];
        
        NSLog(@"AutoComplete URL: %@",urlString);
        
        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSURLSession *delegateFreeSession;
        delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                            delegate:nil
                                                       delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask *task;
        task = [delegateFreeSession dataTaskWithRequest:request
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          
                                          if (!data || !response || error) {
                                              NSLog(@"Google Service Error : %@",[error localizedDescription]);
                                              return;
                                          }
                                          
                                          NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                          NSArray *results = [jSONresult valueForKey:@"predictions"];
                                          
                                          if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]) {
                                              
                                              complete(nil);
                                          }
                                          else
                                          {
                                              complete(results);
                                          }
                                      }];
        [task resume];
    }
    
}
- (void)sendBackTransulent5{
    [_delegate1 sendBackTransulent4];
}
- (void)getAddressWithPlaceID:(NSString *)place
                 onCompletion:(void (^)(NSDictionary *))complete {
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,kGMServerGoogleMapsAPIKey];
    
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                                      delegate:nil
                                                                 delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            
                                                            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                       options:NSJSONReadingAllowFragments error:nil];
                                                            
                                                            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                                                                complete(nil);
                                                                return;
                                                            }
                                                            else{
                                                                complete([jSONresult valueForKey:@"result"]);
                                                            }
                                                        }];
    [task resume];
}
- (void)getNearbyPalces:(void(^)(NSArray *))complition {
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=5000&types=geocode&sensor=true&key=%@", currentLatitude, currentLongitude,kGMServerGoogleMapsAPIKey];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *delegateFreeSession;
    delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                        delegate:nil
                                                   delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task;
    task = [delegateFreeSession dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (!data || !response || error) {
                                          NSLog(@"Google Service Error : %@",[error localizedDescription]);
                                          return;
                                      }
                                      
                                      NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                      NSArray *results = [jSONresult valueForKey:@"results"];
                                      
                                      if (results.count) {
                                          complition(results);
                                      }
                                      else {
                                          complition(nil);
                                      }
                                  }];
    [task resume];
}

/**
 *  Reload table with animation
 */
- (void)reloadTableWithAnimation {
    
    NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:self.addressTableView]);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.addressTableView reloadSections:sections withRowAnimation:UITableViewRowAnimationAutomatic];
}
/*--------------------------------------*/
#pragma mark - Tableview Delegate methods
/*--------------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (arrayOfLocation.count) {
        return arrayOfLocation.count;
    }
    else {
        return arrayOfRecentSearch.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleIdentifier = @"SearchLocationTableViewCellIdentifier";
    SearchLocationTableViewCell *categoryCell;
    if (!categoryCell) {
        categoryCell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    }
    if (!arrayOfLocation.count) {
        categoryCell.locationLabel.text = flStrForObj(arrayOfRecentSearch[indexPath.row][@"location"]);
    }
    else {
        
        NSArray *arrayOfTerms = [arrayOfLocation[indexPath.row][@"terms"] mutableCopy];
        NSArray *arrayOfTypes = [arrayOfLocation[indexPath.row][@"types"] mutableCopy];
        
        isEstablistFound = NO;
        for (NSString *type in arrayOfTypes) {
            if ([type isEqualToString:@"establishment"]) {
                isEstablistFound = YES;
                break;
            }
        }
        NSString *customAddressText = @"";
        if (isEstablistFound) {
            customAddressText = [customAddressText stringByAppendingFormat:@"%@",flStrForObj(arrayOfTerms[0][@"value"])];
        }
        else {
            int count = 0;
            for (NSDictionary *dict in arrayOfTerms) {
                if (count == 2) {
                    break;
                }
                if (count == 0) {
                    customAddressText = [customAddressText stringByAppendingFormat:@"%@, ",flStrForObj(dict[@"value"])];
                }
                else {
                    customAddressText = [customAddressText stringByAppendingFormat:@"%@",flStrForObj(dict[@"value"])];
                }
                count++;
            }
        }
        categoryCell.locationLabel.text = flStrForObj(customAddressText);
    }
    categoryCell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return categoryCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        //SearchLocationTableViewCell *selectLocationCell = [tableView cellForRowAtIndexPath:indexPath];
    //selectLocationCell.accessoryType = UITableViewCellAccessoryCheckmark;
    [self.view endEditing:YES];
     addressSearch = 1;
    if (!arrayOfLocation.count) {
        [self addToRecords:arrayOfRecentSearch[indexPath.row]];
        CLLocationCoordinate2D coordinate;
        NSDictionary *dictionary = arrayOfRecentSearch[indexPath.row];
        _pickUpAddressLabel.text = [dictionary objectForKey:@"location"];
        NSLog(@"the search 1 is %@",_pickUpAddressLabel.text);
        NSLog(@"the search 2 is %@",[dictionary objectForKey:@"location"]);
        NSString *lat = [dictionary objectForKey:@"latitude"];
        coordinate.latitude = [lat floatValue];
        NSString *longi = [dictionary objectForKey:@"longitude"];
        coordinate.longitude = [longi floatValue];
        currentLatitude = coordinate.latitude;
        currentLongitude = coordinate.longitude;
        NSLog(@"the search lat longs is %@%@%@",arrayOfRecentSearch[indexPath.row],_pickUpAddressLabel.text,dictionary);
        [self updateMap:coordinate];
        searchController.searchBar.hidden = YES;
        _locationSearchView.hidden = YES;
        UIImage *btnImage = [UIImage imageNamed:@"back_btn_on.png"];
        [_backButton setImage:btnImage forState:UIControlStateNormal];
        self.navigationController.navigationBarHidden = NO;
    }
    else {
        [[ProgressIndicator sharedInstance] showPIOnView:[[UIApplication sharedApplication] keyWindow]
                                             withMessage:NSLocalizedString(@"Loading...", @"Loading...") ];
        NSString *placeID = arrayOfLocation[indexPath.row][@"reference"];
        [self getAddressWithPlaceID:placeID
                       onCompletion:^(NSDictionary *place) {
                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                           
                           // Check for Address Type
                           // Get Array of Types frpm address
                           NSArray *arrayOfTypes = [place valueForKey:@"types"];
                           
                           BOOL needToTakeName = NO;
                           
                           // Check if it is a company or Establishment
                           for (NSString *typeString in arrayOfTypes) {
                               
                               // If It is Establishment Just take name of Establiment
                               if ([typeString isEqual:@"establishment"]) {
                                   needToTakeName = YES;
                                   break;
                               }
                           }
                           NSString *addressText = @"";
                           // General Address, May be Area, Place, Address
                           if (!needToTakeName) {
                               
                               //                               NSString *zipcode;
                               NSString *state;
                               NSString *country;
                               NSString *city;
                               //                               NSString *subLocality1;
                               NSString *subLocality2;
                               NSString *route;
                               NSString *streetNumber;
                               //                               addressText = [place valueForKey:@"formatted_address"];
                               if(addressText.length == 0) {
                                   
                                   NSArray *dict = [place valueForKey:@"address_components"];
                                   for(int i=0;i<[dict count];i++) {
                                       
                                       NSString *type = [[dict valueForKey:@"types"] objectAtIndex:0];
                                       if([dict[i][@"types"][0] isEqualToString:@"route"]) {
                                           route = flStrForObj(dict[i][@"long_name"]);
                                       }
                                       if([dict[i][@"types"][0] isEqualToString:@"street_number"]) {
                                           streetNumber = flStrForObj(dict[i][@"long_name"]);
                                       }
                                       if ([dict[i][@"types"][0] isEqualToString:@"sublocality_level_1"]) {
                                           if([dict[i][@"types"][2] isEqualToString:@"political"])
                                               subLocality2 = flStrForObj(dict[i][@"long_name"]);
                                           
                                       }
                                       else if([dict[i][@"types"][0] isEqualToString:@"locality"]) {
                                           city = flStrForObj(dict[i][@"long_name"]);
                                       }
                                       else if ([dict[i][@"types"][0] isEqualToString:@"administrative_area_level_1"]) {
                                           state = flStrForObj(dict[i][@"long_name"]);
                                       }
                                       else if ([dict[i][@"types"][0] isEqualToString:@"country"]) {
                                           country = flStrForObj(dict[i][@"long_name"]);
                                       }
                                   }
                                   
                                   if(route.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",route];
                                   }
                                   if(streetNumber.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",streetNumber];
                                   }
                                   
                                   if (subLocality2.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",subLocality2];
                                   }
                                   if (city.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",city];
                                   }
                                   if(state.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",state];
                                   }
                                   if(country.length) {
                                       addressText = [addressText stringByAppendingFormat:@"%@, ",country];
                                   }
                                   if(addressText.length == 0) {
                                       addressText = flStrForObj([place valueForKey:@"name"]);
                                   }
                               }
                           }
                           else {
                               addressText = arrayOfLocation[indexPath.row][@"description"];
                               
                           }
                           
                           
                           NSDictionary *locationDict = [place valueForKey:@"geometry"][@"location"];
                           NSString *addLatitude = flStrForObj(locationDict[@"lat"]);
                           NSString *addLongitude = flStrForObj(locationDict[@"lng"]);
                           
                           NSDictionary *dictionary = @{@"latitude" :flStrForObj(addLatitude),
                                                        @"longitude":flStrForObj(addLongitude),
                                                        @"location" :flStrForObj(addressText)
                                                        };
                           
                           NSLog(@"Selected Address DIct : %@",dictionary);
                           [self addToRecords:dictionary];
                           // [self dismissPushedView];
                           //                           [self dismissViewControllerAnimated:YES completion:nil];
                                                      CLLocationCoordinate2D coordinate;
                           _pickUpAddressLabel.text = dictionary[@"location"];
                           NSLog(@"Selected dragg DIct : %@",dictionary[@"location"]);
                           coordinate.latitude = [dictionary[@"latitude"] doubleValue];
                           coordinate.longitude = [dictionary[@"longitude"] doubleValue];
                           currentLatitude = coordinate.latitude;
                           currentLongitude = coordinate.longitude;
                           NSLog(@"the cur lat longs is %f%f",currentLatitude,currentLongitude);
                           [self updateMap:coordinate];
                           searchController.searchBar.hidden = YES;
                           UIImage *btnImage = [UIImage imageNamed:@"back_btn_on.png"];
                           [_backButton setImage:btnImage forState:UIControlStateNormal];
                           _locationSearchView.hidden = YES;
                           self.navigationController.navigationBarHidden = NO;
//                           if (self.delegate && [self.delegate respondsToSelector:@selector(didApplySearchLocation:)]) {
//                               [self.delegate didApplySearchLocation:dictionary];
//                           }
                       }];
    }
}
- (void)addDoneButtonToKeyboard:(UISearchBar *)searchBar {
    UIToolbar* keyboardToolbar = [[UIToolbar alloc] init];
    [keyboardToolbar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(yourTextViewDoneButtonPressed)];
    keyboardToolbar.items = @[flexBarButton, doneBarButton];
    _searchBarr.inputAccessoryView = keyboardToolbar;
    }
- (void)yourTextViewDoneButtonPressed {
    [self.view endEditing:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *selectionHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), HeaderHeight)];
    selectionHeader.backgroundColor = HeaderColor;
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(selectionHeader.frame) - 20, CGRectGetHeight(selectionHeader.frame) - 10)];
    
    headerLabel.textColor = [UIColor grayColor];
    headerLabel.font = [UIFont fontWithName:HELVETICA_LIGHT size:10];
    [selectionHeader addSubview:headerLabel];
    
    if (!arrayOfLocation.count)
        headerLabel.text = NSLocalizedString(@"RECENT LOCATIONS", @"RECENT LOCATIONS");
    else
        headerLabel.text = NSLocalizedString(@"RESULTS", @"RESULTS");
    
    return selectionHeader;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (arrayOfLocation.count || arrayOfRecentSearch.count) {
        return HeaderHeight;
    }
    return 0;
}
- (void)didUpdateLocation {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [self detectYourLocationButtonAction:nil];
}
- (void)didFailLocation {
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:NSLocalizedString(@"Failed to get your current location.", @"Failed to get your current location.") ];
}
- (IBAction)backButtonAction:(id)sender {
    //if(_locationSearchView.hidden){
        [_delegate sendBackTransulent];
        [self.navigationController popViewControllerAnimated:NO];
        NSLog(@"YESS");
   // }else{
//        [self.view endEditing:YES];
//        UIImage *btnImage = [UIImage imageNamed:@"back_btn_on.png"];
//        [_backButton setImage:btnImage forState:UIControlStateNormal];
//        _locationSearchView.hidden = YES;
//         NSLog(@"NO");
   // }
}
- (IBAction)pickUpSearchButton:(id)sender {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.autocompleteFilter=[[GMSAutocompleteFilter alloc]  init];
    //    acController.autocompleteFilter.type = kGMSPlacesAutocompleteTypeFilterCity;
    acController.autocompleteFilter.country=@"IN";
    float nLat = 17.020102;
    float nLon = 78.001280;
    float sLat = 17.988266;
    float sLon = 78.982600;
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(nLat,nLon);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(sLat, sLon);
    acController.autocompleteBounds = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast
                                                                           coordinate:southWest];
    
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTintColor:UIColorFromRGB(0x4B3B2E)];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
    NSLog(@"OKK");
    

}
- (IBAction)otherAction:(UIButton *)sender {
    if (sender.selected) {
    }
    else {
        sender.selected = YES;
        self.otherLAbel.textColor = kNavigationTitleColor;
        addressType = 3;
        [self checkForAddressTypeButtonState];
    }
    AddressListViewController *addressVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddressListViewControllerSBID"];
    
    addressVC.delegate = self;
    addressVC.isItComingFromCheckOut = YES;
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    [self.navigationController pushViewController:addressVC animated:NO];

}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    NSLog(@"Place name %@", place.name);
    _pickUpAddressLabel.text = place.formattedAddress;
    CLLocationCoordinate2D coordinate;
    coordinate = place.coordinate;
    currentLatitude = coordinate.latitude;
    currentLongitude = coordinate.longitude;
    NSDictionary   *dict = @{@"address1"   :place.formattedAddress,
                             @"address2"   :@"",
                             @"type"       :@"",
                             @"latitude"   :[NSNumber numberWithDouble:currentLatitude],
                             @"longitude"  :[NSNumber numberWithDouble:currentLongitude],
                             @"addressID"  :[TimeFormate getTimeStamp],
                             };
    dictOfformatedAddress = [dict mutableCopy];
    [self configMapView];
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"Place currentLatitude %f", currentLatitude);
    NSLog(@"Place currentLongitude %f", currentLongitude);
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)officeAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"searchButtonValue"];
    NSString *home = [NSString stringWithFormat: @"%@", address2];
    NSLog(@"the saved value is %@",savedValue);
    if([savedValue  isEqual: @"1"]){
        [_delegate sendRecentPickUpAddress:home];
        NSLog(@"the lat officeLongitude are %f%f",officeLatitude,officeLongitude);
        [_delegate sendRecentLattitude:&(officeLatitude) sendRecentLongitude:&(officeLongitude)];
    }else{
        [_delegate sendRecentDropUpAddress:home];
        [_delegate sendRecentLattitude:&(officeLatitude) sendRecentLongitude:&(officeLongitude)];
    }
        addressType = 2;
}
- (IBAction)homeAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"searchButtonValue"];
    NSLog(@"the saved value is %@",savedValue);
    if([savedValue  isEqual: @"1"]){
        NSString *home = [NSString stringWithFormat: @"%@", address1];
        [_delegate sendRecentPickUpAddress:home];
        NSLog(@"the lat longs are %f%f",homeLongitude,homeLatitude);
        [_delegate sendRecentLattitude:&(homeLatitude) sendRecentLongitude:&(homeLongitude)];
            }else{
                NSString *home = [NSString stringWithFormat: @"%@", address1];
                [_delegate sendRecentDropUpAddress:home];
                [_delegate sendRecentLattitude:&(homeLatitude) sendRecentLongitude:&(homeLongitude)];
    }
        addressType = 1;
}
- (void)checkForAddressTypeButtonState {
    [self.view endEditing:YES];
    switch (addressType) {
        case 3:
            self.homeButton.selected = NO;
            self.officeButton.selected = NO;
            self.homeLabel.textColor = labelUnselected;
            self.officeLabel.textColor = labelUnselected;
            break;
        case 1:
            self.officeButton.selected = NO;
            self.otherButton.selected = NO;
            self.officeLabel.textColor = labelUnselected;
            self.otherLAbel.textColor = labelUnselected;
            break;
        case 2:
            self.homeButton.selected = NO;
            self.otherButton.selected = NO;
            self.homeLabel.textColor = labelUnselected;
            self.otherLAbel.textColor = labelUnselected;
            break;
    }
}

- (IBAction)setPickupLocationAction:(id)sender {
    
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"searchButtonValue"];
    NSString *storeLat1 = [[NSNumber numberWithFloat:currentLatitude] stringValue];
    NSString *storeLon1 = [[NSNumber numberWithFloat:currentLongitude] stringValue];
    [[NSUserDefaults standardUserDefaults] setObject:storeLon1 forKey:@"storeLon1"];
    [[NSUserDefaults standardUserDefaults] setObject:storeLat1 forKey:@"storeLat1"];

    if([savedValue  isEqual: @"1"]){
        
        
        [dict setObject:_pickUpAddressLabel.text forKey:@"address"];
        [dict setObject:@(currentLatitude) forKey:@"picLat"];
        [dict setObject:@(currentLongitude) forKey:@"picLon"];
    }else{
        //NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        [dict setObject:_pickUpAddressLabel.text forKey:@"address1"];
        [dict setObject:@(currentLatitude) forKey:@"picLat1"];
        [dict setObject:@(currentLongitude) forKey:@"picLon1"];
    }
    if([_pickUpAddressLabel.text  isEqual: @"Searching..."]){
        
    }else{
    if(_iscomingFromMainHomeView) {
        MainViewController *OAVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                  subType:kCATransitionFromTop
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        OAVC.pickupAddress = dictOfformatedAddress;
        [self.navigationController pushViewController:OAVC animated:NO];
    }else if (_iscomingFromOrderAnything) {
        [self.navigationController popViewControllerAnimated:YES];
        if(self.delegate && [self.delegate respondsToSelector:@selector(selectedAddress:)]) {
            [self.delegate selectedAddress:dictOfformatedAddress];
        }
    }
    else {
          SavePickUpLocationViewController *SLVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SavePickUpLocationViewController"];
    //        SLVC.isComingFromSelectAddres = YES;
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    [dictOfformatedAddress setObject:self.pickUpAddressLabel.text forKey:@"address1"];
    SLVC.dictOfAddress = dictOfformatedAddress;
    //SLVC.isComingFromAddressList = self.isComingFromAddressList;
        NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"searchButtonValue"];
                  //  NSLog(@"Adress %@ ",address);
        NSLog(@"the value is %@",savedValue);
                if([savedValue  isEqual: @"1"]){
        //[self.delegate sendDropTextToViewController:_pickUpAddressLabel.text];
        [self.delegate sendTextToViewController:_pickUpAddressLabel.text];
                    [_delegate sendLattitude:&(currentLatitude) sendLongitude:&(currentLongitude)];
        }else{
            [self.delegate sendDropTextToViewController:_pickUpAddressLabel.text];
            [_delegate sendLattitude:&(currentLatitude) sendLongitude:&(currentLongitude)];
           // [self.delegate sendLattitude:map sendLongitude:<#(double *)#>];
        }
    SLVC.delegate = self;
    [self.navigationController pushViewController:SLVC animated:NO];
    
    }
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    isSearchBarBeganEditing = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
}
//set current location marker
- (void)setMarkerOnCurrentLocation {
    CGFloat latitude = [[[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat]floatValue];
    CGFloat longitude = [[[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong]floatValue];
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.icon = [UIImage imageNamed:(@"drop_location_dot_icon.png")];
    marker.position = CLLocationCoordinate2DMake(latitude, longitude);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView;
}
- (void)configMapView
{
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(currentLatitude, currentLongitude);
    mapView.settings.compassButton = YES;
    mapView.delegate = self;
    mapView.settings.myLocationButton = YES;
    [mapView clear];
    [self updateMap:position];
}
- (void)updateMap:(CLLocationCoordinate2D)position {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:position
                                                               zoom:MAP_ZOOM_LEVEL];
    [mapView setCamera:camera];
}
- (void)getAddressFromDatabase
{
    if (![[iDeliverReachabilityWrapper sharedInstance] isNetworkAvailable]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:NSLocalizedString(@"Seems like you are offline at the moment.Please try again once you are connected to the internet.", @"Seems like you are offline at the moment.Please try again once you are connected to the internet.")];
        return;
    }
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...") ];
    NetworkHandler *networkHandler = [NetworkHandler sharedInstance];
    customerID = flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:CustomerId]);
    [networkHandler composeRequestForCartWithMethod:@"SendThreeAddress"
                                            paramas:@{@"customerId":[[NSUserDefaults standardUserDefaults] objectForKey:CustomerId]}
                                       onComplition:^(BOOL succeeded, NSDictionary *response) {
                                           NSLog(@"response for the get address %@",response);
                                           [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                           if(succeeded) {
                                               if([response[@"errNum"] integerValue] == 1005) {
                                                   [Helper removeSaveData];
                                                   [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                                   return;
                                               }
                                               else if ([response[@"errFlag"] integerValue] == 0) {
                                                   NSLog(@"the address log is %@",response[@"AddressLog"]);
                                                   [self setAddressFromResponse:response[@"AddressLog"]];
                                               }
                                               else {
                                                   [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                               }
                                           }
                                           else {
                                           }
                                       }];
}
- (void)setAddressFromResponse : (NSArray*)addresses  {
   arrayOfDBAddress = [[NSArray alloc] initWithArray:addresses];
    NSLog(@"the arrayOfDBAddress count is %lu",(unsigned long)arrayOfDBAddress.count);
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"searchButtonValue"];
    NSLog(@"the saved value is %@",savedValue);
    if([savedValue  isEqual: @"1"]){
        if(arrayOfDBAddress.count == 0){
            _otherButton.hidden = NO;
            _otherLAbel.hidden = NO;
        }else if(arrayOfDBAddress.count == 1){
                _otherButton.hidden = NO;
                _otherLAbel.hidden = NO;
                _homeButton.hidden = NO;
                _homeLabel.hidden = NO;
                _officeLabel.hidden = YES;
                _officeButton.hidden = YES;
                [self setData];
            }
            else if(arrayOfDBAddress.count == 2){
                _otherButton.hidden = NO;
                _otherLAbel.hidden = NO;
                _homeButton.hidden = NO;
                _homeLabel.hidden = NO;
                _officeLabel.hidden = NO;
                _officeButton.hidden = NO;
                [self setData];
            }
            else if(arrayOfDBAddress.count == 3){
                _otherButton.hidden = NO;
                _otherLAbel.hidden = NO;
                _homeButton.hidden = NO;
                _homeLabel.hidden = NO;
                _officeLabel.hidden = NO;
                _officeButton.hidden = NO;
                [self setData];
            }
    }else if ([savedValue  isEqual: @"2"]){
        if(arrayOfDBAddress.count == 0){
            _otherButton.hidden = NO;
            _otherLAbel.hidden = NO;
        }else if(arrayOfDBAddress.count == 1){
            _otherButton.hidden = NO;
            _otherLAbel.hidden = NO;
            _homeButton.hidden = YES;
            _homeLabel.hidden = YES;
            _officeLabel.hidden = YES;
            _officeButton.hidden = YES;
            //[self setData];
        }
        else if(arrayOfDBAddress.count == 2){
            _otherButton.hidden = NO;
            _otherLAbel.hidden = NO;
            _homeButton.hidden = NO;
            _homeLabel.hidden = NO;
            _officeLabel.hidden = YES;
            _officeButton.hidden = YES;
            [self setData];
        }
        else if(arrayOfDBAddress.count == 3){
            _otherButton.hidden = NO;
            _otherLAbel.hidden = NO;
            _homeButton.hidden = NO;
            _homeLabel.hidden = NO;
            _officeLabel.hidden = NO;
            _officeButton.hidden = NO;
            [self setData];
        }

    }
}
-(void)setData{
    for (int i = 0; i < [arrayOfDBAddress count]; i++)
    {
        _homeButton.tag = i;
        _officeButton.tag = i;
        NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"searchButtonValue"];
        if([savedValue  isEqual: @"1"]){
            if(i == 0){
//                _homeButton.tag = i;
//                _officeButton.tag = i;
                arrayOfDBAddress1 = arrayOfDBAddress[i];
                address1 = [arrayOfDBAddress1 objectForKey:@"address1"];
                NSLog(@"the address1 is %@",address1);
                NSMutableString *home = [NSMutableString stringWithFormat: @"%@", address1];
                NSArray * arr = [home componentsSeparatedByString:@","];
                NSString *combined = [NSString stringWithFormat:@"%@%s%@", arr[0], " ", arr[1]];
//                if(arr.count > 0){
//                   //                }
                //    NSDictionary *Latitude = [arrayOfDBAddress1 objectForKey:@"latitude"];
                //    NSDictionary *longitude = [arrayOfDBAddress1 objectForKey:@"longitude"];
                homeLatitude = [[arrayOfDBAddress1 objectForKey:@"latitude"] floatValue];
                homeLongitude = [[arrayOfDBAddress1 objectForKey:@"longitude"] floatValue];
                _homeLabel.text = combined;
            }else if(i == 1){
                NSDictionary *arrayOfDBAddress5;
                arrayOfDBAddress5 = arrayOfDBAddress[i];
                address2 = [arrayOfDBAddress5 objectForKey:@"address1"];
                NSLog(@"the address2 is %@",address2);
                NSMutableString *home = [NSMutableString stringWithFormat: @"%@", address2];
                NSArray * arr = [home componentsSeparatedByString:@","];
                if(arr.count>1){
                NSString *combined = [NSString stringWithFormat:@"%@%s%@", arr[0], " ", arr[1]];
                    
                officeLatitude = [[arrayOfDBAddress5 objectForKey:@"latitude"] floatValue];
                officeLongitude = [[arrayOfDBAddress5 objectForKey:@"longitude"] floatValue];
                _officeLabel.text = combined;
                }else{
                    officeLatitude = [[arrayOfDBAddress5 objectForKey:@"latitude"] floatValue];
                    officeLongitude = [[arrayOfDBAddress5 objectForKey:@"longitude"] floatValue];
                    _officeLabel.text = home;
                }
            }
        }else{
            if(i == 1){
//                _homeButton.tag = i;
//                _officeButton.tag = i;
                NSDictionary *arrayOfDBAddress6;
                arrayOfDBAddress6 = arrayOfDBAddress[i];
                address1 = [arrayOfDBAddress6 objectForKey:@"address1"];
                NSLog(@"the address1 is %@",address1);
                NSMutableString *home = [NSMutableString stringWithFormat: @"%@", address1];
                NSArray * arr = [home componentsSeparatedByString:@","];
                  if(arr.count>1){
                NSString *combined = [NSString stringWithFormat:@"%@%s%@", arr[0], " ", arr[1]];
                homeLatitude = [[arrayOfDBAddress6 objectForKey:@"latitude"] floatValue];
                homeLongitude = [[arrayOfDBAddress6 objectForKey:@"longitude"] floatValue];
                _homeLabel.text = combined;
                  }else{
                     
                      homeLatitude = [[arrayOfDBAddress6 objectForKey:@"latitude"] floatValue];
                      homeLongitude = [[arrayOfDBAddress6 objectForKey:@"longitude"] floatValue];
                      _homeLabel.text = home;
                  }
            }else if(i == 2){
//                _homeButton.tag = i;
//                _officeButton.tag = i;
                NSDictionary *arrayOfDBAddress6;
                arrayOfDBAddress6 = arrayOfDBAddress[i];
                address2 = [arrayOfDBAddress6 objectForKey:@"address1"];
                NSLog(@"the address2 is %@",address2);
                NSMutableString *home = [NSMutableString stringWithFormat: @"%@", address2];
                NSArray * arr = [home componentsSeparatedByString:@","];
                if(arr.count>1){

                
                        NSString *combined = [NSString stringWithFormat:@"%@%s%@", arr[0], " ", arr[1]];
                        officeLatitude = [[arrayOfDBAddress6 objectForKey:@"latitude"] floatValue];
                        officeLongitude = [[arrayOfDBAddress6 objectForKey:@"longitude"] floatValue];
                        _officeLabel.text = combined;
                    
                }else{
                    
                        officeLatitude = [[arrayOfDBAddress6 objectForKey:@"latitude"] floatValue];
                        officeLongitude = [[arrayOfDBAddress6 objectForKey:@"longitude"] floatValue];
                        _officeLabel.text = home;
                }
            }
        }
    }
}
-(void)sendTransulent{
    NSLog(@"Sucess is sucess");
    [_delegate sendBackTransulent1];
}
- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        if (response && response.firstResult) {
            GMSAddress *address = response.firstResult;
            NSLog(@"Adress %@ ",address);
            NSString *formattedString = @"";
            if (address.thoroughfare) {
                formattedString = [formattedString stringByAppendingFormat:@"%@",address.thoroughfare];
            }
            if (address.subLocality) {
                if (formattedString.length == 0) {
                    formattedString = [formattedString stringByAppendingFormat:@"%@",address.subLocality];
                }
                else {
                    formattedString = [formattedString stringByAppendingFormat:@", %@",address.subLocality];
                }
            }
            if (address.locality) {
                if (formattedString.length == 0) {
                    formattedString = [formattedString stringByAppendingFormat:@"%@",address.locality];
                }
                else {
                    formattedString = [formattedString stringByAppendingFormat:@", %@",address.locality];
                }
            }
            if (address.administrativeArea) {
                
                if (formattedString.length == 0) {
                    formattedString = [formattedString stringByAppendingFormat:@"%@",address.administrativeArea];
                }
                else {
                    formattedString = [formattedString stringByAppendingFormat:@", %@",address.administrativeArea];
                }
            }
            if (address.postalCode) {
                if (formattedString.length == 0) {
                    formattedString = [formattedString stringByAppendingFormat:@"%@",address.postalCode];
                }
                else {
                    formattedString = [formattedString stringByAppendingFormat:@"-%@",address.postalCode];
                }
            }
//            if(addressSearch == 0) {
//                self.pickUpAddressLabel.text = formattedString;
//            }
//            else {
//                addressSearch = 0;
//            }
            CLLocationCoordinate2D addressCoordinates;
            addressCoordinates = address.coordinate;
            currentLatitude  = addressCoordinates.latitude;
            currentLongitude = addressCoordinates.longitude;
            NSLog(@"the cur lat longs is %f%f",currentLatitude,currentLongitude);
//            [self.delegate sendLattitude:&addressCoordinates.latitude];
//            [self.delegate sendLongitude:&addressCoordinates.longitude];
//[self.delegate sendCooridinate:&addressCoordinates];
                        NSDictionary   *dict = @{@"address1"   :flStrForObj(formattedString),
                                     @"address2"   :@"",
                                     @"type"       :@"",
                                     @"zipCode"    :flStrForObj(address.postalCode),
                                     @"city"       :flStrForObj(address.locality),
                                     @"latitude"   :[NSNumber numberWithDouble:currentLatitude],
                                     @"longitude"  :[NSNumber numberWithDouble:currentLongitude],
                                     @"addressID"  :[TimeFormate getTimeStamp],
                                     };
            dictOfformatedAddress = [dict mutableCopy];
        }
    };
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
}
/*-------------------------------*/
#pragma mark - GMSMapview Delegate
/*-------------------------------*/
- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    [self.view endEditing:YES];
}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    CGPoint point1 = self.mapView.center;
    CLLocationCoordinate2D coordinate = [self.mapView.projection coordinateForPoint:point1];
    NSLog(@"datos.lon %f", coordinate.longitude);
    NSLog(@"datos.lat %f", coordinate.latitude);
    NSString *lat = [[NSNumber numberWithFloat:coordinate.latitude] stringValue];
    NSString *lon = [[NSNumber numberWithFloat:coordinate.longitude] stringValue];
    [self getAddressFromServer:lat longitude:lon];
//    NSString *combined = [NSString stringWithFormat:@"%@%@%@", lat,@",", lon];
//    NSString *url = @"https://maps.googleapis.com/maps/api/geocode/json?latlng=";
//    NSString *url2 = @"&key=AIzaSyCOSzrV21ii_0a6d8XC08vARjJH2TYbLJs";
//    NSString *finalUrl = [NSString stringWithFormat:@"%@%@%@", url,combined, url2];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    
//    //NSString *stgLat = [lat ]
//    
//    
//    
//    
//    [request setURL:[NSURL URLWithString:finalUrl]];
//    
//    [request setHTTPMethod:@"GET"];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        
//       // NSDictionary *requestReply = [[NSDictionary alloc] initWithData:data encoding:NSASCIIStringEncoding];
//        NSDictionary *requestReply = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
//        
//        NSArray *results = [requestReply objectForKey:@"results"];
////        if (results.count == 0) {
////             _pickUpAddressLabel.text = @"Location Not found";
////        }else{
//            NSDictionary *results1 = results[0];
//            NSString *address = [results1 objectForKey:@"formatted_address"];
//            _pickUpAddressLabel.text = address;
//            NSLog(@"Request reply: %@", address);
////        }
//        
//    }] resume];
//    
    [self getAddress:coordinate];
}
#pragma mark (MyLocation button Action)
- (BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView {
    
    float lat = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLat];
    float lng = [[NSUserDefaults standardUserDefaults] floatForKey:KNUCurrentLong];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat,lng);
    [self updateMap:position];
    return YES;
}
- (void)didApplySearchLocation:(NSDictionary *)dictionary {
    BOOL shouldGoBack = NO;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [dictionary[@"latitude"] doubleValue];
    coordinate.longitude = [dictionary [@"longitude"] doubleValue];
    currentLatitude = coordinate.latitude;
    currentLongitude = coordinate.longitude;
    NSLog(@"the cur lat longs is %f%f",currentLatitude,currentLongitude);
    [self updateMap:coordinate];
     if([dictionary[@"location"] length]>0) {
        self.pickUpAddressLabel.text = dictionary[@"location"];
    }else {
        self.pickUpAddressLabel.text = dictionary[@"address1"];
        shouldGoBack = YES;
    }
    iscomingFromSearchLocation = YES;
    if(shouldGoBack) {
        [self didAddressSave:dictionary];
    }
}
- (void)getAddressFromServer:(NSString *)Latitude longitude:(NSString *)Longtude
{
    if (![[iDeliverReachabilityWrapper sharedInstance] isNetworkAvailable]) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:NSLocalizedString(@"Seems like you are offline at the moment.Please try again once you are connected to the internet.", @"Seems like you are offline at the moment.Please try again once you are connected to the internet.") ];
        return;
    }
    self.pickUpAddressLabel.text = @"Searching...";
   // [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NetworkHandler *networkHandler = [NetworkHandler sharedInstance];
    [networkHandler composeRequestForCartWithMethod:@"Lat_Long_To_Address"
                                            paramas:@{@"Latitude":Latitude,@"Longitude":Longtude}
                                       onComplition:^(BOOL succeeded, NSDictionary *response) {
                                           NSLog(@"response for offers get code %@",response);
                                           //[[ProgressIndicator sharedInstance]hideProgressIndicator];
                                           BOOL success = response[@"success"];
                                           NSLog(@"the success is %d",success);
                                           NSDictionary *extras = response[@"extras"];
                                           NSLog(@"zone extras is %@",extras);
                                           if(success) {
                                               NSDictionary *status = extras[@"Status"];
                                               //NSDictionary *zonestatus = extras[@"ZoneStatus"];
                                               NSDictionary *addressData = [extras objectForKey:@"AddressData"];
                                               //NSDictionary *addressFound = [addressData objectForKey:@"Address_Found"];
                                               int addressFound = [[addressData valueForKey:@"Address_Found"] integerValue];
                                               NSLog(@"the address success is %d",addressFound);
                                               NSDictionary *address = [addressData objectForKey:@"Address"];
                                              // if([addressFound  isEqual: @"1"]){
                                                  NSString *serverAddress =  [NSString stringWithFormat:@"%@", address];
                                               if(addressFound == 1){
                                                   self.pickUpAddressLabel.text = serverAddress;
                                               }else{
                                                   self.pickUpAddressLabel.text = @"No Location Found";
                                               }
                                               
                                               //}else{
                                                   //self.pickUpAddressLabel.text = @"Searching";
                                               //}
                                               NSLog(@"the addressData is %@",addressData);
                                                                                              NSLog(@"the address status is %@",address);
                                               
                                           }
                                           else {
                                               NSInteger msg1 = [[extras valueForKey:@"msg"] integerValue];
                                               NSString* msg2 = [NSString stringWithFormat:@"%ld", (long)msg1];
                                               if( msg2 && ![msg2 isKindOfClass:[NSNull class]] )
                                               {
                                                   
                                                   [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:@"Network error please try again"];
                                                   //NSLog(@"NSString *tempPhone = %@",tempPhone);
                                               }
                                               else
                                               {
                                                   [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:(ApiMessage[msg1])];
                                               }
                                               NSLog(@"the msg is %ld",(long)msg1);
                                               
                                           }
                                       }];
}
















- (void)didCancelSearchLocation {
}
- (void)didAddressSelect:(NSDictionary *)addressDict {
    [self.navigationController popViewControllerAnimated:NO];
    NSLog(@"the address dict is %@",addressDict);
    //[self.delegate sendSavedAddress:addressDict[@"address1"]];
//    float latitude = [[addressDict objectForKey:@"latitude"] floatValue];
//    float longiTude = [[addressDict objectForKey:@"longitude"] floatValue];
    //NSLog(@"the address lat  dict is %f",longiTude);
    //float latitude = addressDict[@"latitude"];
   // [self.delegate sendSavedLattitude:addressDict[@"latitude"] sendSavedLongitude:addressDict[@"longitude"]];
   // [self.delegate sendSavedLattitude:addressDict[@"latitude"] sendSavedLongitude:addressDict[@"longitude"]];
//    if(self.delegate && [self.delegate respondsToSelector:@selector(selectedAddress:)]) {
//        [self.delegate selectedAddress:addressDict];
//    }
}
- (void)didAddressSave:(NSDictionary*)savedAddress {
   [self.navigationController popViewControllerAnimated:NO];
    if(self.delegate && [self.delegate respondsToSelector:@selector(selectedAddress:)]) {
        [self.delegate selectedAddress:savedAddress];
    }
}
- (void)sendLattitude:(NSString *)Latitude sendLongitude:(NSString *)Longitude sendAddress:(NSString *)address{
    [self.navigationController popViewControllerAnimated:NO];

    [self.delegate sendSavedAddress:address];
    [self.delegate sendSavedLattitude:Latitude sendSavedLongitude:Longitude];
    if(self.delegate && [self.delegate respondsToSelector:@selector(selectedAddress:)]) {
       // [self.delegate selectedAddress:addressDict];
    }
}
/*-------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*-------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification
{
}
/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self.view endEditing:YES];
}
/**
 *  Method to Hide the Keyboard
 */
- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)detectYourLocationButtonAction:(id)sender {
    if(![[NSUserDefaults standardUserDefaults] objectForKey:FormattedAddress]) {
        if (![[iDeliverReachabilityWrapper sharedInstance] isNetworkAvailable]) {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message")  Message:NSLocalizedString(@"Seems like you are offline at the moment.Please try again once you are connected to the internet.", @"Seems like you are offline at the moment.Please try again once you are connected to the internet.") ];
            //            [[ProgressIndicator sharedInstance] showMessage:NSLocalizedString(@"No Internet Connection",@"No Internet Connection") On:self.view];
            return;
        }
        [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Getting your current location.", @"Getting your current location.") ];
        GetCurrentLocation *getCurrentLocationObj = [GetCurrentLocation sharedInstance];
        [getCurrentLocationObj getLocation];
        [getCurrentLocationObj setDelegate:self];
        return;
    }
    
    NSString *currentLat;
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        currentLat = @"";
    else
        currentLat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *currentLong;
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        currentLong = @"";
    else
        currentLong = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    NSString *locationString;
    
    locationString = [[NSUserDefaults standardUserDefaults] objectForKey:FormattedAddress];
    NSDictionary *dictionary = @{@"latitude" :flStrForObj(currentLat),
                                 @"longitude":flStrForObj(currentLong),
                                 @"location" :flStrForObj(locationString)
                                 };
       CLLocationCoordinate2D coordinate;
    _pickUpAddressLabel.text = locationString;
    currentLatitude = [dictionary[@"latitude"] doubleValue];
    currentLongitude = [dictionary[@"longitude"] doubleValue];
    NSLog(@"the location search is %@%f%f",locationString,currentLatitude,currentLongitude);
    currentLatitude = coordinate.latitude;
    currentLongitude = coordinate.longitude;
    NSLog(@"the cur lat longs is %f%f",currentLatitude,currentLongitude);
    [self updateMap:coordinate];
    _locationSearchView.hidden = YES;
}
- (void)initializeSearchController {
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    //searchController.searchResultsUpdater = self;
    searchController.dimsBackgroundDuringPresentation = NO;
    searchController.delegate = self;
    searchController.searchBar.delegate = self;
    searchController.searchBar.showsCancelButton = NO;
    [searchController.searchBar sizeToFit];
    [self.addressTableView setTableHeaderView:searchController.searchBar];
    self.definesPresentationContext = YES;
}
- (void)addToRecords:(NSDictionary *)dictionary {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    NSArray *oldRecords = [[ud arrayForKey:@"recent_addresses"] mutableCopy];
    [ud removeObjectForKey:@"recent_addresses"];
    BOOL isPresentAlready = NO;
    for (NSDictionary *dict in oldRecords) {
        if ([dict[@"location"] isEqual:dictionary[@"location"]]) {
            isPresentAlready = YES;
            break;
        }
    }
    // Ignore if Data is already Present in Records
    if (!isPresentAlready) {
        [tempArray addObject:dictionary];
    }
    // Check for Order Count
    if (oldRecords.count) {
        // Add into Temp Array
        [tempArray addObjectsFromArray:oldRecords];
    }
    
    // Add all the Records Old + New Records in NSuserDefaults
    [ud setObject:tempArray forKey:@"recent_addresses"];
    [ud synchronize];
    
    [self loadRecentHistory];
    //    }
}

- (void)dismissPushedView {
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(didCancelSearchLocation)]) {
        //[self.delegate didCancelSearchLocation];
    }
}

@end
