//
//  SearchLocationViewController.m
//  HandsFree
//
//  Created by Rahul Sharma on 20/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "SearchLocationViewController.h"
#import "SearchLocationTableViewCell.h"
#import "AppDelegate.h"

//#import "CurrentLanguage.h"

@interface SearchLocationViewController ()
{
    double currentLatitude;
    double currentLongitude;
    
    NSArray *arrayOfLocation;
    NSArray *arrayOfRecentSearch;
    BOOL isEstablistFound;
}
@end

@implementation SearchLocationViewController
@synthesize filteredContentList;
#define HeaderHeight 25

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrayOfLocation = [[NSArray alloc] init];
    arrayOfRecentSearch = [[NSArray alloc] init];
    
    self.delegate = self;

    //    self.navigationController.navigationBar.hidden = NO;
   
   
    [_addressSearchBar setShowsCancelButton:YES animated:YES];
    [self loadRecentHistory];
    
//    currentLatitude = [[[NSUserDefaults standardUserDefaults] stringForKey:KNUCurrentLat] doubleValue];
//    currentLongitude = [[[NSUserDefaults standardUserDefaults] stringForKey:KNUCurrentLong] doubleValue];
    
}
- (void)viewWillDisappear:(BOOL)animated {
    
}
/**
 *  Reload Recent History
 */
- (void)loadRecentHistory {
    arrayOfRecentSearch = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"recent_addresses"] mutableCopy];
    [self reloadTableWithAnimation];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Bar Delegates

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.view endEditing:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSString *formattedString = [searchBar.text stringByReplacingOccurrencesOfString:@" "
                                                                          withString:@""];
    if (formattedString.length != 0) {
        [self searchAutocompleteLocationsWithSubstring:formattedString];
    }
    else {
        arrayOfLocation = nil;
        [self reloadTableWithAnimation];
    }
}





- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}


#pragma mark - (Autocomplete SearchBar methods)


- (void)searchAutocompleteLocationsWithSubstring:(NSString *)searchText {
    
//    if (![[iDeliverReachabilityWrapper sharedInstance] isNetworkAvailable]) {
//        [[ProgressIndicator sharedInstance]showMessage:NSLocalizedString(@"No Internet Connection",@"No Internet Connection")
//                                                    On:self.view];
//        return;
//    }
    
    NSLog(@"Search String : %@",searchText);
    [self getGooglePlaceInformation:searchText
                       onCompletion:^(NSArray * results) {
                           NSLog(@"Search result : %@",results);
                           if (results) {
                               arrayOfLocation = [results mutableCopy];
                               [self reloadTableWithAnimation];
                           }
                           else {
                               NSLog(@"Search result not found");
                           }
                       }];
}






#pragma mark - Google API Requests
- (void)getGooglePlaceInformation:(NSString *)searchWord
                     onCompletion:(void (^)(NSArray *))complete {
    
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&region=IN&components=country:IN&types=geocode&language=en&key=%@",searchWord,kGoogleServerKey];
        
        
//                NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&region=BD&components=country:BD&types=geocode&key=%@",searchWord,currentLatitude,currentLongitude,kGMServerGoogleMapsAPIKey];

        
        NSLog(@"AutoComplete URL: %@",urlString);
        
        
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSURLSession *delegateFreeSession;
        delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                            delegate:nil
                                                       delegateQueue:[NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask *task;
        task = [delegateFreeSession dataTaskWithRequest:request
                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          
                                          if (!data || !response || error) {
                                              NSLog(@"Google Service Error : %@",[error localizedDescription]);
                                              return;
                                          }
                                          
                                          NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                          NSArray *results = [jSONresult valueForKey:@"predictions"];
                                          
                                          if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]) {
                                              
                                              complete(nil);
                                          }
                                          else
                                          {
                                              complete(results);
                                          }
                                      }];
        [task resume];
    }
    
}

- (void)getAddressWithPlaceID:(NSString *)place
                 onCompletion:(void (^)(NSDictionary *))complete {
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,kGoogleApiKey];
    
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                                      delegate:nil
                                                                 delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            
                                                            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                       options:NSJSONReadingAllowFragments error:nil];
                                                            
                                                            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                                                                complete(nil);
                                                                return;
                                                            }
                                                            else{
                                                                complete([jSONresult valueForKey:@"result"]);
                                                            }
                                                        }];
    [task resume];
}
- (void)getNearbyPalces:(void(^)(NSArray *))complition {
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?location=%f,%f&radius=5000&components=country:in&types=geocode&sensor=true&key=%@", currentLatitude, currentLongitude,kGoogleApiKey];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *delegateFreeSession;
    delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                        delegate:nil
                                                   delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task;
    task = [delegateFreeSession dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      if (!data || !response || error) {
                                          NSLog(@"Google Service Error : %@",[error localizedDescription]);
                                          return;
                                      }
                                      
                                      NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                      NSArray *results = [jSONresult valueForKey:@"results"];
                                      
                                      if (results.count) {
                                          complition(results);
                                      }
                                      else {
                                          complition(nil);
                                      }
                                  }];
    [task resume];
}

/**
 *  Reload table with animation
 */
- (void)reloadTableWithAnimation {
    
//    NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:self.addressTableView]);
//    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.addressTableView reloadData];
}
/*--------------------------------------*/
#pragma mark - Tableview Delegate methods
/*--------------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrayOfLocation.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
        NSArray *arrayOfTerms = [arrayOfLocation[indexPath.row][@"terms"] mutableCopy];
        NSArray *arrayOfTypes = [arrayOfLocation[indexPath.row][@"types"] mutableCopy];
       isEstablistFound = NO;
           for (NSString *type in arrayOfTypes) {
               if ([type isEqualToString:@"establishment"]) {
                   isEstablistFound = YES;
                   break;
               }
           }
           NSString *customAddressText = @"";
           if (isEstablistFound) {
               customAddressText = [customAddressText stringByAppendingFormat:@"%@",arrayOfTerms[0][@"value"]];
           }
           else {
               int count = 0;
               for (NSDictionary *dict in arrayOfTerms) {
                   if (count == 2) {
                       break;
                   }
                   if (count == 0) {
                       customAddressText = [customAddressText stringByAppendingFormat:@"%@, ",dict[@"value"]];
                   }
                   else {
                       customAddressText = [customAddressText stringByAppendingFormat:@"%@",dict[@"value"]];
                   }
                   count++;
               }
           }
           cell.textLabel.text = customAddressText;
          cell.textLabel.font=[UIFont systemFontOfSize:12];
    return cell;
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    return YES;
}

-(NSDictionary *)setParametersForLocation:(NSString *)str{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"input":str,
                                  @"key":kGoogleServerKey,
                                  };
    return dictForuser;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSArray *arrayOfTerms = [arrayOfLocation[indexPath.row][@"terms"] mutableCopy];
    NSString * place_id = [arrayOfLocation[indexPath.row][@"place_id"] mutableCopy];
    
    [self getAddressWithPlaceID:place_id
                           onCompletion:^(NSDictionary *place) {
        NSLog(@"%@",place);
        AppDelegate *appdelagate = [UIApplication sharedApplication].delegate;
        
        
        [self.delegate offLinesearchLocation:place];
        
    }];
    
    
   // [self.delegate didApplySearchLocation:array];
    
    
    
    NSLog(@"%@",arrayOfTerms);
    
   // [self getGoogleAdrressFromStr:locSearchBar.text];
  //  locTableView.hidden=YES;
 
}





- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (arrayOfLocation.count || arrayOfRecentSearch.count) {
        return HeaderHeight;
    }
    return 0;
}
- (void)didUpdateLocation {
  
    [self detectYourLocationButtonAction:nil];
}
- (void)didFailLocation {
   
}

- (IBAction)detectYourLocationButtonAction:(id)sender {
    
  
    
}

- (IBAction)backButtonAction:(id)sender {
    [self.view endEditing:YES];
   
    //    [self dismissViewControllerAnimated:YES completion:nil];
}
/**
 *  Add To Details to Local Database
 *
 *  @param dictionary With selected Dictionary
 */
- (void)addToRecords:(NSDictionary *)dictionary {
    
    
//    if(!_isComingFromSelectAddres) {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        // Take all the Data from old Records
        NSArray *oldRecords = [[ud arrayForKey:@"recent_addresses"] mutableCopy];
        // Remove From NSUserDefaults
        [ud removeObjectForKey:@"recent_addresses"];
        
        // Check for Whether data is Present
        // Alredy into Recent Search
        BOOL isPresentAlready = NO;
        for (NSDictionary *dict in oldRecords) {
            // Check with ID of Category
            if ([dict[@"location"] isEqual:dictionary[@"location"]]) {
                isPresentAlready = YES;
                break;
            }
        }
        
        // Ignore if Data is already Present in Records
        if (!isPresentAlready) {
            [tempArray addObject:dictionary];
        }
        
        // Check for Order Count
        if (oldRecords.count) {
            // Add into Temp Array
            [tempArray addObjectsFromArray:oldRecords];
        }
        
        // Add all the Records Old + New Records in NSuserDefaults
        [ud setObject:tempArray forKey:@"recent_addresses"];
        [ud synchronize];
        
        [self loadRecentHistory];
//    }
}


    - (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
    {
       // [searchBar setShowsCancelButton:YES animated:YES];
    }
    - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
    {
        [searchBar resignFirstResponder];
        [searchBar setShowsCancelButton:NO animated:YES];
        [searchBar setText:@""];
        [filteredContentList removeAllObjects];
        [self dismissViewControllerAnimated:YES completion:nil];
    }


@end
