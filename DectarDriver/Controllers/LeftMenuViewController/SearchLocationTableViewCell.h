//
//  SearchLocationTableViewCell.h
//  HandsFree
//
//  Created by Rahul Sharma on 20/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchLocationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *locationLabel;


@end
