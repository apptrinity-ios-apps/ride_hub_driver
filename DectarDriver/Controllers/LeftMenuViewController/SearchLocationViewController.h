//
//  SearchLocationViewController.h
//  HandsFree
//
//  Created by Rahul Sharma on 20/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol offLinesearchLocation <NSObject>

/**
 *  Seearch is applied
 *
 *  @param dictionary Selected Search Diction
 */
- (void)offLinesearchLocation:(NSDictionary *)dictionary;
/**
 *  Did Cancel SearchLocation
 */
- (void)didCancelSearchLocation;

@end

@interface SearchLocationViewController : UIViewController

@property (weak, nonatomic) id delegate;

@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
@property (strong, nonatomic) IBOutlet UISearchBar *addressSearchBar;
//@property (assign, nonatomic) BOOL isComingFromSelectAddres;
//@property (assign, nonatomic) BOOL isPickupAddress;

@property(strong,nonatomic)NSMutableArray * filteredContentList;
- (IBAction)detectYourLocationButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;
//- (IBAction)applyButtonAction:(id)sender;

// Search Veriables
@property NSString *substring;

@end
