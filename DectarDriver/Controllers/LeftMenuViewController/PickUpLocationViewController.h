//
//  PickUpLocationViewController.h
//  Ezship
//
//  Created by surendra dathu on 15/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
@protocol SelectAddressViewControllerDelegate <NSObject>
@optional
- (void)selectedAddress:(NSDictionary *)addressDictionary;
- (void)sendTextToViewController:(NSString *)String;
- (void)sendDropTextToViewController:(NSString *)String;
- (void)sendSavedAddress:(NSString *)String;
- (void)sendTransulent:(BOOL *)tansulent;
- (void)sendLattitude:(float *)Latitude sendLongitude:(float *)Longitude;
- (void)sendRecentLattitude:(float *)Latitude sendRecentLongitude:(float *)Longitude;
- (void)sendRecentPickUpAddress:(NSString *)String;
- (void)sendRecentDropUpAddress:(NSString *)String;
- (void)sendBackTransulent;
- (void)sendBackTransulent1;

- (void)sendSavedLattitude:(NSString *)Latitude sendSavedLongitude:(NSString *)Longitude;
@end

@protocol SelectAddressViewControllerDelegate1 <NSObject>
@optional

- (void)sendBackTransulent4;

@end




@interface PickUpLocationViewController : UIViewController <GMSMapViewDelegate,CLLocationManagerDelegate>
- (IBAction)detectYourLocationButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *locationSearchView;
@property (strong, nonatomic) IBOutlet UIImageView *searchImageView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarr;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UITableView *addressTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *officeLabelWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *homeLabelWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *homeButtonWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *officeButtonWidth;
@property (strong, nonatomic) IBOutlet UIButton *otherButton;
@property (strong, nonatomic) IBOutlet UIButton *officeButton;
@property (strong, nonatomic) IBOutlet UILabel *otherLAbel;
@property (strong, nonatomic) IBOutlet UILabel *officeLabel;
@property (strong, nonatomic) IBOutlet UILabel *homeLabel;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UILabel *pickUpAddressLabel;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UIButton *pickUpsearchButton;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;
@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) NSMutableArray *arrayOfAddress;
@property (weak, nonatomic) id<SelectAddressViewControllerDelegate>delegate;
@property (weak, nonatomic) id<SelectAddressViewControllerDelegate1>delegate1;
@property (nonatomic) BOOL iscomingFromMainHomeView;
@property (nonatomic) BOOL iscomingFromOrderAnything;
@property (assign, nonatomic) BOOL isComingFromAddressList;

@end
