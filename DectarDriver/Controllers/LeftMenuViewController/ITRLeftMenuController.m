//
//  LeftMenuController.m
//  ITRAirSideMenu
//
//  Created by kirthi on 12/08/15.
//  Copyright (c) 2015 kirthi. All rights reserved.
//

#import "ITRLeftMenuController.h"
#import "StarterViewController.h"
#import "TripListViewController.h"
#import "AppDelegate.h"
#import "ITRAirSideMenu.h"
#import "OfflineViewController.h"

@interface ITRLeftMenuController (){
    TripListViewController * objTripListVc;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ITRLeftMenuController
@synthesize custIndicatorView,tapBtn,yesBtn,noBtn;

- (IBAction)didSwipeRight:(id)sender {
    ITRAirSideMenu *itrSideMenu = ((AppDelegate *)[UIApplication sharedApplication].delegate).itrAirSideMenu;
    [itrSideMenu hideMenuViewController];
}

+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ITRLeftMenuController class])];
}

#pragma view lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        self.view= [Theme sideSetBackGroundImage:self.view];
    if(IS_IPHONE_4_OR_LESS){
        _tableView.frame=CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y+100, _tableView.frame.size.width, _tableView.frame.size.height);
    }
    tapBtn.layer.cornerRadius=tapBtn.frame.size.width/2;
    tapBtn.layer.borderWidth=2.0;
    tapBtn.layer.borderColor=[UIColor whiteColor].CGColor;
    tapBtn.layer.masksToBounds=YES;
    
    self.yesBtn.layer.masksToBounds = YES;
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.yesBtn.layer.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){20.0, 10.}].CGPath;
     self.yesBtn.layer.mask = maskLayer;
    
    self.noBtn.layer.masksToBounds = YES;
    CAShapeLayer * maskLayer2 = [CAShapeLayer layer];
    maskLayer2.path = [UIBezierPath bezierPathWithRoundedRect: self.noBtn.layer.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerTopRight cornerRadii: (CGSize){20.0, 10.}].CGPath;
    self.noBtn.layer.mask = maskLayer2;
    
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ITRAirSideMenu *itrSideMenu = ((AppDelegate *)[UIApplication sharedApplication].delegate).itrAirSideMenu;
    //update content view controller with setContentViewController
    switch (indexPath.row) {
        case 0:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[StarterViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
            case 1:
                       [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[OfflineViewController controller]] animated:YES];
                       [itrSideMenu hideMenuViewController];
                       break;
        case 2:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[TripListViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
        case 3:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[BankingInfoViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
        case 4:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[PaymentSummaryListViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
        case 5:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[ReferPointsViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
            
            
            case 6:
            [itrSideMenu setContentViewController:[[UINavigationController alloc] initWithRootViewController:[ChangePasswordCabViewController controller]] animated:YES];
            [itrSideMenu hideMenuViewController];
            break;
        case 7:
            // [self Logout];
            self.popView.hidden =  NO;
            break;
        default:
            break;
    }
}
-(void)logoutAction
{
        
}
-(void)Logout{
    self.view.userInteractionEnabled=NO;
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web LogoutDriver:[self setParametersForLogout]
              success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         //if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [Theme ClearUserDetails];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         self.view.userInteractionEnabled=YES;
         //         }else{
         //
         //             [self.view makeToast:kErrorMessage];
         //         }
     }
              failure:^(NSError *error)
     {
         self.view.userInteractionEnabled=YES;
         [self stopActivityIndicator];
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [Theme ClearUserDetails];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         [self.view makeToast:kErrorMessage];
         
     }];
    
   
}
- (IBAction)yesBtnClick:(id)sender {
    [self Logout];
    self.popView.hidden = NO;
    
}
- (IBAction)noBtnClick:(id)sender {
    self.popView.hidden = YES;
}



-(NSDictionary *)setParametersForLogout{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"device":@"IOS"
                                  };
    return dictForuser;
}
-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];
        
        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];
        
    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
    [self.container removeFromSuperview];

}
#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UIImageView *imgView;
     UILabel *txtLbl;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
      
    }
    cell.textLabel.text=@"";
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 14, 25, 25)];
    [cell.contentView addSubview:imgView];
   
    txtLbl = [[UILabel alloc] initWithFrame:CGRectMake(70, 12, 250, 30)];
    [cell.contentView addSubview:txtLbl];
    
     txtLbl.font = [UIFont boldSystemFontOfSize:16];
    NSArray *titles = @[@"Home",@"Offline", @"Trip Summary",@"Bank Account",@"Payment Statements",@"Refer Points",@"Change Password",@"Logout"];
     NSArray *titlesImg = @[@"IcHome", @"IcTrip", @"IcTrip",@"IcBank",@"IcPayment",@"IcBank",@"IcChangePass",@"IcLogout"];
   
    imgView.image=[UIImage imageNamed:titlesImg[indexPath.row]];
    txtLbl.text = titles[indexPath.row];
   txtLbl.textColor = [UIColor whiteColor];
    return cell;
}


@end
