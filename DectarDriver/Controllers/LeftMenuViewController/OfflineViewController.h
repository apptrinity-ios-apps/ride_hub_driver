//
//  OfflineViewController.h
//  RideHubDriver
//
//  Created by INDOBYTES on 23/03/20.
//  Copyright © 2020 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OfflineViewController : UIViewController
+ (instancetype) controller;
@property (weak, nonatomic) IBOutlet UIView *pickUpView;
@property (weak, nonatomic) IBOutlet UIView *dropView;
@property (weak, nonatomic) IBOutlet UILabel *pickUpLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropLabel;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *userNumberField;
@property (weak, nonatomic) IBOutlet UIView *dotView;

@end

NS_ASSUME_NONNULL_END
