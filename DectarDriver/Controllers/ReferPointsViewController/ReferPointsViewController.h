//
//  ReferPointsViewController.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootBaseViewController.h"

@interface ReferPointsViewController :  RootBaseViewController<UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource>
+ (instancetype) controller;
@property (weak, nonatomic) IBOutlet UITableView *referPointsTableview;

@property (weak, nonatomic) IBOutlet UILabel *totalReferPoints_Lbl;

- (IBAction)back_action_btn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *dotLine;








@end
