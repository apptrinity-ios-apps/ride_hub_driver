//
//  ReferPoints_Cell.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferPoints_Cell : UITableViewCell



@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UILabel *name_LBL;
@property (weak, nonatomic) IBOutlet UILabel *referID_Level;
@property (weak, nonatomic) IBOutlet UILabel *referLevel;
@property (weak, nonatomic) IBOutlet UILabel *refer_Type;
@property (weak, nonatomic) IBOutlet UILabel *dateLBL;
@property (weak, nonatomic) IBOutlet UILabel *totalPoints;

@property (weak, nonatomic) IBOutlet UILabel *cr_db_LBL;


@property (strong, nonatomic) IBOutlet UIView *nameBackView;

@property (strong, nonatomic) IBOutlet UIView *imageBackView;
@property (weak, nonatomic) IBOutlet UIView *dotLine;
@property (weak, nonatomic) IBOutlet UIView *greenView;




@end
