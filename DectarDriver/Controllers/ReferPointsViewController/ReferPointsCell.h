//
//  ReferPointsCell.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferPointsCell : UITableViewCell{
    
}

@property (weak, nonatomic) IBOutlet UILabel *customerName_lbl;

@property (weak, nonatomic) IBOutlet UILabel *refer_ID_lbl;

@property (weak, nonatomic) IBOutlet UILabel *refer_Lever_lbl;

@property (weak, nonatomic) IBOutlet UILabel *refer_type_lbl;
@property (weak, nonatomic) IBOutlet UILabel *date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *points_lbl;
@property (weak, nonatomic) IBOutlet UILabel *CR_DB_LBL;

@end
