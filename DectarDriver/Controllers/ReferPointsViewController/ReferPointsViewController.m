//
//  ReferPointsViewController.m
//  RideHubDriver
//
//  Created by nagaraj  kumar on 02/04/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import "ReferPointsViewController.h"
#import "UIViewController+REFrostedViewController.h"
#import "Theme.h"
#import "RootBaseViewController.h"
#import "ReferPoints_Cell.h"
#import "UrlHandler.h"

@interface ReferPointsViewController (){
    
    NSMutableArray * referPointsArr;
    
}


@end

@implementation ReferPointsViewController

+ (instancetype) controller{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"ReferPointsViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getDatasReferPoints];
    [self.referPointsTableview registerNib:[UINib nibWithNibName:@"ReferPoints_Cell" bundle:nil] forCellReuseIdentifier:@"ReferPoints_Cell"];
    self.referPointsTableview.delegate = self;
    self.referPointsTableview.dataSource = self;
    [self.referPointsTableview reloadData];

    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder2];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [referPointsArr count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  186;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    ReferPoints_Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReferPoints_Cell"];
    if (cell == nil) {
        cell = [[ReferPoints_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ReferPoints_Cell"];
    }
   
    
     cell.name_LBL.text = [[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"referrer_name" ];
    
     cell.referID_Level.text = [NSString stringWithFormat:@"Refer ID :  %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"refer_from" ]];
    
     cell.referLevel.text = [NSString stringWithFormat:@"Refer Level :  %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"level" ]];
    
     cell.refer_Type.text = [NSString stringWithFormat:@"Refer Type :  %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"type" ]];
    
     cell.dateLBL.text = [NSString stringWithFormat:@"Date :  %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"date" ]];
    
    
   NSString * transType = [[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"trans_type" ];
    if ([transType  isEqual: @"CR"]){
      cell.cr_db_LBL.textColor =  [UIColor whiteColor];
         cell.cr_db_LBL.text = [NSString stringWithFormat:@"CR - %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"points" ]];
        
    }else {
        cell.cr_db_LBL.textColor = [UIColor whiteColor];
         cell.cr_db_LBL.text = [NSString stringWithFormat:@"DB - %@",[[referPointsArr objectAtIndex:indexPath.row]objectForKey:@"points" ]];
        
    }
   // float shadowSize = 3.0f;
  //  UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(cell.backView.frame.origin.x - 2,
                                                                           //cell.backView.frame.origin.y - 2,
                                                                         //  cell.backView.frame.size.width + shadowSize,
                                                                         //  cell.backView.frame.size.height + shadowSize)];
    
    
//    cell.backView.layer.shadowPath = shadowPath.CGPath;
//    cell.backView.layer.cornerRadius = 5 ;
//    cell.backView.layer.borderColor = [UIColor  colorWithRed:0/255 green:181/255 blue:226/255 alpha:1.0f].CGColor;
//    cell.backView.layer.borderWidth = 1;
    
    
    dispatch_async (dispatch_get_main_queue(), ^
                    {
                        [self setMaskTo:cell.nameBackView byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)];
//                        
//                        cell.backView.layer.masksToBounds = YES;
//                        cell.backView.layer.shadowColor = [UIColor blackColor].CGColor;
//                        cell.backView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
//                        cell.backView.layer.shadowOpacity = 0.5f;
//                        cell.backView.layer.shadowRadius = 3;
                        
                        
//                        cell.imageBackView.layer.masksToBounds = YES;
//                        cell.imageBackView.layer.cornerRadius = cell.imageBackView.frame.size.height/2 ;
//                        cell.imageBackView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//                        cell.imageBackView.layer.borderWidth = 1;
//                        
//                        cell.backView.layer.borderColor = [UIColor  colorWithRed:33.0f/255.0f green:134.0f/255.0f blue:193.0f/255.0f alpha:1.0f].CGColor;
                        
                    });
    
    
  //  [self roundCornersOnView:cell.nameBackView onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:5.0f];
    

    return cell;
    
}


-(NSDictionary *)setParametersReferPoints{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId
                                  };
    return dictForuser;
}

- (void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath *rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    shape.frame = view.bounds;
    view.layer.mask = shape;
    view.layer.masksToBounds=YES;
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br)
    {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        //  maskLayer.frame = roundedView.bounds;   /// p333
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}


-(void)getDatasReferPoints{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web ReferPoints:[self setParametersReferPoints]
          success:^(NSMutableDictionary *responseDictionary)
     {
        
         NSLog(@"%@",responseDictionary);
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             NSString * statusStr =[Theme checkNullValue:[responseDictionary objectForKey:@"status"]];
             if ([statusStr isEqualToString:@"1"]) {
                
                 referPointsArr = [responseDictionary objectForKey:@"data"];
                 [self.referPointsTableview reloadData];
                 self.totalReferPoints_Lbl.text = [NSString stringWithFormat: @"%@",[responseDictionary objectForKey:@"total_points"]];
             }else{
              
                 
             }
             
         }else{
             
             // [self.view makeToast:@"Some error occured"];
         }
     }
          failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:@"No internet Connection"];
         
     }];
    
}


- (IBAction)back_action_btn:(id)sender {
    
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    [self.frostedViewController presentMenuViewController];
}


@end
