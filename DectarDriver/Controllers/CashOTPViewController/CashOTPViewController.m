//
//  CashOTPViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/2/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "CashOTPViewController.h"

@interface CashOTPViewController ()

@end

@implementation CashOTPViewController
@synthesize otpHeaderLbl,OTPTextField,otpTextLbl,requestBtn,custIndicatorView,rideId,rateAmount,OTPStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setFont];
    [self requestForOtp];
    // Do any additional setup after loading the view.
}
-(void)setFont{
    requestBtn.hidden=YES;
    otpHeaderLbl=[Theme setHeaderFontForLabel:otpHeaderLbl];
    otpTextLbl=[Theme setHeaderFontForLabel:otpTextLbl];
    OTPTextField=[Theme setLargeBoldFontForTextField:OTPTextField];
    requestBtn=[Theme setBoldFontForButton:requestBtn];
    
    [otpHeaderLbl setText:JJLocalizedString(@"OTP", nil)];
    [otpTextLbl setText:JJLocalizedString(@"Please_enter_the_OTP", nil)];
    [OTPTextField setPlaceholder:JJLocalizedString(@"Enter_OTP", nil)];
    [requestBtn setTitle:JJLocalizedString(@"Request", nil) forState:UIControlStateNormal];
    
    requestBtn.layer.cornerRadius= requestBtn.frame.size.height/2;
    requestBtn.layer.masksToBounds=YES;
}
-(void)requestForOtp{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getOTP:[self setParametersForOtp]
                     success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             requestBtn.hidden=NO;
             NSString * resdict=[Theme checkNullValue:[responseDictionary objectForKey:@"otp_status"]];
             [self.view makeToast:[Theme checkNullValue:[responseDictionary objectForKey:@"response"]]];
             NSString * currency=[Theme checkNullValue:[Theme findCurrencySymbolByCode:[responseDictionary objectForKey:@"currency"]]];
             rateAmount=[Theme checkNullValue:[NSString stringWithFormat:@"%@ %.02f",currency,[[responseDictionary objectForKey:@"amount"] floatValue]]];
             OTPStr=[Theme checkNullValue:[responseDictionary objectForKey:@"otp"]];
             if([resdict isEqualToString:@"development"]){
                 OTPTextField.text=[Theme checkNullValue:[responseDictionary objectForKey:@"otp"]];
             }
         }else{
             
             [self.view makeToast:kErrorMessage];
         }
     }
                     failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersForOtp{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":rideId
                                  };
    return dictForuser;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didClickRequestBtn:(id)sender {
    if(OTPTextField.text.length==0||![OTPTextField.text isEqualToString:OTPStr]){
        UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Alert !!" message:JJLocalizedString(@"Please_enter_valid_OTP", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }else{
        ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
        [objReceiveCashVC setRideId:rideId];
        [objReceiveCashVC setFareAmt:rateAmount];
        [self.navigationController pushViewController:objReceiveCashVC animated:YES];
    }
}

-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];

    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.view endEditing:YES];
}
@end
