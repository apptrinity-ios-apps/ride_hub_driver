//
//  BankingInfoViewController.h
//  DectarDriver
//
//  Created by Casperon Technologies on 9/11/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITRAirSideMenu.h"
#import "AppDelegate.h"
#import "Theme.h"
#import "WBErrorNoticeView.h"
#import "RootBaseViewController.h"
#import "BankingRecords.h"

@interface BankingInfoViewController : RootBaseViewController<UITextFieldDelegate,UITextViewDelegate,UIScrollViewDelegate>
+ (instancetype) controller;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewHieghtconstraint;
@property (weak, nonatomic) IBOutlet UILabel *accUserNameLbl;
@property (weak, nonatomic) IBOutlet UITextField *userNameTxtField;
@property (weak, nonatomic) IBOutlet UILabel *accUserAddressLbl;
@property (weak, nonatomic) IBOutlet UITextView *accAddressTxtView;
@property (weak, nonatomic) IBOutlet UILabel *accNumberLbl;
@property (weak, nonatomic) IBOutlet UITextField *accNumberText;
@property (weak, nonatomic) IBOutlet UILabel *branchNameLbl;
@property (weak, nonatomic) IBOutlet UITextField *branchNameTxtField;
@property (weak, nonatomic) IBOutlet UILabel *branchAddressLbl;
@property (weak, nonatomic) IBOutlet UITextView *branchAddressTxtview;
@property (weak, nonatomic) IBOutlet UILabel *ifscLbl;
@property (weak, nonatomic) IBOutlet UITextField *ifscTxtField;
@property (weak, nonatomic) IBOutlet UILabel *routingLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIView *scrollBckView;

@property (weak, nonatomic) IBOutlet UITextField *routingTxtField;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *bankNameLbl;
@property (weak, nonatomic) IBOutlet UITextField *bankNameTxtField;
@property (weak, nonatomic) IBOutlet UIView *saveView;
@property (strong, nonatomic) IBOutlet HeaderViewColorHandler *headerView;


- (IBAction)didClickSaveBtn:(id)sender;

- (IBAction)didClickMenuBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *dotLine;
@property (weak, nonatomic) IBOutlet UIView *dotLine1;
@property (weak, nonatomic) IBOutlet UIView *dotLine2;
@property (weak, nonatomic) IBOutlet UIView *dotLine3;
@property (weak, nonatomic) IBOutlet UIView *dotLine9;
@property (weak, nonatomic) IBOutlet UIView *dotLine5;
@property (weak, nonatomic) IBOutlet UIView *dotLine6;
@property (weak, nonatomic) IBOutlet UIView *dotLine7;
@property (weak, nonatomic) IBOutlet UIView *dotLine8;

@property (weak, nonatomic) IBOutlet UITextField *bankAddressField;

@property (weak, nonatomic) IBOutlet UITextField *ifscCodeField;






@end
