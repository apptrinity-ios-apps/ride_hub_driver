//
//  BankingInfoViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 9/11/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "BankingInfoViewController.h"

@interface BankingInfoViewController ()

@end

@implementation BankingInfoViewController
@synthesize headerLbl,scrollView,accUserNameLbl,userNameTxtField,accUserAddressLbl,accAddressTxtView,accNumberLbl,accNumberText,branchNameLbl,branchNameTxtField,branchAddressLbl,branchAddressTxtview,ifscLbl,
ifscTxtField,routingLbl,routingTxtField,saveBtn,bankNameLbl,bankNameTxtField,saveView,imageView,headerView,scrollBckView,dotLine,dotLine1,dotLine2,dotLine3,dotLine9,dotLine5,dotLine6,dotLine7,dotLine8,bankAddressField,ifscCodeField;

+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"BankingInfoVCSID"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewHieghtconstraint.constant = 1600;
    
    [self.view bringSubviewToFront:headerView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotifWhenQuit
                                               object:nil];

    scrollView.hidden=YES;
    saveView.hidden=YES;
    scrollView.delegate = self;
    
   
     userNameTxtField.delegate =self;
     accAddressTxtView.delegate =self;
     accNumberText.delegate =self;
     branchNameTxtField.delegate =self;
     branchAddressTxtview.delegate =self;
     ifscTxtField.delegate =self;
     routingTxtField.delegate =self;
     bankNameTxtField.delegate =self;
    scrollView.showsHorizontalScrollIndicator = NO;

    [self setFont];
    [self getDatasForBanking];
    
    saveView.layer.cornerRadius = saveView.frame.size.height/2;
    saveView.layer.masksToBounds = YES;
    saveBtn.layer.cornerRadius = saveBtn.frame.size.height/2;
    saveBtn.layer.masksToBounds = YES;
    [self setShadow:saveView];
    
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.lineDashPattern = @[@3,@3];
    yourViewBorder.frame = self.dotLine.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.dotLine.bounds].CGPath;
    [self.dotLine.layer addSublayer:yourViewBorder];
    
    
    CAShapeLayer *yourViewBorder1 = [CAShapeLayer layer];
    yourViewBorder1.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder1.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder1.lineDashPattern = @[@3,@3];
    yourViewBorder1.frame = self.dotLine1.bounds;
    yourViewBorder1.path = [UIBezierPath bezierPathWithRect:self.dotLine1.bounds].CGPath;
    [self.dotLine1.layer addSublayer:yourViewBorder1];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dotLine2.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dotLine2.bounds].CGPath;
    [self.dotLine2.layer addSublayer:yourViewBorder2];
    
    CAShapeLayer *yourViewBorder3 = [CAShapeLayer layer];
    yourViewBorder3.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder3.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder3.lineDashPattern = @[@3,@3];
    yourViewBorder3.frame = self.dotLine3.bounds;
    yourViewBorder3.path = [UIBezierPath bezierPathWithRect:self.dotLine3.bounds].CGPath;
    [self.dotLine3.layer addSublayer:yourViewBorder3];
    
    CAShapeLayer *yourViewBorder4 = [CAShapeLayer layer];
    yourViewBorder4.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder4.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder4.lineDashPattern = @[@3,@3];
    yourViewBorder4.frame = self.dotLine9.bounds;
    yourViewBorder4.path = [UIBezierPath bezierPathWithRect:self.dotLine9.bounds].CGPath;
    [self.dotLine9.layer addSublayer:yourViewBorder4];
    
    CAShapeLayer *yourViewBorder5 = [CAShapeLayer layer];
    yourViewBorder5.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder5.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder5.lineDashPattern = @[@3,@3];
    yourViewBorder5.frame = self.dotLine5.bounds;
    yourViewBorder5.path = [UIBezierPath bezierPathWithRect:self.dotLine5.bounds].CGPath;
    [self.dotLine5.layer addSublayer:yourViewBorder5];
    
    CAShapeLayer *yourViewBorder6 = [CAShapeLayer layer];
    yourViewBorder6.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder6.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder6.lineDashPattern = @[@3,@3];
    yourViewBorder6.frame = self.dotLine6.bounds;
    yourViewBorder6.path = [UIBezierPath bezierPathWithRect:self.dotLine6.bounds].CGPath;
    [self.dotLine6.layer addSublayer:yourViewBorder6];
    
    CAShapeLayer *yourViewBorder7 = [CAShapeLayer layer];
    yourViewBorder7.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder7.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder7.lineDashPattern = @[@3,@3];
    yourViewBorder7.frame = self.dotLine7.bounds;
    yourViewBorder7.path = [UIBezierPath bezierPathWithRect:self.dotLine7.bounds].CGPath;
    [self.dotLine7.layer addSublayer:yourViewBorder7];
    
    CAShapeLayer *yourViewBorder8 = [CAShapeLayer layer];
    yourViewBorder8.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder8.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder8.lineDashPattern = @[@3,@3];
    yourViewBorder8.frame = self.dotLine8.bounds;
    yourViewBorder8.path = [UIBezierPath bezierPathWithRect:self.dotLine8.bounds].CGPath;
    [self.dotLine8.layer addSublayer:yourViewBorder8];
    
    
    // Do any additional setup after loading the view.
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
            NSString * rideId=[Theme checkNullValue:[dict objectForKey:@"key1"]];
            NSString * CurrId=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict objectForKey:@"key4"]]];
            NSString * fareStr=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            NSString * fareAmt=[NSString stringWithFormat:@"%@ %@",CurrId,fareStr];
            
            [self stopActivityIndicator];
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:rideId];
            [objReceiveCashVC setFareAmt:fareAmt];
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)getDatasForBanking{
    
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getBankingData:[self setParametersForGetBank]
                 success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             scrollView.hidden=NO;
             saveView.hidden=NO;
             BankingRecords * objBankingRecs=[[BankingRecords alloc]init];
             NSDictionary * dict=responseDictionary[@"response"][@"banking"];
             objBankingRecs.accUserName=[Theme checkNullValue:dict[@"acc_holder_name"]];
             objBankingRecs.accUserAddress=[Theme checkNullValue:dict[@"acc_holder_address"]];
             objBankingRecs.accBankName=[Theme checkNullValue:dict[@"bank_name"]];
             objBankingRecs.accBranchName=[Theme checkNullValue:dict[@"branch_name"]];
             objBankingRecs.accNumber=[Theme checkNullValue:dict[@"acc_number"]];
             objBankingRecs.accBranchAddress=[Theme checkNullValue:dict[@"branch_address"]];
             objBankingRecs.accIfscCode=[Theme checkNullValue:dict[@"swift_code"]];
             objBankingRecs.accRoutingNumber=[Theme checkNullValue:dict[@"routing_number"]];
             [self setDatasToFields:objBankingRecs];
         }else{
             self.view.userInteractionEnabled=YES;
             self.scrollBckView.userInteractionEnabled=YES;
             [self.scrollBckView endEditing:YES];

             [self.view makeToast:kErrorMessage];
         }
     }
                 failure:^(NSError *error)
     {
         self.view.userInteractionEnabled=YES;
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersForGetBank{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId
                                  };
    return dictForuser;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];   //it hides
}
-(void)setFont{
    UIColor *blueColor = [UIColor colorWithRed:58/227 green:196/227 blue:241/227 alpha:1];
    
//    headerLbl=[Theme setHeaderFontForLabel:headerLbl];
    accUserNameLbl=[Theme setNormalFontForLabel:accUserNameLbl];
    accUserAddressLbl=[Theme setNormalFontForLabel:accUserAddressLbl];
    accNumberLbl=[Theme setNormalFontForLabel:accNumberLbl];
    branchNameLbl=[Theme setNormalFontForLabel:branchNameLbl];
    branchAddressLbl=[Theme setNormalFontForLabel:branchAddressLbl];
    ifscLbl=[Theme setNormalFontForLabel:ifscLbl];
    routingLbl=[Theme setNormalFontForLabel:routingLbl];
    bankNameLbl=[Theme setNormalFontForLabel:bankNameLbl];
    bankNameTxtField=[Theme setNormalFontForTextfield:bankNameTxtField];
//    bankNameTxtField.layer.borderColor = blueColor.CGColor;
//    bankNameTxtField.layer.borderWidth = 1;
    userNameTxtField=[Theme setNormalFontForTextfield:userNameTxtField];
//    userNameTxtField.layer.borderColor = blueColor.CGColor;
//    userNameTxtField.layer.borderWidth =1;
    userNameTxtField.layer.masksToBounds = YES;
    accNumberText=[Theme setNormalFontForTextfield:accNumberText];
//    accNumberText.layer.borderColor = blueColor.CGColor;
//    accNumberText.layer.borderWidth = 1;
    branchNameTxtField=[Theme setNormalFontForTextfield:branchNameTxtField];
//    branchNameTxtField.layer.borderColor = blueColor.CGColor;
//    branchNameTxtField.layer.borderWidth = 1;
    ifscTxtField=[Theme setNormalFontForTextfield:ifscTxtField];
//    ifscTxtField.layer.borderColor = blueColor.CGColor;
//    ifscTxtField.layer.borderWidth = 1;
    routingTxtField=[Theme setNormalFontForTextfield:routingTxtField];
//    routingTxtField.layer.borderColor = blueColor.CGColor;
//    routingTxtField.layer.borderWidth = 1;
    
    
    branchAddressTxtview=[Theme setNormalFontForTextView:branchAddressTxtview];
    accAddressTxtView=[Theme setNormalFontForTextView:accAddressTxtView];
    
    saveBtn=[Theme setBoldFontForButton:saveBtn];
   
    saveBtn.layer.cornerRadius=2;
    saveBtn.layer.masksToBounds=YES;
    
    
    branchAddressTxtview.layer.cornerRadius=3;
//    branchAddressTxtview.layer.borderWidth=1;
//    branchAddressTxtview.layer.borderColor=setLightGray.CGColor;
    branchAddressTxtview.layer.masksToBounds=YES;
    branchAddressTxtview.textContainer.lineFragmentPadding = 20;
    
    accAddressTxtView.layer.cornerRadius=3;
//    accAddressTxtView.layer.borderWidth=1;
//    accAddressTxtView.layer.borderColor=setLightGray.CGColor;
    accAddressTxtView.layer.masksToBounds=YES;
    accAddressTxtView.textContainer.lineFragmentPadding = 20;
    
//    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, routingTxtField.frame.origin.y+routingTxtField.frame.size.height+80);
    scrollView.contentSize=CGSizeMake(scrollView.frame.size.width, scrollBckView.frame.origin.y+scrollBckView.frame.size.height+800);
   
    
    [headerLbl setText:JJLocalizedString(@"Bank_Account", nil)];
    [saveBtn setTitle:JJLocalizedString(@"Save", nil) forState:UIControlStateNormal];
    [accUserNameLbl setText:JJLocalizedString(@"Account_Holder_Name", nil)];
    [accUserAddressLbl setText:JJLocalizedString(@"Account_Holder_Address", nil)];
    [accNumberLbl setText:JJLocalizedString(@"Account_Number", nil)];
    [bankNameLbl setText:JJLocalizedString(@"Bank_Name", nil)];
    [branchNameLbl setText:JJLocalizedString(@"Branch_Name", nil)];
    [branchAddressLbl setText:JJLocalizedString(@"Branch_Address", nil)];
    [ifscLbl setText:JJLocalizedString(@"SWIFT_IFSC_code", nil)];
    [routingLbl setText:JJLocalizedString(@"Routing_Number", nil)];

    [self setMandatoryField];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if (scrollView.contentOffset.y >= 100) {
        //reach bottom
       [scrollView setContentOffset:CGPointMake(0.0,routingTxtField.frame.origin.y-200) animated:YES];
    }

}

-(UITextField *)leftPadding:(UITextField *)txtField withImageName:(NSString *)imgName{
    
    return txtField;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if([textField isEqual:userNameTxtField]){
        [scrollView setContentOffset:CGPointMake(0.0, 0) animated:YES];
    }else{
        [scrollView setContentOffset:CGPointMake(0.0,textField.frame.origin.y-200) animated:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height-300);
    [self.scrollBckView endEditing:YES];
    [self.scrollView endEditing:YES];

    [self.scrollView setContentOffset:bottomOffset animated:YES];
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
      [scrollView setContentOffset:CGPointMake(0.0,textView.frame.origin.y-200) animated:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (range.location == 0 && [text isEqualToString:@" "]) {
        return NO;
    }
    return YES;
}


-(void)setMandatoryField{
    for (UIView *subview in scrollView.subviews)
    {
        if ([subview isKindOfClass:[UILabel class]])
        {
            UILabel * lbl=(UILabel *)subview;
            if(![lbl isEqual:routingLbl]){
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"MandatoryImg"];
                
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                
                NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:lbl.text];
                [myString appendAttributedString:attachmentString];
                
                lbl.attributedText = myString;
            }
            
        }else if ([subview isKindOfClass:[UITextField class]]){
             UITextField * txtField=(UITextField *)subview;
            UIView *   arrow = [[UILabel alloc] init];
            arrow.frame = CGRectMake(0, 0,20, 20);
            		
            arrow.contentMode = UIViewContentModeScaleToFill;
            txtField.leftView = arrow;
            txtField.leftViewMode = UITextFieldViewModeAlways;
            txtField.layer.cornerRadius=3;
            txtField.layer.borderWidth=1;
            txtField.layer.borderColor=setLightGray.CGColor;
            txtField.layer.masksToBounds=YES;
        }
    }
    
}

- (IBAction)didClickSaveBtn:(id)sender {
    if([self validateTxtFields]){
        [self.view endEditing:YES];
        self.view.userInteractionEnabled=NO;
        [self showActivityIndicator:YES];
        UrlHandler *web = [UrlHandler UrlsharedHandler];
        [web saveBankingData:[self setParametersForSaveBank]
                  success:^(NSMutableDictionary *responseDictionary)
         {
              self.view.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
                 BankingRecords * objBankingRecs=[[BankingRecords alloc]init];
                 NSDictionary * dict=responseDictionary[@"response"][@"banking"];
                 objBankingRecs.accUserName=[Theme checkNullValue:dict[@"acc_holder_name"]];
                  objBankingRecs.accUserAddress=[Theme checkNullValue:dict[@"acc_holder_address"]];
                  objBankingRecs.accBankName=[Theme checkNullValue:dict[@"bank_name"]];
                  objBankingRecs.accBranchName=[Theme checkNullValue:dict[@"branch_name"]];
                  objBankingRecs.accNumber=[Theme checkNullValue:dict[@"acc_number"]];
                  //objBankingRecs.accBranchAddress=[Theme checkNullValue:dict[@"branch_address"]];
                 //objBankingRecs.accIfscCode=[Theme checkNullValue:dict[@"swift_code"]];
                 objBankingRecs.accRoutingNumber=[Theme checkNullValue:dict[@"routing_number"]];
                  [self setDatasToFields:objBankingRecs];
                 [self.view makeToast:@"Bank_account_information"];
             }else{
                 self.view.userInteractionEnabled=YES;
                 [self.view makeToast:kErrorMessage];
             }
         }
                  failure:^(NSError *error)
         {
             self.view.userInteractionEnabled=YES;
             [self stopActivityIndicator];
             [self.view makeToast:kErrorMessage];
             
         }];
    }
}
-(void)setDatasToFields:(BankingRecords *)objBankingRecs{
    userNameTxtField.text=objBankingRecs.accUserName;
    accNumberText.text=objBankingRecs.accNumber;
    branchNameTxtField.text=objBankingRecs.accBranchName;
    ifscTxtField.text=objBankingRecs.accIfscCode;
    routingTxtField.text=objBankingRecs.accRoutingNumber;
    bankNameTxtField.text=objBankingRecs.accBankName;
    accAddressTxtView.text=objBankingRecs.accUserAddress;
    branchAddressTxtview.text=objBankingRecs.accBranchAddress;
}

-(NSDictionary *)setParametersForSaveBank{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"acc_holder_name":userNameTxtField.text,
                                  @"acc_holder_address":accAddressTxtView.text,
                                  @"acc_number":accNumberText.text,
                                  @"bank_name":bankNameTxtField.text,
                                  @"branch_name":branchNameTxtField.text,
                                  @"branch_address":branchAddressTxtview.text,
                                  @"swift_code":ifscTxtField.text,
                                  @"routing_number":[Theme checkNullValue:routingTxtField.text],
                                  };
    return dictForuser;
}
-(BOOL)validateTxtFields{
    if(userNameTxtField.text.length==0){
        [self showErrorMessage:@"Account_Holder_Name_cannot_be_empty"];
        return NO;
    }else if (accAddressTxtView.text.length==0){
        [self showErrorMessage:@"Account_Holder_Address_cannot_be_empty"];
        return NO;
    }
    else if (accNumberText.text.length==0){
        [self showErrorMessage:@"Account_Number_cannot_be_empty"];
        return NO;
    }
    else if (bankNameTxtField.text.length==0){
        [self showErrorMessage:@"Bank_Name_cannot_be_empty"];
        return NO;
    }
    else if (branchNameTxtField.text.length==0){
        [self showErrorMessage:@"Branch_Name_cannot_be_empty"];
        return NO;
    }
    else if (branchAddressTxtview.text.length==0){
        [self showErrorMessage:@"Branch_Address_cannot_be_empty"];
        return NO;
    }
    else if (ifscTxtField.text.length==0){
        [self showErrorMessage:@"SWIFT_IFSC_cannot_be_empty"];
        return NO;
    }
    
    return YES;
}
-(void)showErrorMessage:(NSString *)str{
    WBErrorNoticeView *notice = [WBErrorNoticeView errorNoticeInView:self.view title:@"Oops !!!" message:JJLocalizedString(str, nil)];
    [notice show];
}
- (IBAction)didClickMenuBtn:(id)sender {
    [self.view endEditing:YES];
    [self.scrollBckView endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
    [self .scrollBckView endEditing:YES];
    [self.scrollView endEditing:YES];
    
    
}

@end
