//
//  CustomMarkerView.h
//  RideHubDriver
//
//  Created by nagaraj  kumar on 12/11/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//NS_ASSUME_NONNULL_BEGIN

@interface CustomMarkerView : UIView
    
    
  @property (weak, nonatomic) IBOutlet UIView *backview;
    
@property (strong, nonatomic) IBOutlet UIView *blueline;
@property (strong, nonatomic) IBOutlet UIView *blueCircleView;


@property (strong, nonatomic) IBOutlet UIImageView *pickupImage;

@property (strong, nonatomic) IBOutlet UILabel *pickUpHeadingLbl;
@property (strong, nonatomic) IBOutlet UILabel *locationLBL;








@end

//NS_ASSUME_NONNULL_END
