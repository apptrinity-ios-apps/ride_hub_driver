//
//  TripDetailViewController.m
//  DectarDriver
//  Created by Casperon Technologies on 8y6/28/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "TripDetailViewController.h"
#import "TripDetailMapViewCell.h"
#import "TripDetailedTableViewCell.h"

@interface TripDetailViewController ()<moveToNectVCFromFareSummary> {
    NSString * currencys;
    NSDictionary * summaryDict;
    NSDictionary * FareDict;
    NSString * PaymentTypeStr;
    NSDictionary * reasonDetailDict;
    double ratingNumber;
     FareSummaryViewController *controller;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *couponHieghtConstant;

@end

@implementation TripDetailViewController
@synthesize Camera,GoogleMap,custIndicatorView,tripId,lattitude,longitude,rideId,rideOption,isLocUpdated,needCash,tripDetailTabelView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ratingNumber = 1.6;
    
     [self getTripDetail];
    
    [self.tripDetailTabelView registerNib:[UINib nibWithNibName:@"TripDetailedTableViewCell" bundle:nil] forCellReuseIdentifier:@"TripDetailedTableViewCell"];
    [self.tripDetailTabelView registerNib:[UINib nibWithNibName:@"TripDetailMapViewCell" bundle:nil] forCellReuseIdentifier:@"TripDetailMapViewCell"];

    self.tripDetailTabelView.delegate = self;
    self.tripDetailTabelView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:kDriverCashPaymentNotifWhenQuit
                                               object:nil];
    
   // Do any additional setup after loading the view.
}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

- (void)viewDidLayoutSubviews
{
//    dispatch_async (dispatch_get_main_queue(), ^
//                    {
////                        [tripDeatilScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+650)];
//
//                        _headerimage.image = [UIImage imageNamed:@"userDefault"];
//
//                        _headerimage.layer.cornerRadius = _headerimage.frame.size.height/2;
//                        _headerimage.layer.masksToBounds = YES;
//
//                    });
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (indexPath.row == 0){
         return  164;
     } else {
         NSString * rideStatus=[Theme checkNullValue:reasonDetailDict[@"ride_status"]];
         NSLog(@"ride status is %@",rideStatus);
         if([rideStatus isEqualToString:@"Cancelled"]){
             return 560;
         } else {
         return 903;
         }
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        TripDetailMapViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripDetailMapViewCell"];
        if (cell == nil) {
            cell = [[TripDetailMapViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TripDetailMapViewCell"];
        }
        
        [cell.mapImageView setImageWithURL:[NSURL URLWithString:@""]
                       placeholderImage:[UIImage imageNamed:@"mapImage"]];

        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
        
    } else {
        TripDetailedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TripDetailedTableViewCell"];
        if (cell == nil) {
            cell = [[TripDetailedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TripDetailedTableViewCell"];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        AppDelegate*delagate = [UIApplication sharedApplication].delegate;
        
        if (ratingNumber == 0) {
            cell.star1.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star2.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star3.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star4.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star5.image = [UIImage imageNamed:@"StarEmpty"];
        } else if (ratingNumber <= 1.5){
            cell.star1.image = [UIImage imageNamed:@"StarFill"];
            cell.star2.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star3.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star4.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star5.image = [UIImage imageNamed:@"StarEmpty"];
        } else if (ratingNumber <= 2.5){
            cell.star1.image = [UIImage imageNamed:@"StarFill"];
            cell.star2.image = [UIImage imageNamed:@"StarFill"];
            cell.star3.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star4.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star5.image = [UIImage imageNamed:@"StarEmpty"];
        } else if (ratingNumber <= 3.5) {
            cell.star1.image = [UIImage imageNamed:@"StarFill"];
            cell.star2.image = [UIImage imageNamed:@"StarFill"];
            cell.star3.image = [UIImage imageNamed:@"StarFill"];
            cell.star4.image = [UIImage imageNamed:@"StarEmpty"];
            cell.star5.image = [UIImage imageNamed:@"StarEmpty"];
        } else if (ratingNumber <= 4.5) {
            cell.star1.image = [UIImage imageNamed:@"StarFill"];
            cell.star2.image = [UIImage imageNamed:@"StarFill"];
            cell.star3.image = [UIImage imageNamed:@"StarFill"];
            cell.star4.image = [UIImage imageNamed:@"StarFill"];
            cell.star5.image = [UIImage imageNamed:@"StarEmpty"];
        } else {
            cell.star1.image = [UIImage imageNamed:@"StarFill"];
            cell.star2.image = [UIImage imageNamed:@"StarFill"];
            cell.star3.image = [UIImage imageNamed:@"StarFill"];
            cell.star4.image = [UIImage imageNamed:@"StarFill"];
            cell.star5.image = [UIImage imageNamed:@"StarFill"];
        }

        NSString * payment=JJLocalizedString(@"Payment", nil);
        cell.paymentLabel.text=[NSString stringWithFormat:@" %@ %@",payment,[Theme checkNullValue:reasonDetailDict[@"pay_status"]]];
        
        NSDictionary * tipsDict=reasonDetailDict[@"tips"];
        NSString * rideStatus=[Theme checkNullValue:reasonDetailDict[@"ride_status"]];
        NSString * ride=JJLocalizedString(@"Ride", nil);
        NSLog(@"%@",rideStatus);
        cell.statusLabel.text=[NSString stringWithFormat:@"%@ %@",ride,rideStatus];
        
        NSString * timer = [Theme checkNullValue:[reasonDetailDict objectForKey:@"pickup_date"]];
        NSString * vehNumber = [Theme checkNullValue:[reasonDetailDict objectForKey:@"vehicle_number"]];
        cell.timerLabel.text= timer;
        cell.vehicleNumberLabel.text = vehNumber;
        cell.locationLabel.text = [Theme checkNullValue:reasonDetailDict[@"pickup"][@"location"]];
        cell.dropLocationLabel.text=[Theme checkNullValue:reasonDetailDict[@"drop"][@"location"]];

        
        NSLog(@"ride status is %@",rideStatus);
        if([rideStatus isEqualToString:@"Finished"]||[rideStatus isEqualToString:@"Completed"] ){
                cell.completedView1.hidden=NO;
                cell.dot3Line.hidden=NO;
                cell.dotL4Ine.hidden=NO;
                cell.paymentLabel.hidden=NO;
            
            NSString * payment=JJLocalizedString(@"Payment", nil);
            cell.paymentLabel.text=[NSString stringWithFormat:@" %@ %@",payment,[Theme checkNullValue:reasonDetailDict[@"pay_status"]]];
            
            NSLog(@"%@",[NSString stringWithFormat:@"%@ %@",payment,[Theme checkNullValue:reasonDetailDict[@"pay_status"]]]);
            
            if(summaryDict!=nil&&[summaryDict count]>0){
                
                NSString * str=[Theme checkNullValue:reasonDetailDict[@"distance_unit"]];
                [Theme saveDistanceString:str];
                if([str isEqualToString:@""]){
                    str=@"km";
                }
                
            cell.rideDistLabel.text=[NSString stringWithFormat:@"%@ %@",[Theme checkNullValue:summaryDict[@"ride_distance"]],str];
            cell.timeTakenLabel.text=[NSString stringWithFormat:@"%@ mins",[Theme checkNullValue:summaryDict[@"ride_duration"]]];
            cell.waitTimeLabel.text=[NSString stringWithFormat:@"%@ mins",[Theme checkNullValue:summaryDict[@"waiting_duration"]]];
            } else {
                cell.rideDistLabel.text= @"0 km";
                cell.timeTakenLabel.text= @"0 min";
                cell.waitTimeLabel.text= @"0 min";
            }
            
            if(FareDict!=nil&&[FareDict count]>0){
            cell.totalBillAmountLbl.text=[NSString stringWithFormat:@"  %@ %@",currencys,[Theme checkNullValue:FareDict[@"total_bill"]]];
            cell.totslPaidAmountLbl.text=[NSString stringWithFormat:@"  %@ %@",currencys,[Theme checkNullValue:FareDict[@"total_paid"]]];
            cell.paymentType.text = [NSString stringWithFormat:@"  %@",PaymentTypeStr];
            cell.couponDiscAmountLabel.text = [NSString stringWithFormat:@"%@ %@",currencys,[Theme checkNullValue:FareDict[@"coupon_discount"]]];
            cell.serviceTaxAmountLbl.text = [NSString stringWithFormat:@"%@ %@",currencys,[Theme checkNullValue:FareDict[@"service_tax"]]];
            } else {
                cell.serviceTaxAmountLbl.text = @"$0";
                cell.couponDiscAmountLabel.text = @"$0";
            }

            NSString * tipsStatusStr=[Theme checkNullValue:tipsDict[@"tips_status"]];
            if([tipsStatusStr isEqualToString:@"1"]){
            cell.tipsAmountLabel.text=[NSString stringWithFormat:@"%@ %@",currencys,[Theme checkNullValue:tipsDict[@"tips_amount"]]];
            } else {
                cell.tipsAmountLabel.text = @"$0";
            }
            if([rideStatus isEqualToString:@"Finished"]){
                cell.paymentView1.hidden=NO;
                cell.timeBackView.hidden = NO;
                cell.cancelView.hidden = YES;
                cell.continueRideView1.hidden = YES;
                cell.TimeBackViewHeight.constant = 231;
            }else{
                cell.paymentView1.hidden=YES;
                cell.TimeBackViewHeight.constant = 231;
                cell.timeBackView.hidden = NO;
                cell.cancelView.hidden = YES;
                cell.continueRideView1.hidden = YES;
            }
        }else if([rideStatus isEqualToString:@"Cancelled"]){
            cell.paymentView1.hidden=YES;
            cell.cancelView.hidden = YES;
            cell.rideDistLabel.text = @"0 km";
            cell.timeTakenLabel.text = @"0 min";
            cell.waitTimeLabel.text = @"0 min";
            cell.dotL4Ine.hidden = YES;
        }
        
        else if([rideStatus isEqualToString:@"Onride"] || [rideStatus isEqualToString:@"Confirmed"] || [rideStatus isEqualToString:@"Arrived"] ||[rideStatus isEqualToString:@"Begin"])
        {
            cell.statusLabel.backgroundColor = [UIColor blueColor];
            cell.paymentView1.hidden=YES;
            cell.timeBackView.hidden=YES;
            cell.TimeBackViewHeight.constant = 0;
            cell.cancelView.hidden = NO;
            cell.continueRideView1.hidden = NO;
        }
        else {
            cell.timeBackView.hidden = YES;
            cell.TimeBackViewHeight.constant = 0;
            cell.statusLabel.backgroundColor = [UIColor redColor];
        }
        
        if([[Theme checkNullValue:FareDict[@"wallet_usage"]] integerValue]>0){
                cell.serviceTaxAmountLbl.hidden=NO;
            NSString* byWallet=JJLocalizedString(@"Paid_by_Wallet_money", nil);

        }
        
        if([[Theme checkNullValue:FareDict[@"coupon_discount"]] integerValue]==0){
            cell.couponDiscAmountLabel.text = @"0";
        }
        else {
//                 cell.serviceTaxAmountLbl.hidden=NO;
            
            NSString* bycoupon=JJLocalizedString(@"Paid_by_Coupon_offer", nil);
            
        cell.couponDiscAmountLabel.text=[NSString stringWithFormat:@" : $%@ %@%@",bycoupon,currencys,[Theme checkNullValue:FareDict[@"coupon_discount"]]];
        }
        if([[Theme checkNullValue:reasonDetailDict[@"do_cancel_action"]]isEqualToString:@"1"]){
                cell.cancelView.hidden=NO;
        }else {
            
        if (cell.completedView1.hidden == YES)
            {
            cell.timeBackView.frame = CGRectMake(cell.timeBackView.frame.origin.x, cell.completedView1.frame.origin.y, cell.timeBackView.frame.size.width, cell.timeBackView.frame.size.height);
            }
        }
        rideOption=[Theme checkNullValue:reasonDetailDict[@"continue_ride"]];
        if([rideOption isEqualToString:@"arrived"]||[rideOption isEqualToString:@"begin"])
        {
            
            dispatch_async (dispatch_get_main_queue(), ^{
                
                [super viewDidLayoutSubviews];
                [cell.continueRideView1.superview setNeedsLayout];
                [cell.continueRideView1.superview layoutIfNeeded];
                
                cell.continueRideView1.hidden=NO;
                cell.timeBackView.hidden = YES;

            });
            
        }else if ([rideOption isEqualToString:@"end"]){
            
            dispatch_async (dispatch_get_main_queue(), ^{
                
                [super viewDidLayoutSubviews];
                [cell.timeBackView.superview setNeedsLayout];
                [cell.timeBackView.superview layoutIfNeeded];
                
                cell.continueRideView1.hidden=NO;
                
            });
            
        }
            else if ([rideOption isEqualToString:@"end"]){
                cell.continueRideView1.hidden=NO;
                cell.continueRideView1.frame=cell.cancelView.frame;
        }else{
                cell.continueRideView1.hidden=YES;
            }
            [self loadGoogleMap];
        
        [cell.cancelRideBtn1 addTarget:self action:@selector(didClickCancelRideBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.requestPaymentBtn1 addTarget:self action:@selector(didClickreceivePaymentBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.continueRideBtn1 addTarget:self action:@selector(didClickContinueRideBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
    return cell;

    }
}

- (void)receiveNotification:(NSNotification *) notification
{
    if(self.view.window){
        NSDictionary * dict=notification.userInfo;
        if([[NSString stringWithFormat:@"%@",[Theme checkNullValue:[dict objectForKey:@"action"]]] isEqualToString:@"receive_cash"]){
           
            NSString * CurrId=[Theme findCurrencySymbolByCode:[Theme checkNullValue:[dict objectForKey:@"key4"]]];
            NSString * fareStr=[Theme checkNullValue:[dict objectForKey:@"key3"]];
            NSString * fareAmt=[NSString stringWithFormat:@"%@ %@",CurrId,fareStr];
            
            [self stopActivityIndicator];
            ReceiveCashViewController * objReceiveCashVC=[self.storyboard  instantiateViewControllerWithIdentifier:@"ReceiveCashVCSID"];
            [objReceiveCashVC setRideId:rideId];
            objReceiveCashVC.RideId = rideId;
            [objReceiveCashVC setFareAmt:fareAmt];
            [self.navigationController pushViewController:objReceiveCashVC animated:YES];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{

    [self getTripDetail];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)loadGoogleMap{
    if(TARGET_IPHONE_SIMULATOR)
    {
        Camera = [GMSCameraPosition cameraWithLatitude:13.0827
                                             longitude:80.2707
                                                  zoom:15];
    }else{
        Camera = [GMSCameraPosition cameraWithLatitude:lattitude
                                             longitude:longitude
                                                  zoom:15 ];
//                                               bearing:90
//                                          viewingAngle:0];
        
        // new change  bearing and view angle
    }
//    GoogleMap = [GMSMapView mapWithFrame:CGRectMake(0, 0, mapview.frame.size.width, mapview.frame.size.height) camera:Camera];
    GMSMarker * marker=[[GMSMarker alloc]init];
    marker.position = Camera.target;
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle-silver" withExtension:@"json"];
    NSError *error;
    
    //     Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    
    if (!style) {
        NSLog(@"The style definition could not be loaded: %@", error);
    }
    
    GoogleMap.mapStyle = style;
    
    marker.icon = [UIImage imageNamed:@"LocMarker"];
//    marker.snippet = [NSString stringWithFormat:@"%@",locationLbl.text];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = GoogleMap;
   // GoogleMap.myLocationEnabled = YES;
    GoogleMap.userInteractionEnabled=YES;
    GoogleMap.delegate = self;
//    [mapview addSubview:GoogleMap];
}

-(void)getTripDetail{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web getDriverTripDetail:[self setParametersDriverTripDetail]
                     success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             
//             completedView.hidden=YES;
             
             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];

              needCash = [Theme checkNullValue:responseDictionary[@"response"][@"receive_cash"]];
             reasonDetailDict = responseDictionary[@"response"][@"details"];
             
             NSDictionary * latLongDict=reasonDetailDict[@"pickup"][@"latlong"];
             NSDictionary * tipsDict=reasonDetailDict[@"tips"];
             summaryDict=reasonDetailDict[@"summary"];
              FareDict=reasonDetailDict[@"fare"];
             PaymentTypeStr = reasonDetailDict[@"payment_type"];
  
             rideId=[Theme checkNullValue:reasonDetailDict[@"ride_id"]];
//             headerLbl.text=[NSString stringWithFormat:@"ID : %@",rideId];
              currencys=[Theme findCurrencySymbolByCode:[Theme checkNullValue:reasonDetailDict[@"currency"]]];
             
//             NSString * rideStatus=[Theme checkNullValue:reasonDetailDict[@"ride_status"]];
//             NSString * ride=JJLocalizedString(@"Ride", nil);
//             NSLog(@"%@",rideStatus);
             
             
//             statusLbl.text=[NSString stringWithFormat:@"%@ %@",ride,rideStatus];
//             timerLbl.text=[Theme checkNullValue:[reasonDetailDict objectForKey:@"pickup_date"]];
//
//             _headerTimeLbl.text = timerLbl.text;
             
//             _headerimage.image = [UIImage imageNamed:@"userDefault"];
             
             
//             AppDelegate*delagate = [UIApplication sharedApplication].delegate;
             
//             self.vehicleNumberLBL.text = delagate.vehicleNumber;
             
             
//             locationLbl.text=[Theme checkNullValue:reasonDetailDict[@"pickup"][@"location"]];
//             _dropLocationLbl.text=[Theme checkNullValue:reasonDetailDict[@"drop"][@"location"]];
             
             
             lattitude=[[NSString stringWithFormat:@"%@",latLongDict[@"lat"]] doubleValue];
             longitude=[[NSString stringWithFormat:@"%@",latLongDict[@"lon"]] doubleValue];
             
//             NSString * str=[Theme checkNullValue:reasonDetailDict[@"distance_unit"]];
//             [Theme saveDistanceString:str];
//             if([str isEqualToString:@""]){
//                 str=@"km";
//             }
             
//             NSLog(@"ride status is %@",rideStatus);
//             if([rideStatus isEqualToString:@"Finished"]||[rideStatus isEqualToString:@"Completed"] ){
////                 completedView.hidden=NO;
////                 dotLine3.hidden=NO;
////                 dotLIne4.hidden=NO;
////
////                 paymentLbl.hidden=NO;
////                 NSString * payment=JJLocalizedString(@"Payment", nil);
////                 paymentLbl.text=[NSString stringWithFormat:@" %@ %@",payment,[Theme checkNullValue:reasonDetailDict[@"pay_status"]]];
////                 NSLog(@"%@",[NSString stringWithFormat:@"%@ %@",payment,[Theme checkNullValue:reasonDetailDict[@"pay_status"]]]);
//                 if(summaryDict!=nil&&[summaryDict count]>0){
//
//
////                     rideDistLbl.text=[NSString stringWithFormat:@"%@ %@",[Theme checkNullValue:summaryDict[@"ride_distance"]],str];
////
////                     timeTakenLbl.text=[NSString stringWithFormat:@"%@ mins",[Theme checkNullValue:summaryDict[@"ride_duration"]]];
////                     waitTimeLbl.text=[NSString stringWithFormat:@"%@ mins",[Theme checkNullValue:summaryDict[@"waiting_duration"]]];
//                 }
//                 if(FareDict!=nil&&[FareDict count]>0){
////                     totalAmountLbl.text=[NSString stringWithFormat:@"  %@ %@",currencys,[Theme checkNullValue:FareDict[@"total_bill"]]];
////                     totslPaidLbl.text=[NSString stringWithFormat:@"  %@ %@",currencys,[Theme checkNullValue:FareDict[@"total_paid"]]];
////                 //    totslPaidLbl.text= @"Deepthi";
////                    paymentType_Lbl.text = [NSString stringWithFormat:@"  %@",PaymentTypeStr];
//
//                  // service_tax
//
////                     serviceTax_Lbl.text = [NSString stringWithFormat:@"  %@ %@",currencys,[Theme checkNullValue:FareDict[@"service_tax"]]];
//
//                 }
////                 tipsLbl.text=@"";
//                 NSString * tipsStatusStr=[Theme checkNullValue:tipsDict[@"tips_status"]];
//                 if([tipsStatusStr isEqualToString:@"1"]){
////                     tipsLbl.text=[NSString stringWithFormat:@"Tips : %@ %@",currencys,[Theme checkNullValue:tipsDict[@"tips_amount"]]];
//                 }
//                 if([rideStatus isEqualToString:@"Finished"]){
////                     self.paymentView.hidden=NO;
//
//
//
//                 }else{
////                      self.paymentView.hidden=YES;
//                 }
//             }else if([rideStatus isEqualToString:@"Cancelled"]){
////                self.paymentView.hidden=YES;
//
//
//             }
//
//             else if([rideStatus isEqualToString:@"Onride"] || [rideStatus isEqualToString:@"Confirmed"] || [rideStatus isEqualToString:@"Arrived"] ||[rideStatus isEqualToString:@"Begin"])
//             {
//                 //statusLbl.backgroundColor = [UIColor blueColor];
////                 self.paymentView.hidden=YES;
////                 self.timeBckView.hidden=YES;
//             }
//             else{
////                 timeBckView.hidden = YES;
////                 statusLbl.backgroundColor = [UIColor redColor];
//             }
             
             
             
//             if([[Theme checkNullValue:FareDict[@"wallet_usage"]] integerValue]>0){
////                 walletLbl.hidden=NO;
//                // _couponHieghtConstant.constant = 27;
//
//                 NSString* byWallet=JJLocalizedString(@"Paid_by_Wallet_money", nil);
//
////                 walletLbl.text=[NSString stringWithFormat:@" : $%@ %@ %@",currencys,byWallet,[Theme checkNullValue:FareDict[@"wallet_usage"]]];
//             }
//             if([[Theme checkNullValue:FareDict[@"coupon_discount"]] integerValue]==0){
//
//                  dispatch_async(dispatch_get_main_queue(), ^{
////                 self.couponView.hidden = YES;
////
////                 self.totalPaidView.frame = CGRectMake(timeBckView.frame.origin.x,28, self.timeBckView.frame.size.width, 28);
////                 self.TimeBackViewHieghtConstraint.constant = 56+27+28;
//                  });
//
//             }
//             else{
//
////                 walletLbl.hidden=NO;
//                // _couponHieghtConstant.constant = 0;
//
//                 NSString* bycoupon=JJLocalizedString(@"Paid_by_Coupon_offer", nil);
//
////                 walletLbl.text=[NSString stringWithFormat:@" : $%@ %@%@",bycoupon,currencys,[Theme checkNullValue:FareDict[@"coupon_discount"]]];
//             }
             
//             if([[Theme checkNullValue:reasonDetailDict[@"do_cancel_action"]]isEqualToString:@"1"]){
////                 cancelView.hidden=NO;
//                 //tripDeatilScrollView.contentSize=CGSizeMake(tripDeatilScrollView.frame.size.width, completedView.frame.origin.y+completedView.frame.size.height+600);
//             }else
//             {
//
////                 if (completedView.hidden == YES)
////                 {
////                    timeBckView.frame = CGRectMake(timeBckView.frame.origin.x, completedView.frame.origin.y, timeBckView.frame.size.width, timeBckView.frame.size.height);
////                 }
//
//                 //tripDeatilScrollView.contentSize=CGSizeMake(tripDeatilScrollView.frame.size.width, completedView.frame.origin.y+completedView.frame.size.height+300);
//             }
//             rideOption=[Theme checkNullValue:reasonDetailDict[@"continue_ride"]];
//             if([rideOption isEqualToString:@"arrived"]||[rideOption isEqualToString:@"begin"])
//             {
//
//                 dispatch_async (dispatch_get_main_queue(), ^{
//
//                 [super viewDidLayoutSubviews];
////                 [self.continueRideView.superview setNeedsLayout];
////                 [self.continueRideView.superview layoutIfNeeded];
//
////                 continueRideView.hidden=NO;
////                 timeBckView.hidden = YES;
////              continueRideView.frame = CGRectMake(completedView.frame.origin.x, completedView.frame.origin.y+80, continueRideView.frame.size.width,45);
////             cancelView.frame = CGRectMake(timeBckView.frame.origin.x, timeBckView.frame.origin.y+60, cancelView.frame.size.width, 45);
//
//                });
//
//             }else if ([rideOption isEqualToString:@"end"]){
//
//                 dispatch_async (dispatch_get_main_queue(), ^{
//
//                     [super viewDidLayoutSubviews];
////                     [self.timeBckView.superview setNeedsLayout];
////                     [self.timeBckView.superview layoutIfNeeded];
//
////                     continueRideView.hidden=NO;
////                     timeBckView.frame = CGRectMake(completedView.frame.origin.x, completedView.frame.origin.y+80, timeBckView.frame.size.width,timeBckView.frame.size.height);
////                     continueRideView.frame = CGRectMake(timeBckView.frame.origin.x, timeBckView.frame.origin.y+90, continueRideView.frame.size.width, 45);
//
//                 });
//
//             }
//             else if ([rideOption isEqualToString:@"end"]){
//                 continueRideView.hidden=NO;
//                 continueRideView.frame=cancelView.frame;
//             }else{
//                 continueRideView.hidden=YES;
//             }
             [self loadGoogleMap];
             [self.tripDetailTabelView reloadData];
             
         }  else{
             
             [self.view makeToast:kErrorMessage];
         }
        
     }
            failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.view makeToast:kErrorMessage];
         
     }];
   [self stopActivityIndicator];
}

-(NSDictionary *)setParametersDriverTripDetail{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":tripId
                                  };
    return dictForuser;
}

-(void)showActivityIndicator:(BOOL)isShow{
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];


    }
}
-(void)stopActivityIndicator{
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)didClickCancelRideBtn:(id)sender {
   
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    CancelRideViewController * objCancelVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CancelVCSID"];
    [objCancelVC setRideId:rideId];
    [self.navigationController pushViewController:objCancelVC animated:YES];
}

- (IBAction)didClickBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didClickReceiveCashBtn:(id)sender {
    
    //sendrequestForPayment
    
}

- (IBAction)didClickreceivePaymentBtn:(id)sender {
    
    FareRecords * objFareRecrds=[[FareRecords alloc]init];
    objFareRecrds.currency=[Theme checkNullValue:currencys];
    NSString * str=[Theme checkNullValue:[Theme getCurrentDistanceString]];
    if([str isEqualToString:@""]){
        str=@"km";
    }
    NSString * str1=[NSString stringWithFormat:@"%@ %@",[Theme checkNullValue:[summaryDict objectForKey:@"ride_distance"]],str];
    objFareRecrds.rideDistance=str1;
    objFareRecrds.rideDuration=[Theme checkNullValue:[summaryDict objectForKey:@"ride_duration"]];
    objFareRecrds.rideFare=[Theme checkNullValue:[NSString stringWithFormat:@"%.02f",[[FareDict objectForKey:@"grand_bill"] floatValue]]];
    objFareRecrds.waitingDuration=[Theme checkNullValue:[summaryDict objectForKey:@"waiting_duration"]];
    objFareRecrds.needPayment=[Theme checkNullValue:@"YES"];
    objFareRecrds.needCash=needCash;
    ReceiveCashViewController * RCV = [[ReceiveCashViewController alloc]init];
    RCV.RideId=rideId;
    controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FareSummaryVCSID"];
    //controller.view.frame=self.view.frame;
    controller.objFareRecs=[[FareRecords alloc]init];
    [controller setObjFareRecs:objFareRecrds];
    controller.rideID = rideId;
    controller.delegate=self;
    [controller setIsCloseEnabled:YES];
    [controller presentInParentViewController:self];
    
}

- (IBAction)didClickContinueRideBtn:(id)sender {
   
//        continueRideBtn.userInteractionEnabled=NO;
        if([rideOption isEqualToString:@"arrived"]){
            [self loadContinueRide];
            [self performSelector:@selector(enableContinueBtn) withObject:nil afterDelay:10];
        }else if ([rideOption isEqualToString:@"begin"]){
             [self loadContinueRide];
            [self performSelector:@selector(enableContinueBtn) withObject:nil afterDelay:10];
        }
        else if ([rideOption isEqualToString:@"end"]){
            
            [self loadContinueRide];
            [self performSelector:@selector(enableContinueBtn) withObject:nil afterDelay:10];
            
//            EndTripViewController * objEndTripVc=[self.storyboard instantiateViewControllerWithIdentifier:@"EndTripVCSID"];
//            [objEndTripVc setRideID:rideId];
//            [self.navigationController pushViewController:objEndTripVc animated:YES];
        }
    
}

-(void)enableContinueBtn{
//    continueRideBtn.userInteractionEnabled=YES;
}
-(void)loadContinueRide{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web continueRide:[self setParametersForAcceptRide]
              success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             
             NSDictionary * dict=responseDictionary[@"response"][@"user_profile"];
             RiderRecords * objRiderRecords=[[RiderRecords alloc]init];
             objRiderRecords.RideId=[Theme checkNullValue:[dict objectForKey:@"ride_id"]];
             objRiderRecords.UserId=[Theme checkNullValue:[dict objectForKey:@"user_id"]];
             objRiderRecords.userEmail=[Theme checkNullValue:[dict objectForKey:@"user_email"]];
             objRiderRecords.userImage=[Theme checkNullValue:[dict objectForKey:@"user_image"]];
             objRiderRecords.userName=[Theme checkNullValue:[dict objectForKey:@"user_name"]];
             objRiderRecords.userLocation=[Theme checkNullValue:[dict objectForKey:@"pickup_location"]];
             objRiderRecords.userLattitude=[Theme checkNullValue:[dict objectForKey:@"pickup_lat"]];
             objRiderRecords.userLongitude=[Theme checkNullValue:[dict objectForKey:@"pickup_lon"]];
             objRiderRecords.userReview=[Theme checkNullValue:[dict objectForKey:@"user_review"]];
             objRiderRecords.userTime=[Theme checkNullValue:[dict objectForKey:@"pickup_time"]];
             objRiderRecords.userPhoneNumber=[Theme checkNullValue:[dict objectForKey:@"phone_number"]];
              objRiderRecords.hasDropLocation=[Theme checkNullValue:[dict objectForKey:@"drop_location"]];
              objRiderRecords.dropAddress=[Theme checkNullValue:[dict objectForKey:@"drop_loc"]];
              objRiderRecords.dropLat=[Theme checkNullValue:[dict objectForKey:@"drop_lat"]];
              objRiderRecords.dropLong=[Theme checkNullValue:[dict objectForKey:@"drop_lon"]];
             
             AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication ]delegate];
             delegate.appdelegateRideID = objRiderRecords.RideId;
             delegate.drop_lat = objRiderRecords.dropLat;
             delegate.drop_long = objRiderRecords.dropLong;
             
             if([rideOption isEqualToString:@"arrived"]){
                 RideUserViewController * objRideUserVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RideUserVCSID"];
                 [objRideUserVc setObjRiderRecords:objRiderRecords];
                 [self.navigationController pushViewController:objRideUserVc animated:YES];
             }else if ([rideOption isEqualToString:@"begin"]){
                 TripViewController * objTripVc=[self.storyboard instantiateViewControllerWithIdentifier:@"TripVCSID"];
                 [objTripVc setObjRiderRecs:objRiderRecords];
                 [self.navigationController pushViewController:objTripVc animated:YES];
             }
             else if ([rideOption isEqualToString:@"end"]){
                  EndTripViewController * objEndTripVc=[self.storyboard instantiateViewControllerWithIdentifier:@"EndTripVCSID"];
                objEndTripVc.dropAddress =[Theme checkNullValue:[dict objectForKey:@"drop_loc"]];
                  //[objEndTripVc setRideID:rideId];
                 objEndTripVc.rideID=rideId;
                 AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication ]delegate];
                 delegate.appdelegateRideID = rideId;
                  [self.navigationController pushViewController:objEndTripVc animated:YES];
             }
            
         }else{
             
             [self.view makeToast:kErrorMessage];
             
         }
         
     }
              failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [self.navigationController popViewControllerAnimated:NO];
         
     }];
}
-(NSDictionary *)setParametersForAcceptRide{
    NSString * latitude1=@"";
    NSString * longitude1=@"";
    if(location.coordinate.latitude!=0){
        latitude1=[NSString stringWithFormat:@"%f",location.coordinate.latitude];
        longitude1=[NSString stringWithFormat:@"%f",location.coordinate.longitude];
    }
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":rideId,
                                  };
    return dictForuser;
}
-(void)moveToNextVc:(NSInteger )index{
    if(index==1){
        CashOTPViewController * objCashOtpController=[self.storyboard instantiateViewControllerWithIdentifier:@"CashOTPVCSID"];
        [objCashOtpController setRideId:rideId];
        [self.navigationController pushViewController:objCashOtpController animated:YES];
    }else if (index==0){
        [controller.view makeToast:@"Please_wait_until_transaction"];
        [self sendrequestForPayment];
    }else if (index==2){
        [self sendrequestForNoPayment];
    }
    else if (index==3){
        [self stopActivityIndicator];
    }
}
-(void)sendrequestForPayment{
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web requestForPayment:[self setParametersForPaymentRequest]
                   success:^(NSMutableDictionary *responseDictionary)
     {
         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
             [controller.view makeToast:@"Please wait until transaction is complete. Don't minimize or go back"];
             PaymentWaitingViewController * objPayWait=[self.storyboard instantiateViewControllerWithIdentifier:@"PaymentWaitingVCSID"];
             [objPayWait setRideIdPay:rideId];
             [self.navigationController pushViewController:objPayWait animated:YES];
         }else{
             [self stopActivityIndicator];
             [controller.view makeToast:kErrorMessage];
         }
     }
                   failure:^(NSError *error)
     {
         [self stopActivityIndicator];
         [controller.view makeToast:kErrorMessage];
         
     }];
}
-(NSDictionary *)setParametersForPaymentRequest{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"ride_id":rideId
                                  };
    return dictForuser;
}
-(void)sendrequestForNoPayment{
    RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
    [objRatingsVc setRideid:rideId];
    [self.navigationController pushViewController:objRatingsVc animated:YES];
    
    //    [self showActivityIndicator:YES];
    //    UrlHandler *web = [UrlHandler UrlsharedHandler];
    //    [web NoNeedPayment:[self setParametersForPaymentRequest]
    //               success:^(NSMutableDictionary *responseDictionary)
    //     {
    //         if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
    //             RatingsViewController * objRatingsVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RatingsVCSID"];
    //             [objRatingsVc setRideid:rideID];
    //             [self.navigationController pushViewController:objRatingsVc animated:YES];
    //         }else{
    //             [self stopActivityIndicator];
    //             [controller.view makeToast:kErrorMessage];
    //         }
    //     }
    //               failure:^(NSError *error)
    //     {
    //         [self stopActivityIndicator];
    //         [controller.view makeToast:kErrorMessage];
    //         
    //     }];
    
    
}


@end
