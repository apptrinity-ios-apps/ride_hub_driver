//
//  TripDetailedTableViewCell.m
//  RideHubDriver
//
//  Created by INDOBYTES on 20/12/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import "TripDetailedTableViewCell.h"

@implementation TripDetailedTableViewCell

@synthesize paymentLabel,statusLabel,back2View,locationLabel,dropLocationLabel,greenDotLine1,dot2LIne,back3View,timerLabel,back4View,back5View,star1,star2,star3,star4,star5,vehicleNumberLabel,dot3Line,completedView1,rideDistLabel,timeTakenLabel,waitTimeLabel,dotL4Ine,timeBackView,TimeBackViewHeight,AmountView1,paymentTypeLabel,paymentType,totalBillAmountLbl,totalBillLabel,couponView1,couponDiscAmountLabel,couponDiscLabel,serviceTaxAmountLbl,serviceTaxLabel,tipView,tipsLabel,tipsAmountLabel,totalPaidView1,totslPaidAmountLbl,totslPaidLabel,continueRideView1,continueRideBtn1,paymentView1,requestPaymentBtn1,cancelView,cancelRideBtn1;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    statusLabel.layer.cornerRadius = statusLabel.frame.size.height/2;
    statusLabel.layer.masksToBounds = YES;
    
    back2View.layer.cornerRadius = 5;
    back3View.layer.cornerRadius = 5;
    back4View.layer.cornerRadius = 5;
    back5View.layer.cornerRadius = 5;
    completedView1.layer.cornerRadius = 5;
    timeBackView.layer.cornerRadius = 5;
    
    [self setShadow:back2View];
    [self setShadow:back3View];
    [self setShadow:back4View];
    [self setShadow:back5View];
    [self setShadow:completedView1];
    [self setShadow:totalPaidView1];
    [self setShadow:cancelView];
    [self setShadow:paymentView1];
    [self setShadow:continueRideView1];
    
    cancelView.layer.cornerRadius = cancelView.frame.size.height/2;
    cancelRideBtn1.layer.cornerRadius = cancelRideBtn1.frame.size.height/2;
    paymentView1.layer.cornerRadius = paymentView1.frame.size.height/2;
    requestPaymentBtn1.layer.cornerRadius = requestPaymentBtn1.frame.size.height/2;
    continueRideView1.layer.cornerRadius = continueRideView1.frame.size.height/2;
    continueRideBtn1.layer.cornerRadius = continueRideBtn1.frame.size.height/2;
    
    paymentType.layer.cornerRadius = 5;
    totalBillLabel.layer.cornerRadius = 5;
    couponDiscLabel.layer.cornerRadius = 5;
    serviceTaxLabel.layer.cornerRadius = 5;
    totslPaidLabel.layer.cornerRadius = 5;
    paymentTypeLabel.layer.cornerRadius = 5;
    totalBillAmountLbl.layer.cornerRadius = 5;
    couponDiscAmountLabel.layer.cornerRadius = 5;
    serviceTaxAmountLbl.layer.cornerRadius = 5;
    tipsLabel.layer.cornerRadius = 5;
    tipsAmountLabel.layer.cornerRadius = 5;
    totslPaidAmountLbl.layer.cornerRadius = 5;
    
    paymentType.layer.borderWidth = 1;
    totalBillLabel.layer.borderWidth = 1;
    couponDiscLabel.layer.borderWidth = 1;
    serviceTaxLabel.layer.borderWidth = 1;
    totslPaidLabel.layer.borderWidth = 1;
    
    paymentType.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    totalBillLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    couponDiscLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    serviceTaxLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    tipsLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    tipsAmountLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    totslPaidLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    
    paymentTypeLabel.layer.borderWidth = 1;
    totalBillAmountLbl.layer.borderWidth = 1;
    couponDiscAmountLabel.layer.borderWidth = 1;
    serviceTaxAmountLbl.layer.borderWidth = 1;
    tipsLabel.layer.borderWidth = 1;
    tipsAmountLabel.layer.borderWidth = 1;
    totslPaidAmountLbl.layer.borderWidth = 1;
    
    paymentTypeLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    totalBillAmountLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    couponDiscAmountLabel.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    serviceTaxAmountLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    totslPaidAmountLbl.layer.borderColor = [UIColor colorWithRed:6/255.0 green:152/255.0 blue:212/255.0 alpha:0.4f].CGColor;
    
    CAShapeLayer *yourViewBorder = [CAShapeLayer layer];
    yourViewBorder.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder.lineDashPattern = @[@3,@3];
    yourViewBorder.frame = self.dot2LIne.bounds;
    yourViewBorder.path = [UIBezierPath bezierPathWithRect:self.dot2LIne.bounds].CGPath;
    [self.dot2LIne.layer addSublayer:yourViewBorder];
    
    CAShapeLayer *yourViewBorder2 = [CAShapeLayer layer];
    yourViewBorder2.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder2.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder2.lineDashPattern = @[@3,@3];
    yourViewBorder2.frame = self.dot3Line.bounds;
    yourViewBorder2.path = [UIBezierPath bezierPathWithRect:self.dot3Line.bounds].CGPath;
    [self.dot3Line.layer addSublayer:yourViewBorder2];
    
    CAShapeLayer *yourViewBorder3 = [CAShapeLayer layer];
    yourViewBorder3.strokeColor = [UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:0.58f].CGColor;
    yourViewBorder3.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder3.lineDashPattern = @[@3,@3];
    yourViewBorder3.frame = self.dotL4Ine.bounds;
    yourViewBorder3.path = [UIBezierPath bezierPathWithRect:self.dotL4Ine.bounds].CGPath;
    [self.dotL4Ine.layer addSublayer:yourViewBorder3];

    CAShapeLayer *yourViewBorder4 = [CAShapeLayer layer];
    yourViewBorder4.strokeColor = [UIColor colorWithRed:81/255.0f green:190/255.0f blue:19/255.0f alpha:0.58f].CGColor;
    yourViewBorder4.fillColor = nil;//[UIColor colorWithRed:3/255.0f green:116/255.0f blue:187/255.0f alpha:1.0f].CGColor;
    yourViewBorder4.lineDashPattern = @[@3,@3];
    yourViewBorder4.frame = self.greenDotLine1.bounds;
    yourViewBorder4.path = [UIBezierPath bezierPathWithRect:self.greenDotLine1.bounds].CGPath;
    [self.greenDotLine1.layer addSublayer:yourViewBorder4];

}

-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
