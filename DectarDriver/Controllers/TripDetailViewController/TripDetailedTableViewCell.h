//
//  TripDetailedTableViewCell.h
//  RideHubDriver
//
//  Created by INDOBYTES on 20/12/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TripDetailedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) IBOutlet UIView *back2View;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropLocationLabel;
@property (strong, nonatomic) IBOutlet UIView *greenDotLine1;

@property (strong, nonatomic) IBOutlet UIView *dot2LIne;

@property (strong, nonatomic) IBOutlet UIView *back3View;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIView *back4View;
@property (weak, nonatomic) IBOutlet UIImageView *star1;
@property (weak, nonatomic) IBOutlet UIImageView *star2;
@property (weak, nonatomic) IBOutlet UIImageView *star3;
@property (weak, nonatomic) IBOutlet UIImageView *star4;
@property (weak, nonatomic) IBOutlet UIImageView *star5;

@property (strong, nonatomic) IBOutlet UIView *back5View;
@property (strong, nonatomic) IBOutlet UILabel *vehicleNumberLabel;

@property (strong, nonatomic) IBOutlet UIView *dot3Line;

@property (weak, nonatomic) IBOutlet UIView *completedView1;
@property (weak, nonatomic) IBOutlet UILabel *rideDistLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeTakenLabel;
@property (weak, nonatomic) IBOutlet UILabel *waitTimeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TimeBackViewHeight;
@property (strong, nonatomic) IBOutlet UIView *dotL4Ine;
@property (weak, nonatomic) IBOutlet UIView *timeBackView;
@property (strong, nonatomic) IBOutlet UIView *AmountView1;
@property (weak, nonatomic) IBOutlet UILabel *paymentTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentType;
@property (weak, nonatomic) IBOutlet UILabel *totalBillAmountLbl;
@property (strong, nonatomic) IBOutlet UILabel *totalBillLabel;
@property (strong, nonatomic) IBOutlet UIView *couponView1;
@property (weak, nonatomic) IBOutlet UILabel *couponDiscAmountLabel;
@property (strong, nonatomic) IBOutlet UILabel *couponDiscLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceTaxAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *serviceTaxLabel;
@property (weak, nonatomic) IBOutlet UIView *tipView;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipsAmountLabel;
@property (strong, nonatomic) IBOutlet UIView *totalPaidView1;
@property (weak, nonatomic) IBOutlet UILabel *totslPaidAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *totslPaidLabel;

@property (weak, nonatomic) IBOutlet UIView *continueRideView1;
@property (weak, nonatomic) IBOutlet UIButton *continueRideBtn1;

@property (weak, nonatomic) IBOutlet UIView *paymentView1;
@property (strong, nonatomic) IBOutlet UIButton *requestPaymentBtn1;

@property (weak, nonatomic) IBOutlet UIView *cancelView;
@property (weak, nonatomic) IBOutlet UIButton *cancelRideBtn1;





@end

NS_ASSUME_NONNULL_END
