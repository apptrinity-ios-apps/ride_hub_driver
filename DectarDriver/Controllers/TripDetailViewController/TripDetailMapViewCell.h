//
//  TripDetailMapViewCell.h
//  RideHubDriver
//
//  Created by INDOBYTES on 19/12/19.
//  Copyright © 2019 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TripDetailMapViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;


@end

NS_ASSUME_NONNULL_END
