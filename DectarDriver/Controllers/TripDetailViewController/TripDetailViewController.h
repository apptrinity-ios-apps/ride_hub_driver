//
//  TripDetailViewController.h
//  DectarDriver
//
//  Created by Casperon Technologies on 8/28/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "RTSpinKitView.h"
#import "UrlHandler.h"
#import "Theme.h"
#import "RootBaseViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "EndTripViewController.h"
#import "FareSummaryViewController.h"
#import "DectarCustomColor.h"
@interface TripDetailViewController : RootBaseViewController<GMSMapViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource>{
     CLLocation *location;
}
@property(nonatomic, strong) CLLocationManager *locationManager;
@property(strong,nonatomic)RTSpinKitView * custIndicatorView;

@property(strong,nonatomic)GMSCameraPosition * Camera;
@property(strong,nonatomic)GMSMapView * GoogleMap;
@property(strong,nonatomic)NSString * rideId;
@property(strong,nonatomic)NSString * rideOption;
@property(strong,nonatomic)NSString * needCash;

@property(strong,nonatomic)NSString * tripId;
@property(assign,nonatomic)float lattitude;
@property(assign,nonatomic)float longitude;

@property (assign, nonatomic) BOOL isLocUpdated;

@property (weak, nonatomic) IBOutlet UITableView *tripDetailTabelView;

@property (strong, nonatomic) IBOutlet UIView *dotLine1;

- (IBAction)didClickBackBtn:(id)sender;

@property UIView * container;


@end
