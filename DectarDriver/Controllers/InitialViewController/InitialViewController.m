//
//  InitialViewController.m
//  DectarDriver
//
//  Created by Casperon Technologies on 8/21/15.
//  Copyright (c) 2015 Casperon Technologies. All rights reserved.
//

#import "InitialViewController.h"
#import "LoginViewController.h"
@interface InitialViewController ()

@end

@implementation InitialViewController
@synthesize signInBtn,registerBtn;
+ (instancetype) controller{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    signInBtn.layer.cornerRadius =  signInBtn.frame.size.height/2;
    //signInBtn.layer.borderWidth = 1;
   // registerBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    registerBtn.layer.cornerRadius = registerBtn.frame.size.height/2;
  //  registerBtn.layer.borderWidth = 1;
   // signInBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [self setFontAndColor];
    
    [self setShadow:signInBtn];
    [self setShadow:registerBtn];
   
}
-(void)
setShadow:(UIView * )view{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 3;
    view.layer.shadowOpacity = 0.3;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];   //it hides
    AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
    [testAppDelegate getInitialDatas];
    
}
-(void)applicationLanguageChangeNotification:(NSNotification*)notification{
    
    
    [self.signInBtn setTitle:JJLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [self.registerBtn setTitle:JJLocalizedString(@"Register", nil) forState:UIControlStateNormal];
}

-(void)setFontAndColor{
    
    signInBtn=[Theme setBoldFontForButton:signInBtn];
    registerBtn=[Theme setBoldFontForButton:registerBtn];
    
   // [signInBtn setBackgroundColor:SetBlueColor];
//    signInBtn.layer.cornerRadius=2;
//    registerBtn.layer.cornerRadius=2;
//    signInBtn.layer.masksToBounds=YES;
//    registerBtn.layer.masksToBounds=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)googleBtnAction:(UIButton *)sender {
    
    
}

- (IBAction)twitterBtnAction:(UIButton *)sender {
    
    
}

- (IBAction)faceBookBtnAction:(UIButton *)sender {
    
    
}



- (IBAction)didClickRegisterBtn:(id)sender {
//    UIAlertView * alert=[[UIAlertView alloc]initWithTitle:@"Cabily Partner" message:@"Please visit our website to register. Thank you" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    
    SignUpWebViewController * objSignUpVc=[self.storyboard instantiateViewControllerWithIdentifier:@"SignUpWebVSSID"];
    [self.navigationController pushViewController:objSignUpVc animated:YES];
    //RegisterMainVCSID
    
//        RegisterViewController * objSignUpVc=[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVCSID"];
//        [self.navigationController pushViewController:objSignUpVc animated:YES];
}

- (IBAction)didClickSignInBtn:(id)sender {
    signInBtn.userInteractionEnabled=NO;
    LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVCSID"];
    [self.navigationController pushViewController:objLoginVc animated:YES];
    signInBtn.userInteractionEnabled=YES;
}

@end
