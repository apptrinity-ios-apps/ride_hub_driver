//
//  DEMOMenuViewController.m
//  REFrostedViewControllerStoryboards
//
//  Created by Roman Efimov on 10/9/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "DEMOMenuViewController.h"
#import "StarterViewController.h"

#import "UIViewController+REFrostedViewController.h"
#import "DEMONavigationController.h"
#import "Theme.h"
#import "MenuTableViewCell.h"
#import "TripListViewController.h"
#import "ChangePasswordCabViewController.h"
#import "PaymentSummaryListViewController.h"
#import "BankingInfoViewController.h"
#import "ChangeLanguageViewController.h"
#import "InviteFriendsViewController.h"
#import "SideMenuHeaderCell.h"
#import "DriverInfoRecords.h"
#import "OfflineViewController.h"



@interface DEMOMenuViewController (){
    UIButton * btn;
    UIView *pview;
}

@end

@implementation DEMOMenuViewController
@synthesize titleArray;
@synthesize custIndicatorView,ImgArray;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    titleArray=[[NSMutableArray alloc]initWithObjects:@"Home",@"Offline",@"Trip_Summary",@"Bank_Account",@"Payment_Statements",@"Referal Points",@"Invite and Earn",@"Change_Password",@"Logout",nil];
    
    ImgArray=[[NSMutableArray alloc]initWithObjects:@"IcHome",@"IcTrip",@"IcTrip",@"IcBank",@"IcPayment",@"refer",@"IcInvite",@"IcChangePass",@"IcLogout", nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.separatorColor = [UIColor colorWithRed:210/255.0f green:210/255.0f blue:210/255.0f alpha:1.0f];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SideMenuHeaderCell" bundle:nil];
    
    NSString *SectionHeaderViewIdentifier = @"SideMenuHeaderCell";
    
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    
    
    [self.tableView registerNib:sectionHeaderNib forCellReuseIdentifier:SectionHeaderViewIdentifier];
    
    
    //    self.tableView.tableHeaderView = ({
    //        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 0, 50)];
    //        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    //
    //       // imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    //
    ////        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
    ////        NSString * imgstr=[myDictionary objectForKey:@"driver_image"];
    ////
    ////        [imageView sd_setImageWithURL:[NSURL URLWithString:imgstr] placeholderImage:[UIImage imageNamed:@"PlaceHolderImg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    ////
    ////        }];
    //
    //        [imageView setImage:[UIImage imageNamed:@"Newlogo"]];
    //
    //        imageView.layer.masksToBounds = YES;
    //       // imageView.layer.cornerRadius = 50.0;
    //        //imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    //       // imageView.layer.borderWidth = 3.0f;
    //        imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    //        imageView.layer.shouldRasterize = YES;
    //        imageView.clipsToBounds = YES;
    //
    ////        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(130, 68, 0, 24)];
    ////
    ////        label.text = [Theme checkNullValue:[myDictionary objectForKey:@"driver_name"]];
    ////        label.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:17];
    ////        label.backgroundColor = [UIColor clearColor];
    ////        label.textColor = [UIColor whiteColor];
    ////        [label sizeToFit];
    //
    ////        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(130, 97, 0, 24)];
    ////
    ////        label1.text = [Theme checkNullValue:[myDictionary objectForKey:@"vehicle_model"]];
    ////        label1.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
    ////        label1.backgroundColor = [UIColor clearColor];
    ////        label1.textColor = [UIColor whiteColor];
    ////        [label1 sizeToFit];
    //        //label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    //        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 174, self.tableView.frame.size.width, .5)];
    //        line.backgroundColor = self.tableView.separatorColor;
    //        [view addSubview:imageView];
    //
    ////        [view addSubview:label];
    ////        [view addSubview:label1];
    //        //[view addSubview:line];
    //        view;
    //
    //    });
    
    //  self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sideMenu"]];
    
    
    self.tableView.backgroundColor = [UIColor lightTextColor];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

-(void)yesBtnClick{
    
    [pview removeFromSuperview];
    [self Logout];
    
}

-(void)noBtnClick{
    
    [pview removeFromSuperview];
}
//-(void)logOutAction{
//    NSLog(@"88888888");
//}
//
//-(void)noAction{
//    NSLog(@"88888888");
//}
#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor colorWithRed:6/255.0f green:44/255.0f blue:80/255.0f alpha:1.0f];
    cell.textLabel.font = [UIFont fontWithName:@"Poppins-Regular" size:15];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex
{
    
    static NSString *sideMenuHeaderCell = @"SideMenuHeaderCell";
    
    if (sectionIndex == 0){
        // return nil;
        SideMenuHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:sideMenuHeaderCell];

        NSDictionary * dict = [Theme retrieveUserData];
       
        cell.name_LBL.text =   [dict objectForKey:@"driver_name"];
        
        NSString * imgstr=[dict objectForKey:@"driver_image"];
        
        [cell.profile_imageView sd_setImageWithURL:[NSURL URLWithString:imgstr] placeholderImage:[UIImage imageNamed:@"PlaceHolderImg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        
        //    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        //    view.backgroundColor = [UIColor greenColor];
        //
        //    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 0, 0)];
        //    label.text = @"Friends Online";
        //    label.font = [UIFont systemFontOfSize:15];
        //    label.textColor = [UIColor whiteColor];
        //    label.backgroundColor = [UIColor clearColor];
        //    [label sizeToFit];
        //    [view addSubview:label];
        return cell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 142;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];
    
    if ( indexPath.row == 0) {
        StarterViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterVCSID"];
        navigationController.viewControllers = @[homeViewController];
    } else if(indexPath.row==1) {
        OfflineViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OfflineViewController"];
        navigationController.viewControllers = @[secondViewController];
    }else if(indexPath.row==2) {
        TripListViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TripListVCSID"];
       
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==3) {
        BankingInfoViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BankingInfoVCSID"];
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==4) {
        PaymentSummaryListViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentListVCSID"];
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==5) {
        ReferPointsViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReferPointsViewController"];
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==6) {
        InviteFriendsViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriendsViewController"];
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==7) {
        ChangePasswordCabViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVCSID"];
        navigationController.viewControllers = @[secondViewController];
    }
    else if(indexPath.row==8) {
        
        [self signout];
        // [self performSelector:@selector(openEmailfeedback) withObject:self afterDelay:0.3];
    }else if (indexPath.row==9) {
        
        //        ChangeLanguageViewController *secondViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangeLanguageVCSID"];
        //        navigationController.viewControllers = @[secondViewController];
    }
    
    
    
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
}

-(void)openEmailfeedback{
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"feedBackEmail"
     object:self userInfo:nil];
    
}

-(void)Logout {
    
    [self showActivityIndicator:YES];
    UrlHandler *web = [UrlHandler UrlsharedHandler];
    [web LogoutDriver:[self setParametersForLogout]
              success:^(NSMutableDictionary *responseDictionary)
     {
         [self stopActivityIndicator];
         //if ([[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"status"]]isEqualToString:@"1"]) {
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [Theme ClearUserDetails];
         [testAppDelegate logoutXmpp];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         self.view.userInteractionEnabled=YES;
         //         }else{
         //
         //             [self.view makeToast:kErrorMessage];
         //         }
     }
              failure:^(NSError *error)
     {
         
         [self stopActivityIndicator];
         AppDelegate *testAppDelegate = [UIApplication sharedApplication].delegate;
         [testAppDelegate logoutXmpp];
         [Theme ClearUserDetails];
         LoginViewController * objLoginVc=[self.storyboard instantiateViewControllerWithIdentifier:@"InitialVCSID"];
         UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objLoginVc];
         testAppDelegate.window.rootViewController = navigationController;
         self.view.userInteractionEnabled=YES;
         [self.view makeToast:kErrorMessage];
         
     }];
}

-(void)showActivityIndicator:(BOOL)isShow {
    if(isShow==YES){
//        if(custIndicatorView==nil){
//            custIndicatorView = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:SetThemeColor];
//
//        }
//        custIndicatorView.center =self.view.center;
//        [custIndicatorView startAnimating];
//        [self.view addSubview:custIndicatorView];
//        [self.view bringSubviewToFront:custIndicatorView];

        self.container = [[UIView alloc] init];
        self.container.frame = self.view.frame;
        self.container.center = self.view.center;
        self.container.backgroundColor = [UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        
        UIView * loadingview = [[UIView alloc] init];
        loadingview.frame = CGRectMake(0, 0, 80, 80);
        loadingview.center = self.view.center;
        loadingview.clipsToBounds = YES;
        loadingview.backgroundColor = [UIColor clearColor];
        
        
        NSString *filepath=[[NSBundle mainBundle]  pathForResource:@"ezgif.com-resize" ofType:@"gif"];
        NSData *gif=[NSData dataWithContentsOfFile:filepath];
        UIWebView *webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [webView loadData:gif MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
        
        webView.userInteractionEnabled = NO;
        webView.layer.cornerRadius = webView.frame.size.height/2;
        webView.layer.masksToBounds = YES;
        webView.backgroundColor = [UIColor clearColor];
        
        [loadingview addSubview:webView];
        [self.container addSubview:loadingview];
        [self.view addSubview: self.container];



    }
}

-(void)stopActivityIndicator {
    [custIndicatorView stopAnimating];
    custIndicatorView=nil;
     [self.container removeFromSuperview];
}

-(NSDictionary *)setParametersForLogout{
    NSString * driverId=@"";
    if([Theme UserIsLogin]){
        NSDictionary * myDictionary=[Theme DriverAllInfoDatas];
        driverId=[myDictionary objectForKey:@"driver_id"];
    }
    NSDictionary *dictForuser = @{
                                  @"driver_id":driverId,
                                  @"device":@"IOS"
                                  };
    return dictForuser;
}

#pragma mark -
#pragma mark UITableView Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==[titleArray count]){
        return 100;
    }
    return 70;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return [titleArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *lastCellIdentifier = @"LastCellIdentifier";
    static NSString *NormalCellIdentifier = @"MenuListIdentifier";
    
    if(indexPath.row==([titleArray count])){ //This is last cell so create normal cell
        UITableViewCell *lastcell = [tableView dequeueReusableCellWithIdentifier:lastCellIdentifier];
        if(!lastcell){
            lastcell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:lastCellIdentifier];
            CGRect frame = CGRectMake((self.tableView.frame.size.width/2)-(200/2),40,200,50);
            UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [aButton addTarget:self action:@selector(btnAddRowTapped:) forControlEvents:UIControlEventTouchUpInside];
            aButton.frame = frame;
            btn=aButton;
            aButton.layer.cornerRadius=5;
            aButton.layer.masksToBounds=YES;
            [aButton setTitle:JJLocalizedString(@"LOGOUT", nil) forState:UIControlStateNormal];
            aButton=[Theme setBoldFontForButton:aButton];
            aButton.backgroundColor=SetThemeColor;
            aButton.titleLabel.textColor = [UIColor colorWithRed:6/255.0f green:44/255.0f blue:80/255.0f alpha:1.0f];
            [lastcell addSubview:aButton];
            lastcell.separatorInset = UIEdgeInsetsMake(0.f, lastcell.bounds.size.width, 0.f, 0.f);
        }else{
            if(btn){
                [btn setTitle:JJLocalizedString(@"LOGOUT", nil) forState:UIControlStateNormal];
            }
        }
        return lastcell;
    }else{
        MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NormalCellIdentifier];
        if (cell == nil) {
            cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:NormalCellIdentifier];
        }
        
        cell.titleLbl.text=JJLocalizedString([titleArray objectAtIndex:indexPath.row], nil);
        // cell.titleLbl.textColor = [UIColor whiteColor];
        cell.IconImgView.image=[UIImage imageNamed:[ImgArray objectAtIndex:indexPath.row]];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.titleLbl.textColor = [UIColor colorWithRed:6/255.0f green:44/255.0f blue:80/255.0f alpha:1.0f];
        cell.titleLbl.font = [UIFont fontWithName:@"Poppins-Regular" size:15];
        
        return cell;
    }
    
}
-(IBAction)btnAddRowTapped:(id)sender{
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];
    //navigationController.lougoutCheck = @"YES";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"logoutCheck"];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
    
}
-(void)signout{
    
    DEMONavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"StarterNavVCSID"];
    //navigationController.lougoutCheck = @"YES";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"YES" forKey:@"logoutCheck"];
    self.frostedViewController.contentViewController = navigationController;
    [self.frostedViewController hideMenuViewController];
    
    
}
@end
